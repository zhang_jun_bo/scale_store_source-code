#pragma once
// -------------------------------------------------------------------------------------
namespace scalestore {
namespace storage {

// -------------------------------------------------------------------------------------
// metadata are placed in the catalogPID {0/0} is located on node 0
// -------------------------------------------------------------------------------------
enum class DS_TYPE : uint8_t{ //数据结构类型
   EMPTY = 0,
   BARRIER = 1,
   TABLE = 2,
   BTREE = 3,
   LLIST = 4,
   MISC = 5, // other data which need to be shared e.g. keys
};
// -------------------------------------------------------------------------------------
struct CatalogEntry{
   uint64_t datastructureId;  // 数据结构的唯一标识符
   DS_TYPE type;             // 数据结构的类型
   PID pid;                  // 数据结构的位置标识，例如B树的根节点位置// e.g. root in btree/ barrier/ first page in table
};
// -------------------------------------------------------------------------------------
struct CatalogPage{  // 目录页面的数据结构
// ATTENTION: when adding new variables adapt maxEntries
   uint64_t datastructureIds {0};   // 目录中数据结构的数量
   uint64_t numEntries = 0;      // 目录中实际存储的条目数量
   static constexpr uint64_t maxEntries = (EFFECTIVE_PAGE_SIZE - (sizeof(uint64_t) + sizeof(uint64_t)  + sizeof(uint64_t) + sizeof(bool))) / (sizeof(CatalogEntry));
   bool isInitialized = false; // node 0 initializes the global catalog // 节点0初始化全局目录的标志
   CatalogEntry entries[maxEntries];   // 目录中存储的条目数组
};
// -------------------------------------------------------------------------------------
// 目录的管理器
struct Catalog{
    // 初始化目录，确保目录页面被正确初始化
   void init(NodeID nodeId){
         if (nodeId == CATALOG_OWNER) { // implicit dependency to BM as catalog page need to exist
         ExclusiveBFGuard guard(CATALOG_PID); 
         auto& catalogPage = guard.getFrame().page->getTuple<CatalogPage>(0);
         catalogPage.isInitialized = true;   // 将目录页面标记为已初始化
         guard.getFrame().epoch = (~uint64_t(0));  // 设置目录页面的纪元为最大值
      }else{// 如果当前节点不是目录的所有者
         bool isInitialized = false;
         while(!isInitialized){
            // 使用共享锁创建一个共享锁的guard对象
            SharedBFGuard guard(CATALOG_PID);
            guard.getFrame().epoch = (~uint64_t(0));
            auto& catalogPage = guard.getFrame().page->getTuple<CatalogPage>(0);
            isInitialized = catalogPage.isInitialized;
         }
      }
   }
   // 这段代码看起来是一个获取目录条目的函数，使用了一种无限循环的方式，
   // 通过 SharedBFGuard 对象来获取对共享资源的访问，
   // 并在条件满足时返回对应的 CatalogEntry 引用。   

   CatalogEntry& getCatalogEntry(uint64_t id)
   {
      do {
         SharedBFGuard guard(CATALOG_PID);   // 创建 SharedBFGuard 对象，使用 CATALOG_PID 作为参数
         guard.getFrame().epoch = (~uint64_t(0));// 将 guard 对象关联的 frame 的 epoch 设置为 64 位无符号整数的最大值
         auto& catalogPage = guard.getFrame().page->getTuple<CatalogPage>(0);
         if (catalogPage.datastructureIds >= id && catalogPage.entries[id].type != DS_TYPE::EMPTY)  // not yet initialized
            return catalogPage.entries[id];
      } while (true);
   }

   // 这段代码看起来是一个向目录中插入新条目的函数。
   // 它使用 ExclusiveBFGuard 对象来获取对共享资源的独占访问，
   // 并在目录的下一个位置插入一个新的 CatalogEntry，
   // 返回新插入的 CatalogEntry 的 datastructureId。
   uint64_t insertCatalogEntry(DS_TYPE type, PID pid){    
      ExclusiveBFGuard guard(CATALOG_PID);
      guard.getFrame().epoch = (~uint64_t(0));
      auto& catalogPage = guard.getFrame().page->getTuple<CatalogPage>(0);
      catalogPage.entries[catalogPage.datastructureIds] = {.datastructureId = catalogPage.datastructureIds, .type = type, .pid=pid};
      auto tmp = catalogPage.datastructureIds;
      catalogPage.datastructureIds++;
      return tmp;
   }
   
};
}  // storage
}  // scalestore
