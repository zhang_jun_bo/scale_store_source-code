#pragma once
// -------------------------------------------------------------------------------------
#include <cstdint>
#include <cassert>
// -------------------------------------------------------------------------------------
namespace scalestore
{
namespace storage
{
// -------------------------------------------------------------------------------------
// constexpr uint64_t PAGE_SIZE = 1024 * 1024 * 2;
// constexpr uint64_t PAGE_SIZE = 1024 * 256 * 1;
constexpr uint64_t PAGE_SIZE = 1024 * 4;//页面的大小为PAGE_SIZE，
// -------------------------------------------------------------------------------------
struct alignas(512) Page {//Page的结构体，用于表示一个页面。并且使用alignas(512)进行了对齐，确保结构体的内存布局在512字节的边界上。
   volatile std::size_t magicDebuggingNumber = 999; // INIT used for RDMA debugging，用于进行RDMA调试
   uint8_t data[PAGE_SIZE - sizeof(magicDebuggingNumber)];//用于存储实际的数据
   uint8_t* begin() { return data; }//函数返回指向data数组的指针，表示页面的起始位置。
   uint8_t* end() { return data + (PAGE_SIZE - sizeof(magicDebuggingNumber)) + 1; } // one byte past the end，函数返回指向页面末尾的指针，这里指向末尾的下一个字节位置
   template <typename T>
   T& getTuple(uint64_t idx)//用于获取页面中特定索引位置的数据元组。
   {//接受一个uint64_t类型的索引参数idx
      uint8_t* ptr = data + idx;//计算出对应数据元组的指针
      ensure((ptr + sizeof(T)) < end()); // CHECK，它会使用ensure函数确保获取数据元组的指针不会超出页面的末尾。
      return *(reinterpret_cast<T*>(ptr));//使用reinterpret_cast将指针转换为模板类型T的引用。
   }
};
// -------------------------------------------------------------------------------------
static constexpr uint64_t EFFECTIVE_PAGE_SIZE = sizeof(Page::data);//表示页面的有效大小为data数组的大小。
// -------------------------------------------------------------------------------------
}  // namespace storage

}  // namespace scalestore


