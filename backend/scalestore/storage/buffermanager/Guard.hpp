#pragma once
#include "BufferFrame.hpp"

namespace scalestore {
namespace storage {

// 状态枚举，表示缓冲帧的不同状态
enum class STATE : uint8_t{
   UNINITIALIZED, SSD, REMOTE, LOCAL_POSSESSION_CHANGE, REMOTE_POSSESSION_CHANGE, RETRY, INITIALIZED, MOVED, NOT_FOUND,
};
// 阻塞？？？？
enum class CONTENTION_METHOD { BLOCKING, NON_BLOCKING};
// 锁的状态枚举
enum class LATCH_STATE { UNLATCHED, OPTIMISTIC, SHARED, EXCLUSIVE };

struct Guard{
   Version vAcquired = 0; // version at which the guard was acquired
   BufferFrame* frame = nullptr;    //指向缓冲帧等于页面（物理地址空间）指针
   STATE state = STATE::UNINITIALIZED;
   CONTENTION_METHOD method = CONTENTION_METHOD::BLOCKING;//????
   LATCH_STATE latchState = LATCH_STATE::UNLATCHED;

   Guard() = default;
   //移动状态构造函数
   Guard(Guard&& other)    noexcept : vAcquired(other.vAcquired), frame(other.frame),state(other.state), method(other.method), latchState(other.latchState) // move constructor
    {
       other.state = STATE::MOVED;
    }
   // 页的状态移动运算符（重载运算符）
   Guard& operator=(Guard&& other) noexcept // move assignment
    {
       // destruct visible ressources
       if(latchState == LATCH_STATE::EXCLUSIVE)
          frame->latch.unlatchExclusive();
       if(latchState == LATCH_STATE::SHARED)
          frame->latch.unlatchShared();
      // 赋值操作
       vAcquired = other.vAcquired;
       frame =  other.frame;
       state =  other.state;
       method = other.method;
       latchState = other.latchState;
      // 清空原有资源
       other.vAcquired = 0;
       other.frame = nullptr;
       other.latchState = LATCH_STATE::UNLATCHED;
       other.state = STATE::MOVED; // is this ok?
       return *this;
    }
   // -------------------------------------------------------------------------------------
   // 复制赋值运算符和复制构造函数被删除，禁止对象的复制
   Guard& operator=(Guard& other) = delete;
   Guard(Guard& other) = delete;  // copy constructor 
   // -------------------------------------------------------------------------------------
   // 与另一个 Guard 对象交换资源
   void swapWith(Guard& other)
   {
      std::swap(vAcquired, other.vAcquired);
      std::swap(frame, other.frame);
      std::swap(state, other.state);
      std::swap(method, other.method);
      std::swap(latchState, other.latchState);
   }
   // downgrade and upgrade check remote state   

   // 检查是否需要降级到指定状态
   bool needDowngrade(LATCH_STATE desired){
      if((desired == LATCH_STATE::EXCLUSIVE) && (desired != latchState))
         throw std::runtime_error("Would need latch update ");
      return (desired != latchState);
   }
   // 降级到指定状态
   bool downgrade(LATCH_STATE desired)
   {
      // 如果目标状态为 EXCLUSIVE，确保当前状态也为 EXCLUSIVE，并直接返回 true
      if (desired == LATCH_STATE::EXCLUSIVE) {
         ensure(desired == latchState);
         return true;
      } else if (desired == LATCH_STATE::SHARED) {
         // ATTENTION
         // we have a bug in glibc https://sourceware.org/bugzilla/show_bug.cgi?id=23861
         // due some nodes beeing ubuntu 18.04
         // therefore we currently cannot downgrade
         ensure(frame->latch.isLatched());
         auto v = frame->latch.version.load();
         frame->latch.unlatchExclusive();
         // 尝试获取 SHARED 锁
         if(!frame->latch.tryLatchShared()){
            // 获取失败时，将状态设置为 RETRY 并返回 false
            latchState = LATCH_STATE::UNLATCHED;
            state = STATE::RETRY;
            return false;
         }
         // frame->latch.latchShared();
         // 检查版本是否正确
         if( (v + 2) != frame->latch.version){
            // 版本不正确时，释放 SHARED 锁，设置状态为 RETRY，并返回 false
            frame->latch.unlatchShared();
            vAcquired = v+2;
            latchState = LATCH_STATE::UNLATCHED;
            state = STATE::RETRY;
            return false;
         }
         // 版本正确时，更新版本信息，设置状态为 SHARED，返回 true
         vAcquired = frame->latch.version;
         latchState = LATCH_STATE::SHARED;
         return true;
      } else if (desired == LATCH_STATE::OPTIMISTIC) {
         // 确保当前版本与 Guard 获得锁时的版本一致
         ensure(vAcquired == frame->latch.version);
         // 根据当前状态执行相应的降级操作
         if (latchState == LATCH_STATE::EXCLUSIVE){
            // 从 EXCLUSIVE 降级到 OPTIMISTIC
            vAcquired = frame->latch.downgradeExclusiveToOptimistic();
            latchState = LATCH_STATE::OPTIMISTIC; 
         }else if (latchState == LATCH_STATE::SHARED){
            // 从 SHARED 降级到 OPTIMISTIC
            vAcquired = frame->latch.downgradeSharedToOptimistic();
            latchState = LATCH_STATE::OPTIMISTIC; 
         }
      }
      return true;
   }

   // if we have an optimistic guard we need to check retry method?
   // 解锁当前 Guard 对象，释放相关资源
   void unlock(){
      if(latchState == LATCH_STATE::EXCLUSIVE){
         // 确保当前锁处于被锁定状态
         ensure(frame->latch.isLatched());
         // 解锁 EXCLUSIVE 锁，设置状态为 UNINITIALIZED
         frame->latch.unlatchExclusive();
         state = STATE::UNINITIALIZED;
      }
      else if(latchState == LATCH_STATE::SHARED){
         // 解锁 SHARED 锁，设置状态为 UNINITIALIZED
         frame->latch.unlatchShared();
         state = STATE::UNINITIALIZED;
      }
      else if(latchState == LATCH_STATE::OPTIMISTIC){
         // 如果乐观锁读取解锁失败，将状态设为 RETRY
         if(!frame->latch.optimisticReadUnlatchOrRestart(vAcquired)){
            state = STATE::RETRY;
         }
      }
      // 将锁状态设为 UNLATCHED
      latchState = LATCH_STATE::UNLATCHED;
      return;
   }
};
   
}  // storage
}  // scalestore
