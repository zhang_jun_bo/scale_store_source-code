#pragma once
#include "scalestore/utils/BatchQueue.hpp"
#include "scalestore/utils/RandomGenerator.hpp"

namespace scalestore {
namespace storage {

// Attention: Best over-allocate space due to half full batches
template <typename T, size_t Partitions, size_t BatchSize, template <class, size_t> class Container>//T：代表队列中元素的类型。Partitions：代表队列的分区数目，应为 2 的幂次方。BatchSize：代表每个分区中的批处理大小。Container：代表存储每个批处理的容器类型。
struct PartitionedQueue {
   static_assert(Partitions && ((Partitions & (Partitions - 1)) == 0), "Partitions not power of 2");//使用了静态断言来检查 Partitions 是否为 2 的幂次方，如果不是，则会触发编译错误
   // -------------------------------------------------------------------------------------
   using B = utils::Batch<T, BatchSize, Container>;//B：是 utils::Batch<T, BatchSize, Container> 的别名。Batch 是一个用于存储固定大小批处理元素的类型。
   // using Batch = Container<T, BatchSize>;
   using BQueue = utils::BatchQueue<T, BatchSize, Container>;//BQueue：是 utils::BatchQueue<T, BatchSize, Container> 的别名。BatchQueue 是一个用于存储批处理的队列类型。
   // -------------------------------------------------------------------------------------
   struct BatchHandle {//用于管理批处理的生命周期
      B* batch = nullptr; // 指向批处理的指针，初始化为空指针
      BQueue* queue = nullptr;// 指向队列的指针，初始化为空指针

      ~BatchHandle() { // 析构函数，用于在对象销毁时执行一些清理操作
         if (batch != nullptr) { // 如果批处理指针不为空
            if (batch->container.empty()) {// 如果批处理中的容器为空
               queue->push_empty(batch);  // return full batch to full queue，将批处理返回到空队列
            } else {
               queue->push_full(batch);  // return full batch to full queue，将批处理返回到满队列
            }
            batch = nullptr; // 将批处理指针设置为空指针
            queue = nullptr;// 将队列指针设置为空指针
         }
      }
   };
   // -------------------------------------------------------------------------------------
   std::vector<std::unique_ptr<BQueue>> queues;  // std::vector<std::unique_ptr<BatchQueue>> queues;
   // -------------------------------------------------------------------------------------
   // tl data
   inline static thread_local uint64_t seed = 0;
   // -------------------------------------------------------------------------------------
   PartitionedQueue(size_t expectedElements) {
      uint64_t queueSize = (expectedElements / Partitions);// 计算每个队列的预期容量
      queues.reserve(Partitions);// 预留容器空间，避免频繁重新分配内存
      for (uint64_t p_i = 0; p_i < Partitions; p_i++) {// 遍历每个分区
         queues.emplace_back(std::make_unique<BQueue>(queueSize));// 在容器中添加一个新的具有预期容量的 BQueue 对象
      }
   }
   // -------------------------------------------------------------------------------------
   void push(const T& e, BatchHandle& b_handle) {//函数的作用是将元素 e 推入批处理队列中，直到成功推入为止。
      while (!try_push(e, b_handle))//try_push(e, b_handle) 函数用于尝试将元素 e 推入批处理队列中，并将结果返回。
         ;//循环，只要尝试推入元素失败，就一直执行循环体内的代码。
   }
   // -------------------------------------------------------------------------------------
   bool try_push(const T& e, BatchHandle& b_handle) {// // 检查 BatchHandle 中的批次是否为空
      if (b_handle.batch == nullptr) {
      // 生成一个随机数作为需要操作的队列编号
         auto p_id = utils::RandomGenerator::getRandU64Fast() & (Partitions - 1);
         // 尝试从队列中获取一个空的批次
         auto rc = queues[p_id]->try_pop_empty(b_handle.batch);
         // 设置 BatchHandle 中的队列和批次信息
         b_handle.queue = queues[p_id].get();
         // 如果获取失败，将 BatchHandle 中的批次和队列置为空，返回 false
         if (!rc) {
            b_handle.batch = nullptr;
            b_handle.queue = nullptr;
            return false;
         }
      }
      // -------------------------------------------------------------------------------------
      if (b_handle.batch->container.try_push(e)) [[likely]] return true;// 尝试将元素 e 推入到当前批次的容器中，如果成功则直接返回 true
      // -------------------------------------------------------------------------------------
      b_handle.queue->push_full(b_handle.batch);  // cannot fail
      // 如果推入失败，则将当前批次推入到队列中，这里注释指出该操作不会失败
      // -------------------------------------------------------------------------------------
      // get empty batch，// 获取一个空的批次
      auto p_id = utils::RandomGenerator::getRandU64Fast() & (Partitions - 1);
      auto rc = queues[p_id]->try_pop_empty(b_handle.batch);
      b_handle.queue = queues[p_id].get();
      if (rc) return b_handle.batch->container.try_push(e);// 如果成功获取到空的批次，则将当前批次指向获取到的批次，并返回尝试推入元素到批次的容器的结果
      // -------------------------------------------------------------------------------------
      for (auto& q_ptr : queues) {// 如果没有成功获取到空的批次，则遍历所有队列
         if (q_ptr->try_pop_empty(b_handle.batch)) {
            b_handle.queue = q_ptr.get();
            return b_handle.batch->container.try_push(e);
         }
      }// 如果成功从某个队列获取到空的批次，则将当前批次指向获取到的批次，并返回尝试推入元素到批次的容器的结果
      b_handle.batch = nullptr;
      b_handle.queue = nullptr;
      return false;// 如果无法获取到空的批次，则置空批次和队列，并返回 false 表示推入失败
   }
   // -------------------------------------------------------------------------------------
   T pop(BatchHandle& b_handle){//用于从批处理中弹出一个元素。
      T result;//T 是泛型类型，代表要弹出的元素的类型
      while(!try_pop(result,b_handle))//循环的目的是一直尝试从批处理中弹出元素，直到成功弹出为止。
         ;//try_pop(result, b_handle) 是一个函数调用，用于尝试从批处理中弹出一个元素，并将该元素存储到 result 变量中。
      return result;//result 是存储要弹出的元素的变量。
   }
   // -------------------------------------------------------------------------------------
   bool try_pop(T& e, BatchHandle& b_handle) {//用于尝试从队列中弹出一个元素。
   //e 是一个引用，用于存储弹出的元素。b_handle 是 BatchHandle 类型的参数，用于指示弹出的批处理的状态。
      if (b_handle.batch == nullptr) {//检查 b_handle 是否为 nullptr。如果是，表示当前没有可用的批处理。
         auto p_id = utils::RandomGenerator::getRandU64Fast() & (Partitions - 1);//生成一个随机数 p_id，并将其与 Partitions - 1 进行位与运算。
         auto rc = queues[p_id]->try_pop_full(b_handle.batch);//通过分区 ID 获取对应的队列，并调用 try_pop_full 函数尝试从队列中弹出一个批处理，并将其存储到 b_handle.batch 中。
         b_handle.queue = queues[p_id].get();//将当前队列的指针赋值给 b_handle.queue。
         if (!rc) {
            b_handle.batch = nullptr;
            b_handle.queue = nullptr;
            return false;
         }//弹出失败返回false
      }
      // -------------------------------------------------------------------------------------
      if (b_handle.batch->container.try_pop(e)) [[likely]] return true;//尝试从批处理容器中弹出一个元素。如果成功弹出元素，则返回 true。
      // -------------------------------------------------------------------------------------
      assert(b_handle.batch->container.get_size() == 0);//断言批处理容器的大小为 0，以确保在弹出元素后容器已为空
      b_handle.queue->push_empty(b_handle.batch);//将当前的批处理返回到队列中
      // -------------------------------------------------------------------------------------
      // get full batch
      auto p_id = utils::RandomGenerator::getRandU64Fast() & (Partitions - 1);//生成一个随机数 p_id，将其与 (Partitions - 1) 进行位与运算，用于选择一个随机的分区 ID。
      if (queues[p_id]->try_pop_full(b_handle.batch)) {//尝试从另一个随机选取的队列中获取一个完整的批处理，并将其存储到 b_handle.batch 中。
         b_handle.queue = queues[p_id].get();
         return b_handle.batch->container.try_pop(e);
      }//如果成功获取到一个完整的批处理，则更新 b_handle 的队列指针，并尝试从批处理容器中弹出一个元素，最后返回弹出结果。
      // -------------------------------------------------------------------------------------
      for (auto& q_ptr : queues) {//循环遍历 queues 容器中的每个队列指针 q_ptr。
         if (q_ptr->try_pop_full(b_handle.batch)) {//每个队列 q_ptr，尝试从中获取一个完整的批处理。
            b_handle.queue = q_ptr.get();
            return b_handle.batch->container.try_pop(e);//如果成功获取到一个批处理，则更新 b_handle 的队列指针，并尝试从批处理容器中弹出一个元素，最后返回弹出结果。
         }
      }
      b_handle.batch = nullptr;
      b_handle.queue = nullptr;
      return false;//返回 false，表示没有成功获取到批处理。
   }


   // -------------------------------------------------------------------------------------
   // util functions
   // -------------------------------------------------------------------------------------
   void assert_no_duplicates() {//函数的目的是确保每个队列中没有重复的元素。
      for (auto& q_ptr : queues) {//遍历了 queues 容器中的每个队列指针 q_ptr，然后对每个队列调用 assert_no_duplicates 函数
         q_ptr->assert_no_duplicates();
      }
   }
   // -------------------------------------------------------------------------------------
   void reset() {//函数的目的是将每个队列重置为初始状态，清空其中的元素。
      for (auto& q_ptr : queues) {//它遍历了 queues 容器中的每个队列指针 q_ptr，然后对每个队列调用 reset 函数
         q_ptr->reset();
      }
   }
   // -------------------------------------------------------------------------------------
   void assert_no_leaks() {//函数的目的可能是确保队列中没有内存泄漏的情况，即所有在队列中分配的内存都正确地被释放。
      for (auto& q_ptr : queues) {//它遍历了 queues 容器中的每个队列指针 q_ptr，然后对每个队列调用 assert_no_leaks 函数。
         q_ptr->assert_no_leaks();
      }
   }
   // -------------------------------------------------------------------------------------
   uint64_t approx_size() {//它返回整个队列集合中所有元素的大致大小。
      uint64_t size = 0;//uint64_t 类型的变量 size 来记录总大小。
      std::for_each(std::begin(queues), std::end(queues), [&](auto& queue) { size += queue->approx_size(); });
      //遍历 queues 容器中的每个队列，对每个队列调用 approx_size 函数，并将其返回的大小加到 size 变量中。
      return size;//函数返回变量 size，其值表示所有队列中元素的大致总大小。
   }
   // -------------------------------------------------------------------------------------
   // cheap random function

   // -------------------------------------------------------------------------------------
};

}  // storage
}  // scalestore
