#include "AsyncReadBuffer.hpp"
// -------------------------------------------------------------------------------------
#include <signal.h>
#include <cstring>
#include <memory>
// -------------------------------------------------------------------------------------
namespace scalestore
{
namespace storage
{
// -------------------------------------------------------------------------------------
//这个构造函数用于初始化 AsyncReadBuffer对象
AsyncReadBuffer::AsyncReadBuffer(int fd, u64 page_size, u64 batch_max_size) : fd(fd), page_size(page_size), batch_max_size(batch_max_size), free_slots(batch_max_size)//构造函数
{//fd表示文件描述符，page_size表示页面大小，batch_max_size表示批量最大大小。
   // write_buffer = std::make_unique<Page[]>(batch_max_size);
   write_buffer_commands = std::make_unique<WriteCommand[]>(batch_max_size);
   iocbs = std::make_unique<struct iocb[]>(batch_max_size);
   iocbs_ptr = std::make_unique<struct iocb*[]>(batch_max_size);
   events = std::make_unique<struct io_event[]>(batch_max_size);//使用std::make_unique创建了一些成员变量的唯一指针， 指针将用于异步读取数据和处理IO事件。
   // -------------------------------------------------------------------------------------
   memset(&aio_context, 0, sizeof(aio_context));//将aio_context的内存清零，aio_context是一个io_context_t类型的结构体，用于管理异步IO操作的上下文。
   const int ret = io_setup(batch_max_size, &aio_context);//调用io_setup函数创建一个异步IO上下文，并将其存储在aio_context中。
   if (ret != 0) {
      throw std::runtime_error(("io_setup failed, ret code = " + std::to_string(ret)));
   }//如果io_setup返回值不为0，则表示创建异步IO上下文失败，抛出一个std::runtime_error异常。
   // init free slots
   for(uint64_t s_i = 0; s_i < batch_max_size; ++s_i)
      free_slots.add(s_i);
}//循环将所有的空闲槽位添加到free_slots中，free_slots是一个自定义的循环队列，用于跟踪空闲槽位的索引。

// -------------------------------------------------------------------------------------
}  // namespace storage
}  // namespace scalestore
   // -------------------------------------------------------------------------------------
