#pragma once
#include "BufferFrame.hpp"
#include "Defs.hpp"
#include "scalestore/storage/datastructures/RingBuffer.hpp"
// -------------------------------------------------------------------------------------
// -------------------------------------------------------------------------------------
#include <libaio.h>
#include <memory>

#include <functional>
#include <list>
#include <unordered_map>
// -------------------------------------------------------------------------------------
namespace scalestore
{
namespace storage
{
// -------------------------------------------------------------------------------------
class AsyncReadBuffer
{
  private:
   struct WriteCommand {
      BufferFrame* bf;
      PID pid;
      uint64_t client_slot; // message handler specific。消息处理程序特定的客户端槽位。
      bool recheck_msg = false;//重新检查消息的标志，初始值为false。
   };
   io_context_t aio_context;//用于管理异步IO操作的上下文。
   int fd;//文件描述符
   u64 page_size, batch_max_size;//页面大小；批量最大大小。
   u64 pending_requests = 0;//待处理的请求数量。
   u64 ready_to_submit = 0;//准备提交的请求数量。
   u64 outstanding_ios = 0;//未完成的IO操作数量。


#define AIO_RING_MAGIC 0xa10a10a1//AIO_RING_MAGIC被定义为0xa10a10a1，可能用于在magic字段中进行魔术数校验。
struct aio_ring {
	unsigned id; /** kernel internal index number内核内部索引号。 */
	unsigned nr; /** number of io_events IO事件的数量 */
	unsigned head;//环形缓冲区头部索引。
	unsigned tail;//环形缓冲区尾部索引。

	unsigned magic;//魔术数，用于标识aio_ring结构体的类型。
	unsigned compat_features;//兼容特性。
	unsigned incompat_features;//不兼容特性。
	unsigned header_length; /** size of aio_ring 结构体的头部长度。 */

	struct io_event events[0];//用于存储IO事件。
};

/* Stolen from kernel arch/x86_64.h */
#ifdef __x86_64__
#define read_barrier() __asm__ __volatile__("lfence" ::: "memory")
#else
#ifdef __i386__
#define read_barrier() __asm__ __volatile__("" : : : "memory")
#else
#define read_barrier() __sync_synchronize()
#endif
#endif

   /* Code based on axboe/fio:
    * https://github.com/axboe/fio/blob/702906e9e3e03e9836421d5e5b5eaae3cd99d398/engines/libaio.c#L149-L172
    */
        // 内联函数，用于获取异步 I/O 事件
        inline static int own_io_getevents(io_context_t ctx,
                                           long min_nr,
                                           long max_nr,
                                           struct io_event* events,
                                           struct timespec* timeout) {
            int i = 0;

            struct aio_ring* ring = (struct aio_ring*)ctx;
            if (ring == NULL || ring->magic != AIO_RING_MAGIC) {
                goto do_syscall;
            }

            while (i < max_nr) {
                unsigned head = ring->head;
                if (head == ring->tail) {
                    /* There are no more completions 没有更多的完成事件*/
                    return i;
                    // break;
                } else {
                    /* There is another completion to reap 还有另一个完成事件要处理*/
                    events[i] = ring->events[head];
                    read_barrier();
                    ring->head = (head + 1) % ring->nr;
                    i++;
                }
            }

            if (i == 0 && timeout != NULL && timeout->tv_sec == 0 && timeout->tv_nsec == 0) {
                /* Requested non blocking operation. 请求非阻塞操作。*/
                return 0;
            }

            if (i >= min_nr) {
                return i;
            }

            do_syscall:
            return syscall(__NR_io_getevents, ctx, min_nr - i, max_nr - i, &events[i], timeout);
        }

    public:
        std::unique_ptr<Page[]> write_buffer;  // 写入缓冲区
        std::unique_ptr<WriteCommand[]> write_buffer_commands;// 写入命令数组
        std::unique_ptr<struct iocb[]> iocbs;// I/O 控制块数组
        std::unique_ptr<struct iocb *[]> iocbs_ptr;// I/O 控制块指针数组
        std::unique_ptr<struct io_event[]> events; // 异步 I/O 事件数组
        RingBuffer <uint64_t> free_slots;// 空闲槽位数组
        // -------------------------------------------------------------------------------------
        // Debug
        // -------------------------------------------------------------------------------------
        // 构造函数，用于初始化 AsyncReadBuffer对象
        AsyncReadBuffer(int fd, u64 page_size, u64 batch_max_size);
        // Caller takes care of sync
        // bool full();
        // void add(BufferFrame& bf, PID pid, uint64_t client_slot, bool recheck_msg);
        // u64 submit();
        // u64 pollEventsSync();
   // void getReadBfs(std::function<void(BufferFrame&, uint64_t, bool)> callback, u64 n_events);

   // -------------------------------------------------------------------------------------
   inline bool full() {
       // 如果进行中的 I/O 操作数量与准备提交的 I/O 操作数量之和超过了批量最大大小减 2，
       // 则认为缓冲区已满，因为要给提交的 I/O 操作腾出一些槽位
       if ((outstanding_ios + ready_to_submit) >= batch_max_size - 2) {
           return true;
       } else {
           return false;
       }
   }

        // -------------------------------------------------------------------------------------
        // 向异步读取缓冲区中添加写入命令
        // 参数：
        //   - bf: 缓冲帧引用
        //   - pid: 进程标识符
        //   - client_slot: 客户槽位
        //   - recheck_msg: 是否重新检查消息
        inline void add(BufferFrame &bf, PID pid, uint64_t client_slot, bool recheck_msg) {
            // 确保异步读取缓冲区不满
            ensure(!full());
            // 确保缓冲帧的页面地址是 512 的倍数
            ensure(u64(bf.page) % 512 == 0);
            // 确保进行中的 I/O 操作数量与准备提交的 I/O 操作数量之和不超过批量最大大小
            ensure((outstanding_ios + ready_to_submit) <= batch_max_size);
            // -------------------------------------------------------------------------------------
            // 从空闲槽位中取出一个槽位，并更新已准备提交的槽位数量
      auto slot = free_slots.remove();
      auto slot_ptr = ready_to_submit++;
            // 填充写入命令结构体
      write_buffer_commands[slot].bf = &bf;
      write_buffer_commands[slot].pid = pid;
      write_buffer_commands[slot].client_slot = client_slot;
            write_buffer_commands[slot].recheck_msg = recheck_msg;
            bf.page->magicDebuggingNumber = pid;

            // 设置 I/O 操作的读取参数
            void *write_buffer_slot_ptr = bf.page;
            io_prep_pread(&iocbs[slot], fd, write_buffer_slot_ptr, page_size, page_size * pid.plainPID());
            iocbs[slot].data = write_buffer_slot_ptr;
            iocbs_ptr[slot_ptr] = &iocbs[slot];
        }
        // -------------------------------------------------------------------------------------
        // 提交异步读取请求
// 返回值：成功提交的请求数量
        /*
         * 这个函数负责将已准备提交的异步读取请求提交给异步 I/O 上下文。
         * 如果有请求被提交，它会确保成功提交的请求数量与已准备提交的请求数量相等，
         * 并更新进行中的 I/O 操作数量和已准备提交的请求数量。如果没有已准备提交的请求，则返回 0。
         * */
        inline u64 submit() {
            // 如果有已准备提交的请求
            if (ready_to_submit > 0) {
                // 使用 io_submit 提交异步读取请求
                int ret_code = io_submit(aio_context, ready_to_submit, iocbs_ptr.get());
                // 确保成功提交的请求数量与已准备提交的请求数量相等
                ensure(ret_code == s32(ready_to_submit));
                // 更新进行中的 I/O 操作数量和已准备提交的请求数量
                outstanding_ios += ready_to_submit;
                ready_to_submit = 0;
                return ret_code;
            }
            // 如果没有已准备提交的请求，则返回 0
            return 0;
        }
        // -------------------------------------------------------------------------------------
// 同步等待并处理异步 I/O 事件
// 返回值：已处理的事件数量
/*
 * 这个函数负责同步等待并处理异步I/O 事件。
 * 如果有进行中的 I/O 操作，它使用 own_io_getevents 函数获取已完成的 I/O 事件，并确保进行中的 I/O 操作数量不小于已处理的事件数量。
 * 然后，它更新进行中的 I/O 操作数量，并返回已处理的事件数量。如果没有进行中的 I/O 操作，则返回 0。
 * */
        inline u64 pollEventsSync() {
            // 如果有进行中的 I/O 操作
            if (outstanding_ios > 0) {
                // 使用 own_io_getevents 函数获取已完成的 I/O 事件
                const int done_requests = own_io_getevents(aio_context, outstanding_ios, outstanding_ios, events.get(),
                                                           NULL);
                // 如果获取事件失败，抛出异常
                if (done_requests < 0)
                    throw std::runtime_error("io_getevents failed" + std::to_string(done_requests));
                // 确保进行中的 I/O 操作数量不小于已处理的事件数量
                ensure(outstanding_ios >= (uint64_t)
                done_requests);
                // 更新进行中的 I/O 操作数量
                outstanding_ios -= (uint64_t)
                done_requests;
                // 返回已处理的事件数量
                return done_requests;
            }
            // 如果没有进行中的 I/O 操作，则返回 0
            return 0;
        }
        // -------------------------------------------------------------------------------------
        /*
         * 这个函数负责从异步 I/O 事件中获取已读取的缓冲帧，并通过回调函数进行处理。
         * 它遍历了每个事件，计算了相应的槽位索引，将槽位添加回空闲槽位数组，并确保事件的结果为期望的页面大小。
         * 如果事件的 res2 不为 0，它会抛出信号 TRAP。然后，它调用提供的回调函数处理相应的缓冲帧。
         * */
        inline void getReadBfs(std::function<void(BufferFrame & , uint64_t, bool)> callback, u64 n_events) {
            // 遍历每一个事件
            for (u64 i = 0; i < n_events; i++) {
                // 计算槽位索引，通过事件对象的指针减去 iocbs 数组的首地址得到槽位索引
                // const auto slot = (u64(events[i].data) - u64(write_buffer.get())) / page_size;
                const auto slot = ((iocb * )(events[i].obj)) - (((iocbs.get())));
                // 将槽位添加回空闲槽位数组
                free_slots.add(slot);
                // -------------------------------------------------------------------------------------
                // 确保事件的结果为期望的页面大小
                ensure(events[i].res == page_size);
                // 如果事件的 res2 不为 0，抛出信号 TRAP
                if (!(events[i].res2 == 0)) {
                    raise(SIGTRAP);
                }
                // 调用回调函数处理缓冲帧
                callback(*write_buffer_commands[slot].bf, write_buffer_commands[slot].client_slot,
                         write_buffer_commands[slot].recheck_msg);
            }
        }
};
// -------------------------------------------------------------------------------------
}  // namespace storage
}  // namespace scalestore
// -------------------------------------------------------------------------------------
