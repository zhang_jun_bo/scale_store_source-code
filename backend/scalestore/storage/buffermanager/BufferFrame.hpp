#pragma once
// -------------------------------------------------------------------------------------
#include "../../syncprimitives/HybridLatch.hpp"
#include "../datastructures/Bitmap.hpp"
#include "Defs.hpp"
#include "Page.hpp"
// -------------------------------------------------------------------------------------
namespace scalestore
{
namespace storage
{
// -------------------------------------------------------------------------------------
// Which node posses the bufferframe and in which mode
// -------------------------------------------------------------------------------------
// 用于表示缓冲帧的拥有者信息的联合体
union Possessors {
   NodeID exclusive;                // 互斥访问的节点ID
   StaticBitmap<64> shared;         // 共享访问的位图，支持64个节点
};

// 枚举类型，表示缓冲帧的占有状态
enum class POSSESSION : uint8_t {
   NOBODY = 0,   // 无任何占有者，初始化或释放状态
   EXCLUSIVE = 1,  // 互斥占有
   SHARED = 2,     // 共享占有
};

// -------------------------------------------------------------------------------------
// STATES
// -------------------------------------------------------------------------------------
// 枚举类型，表示缓冲帧的状态
enum class BF_STATE : uint8_t {
   FREE = 0,                // 空闲状态
   HOT = 1,                 // 热状态
   EVICTED = 2,             // 被驱逐，但由于远程所有权仍然安装在哈希表中
   IO_RDMA = 3,             // RDMA IO
   IO_SSD = 4,              // SSD IO
   INVALIDATION_EXPECTED = 8,  // 预期的失效状态
};

// -------------------------------------------------------------------------------------
struct BufferFrame {
   PID pid = EMPTY_PID;          // 页面ID，初始化为 EMPTY_PID
   BufferFrame* next = nullptr;  // 用于构建链表的指针，在Intrusive Datastructures中使用
   Page* page = nullptr;         // 指向页面的指针
   std::atomic<uint64_t> pVersion = 0;  // 页面版本号的原子变量
   std::atomic<uint64_t> epoch {EMPTY_EPOCH};  // 当前使用的纪元
   OptimisticLatch ht_bucket_latch;  // 用于哈希表桶的乐观锁
   Possessors possessors = {0};  // 拥有者信息的联合体，由 POSSESSION 管理
   std::atomic<BF_STATE> state = BF_STATE::FREE;  // 缓冲帧状态，重要，用于保护哈希表桶
   std::atomic<bool> mhWaiting = false;  // 是否等待多级哈希
   std::atomic<bool> cooling = false;    // 是否处于冷却状态
   bool dirty = false;  // 是否为脏页
   bool isHtBucket = false;  // 是否是内联的哈希表桶
   POSSESSION possession = POSSESSION::NOBODY;  // 占有状态
   HybridLatch latch;  // 混合锁

   // -------------------------------------------------------------------------------------
 
   // @brief 填充缓冲帧的信息
   // @param pid 缓冲帧对应的页面ID
   // @param page 指向页面的指针
   // @param possession 缓冲帧的占有状态

   void fill(PID pid, Page* page, POSSESSION possession)
   {
      this->pid = pid;         // 设置页面ID
      this->page = page;       // 设置指向页面的指针
      this->possession = possession;  // 设置占有状态
   }
   // -------------------------------------------------------------------------------------
   //  
   //  @brief 重置缓冲帧的状态，将其恢复到初始状态
   //  
   void reset()
   {
      pVersion = 0;                 // 重置页面版本号
      pid = EMPTY_PID;              // 重置页面ID为空
      if (!isHtBucket)              // 如果不是哈希表桶（不是内联的哈希表桶）
         next = nullptr;            // 重置链表指针为空
      page = nullptr;               // 重置指向页面的指针为空
      possession = POSSESSION::NOBODY;  // 重置占有状态为无占有者
      possessors = {0};             // 重置拥有者信息为初始状态
      mhWaiting = false;            // 不等待多级哈希
      epoch = EMPTY_EPOCH;          // 重置时期为初始时期
      dirty = false;                // 将脏页标志重置为 false
      state = BF_STATE::FREE;       // 重置缓冲帧状态为空闲状态
   }
   // -------------------------------------------------------------------------------------
   // Protocol possessor information
   // -------------------------------------------------------------------------------------
   // @brief 设置缓冲帧的占有状态
   // @param possession_ 要设置的占有状态
 
   void setPossession(POSSESSION possession_)
   {
      possession = possession_;  // 将传入的占有状态设置给缓冲帧
   }

   // @brief 设置缓冲帧的拥有者信息为指定节点ID
   // @param nodeId 要设置的节点ID
   // @throws std::runtime_error 如果缓冲帧当前没有拥有者
 
   void setPossessor(NodeID nodeId)
   {
      if (possession == POSSESSION::NOBODY)
         throw std::runtime_error("Object not in possession");
      else if (possession == POSSESSION::EXCLUSIVE)
         possessors.exclusive = nodeId;               // 如果是互斥占有，将节点ID设置为拥有者
      else if (possession == POSSESSION::SHARED)
         possessors.shared.set(nodeId);               // 如果是共享占有，将节点ID添加到位图中
   }
   // -------------------------------------------------------------------------------------

   //   @brief 检查指定节点ID是否是缓冲帧的拥有者
   //   @param nodeId 要检查的节点ID
   //   @return true 如果节点是拥有者，否则返回 false

   bool isPossessor(NodeID nodeId)
   {
      if (possession == POSSESSION::EXCLUSIVE)
         return (possessors.exclusive == nodeId);             // 如果是互斥占有，检查节点ID是否与拥有者一致
      else if (possession == POSSESSION::SHARED)
         return possessors.shared.test(nodeId);               // 如果是共享占有，检查节点ID是否在位图中
      return false;                                           // 如果没有拥有者，返回 false
   }
   
   // 检查是否为脏页的函数
   bool isDirty(){
      return dirty;
   }
   // 设置为脏页的函数
   void setDirty(){
      dirty = true;
   }
};
// -------------------------------------------------------------------------------------
// 缓冲帧的大小断言
static_assert(sizeof(BufferFrame) <= 128, "Bufferframe spans more than two cachelines");
}  // namespace storage
}  // namespace scalestore
