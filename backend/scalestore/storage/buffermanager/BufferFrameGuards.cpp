#include "BufferFrameGuards.hpp"
// -------------------------------------------------------------------------------------

namespace scalestore {
namespace storage {
// -------------------------------------------------------------------------------------
// ExclusiveBFGuard
// -------------------------------------------------------------------------------------
// new Page

/**
@brief ExclusiveBFGuard 类的构造函数
构造函数用于创建 ExclusiveBFGuard 实例，并初始化其成员变量。
并通过调用全局缓冲池的 newPage 函数获得一个新的页面。
然后，将页面标记为脏页，设置锁状态为互斥访问，状态为已初始化，并获取缓冲帧的锁版本号。
*/
ExclusiveBFGuard::ExclusiveBFGuard() {
   g.frame = &BM::global->newPage();        // 获取全局缓冲池的新页面，并将其地址赋给 g.frame
   g.frame->dirty = true;                   // 设置新页面为脏页
   g.latchState = LATCH_STATE::EXCLUSIVE;   // 设置乐观锁状态为互斥访问
   g.state = STATE::INITIALIZED;            // 设置状态为已初始化
   g.vAcquired = g.frame->latch.version;    // 获取缓冲帧的锁版本号
};

/*
创建一个 ExclusiveBFGuard 实例，并通过调用全局缓冲池的 newRemotePage 函数获取一个远程页面，同时传入远程节点的ID。
然后，将页面标记为脏页，设置锁状态为互斥访问，状态为已初始化，并获取缓冲帧的锁版本号。
这用于在远程节点上进行互斥访问。
*/
ExclusiveBFGuard::ExclusiveBFGuard(NodeID remoteNodeId) {
   g.frame = &BM::global->newRemotePage(remoteNodeId);   // 获取全局缓冲池的远程页面，并将其地址赋给 g.frame
   g.frame->dirty = true;                                // 设置远程页面为脏页
   g.latchState = LATCH_STATE::EXCLUSIVE;               // 设置乐观锁状态为互斥访问
   g.state = STATE::INITIALIZED;                        // 设置状态为已初始化
   g.vAcquired = g.frame->latch.version;                // 获取缓冲帧的锁版本号
}

/*
 * @brief ExclusiveBFGuard 类的构造函数，通过页面ID锁定页面
 * 
 * @param pid 页面ID
 */
ExclusiveBFGuard::ExclusiveBFGuard(PID pid) {
   g = BM::global->fix(pid, Exclusive());   // 通过页面ID锁定页面，使用 Exclusive 锁
   g.frame->dirty = true;                    // 设置锁定的页面为脏页
};

/*
 * @brief ExclusiveBFGuard 类的移动构造函数
 * 
 * @param xGuard 要移动的 ExclusiveBFGuard 实例
 */
ExclusiveBFGuard::ExclusiveBFGuard(ExclusiveBFGuard&& xGuard) {
   g = std::move(xGuard.g);  // 使用移动语义移动 xGuard 的状态到当前实例
}

/*
 * @brief 移动赋值运算符，用于将一个 ExclusiveBFGuard 实例的状态移动到另一个实例
 * 
 * @param xGuard 要移动的 ExclusiveBFGuard 实例
 * @return ExclusiveBFGuard& 移动后的实例的引用
 */
ExclusiveBFGuard& ExclusiveBFGuard::operator=(ExclusiveBFGuard&& xGuard) {
   g = std::move(xGuard.g);  // 使用移动语义移动 xGuard 的状态到当前实例
   return *this;             // 返回移动后的实例的引用
}
// -------------------------------------------------------------------------------------
ExclusiveBFGuard::ExclusiveBFGuard(SharedBFGuard&& sGuard) {
   ensure(sGuard.g.latchState == LATCH_STATE::SHARED);  // 确保 SharedBFGuard 实例的锁状态为共享
   ensure(g.state == STATE::UNINITIALIZED);             // 确保当前 ExclusiveBFGuard 实例的状态为未初始化
   // -------------------------------------------------------------------------------------
   PID pid = sGuard.getFrame().pid;    // 获取 SharedBFGuard 实例的缓冲帧的页面ID
   // -------------------------------------------------------------------------------------
   // 将 SharedBFGuard 实例的状态和锁状态设置为 MOVED 和 UNLATCHED   
   sGuard.g.state = STATE::MOVED;
   sGuard.g.latchState = LATCH_STATE::UNLATCHED;
   // 对 SharedBFGuard 实例的缓冲帧进行解锁，解除共享锁
   sGuard.getFrame().latch.unlatchShared();
   // -------------------------------------------------------------------------------------
   // fix again
   // check version
   // check pointer

   g =  BM::global->fix(pid, Exclusive());   // 再次锁定指定页面ID的缓冲帧，使用 Exclusive 锁
   ensure(g.state == STATE::INITIALIZED);    // 确保锁定后的状态为已初始化，即确保缓冲帧处于已初始化状态
   // check ptr could be changed due to remote 
   if((&getFrame() != &sGuard.getFrame())){  // 检查锁定后的缓冲帧指针是否与之前的 SharedBFGuard 实例的缓冲帧指针相同
      g.state = STATE::RETRY;    // 如果不相同，设置状态为 RETRY
   }
   // check version must be updated now   检查版本号是否已经更新，如果没有更新，设置状态为 RETRY
   if((g.vAcquired != sGuard.g.vAcquired + 0b10)){
      g.state = STATE::RETRY;
   }
   // 如果状态不是 RETRY，则将缓冲帧标记为脏页
   if(g.state != STATE::RETRY){
      g.frame->dirty = true;
   }
}
// -------------------------------------------------------------------------------------
// O - X
/*
 * @brief 从 OptimisticBFGuard 实例移动构造 ExclusiveBFGuard 实例
 * 
 * @param oGuard 要移动的 OptimisticBFGuard 实例
 */
ExclusiveBFGuard::ExclusiveBFGuard(OptimisticBFGuard&& oGuard) {
   ensure(oGuard.g.latchState == LATCH_STATE::OPTIMISTIC);  // 确保 OptimisticBFGuard 实例的锁状态为乐观锁
   ensure(g.state == STATE::UNINITIALIZED);                  // 确保当前 ExclusiveBFGuard 实例的状态为未初始化
   // -------------------------------------------------------------------------------------
   PID pid = oGuard.getFrame().pid;                         // 获取 OptimisticBFGuard 实例的缓冲帧的页面ID
   // -------------------------------------------------------------------------------------
   oGuard.g.state = STATE::MOVED;                           // 设置 OptimisticBFGuard 实例的状态为 MOVED
   oGuard.g.latchState = LATCH_STATE::UNLATCHED;            // 设置 OptimisticBFGuard 实例的锁状态为 UNLATCHED
   // -------------------------------------------------------------------------------------
   g.state = STATE::RETRY;                                  // 设置当前 ExclusiveBFGuard 实例的状态为 RETRY
   // -------------------------------------------------------------------------------------
   if (!oGuard.retry()) {
      g.state = STATE::INITIALIZED;                        // 如果 oGuard 不需要 RETRY，将当前实例的状态设置为 INITIALIZED
      // -------------------------------------------------------------------------------------
      g = BM::global->fix(pid, Exclusive());               // 锁定指定页面ID的缓冲帧，使用 Exclusive 锁
      ensure(g.state == STATE::INITIALIZED);               // 确保锁定后的状态为已初始化
      ensure(g.latchState == LATCH_STATE::EXCLUSIVE);      // 确保锁定后的缓冲帧的锁状态为互斥访问
      // -------------------------------------------------------------------------------------
      // 检查锁定后的缓冲帧指针是否与之前的 OptimisticBFGuard 实例的缓冲帧指针相同
      if (&getFrame() != &oGuard.getFrame()) {
         g.state = STATE::RETRY;                           // 如果不同，将状态设置为 RETRY
      }
      // 检查版本号是否已经更新，如果没有更新，将状态设置为 RETRY
      if (g.vAcquired != oGuard.g.vAcquired + 0b10) {
         g.state = STATE::RETRY;
      }      
   }
   
   if (g.state != STATE::RETRY) {
      g.frame->dirty = true;                               // 如果状态不是 RETRY，则将缓冲帧标记为脏页
   }

   if (g.state == STATE::RETRY && g.latchState == LATCH_STATE::EXCLUSIVE) {
      ensure(g.frame->latch.isLatched());
      g.frame->latch.unlatchExclusive();                   // 如果状态是 RETRY 且锁状态为互斥访问，解除互斥锁
      g.latchState = LATCH_STATE::UNLATCHED;               // 设置当前实例的锁状态为 UNLATCHED
   }
   // -------------------------------------------------------------------------------------
}
// -------------------------------------------------------------------------------------
/*
 * @brief 回收缓冲帧资源
 * 
 * 该函数用于将当前 ExclusiveBFGuard 实例所持有的缓冲帧资源回收，
 * 包括释放缓冲帧到全局缓冲池，并将实例的状态和锁状态设置为未初始化和解锁状态。
 */
void ExclusiveBFGuard::reclaim() {
   BM::global->reclaimPage(*g.frame);    // 释放缓冲帧到全局缓冲池
   g.state = STATE::UNINITIALIZED;      // 设置实例的状态为未初始化
   g.latchState = LATCH_STATE::UNLATCHED;  // 设置实例的锁状态为解锁状态
}
// -------------------------------------------------------------------------------------
ExclusiveBFGuard::~ExclusiveBFGuard() {


   if(g.state == STATE::INITIALIZED && g.latchState == LATCH_STATE::EXCLUSIVE){
      assert(g.frame->latch.isLatched());
      g.frame->latch.unlatchExclusive();
      g.state = STATE::UNINITIALIZED;
      g.latchState = LATCH_STATE::UNLATCHED;
   }   
}
// -------------------------------------------------------------------------------------
// SharedBFGuard
// -------------------------------------------------------------------------------------
SharedBFGuard::SharedBFGuard(PID pid)
{
   g = BM::global->fix(pid, Shared());    // 通过页面ID锁定页面，使用 Shared 锁
};
// -------------------------------------------------------------------------------------
SharedBFGuard::SharedBFGuard(SharedBFGuard&& sGuard) {
   g = std::move(sGuard.g);      // 使用移动语义移动 sGuard 的状态到当前实例
}
SharedBFGuard& SharedBFGuard::operator=(SharedBFGuard&& sGuard){
   g = std::move(sGuard.g);      // 使用移动语义移动 sGuard 的状态到当前实例
   return *this;     // 返回移动后的实例的引用
}
// downgrade X - S
// no need to update remote possession information
SharedBFGuard::SharedBFGuard(ExclusiveBFGuard&& xGuard) {   
   ensure(xGuard.g.latchState == LATCH_STATE::EXCLUSIVE);   // 确保 ExclusiveBFGuard 实例的锁状态为互斥访问
   ensure(g.state == STATE::UNINITIALIZED);     // 确保当前 SharedBFGuard 实例的状态为未初始化
   ensure(xGuard.getFrame().latch.isLatched());     // 确保 ExclusiveBFGuard 实例的缓冲帧已经被锁定
   // -------------------------------------------------------------------------------------
   g = std::move(xGuard.g);      // 使用移动语义移动 xGuard 的状态到当前实例
   // -------------------------------------------------------------------------------------
   g.frame->latch.unlatchExclusive();     // 解除 ExclusiveBFGuard 实例的互斥锁
   // -------------------------------------------------------------------------------------
   // try lock shared
   g.state = STATE::RETRY;    // 尝试以共享锁的方式重新锁定
   if(getFrame().latch.tryLatchShared()){
      if((getFrame().latch.version == (g.vAcquired + 0b10))){
         g.state = STATE::INITIALIZED;    // 如果成功，设置当前实例的状态为已初始化
         g.latchState = LATCH_STATE::SHARED;    // 设置当前实例的锁状态为共享访问
         g.vAcquired = g.vAcquired + 0b10;      // 更新版本号
      }else{
         getFrame().latch.unlatchShared();   // 如果版本号不匹配，解除共享锁
      }
   }
}
//
// upgrade O - S
SharedBFGuard::SharedBFGuard(OptimisticBFGuard&& oGuard)
{
   ensure(oGuard.g.latchState == LATCH_STATE::OPTIMISTIC);     // 确保 OptimisticBFGuard 实例的锁状态为乐观锁
   ensure(g.state == STATE::UNINITIALIZED);        // 确保当前 SharedBFGuard 实例的状态为未初始化
   // -------------------------------------------------------------------------------------
   g = std::move(oGuard.g);  // copy   // 使用移动语义移动 oGuard 的状态到当前实例
   // -------------------------------------------------------------------------------------
   // try lock shared
   g.state = STATE::RETRY;
   if (getFrame().latch.tryLatchShared()) {
      if ((getFrame().latch.version == (g.vAcquired))) {
         g.state = STATE::INITIALIZED;
         g.latchState = LATCH_STATE::SHARED;    // 设置当前实例的锁状态为共享访问
      } else {
         getFrame().latch.unlatchShared();      // 如果版本号不匹配，解除共享锁
      }
   }
}
/*
 * @brief SharedBFGuard 类的析构函数
 * 
 * 如果 SharedBFGuard 实例的状态为已初始化且锁状态为共享访问，
 * 则在析构函数中解除共享锁，将状态设置为已初始化，锁状态设置为解锁状态。
 */
SharedBFGuard::~SharedBFGuard() {
   if (g.state == STATE::INITIALIZED && g.latchState == LATCH_STATE::SHARED) {
      g.frame->latch.unlatchShared();     // 解除共享锁
      g.state = STATE::INITIALIZED;       // 将状态设置为已初始化
      g.latchState = LATCH_STATE::UNLATCHED;  // 将锁状态设置为解锁状态
   }
}
// -------------------------------------------------------------------------------------
// OptimisticBFGuard
// -------------------------------------------------------------------------------------
/*
 * @brief OptimisticBFGuard 类的构造函数，通过页面ID锁定页面
 * 
 * @param pid 页面ID
 */
OptimisticBFGuard::OptimisticBFGuard(PID pid) {
   g = BM::global->fix(pid, Optimistic());   // 通过页面ID锁定页面，使用 Optimistic 锁
};

/*
 * @brief OptimisticBFGuard 类的移动构造函数
 * 
 * @param oGuard 要移动的 OptimisticBFGuard 实例
 */
OptimisticBFGuard::OptimisticBFGuard(OptimisticBFGuard&& oGuard) {
   g = std::move(oGuard.g);  // 使用移动语义移动 oGuard 的状态到当前实例
}

/*
 * @brief 移动赋值运算符，用于将一个 OptimisticBFGuard 实例的状态移动到另一个实例
 * 
 * @param oGuard 要移动的 OptimisticBFGuard 实例
 * @return OptimisticBFGuard& 移动后的实例的引用
 */
OptimisticBFGuard& OptimisticBFGuard::operator=(OptimisticBFGuard&& oGuard) {
   g = std::move(oGuard.g);  // 使用移动语义移动 oGuard 的状态到当前实例
   return *this;             // 返回移动后的实例的引用
}
// -------------------------------------------------------------------------------------
// downgrades
// no need to account for remote possession information
/* 
* @brief 从 ExclusiveBFGuard 实例移动构造 OptimisticBFGuard 实例
 * 
 * @param xGuard 要移动的 ExclusiveBFGuard 实例
 */
OptimisticBFGuard::OptimisticBFGuard(ExclusiveBFGuard&& xGuard) {
   ensure(xGuard.g.latchState == LATCH_STATE::EXCLUSIVE);  // 确保 ExclusiveBFGuard 实例的锁状态为互斥访问
   ensure(g.state == STATE::UNINITIALIZED);               // 确保当前 OptimisticBFGuard 实例的状态为未初始化
   ensure(xGuard.getFrame().latch.isLatched());           // 确保 ExclusiveBFGuard 实例的缓冲帧已经被锁定
   // -------------------------------------------------------------------------------------
   g = std::move(xGuard.g);  // 使用移动语义移动 xGuard 的状态到当前实例
   // -------------------------------------------------------------------------------------
   g.frame->latch.unlatchExclusive();  // 解除 ExclusiveBFGuard 实例的互斥锁
   // -------------------------------------------------------------------------------------
    // try lock shared   // 尝试以乐观锁的方式重新锁定
   g.state = STATE::RETRY;
   if (getFrame().latch.optimisticCheckOrRestart(g.vAcquired + 0b10)) {
      g.state = STATE::INITIALIZED;             // 如果成功，设置当前实例的状态为已初始化
      g.latchState = LATCH_STATE::OPTIMISTIC;   // 设置当前实例的锁状态为乐观锁
      g.vAcquired = g.vAcquired + 0b10;         // 更新版本号
   }
}
// -------------------------------------------------------------------------------------
/* 
* @brief 从 SharedBFGuard 实例移动构造 OptimisticBFGuard 实例
 * 
 * @param sGuard 要移动的 SharedBFGuard 实例
 */
OptimisticBFGuard::OptimisticBFGuard(SharedBFGuard&& sGuard) {
   ensure(sGuard.g.latchState == LATCH_STATE::SHARED);  // 确保 SharedBFGuard 实例的锁状态为共享访问
   ensure(g.state == STATE::UNINITIALIZED);             // 确保当前 OptimisticBFGuard 实例的状态为未初始化
   // -------------------------------------------------------------------------------------
   g = std::move(sGuard.g);  // 使用移动语义移动 sGuard 的状态到当前实例
   // -------------------------------------------------------------------------------------
   g.frame->latch.unlatchShared();  // 解除 SharedBFGuard 实例的共享锁
   // -------------------------------------------------------------------------------------
   // 尝试以乐观锁的方式重新锁定
   g.state = STATE::RETRY;
   if (getFrame().latch.optimisticCheckOrRestart(g.vAcquired)) {
      g.state = STATE::INITIALIZED;             // 如果成功，设置当前实例的状态为已初始化
      g.latchState = LATCH_STATE::OPTIMISTIC;   // 设置当前实例的锁状态为乐观锁
      g.vAcquired = g.vAcquired;                // 更新版本号
   }
}
// -------------------------------------------------------------------------------------
}  // storage
}  // scalestore

