#pragma once
// -------------------------------------------------------------------------------------
#include "scalestore/rdma/CommunicationManager.hpp"
#include "scalestore/storage/buffermanager/Buffermanager.hpp"
#include "scalestore/rdma/MessageHandler.hpp"
#include "scalestore/storage/buffermanager/AsyncWriteBuffer.hpp"
// -------------------------------------------------------------------------------------

namespace scalestore
{
namespace storage
{
using namespace rdma;

constexpr uint64_t batchSize = 32; // keeps rdma latency reasonable low ，保持 RDMA 的延迟较低
constexpr uint64_t pp_batch_size = 128; // most robust and fast due to cache ，由于缓存，最可靠且快速
constexpr uint64_t required_samples = 600; // reasonable high confidence in sampling ，取样具有合理的高置信度
constexpr uint64_t outgoingBachtes = 2;//输出批次的数量
constexpr uint64_t minOutgoingElements = 16;// 输出元素的最小数量
constexpr uint64_t maxOutstandingWrites = 32;// 最大未完成的写入数量
constexpr uint64_t WR_READ_ID = 99;// 读操作 WR 的 ID
constexpr uint64_t BITMAP_BATCH = 8 * 2; // two cacheline a 8 uint64_t， 两个缓存线每个有 8 个 uint64_t 元素

struct EvictionEntry {
   PID pid;//表示进程的标识符 (PID)；
   uintptr_t offset;//表示偏移量的无符号整数
   uint64_t pVersion;//表示版本号的无符号整数
};

struct EvictionRequest {
   uint64_t bmId;//表示位图的标识符
   uint64_t pId;//表示进程的标识符
   uint64_t failedSend = 0;//表示发送失败的次数，初始值为 0；
   uint64_t elements;//表示当前 EvictionRequest 结构体中的元素数量；
   EvictionEntry entries[batchSize];//一个大小为 batchSize 的 EvictionEntry 数组，用于存储 EvictionEntry 结构体。
   void reset() {函数用于重置两个变量为0
      elements = 0;
      failedSend = 0;
   }
   bool empty(){ return elements == 0;}//empty() 函数用于检查EvictionRequest 结构体是否为空
   bool full(){ return elements == batchSize;}//full() 函数用于检查 EvictionRequest 结构体是否已满
   void add(EvictionEntry&& entry){//add(EvictionEntry&& entry) 函数用于向 EvictionRequest 结构体添加一个 EvictionEntry，将其移动到 entries 数组中，并增加 elements 的值。
      ensure(elements < batchSize);
      entries[elements] = std::move(entry);
      elements++;
   }
};
struct EvictionResponse {
   PID pids[batchSize];//用于存储进程标识符 (PID)。

   void reset() { elements = 0;}//reset() 函数用于重置 EvictionResponse 结构体的成员变量，将 elements 设置为 0。
   bool empty(){ return elements == 0;}//empty() 函数用于检查 EvictionResponse 结构体是否为空，即 elements 是否为 0。
   bool full(){ return elements == batchSize;}//full() 函数用于检查 EvictionResponse 结构体是否已满，即 elements 是否等于 batchSize。
   void add(PID pid){
   //函数用于向 EvictionResponse 结构体的 pids 数组中添加一个进程标识符，将其存储在 pids[elements] 的位置，并增加 elements 的值。
      ensure(elements < batchSize);//是一个错误处理措施，用于确保在将 pid 添加到 pids 数组之前，elements 的值小于 batchSize
      pids[elements] = pid;//将 pid 存储到 pids 数组中的索引为 elements 的位置。
      elements++;
   }

   size_t elements;//表示当前 EvictionResponse 结构体中的元素数量。
   uint64_t bmId;//表示位图的标识符
   uint64_t pId;//表示进程的标识符
};

struct PageProvider {
   std::atomic<bool> finishedInit = false;//finishedInit，一个 std::atomic<bool> 类型的变量，用于表示初始化是否完成。
   std::atomic<bool> threadsRunning = true;//threadsRunning，一个 std::atomic<bool> 类型的变量，用于表示线程是否正在运行。
   std::atomic<size_t> threadCount = 0;//threadCount，一个 std::atomic<size_t> 类型的变量，用于表示线程数量。
   rdma::CM<InitMessage>& cm;//cm，一个对 rdma::CM<InitMessage> 类型的引用。
   storage::Buffermanager& bm;//bm，一个对 storage::Buffermanager 类型的引用。
   std::vector<MessageHandler::MailboxPartition>& mhPartitions;//mhPartitions，一个对 std::vector<MessageHandler::MailboxPartition> 类型的引用。
   const uint64_t freeBFLimit;//freeBFLimit，一个 const uint64_t 类型的常量，表示空闲页帧的限制。
   const uint64_t coolingBFLimit;//coolingBFLimit，一个 const uint64_t 类型的常量，表示冷却的页帧的限制。
   s32 ssd_fd;//ssd_fd，一个 s32 类型的变量，表示 SSD 的文件描述符。
   std::vector<std::thread> pp_threads;//pp_threads，一个 std::vector<std::thread> 类型的容器，用于存储线程对象。
   void startThread();//用于启动线程。
   void stopThread();//用于停止线程。
   void init();//用于初始化
   PageProvider(CM<rdma::InitMessage>& cm,
                storage::Buffermanager& bm,
                std::vector<MessageHandler::MailboxPartition>& mhPartitions,
                s32 ssd_fd);//一个构造函数，用于初始化 PageProvider 对象。
   ~PageProvider();//析构函数，用于清理资源。


   template<typename T>
   struct MsgSlots{
      T* current;//一个指向类型 T 的指针，用于表示当前的消息槽。
      T* prev;//一个指向类型 T 的指针，用于表示先前的消息槽。

      void swap(){
         std::swap(current, prev);//swap() 成员函数，用于交换 current 和 prev 指针的值。
      }
   };

   
   struct alignas(CACHE_LINE) ConnectionContext {
      // requests
      uintptr_t mbOffset;//一个 uintptr_t 类型的变量，表示请求的消息槽偏移
      uintptr_t plOffset;//一个 uintptr_t 类型的变量，表示请求的页面偏移。
      // resonses
      bool responseExpected = false;//一个 bool 类型的变量，表示是否期望响应。
      bool readScheduled = false;//一个 bool 类型的变量，表示读操作是否已计划。
      bool responseOutstanding = false;//一个 bool 类型的变量，表示是否有待完成的响应。
      bool readCompleted = false; // for read //一个 bool 类型的变量，表示读操作是否已完成。
      bool pollCompleted = false; // for writes//一个 bool 类型的变量，表示写操作是否已完成。
      uintptr_t respMbOffset;//一个 uintptr_t 类型的变量，表示响应的消息槽偏移。
      uintptr_t respPlOffset;//一个 uintptr_t 类型的变量，表示响应的页面偏移。
      MsgSlots<EvictionResponse> outgoingResp;//一个将类型 EvictionResponse 传递给 MsgSlots 模板的变量，用于存储传出的响应。
      MsgSlots<EvictionRequest> outgoing;  // from which request will be sent//一个将类型 EvictionRequest 传递给 MsgSlots 模板的变量，用于存储传出的请求。
      std::vector<BufferFrame*> latchedFrames;  // local latched frames to answer requests//一个 std::vector<BufferFrame*> 类型的变量，用于存储本地缓存的帧，以回答请求。
      MsgSlots<std::vector<BufferFrame*>> inflightFrames;  // local latched frames to answer requests need also two?//一个将类型 std::vector<BufferFrame*> 传递给 MsgSlots 模板的变量，用于存储本地缓存的帧，以回答请求（是否需要两个？）。
      rdma::RdmaContext* rctx;//一个指向 rdma::RdmaContext 类型的指针，表示 RDMA 上下文。
      uint64_t wqe;  // wqe currently outstanding will be resetted with reads completions//一个 uint64_t 类型的变量，表示当前未完成的工作队列条目。
      NodeID bmId;   // id to which incoming client connection belongs//一个 NodeID 类型的变量，表示传入的客户端连接所属的 ID。
      uint64_t requestLatency {0};//一个 uint64_t 类型的变量，表示请求延迟。
      uint64_t responseLatency {0};//一个 uint64_t 类型的变量，表示响应延迟。

      struct RdmaReadBuffer {
         struct ibv_send_wr sq_wr[batchSize];//一个包含 batchSize 个 ibv_send_wr 类型元素的数组，用于存储将被投递到 Send Queue (SQ) 的 Work Request (WR)。
         struct ibv_sge send_sgl[batchSize];//一个包含 batchSize 个 ibv_sge 类型元素的数组，用于存储将被传输的 Scatter-Gather Element (SGE) 的地址、长度和 L-Key。
         struct ibv_send_wr* bad_wr;//一个指向 ibv_send_wr 类型的指针，表示最后一个 SQ WR 投递失败的位置。
         size_t elements = 0;//一个 size_t 类型的变量，表示当前已添加的 SQ WR 的数量。

         void add(ConnectionContext& ctx, void* memAddr, uint64_t remoteOffset)
         {//一个将类型为 ConnectionContext、void* 和 uint64_t 的参数传递给它的方法，用于将要被投递到 SQ 的 RDMA READ 操作添加到缓冲区中。
            RdmaContext& rCtx = *(ctx.rctx);//从 ConnectionContext 参数中读取 RDMA 上下文的引用
            send_sgl[elements].addr = (uint64_t)(unsigned long)memAddr;
            send_sgl[elements].length = sizeof(Page);
            send_sgl[elements].lkey = rCtx.mr->lkey;//根据帧的地址和 RDMA 远程偏移地址设置 send_sgl 成员变量。
            sq_wr[elements].opcode = IBV_WR_RDMA_READ;
            sq_wr[elements].send_flags = 0;  // unsignaled>
            sq_wr[elements].sg_list = &send_sgl[elements];
            sq_wr[elements].num_sge = 1;
            sq_wr[elements].wr.rdma.rkey = rCtx.rkey;
            sq_wr[elements].wr.rdma.remote_addr = remoteOffset;
            sq_wr[elements].wr_id = WR_READ_ID;
            sq_wr[elements].next = nullptr;//将 ibv_send_wr 的各种字段设置为正确的值，并将其添加到缓冲区中。
            // modify previous element
            if (elements > 0) {
               sq_wr[elements - 1].next = &sq_wr[elements];
            }//如果缓冲区中已经有多个 WR，则更新前一个 WR 的 next 指针以指向当前 WR。
            elements++;
         }
         void postRead(ConnectionContext& ctx)//函数用于将已添加到缓冲区的 RDMA READ 操作投递到 Send Queue (SQ)。
         {//函数有一个参数 ConnectionContext& ctx，它表示连接的上下文。
            RdmaContext& rCtx = *(ctx.rctx);//ConnectionContext 参数中获取 RDMA 上下文的引用，并将其存储在一个名为 rCtx 的变量中。
            // set last eleement signaled
            sq_wr[elements -1].send_flags = IBV_SEND_SIGNALED; 
             //函数将缓冲区中最后一个 WR 的 send_flags 字段设置为 IBV_SEND_SIGNALED，表示该 WR 在完成后将发送一个完成通知。
            ensure(elements > 0);
            auto ret = ibv_post_send(rCtx.id->qp, &sq_wr[0], &bad_wr);
            //函数使用 ibv_post_send 函数将缓冲区中第一个 WR 投递到与 rCtx.id->qp 关联的队列。&sq_wr[0] 表示第一个 WR 的地址，&bad_wr 表示指向最后一个 WR 投递失败位置的指针。
            if (ret)
               throw std::runtime_error("Failed to post send request" + std::to_string(ret) + " " +
                                        std::to_string(errno));
         };//ibv_post_send 函数返回非零值，表示投递失败，函数将抛出一个 std::runtime_error 异常，其中包含错误码和错误号的字符串表示。
         bool pollCompletion(ConnectionContext& ctx)
         {//目的是检查RDMA操作完成队列的状态并处理完成的工作请求。
            RdmaContext& rCtx = *(ctx.rctx);

            if (ctx.readCompleted) {
               elements = 0;
               return true;
            }

            for (uint64_t i = 0; i < 2; i++) {
               int comp{0};//用于记录RDMA操作完成队列中的完成工作请求数量
               ibv_wc wcReturn;//用于存储调用 rdma::pollCompletion 后返回的工作完成信息。
               comp = rdma::pollCompletion(rCtx.id->qp->send_cq, 1, &wcReturn);
               //调用 rdma::pollCompletion 函数，传入RDMA上下文和完成队列，检查完成队列中是否有已完成的工作请求。
               //wcReturn 用于存储返回的工作完成信息。
               if (comp > 0 && wcReturn.wr_id == WR_READ_ID) {
                  elements = 0;
                  ctx.wqe = 0;
                  return true;
               }//如果 comp > 0 并且 wcReturn.wr_id 等于 WR_READ_ID ，则表示有完成的读操作请求。在这种情况下，将 elements 设置为0，将 ctx.wqe 设置为0，并返回 true。
               if(comp > 0){
                  // mark pollcompletion
                  ctx.pollCompleted = true;
               }//标记pollCompletion，表示已经执行了RDMA操作完成队列的轮询（pollCompletion）。
            }//如果 comp > 0 ，则表示完成队列中有其他类型的工作请求完成。在这种情况下，将 ctx.pollCompleted 标记为 true。

            return false;
            // reset
         }
      };

      RdmaReadBuffer rdmaReadBuffer;//使用 rdmaReadBuffer 对象进行RDMA读取操作
   };

   
   // -------------------------------------------------------------------------------------
   // Mailbox partition per thread
   struct Partition {
      Partition(uint8_t* mailboxes,//指向 uint8_t 类型的邮箱数组的指针。
                EvictionRequest* incoming,  // incoming requests，指向 EvictionRequest 类型的入站请求的指针。
                uint8_t* respMailboxes,//指向 uint8_t 类型的响应邮箱数组的指针
                EvictionResponse* respIncoming,  // incoming requests，指向 EvictionResponse 类型的响应的入站请求的指针。
                uint64_t numberMailboxes,//表示邮件箱的数量的 uint64_t 类型的变量。
                uint64_t begin,//表示某个范围的起始值的 uint64_t 类型的变量。
                uint64_t end)//表示某个范围的结束值的 uint64_t 类型的变量。
          : mailboxes(mailboxes),
            incoming(incoming),
            respMailboxes(respMailboxes),
            respIncoming(respIncoming),
            numberMailboxes(numberMailboxes),
            cctxs(FLAGS_nodes),
            begin(begin),
            end(end)
      {//构造函数
      }

      // -------------------------------------------------------------------------------------
      // requests
      uint8_t* mailboxes = nullptr;//指向 int8_t 类型的邮箱数组的指针，初始值为 nullpt
      EvictionRequest* incoming = nullptr;//指向 EvictionRequest 类型的入站请求的指针，初始值为 nullptr。
      // -------------------------------------------------------------------------------------
      // responses
      uint8_t* respMailboxes = nullptr;//指向 uint8_t 类型的响应邮箱数组的指针，初始值为 nullptr。
      EvictionResponse* respIncoming = nullptr;//指向 EvictionResponse 类型的响应的入站请求的指针，初始值为 nullptr。
      // -------------------------------------------------------------------------------------
      uint64_t numberMailboxes;  // remote nodes，用于表示远程节点数量的 uint64_t 类型的变量。
      std::vector<ConnectionContext> cctxs;//是一个 ConnectionContext 类型的 std::vector 容器，表示某个上下文的多个连接。
      uint64_t begin;  // pTabel partitions，表示 pTabel 分区起始值的 uint64_t 类型的变量。
      uint64_t end;//表示 pTabel 分区结束值的 uint64_t 类型的变量。
   };
   // -------------------------------------------------------------------------------------


   // -------------------------------------------------------------------------------------

   std::vector<Partition> partitions;
//该容器类型是一个存储 Partition 结构体对象的动态数组（即 vector），可以动态地增加或减少容器中元素的数量
   /// TODO: remove debug counter
   uint64_t requests {0};//表示 requests 是一个 uint64_t 类型的变量，初值被赋为 0。
   uint64_t responses_sent {0};//表示 responses_sent 是一个 uint64_t 类型的变量，初值被赋为 0。

   
   template <typename MSG>//表示该函数是一个模板函数，需要使用 MSG 类型的参数。
   void writeRequest(uint64_t partitionId, NodeID nodeId, MSG& msg)//代码实现了向一个特定的分区和节点发送一个请求。
   {//函数参数：partitionId 表示分区ID，nodeId 表示节点ID， msg 表示要发送的消息。
      ConnectionContext& ctx = partitions[partitionId].cctxs[nodeId];
      //先通过 partitionId 找到对应的分区，然后通过 nodeId 找到该节点所属的 ConnectionContext 对象。
      auto& wqe = ctx.wqe;
      //表示当前节点已经发送的请求个数，初始值为0。
      rdma::completion signal = (wqe == maxOutstandingWrites) ? rdma::completion::signaled : rdma::completion::unsignaled;
     //根据当前节点已发送的请求数 wqe 和最大限制请求数 maxOutstandingWrites 来决定此次请求发送是否需要进行信号处理。
      uint8_t flag = 1;//表示一个标记位，它是一个 uint8_t 类型的值，值为1。
      rdma::postWriteBatch(*(ctx.rctx), signal,
                           RDMABatchElement{.memAddr = &msg, .size = (sizeof(MSG)), .remoteOffset = ctx.plOffset},
                           RDMABatchElement{.memAddr = &flag, .size = (sizeof(uint8_t)), .remoteOffset = ctx.mbOffset});
      //使用 RDMA 方式发送该请求，其中包含两个批量写命令，第一个命令是将 msg 发送至远程内存地址 ctx.plOffset，第二个命令是将 flag 的值发送至 ctx.mbOffset，其中 .*memAddr 表示这是一个结构体成员，使用 . 调用成员指针，.*size 和 .*remoteOffset 同理。
      
      if ((wqe == maxOutstandingWrites && !ctx.pollCompleted)) {
//判断是否达到最大请求数 maxOutstandingWrites 且节点所有请求都已经被处理（即已经轮询完completion queue），如果是则继续下面的操作。
         int comp{0};//用于存储返回的完成事件个数。
         ibv_wc wcReturn;//该结构用于存储返回的完成事件具体内容。
         while (comp == 0) {//如果当前完成事件队列并无新的事件完成，那么就不断轮询检查，comp 表示检测到的完成事件个数，如果为0则继续检查。
            comp = rdma::pollCompletipushon(ctx.rctx->id->qp->send_cq, 1, &wcReturn);//使用 RDMA 方式轮询检查完成事件队列是否有新的完成事件。最后返回一个事件个数，如果返回 0，说明当前无新的完成事件。
            if(wcReturn.wr_id == WR_READ_ID){
               comp = 0;
               ctx.readCompleted = true;
            }
              //如果检测到的事件的 wr_id 等于一个预定义的 WR_READ_ID 常量值，则说明数据已经被读取完成（即 RDMA 读操作已经完成），将 readCompleted 标记设置为 true。
         }
         ensure(wcReturn.wr_id != WR_READ_ID);
         wqe = 0;
         ctx.pollCompleted = false;
      }//如果检测到的事件的 wr_id 不等于预定义的 WR_READ_ID 常量值，则调用一个函数 ensure()，确保返回值不是预期的错误码 WR_READ_ID，如果是，则会抛出异常。
      wqe++;//表示当前节点已经发送的请求数 wqe 增加1。
   }

   
//   template <typename MSG>//模板函数 ，接受一个类型为 MSG 的参数 msg，用于将响应数据写入到远程节点。
   void writeResponse(uint64_t partitionId, NodeID nodeId, MSG& msg)
   {
      ConnectionContext& ctx = partitions[partitionId].cctxs[nodeId];//获取指定分区和节点的连接上下文 ctx。
      auto& wqe = ctx.wqe;
      rdma::completion signal = (wqe == maxOutstandingWrites) ? rdma::completion::signaled : rdma::completion::unsignaled;
      //根据当前已经发送的写请求数 wqe 和最大并发写请求数 maxOutstandingWrites，决定本次写操作是否需要信号量。
      uint8_t flag = 1;//赋值为 1，用于标记写操作的完成。
      rdma::postWriteBatch(*(ctx.rctx), signal,
                           RDMABatchElement{.memAddr = &msg, .size = (sizeof(MSG)), .remoteOffset = ctx.respPlOffset},
                           RDMABatchElement{.memAddr = &flag, .size = (sizeof(uint8_t)), .remoteOffset = ctx.respMbOffset});
 //调用 rdma::postWriteBatch 方法来发送批量写请求，包括写入数据 msg 和写入标志 flag，同时指定内存地址、大小和远程偏移量。
      if ((wqe == maxOutstandingWrites && !ctx.pollCompleted)) {
      //如果已经达到最大并发写请求数，并且连接上下文的 pollCompleted 标志为假，则进入一个循环，通过调用 rdma::pollCompletion 方法来等待写操作的完成。
         int comp{0};
         ibv_wc wcReturn;
         while (comp == 0) {
            comp = rdma::pollCompletion(ctx.rctx->id->qp->send_cq, 1, &wcReturn);
            if(wcReturn.wr_id == WR_READ_ID){
            //如果写请求完成且完成的操作 ID 不是读请求的 ID (WR_READ_ID)，则将连接上下文的 readCompleted 标志设置为真，表示读请求已经完成。
               comp = 0;
               ctx.readCompleted = true;
            }
         }
         ensure(wcReturn.wr_id != WR_READ_ID);
         //reset
         wqe = 0;
         ctx.pollCompleted = false;
      }//重置当前发送的写请求数 wqe 为 0，将连接上下文的 pollCompleted 标志设置为假。
      wqe++;
   }

};
}  // namespace storage
}  // namespace scalestore

