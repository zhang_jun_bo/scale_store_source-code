template <CONTENTION_METHOD method, typename ACCESS>
// 在缓冲区管理器中查找或插入缓冲区帧的函数
// 参数：
//   - pid: 要查找或插入的帧的页面ID
//   - functor: 访问操作的函数对象，可能包括读取或写入等操作
//   - nodeId_: 要查找或插入的节点ID
// 返回值：
//   - Guard：代表对帧进行保护的对象，可能是独占或共享的保护


/*
findFrameOrInsert
查找当前访问页面，缓存哈希桶找不到，从ssd/rdma找到，弹出空闲缓冲帧，将页面插入到缓存哈希桶


*/
Guard Buffermanager::findFrameOrInsert(PID pid, ACCESS functor, NodeID nodeId_)  // move functor
{
   Guard g;    // 创建一个 Guard 对象，用于管理缓冲区页面的访问
   auto& b = pTable[pid];     // 获取缓冲区页面对应的 Bucket 对象

   auto& ht_latch = b.ht_bucket_latch;    //获取哈希桶的锁
   // -------------------------------------------------------------------------------------
restart:
   auto b_version = ht_latch.optimisticLatchOrRestart();    //获取一个乐观锁
   if (!b_version.has_value()) goto restart;
   // -------------------------------------------------------------------------------------
   // handle fast and slow path in one loop
   BufferFrame* b_ptr = &b;   //创建了指向b对象的指针b_ptr。这样b_ptr就指向了BufferFrame对象b的内存地址。
   BufferFrame** current_slot = &b_ptr;   //创建了指向指针 b_ptr 的指针 current_slot
   // -------------------------------------------------------------------------------------
   while (*current_slot) {
      BufferFrame* tmp = *current_slot;    //复制指针值，下一个字段的值会改变 // copy pointer value next field in BF can change;
      RESTART(!ht_latch.optimisticUnlatch(b_version.value()), restart); //检查并验证乐观锁是否成功解锁 // validate against that
      if (tmp->pid == pid) {  // 找到指定页面ID的帧
         g.frame = tmp;
         RESTART(!ht_latch.optimisticUnlatch(b_version.value()), restart);
         functor(g, nodeId_);    //functor(g, nodeId_);：使用传入的访问函数对帧进行操作。
         // -------------------------------------------------------------------------------------
         // non-blocking does not restart
         if constexpr (method == CONTENTION_METHOD::NON_BLOCKING) {
            if (!ht_latch.checkOrRestart(b_version.value())) {    //检查乐观锁是否成功，如果失败则撤销操作并返回
               functor.undo(g);
            }
            return g;
         }
         // -------------------------------------------------------------------------------------
         // blocking goes to restart
         if (g.state == STATE::RETRY) goto restart;     //如果 Guard 的状态为RETRY,则重新开始
         if (!ht_latch.checkOrRestart(b_version.value())) {  // Lock-coupling
            functor.undo(g);
            goto restart;
         }
         return g;
      }
      current_slot = &(tmp->next);                                       // take address of next field
      RESTART(!ht_latch.optimisticUnlatch(b_version.value()), restart);  // validate against a nullptr change
   }
   // -------------------------------------------------------------------------------------
   // Insert
   // -------------------------------------------------------------------------------------
   RESTART(!ht_latch.optimisticUnlatch(b_version.value()), restart);         // validate against a nullptr change
   Page* page = pageFreeList.pop(threads::ThreadContext::my().page_handle);  //从空闲页面列表中弹出一个页面// we can block because no locks are acquired
   if (!ht_latch.upgradeToExclusiveLatch(b_version.value())) { //尝试升级乐观锁为独占锁。
      pageFreeList.push(page, threads::ThreadContext::my().page_handle);
      goto restart;     //升级失败，说明在获取页面后锁已被其他线程获取，将之前弹出的页面重新推回到页面列表中，然后重新开始
   }
   // -------------------------------------------------------------------------------------
   g.frame = &b;  //将 Guard 对象的 frame 成员设置为当前 Bucket 对象的地址。
   if (b.state != BF_STATE::FREE) {  // inline bucket not empty
      if (!frameFreeList.try_pop(*current_slot, threads::ThreadContext::my().bf_handle)) {
         ht_latch.unlatchExclusive();  //无空闲缓冲帧，解除独占锁
         pageFreeList.push(page, threads::ThreadContext::my().page_handle);   //弹出的页面重新推回到页面列表中
         goto restart;     //重新开始
      }
      g.frame = *current_slot;
   }
   // -------------------------------------------------------------------------------------
   g.frame->latch.latchExclusive(); // POSSIBLE ERROR?
   ensure(g.frame->pid == EMPTY_PID);  //确保新帧的页面ID为 EMPTY_PID。
   g.frame->state =     //根据进程ID的归属关系，设置新帧的状态为 SSD 或 RDMA。
       (pid.getOwner() == nodeId) ? BF_STATE::IO_SSD : BF_STATE::IO_RDMA;  // important to modify state before releasing the hashtable latch
   g.frame->page = page;
   g.frame->pid = pid;
   g.frame->epoch = globalEpoch.load();   //缓冲帧的全局纪元(驱逐页面时会用到)
   g.frame->setPossession(POSSESSION::NOBODY);  //设置新帧的所有权为 NOBODY。
   // -------------------------------------------------------------------------------------
   ht_latch.unlatchExclusive();    //释放哈希桶独占锁
   // -------------------------------------------------------------------------------------
   g.state = (pid.getOwner() == nodeId) ? STATE::SSD : STATE::REMOTE;
   g.vAcquired = g.frame->latch.version;  //设置锁存的版本号  这个版本号可能用于跟踪缓冲区帧的锁版本。
   g.latchState = LATCH_STATE::EXCLUSIVE;    //设置g为独占状态，Guard 对象 g 包含了一个对缓冲区帧的独占锁。
   return g;
}

template <CONTENTION_METHOD method, typename ACCESS>
Guard Buffermanager::findFrame(PID pid, ACCESS functor, NodeID nodeId_)  // move functor
{
   Guard g;
   auto& b = pTable[pid];
   auto& ht_latch = b.ht_bucket_latch;
   // -------------------------------------------------------------------------------------
restart:
   auto b_version = ht_latch.optimisticLatchOrRestart();
   if (!b_version.has_value()) goto restart;
   // -------------------------------------------------------------------------------------
   // handle fast and slow path in one loop
   BufferFrame* b_ptr = &b;
   BufferFrame** current_slot = &b_ptr;
   // -------------------------------------------------------------------------------------
   while (*current_slot) {
      BufferFrame* tmp = *current_slot;                                  // copy pointer value next field in BF can change;
      RESTART(!ht_latch.optimisticUnlatch(b_version.value()), restart);  // validate against that
      if (tmp->pid == pid) {
         g.frame = tmp;
         RESTART(!ht_latch.optimisticUnlatch(b_version.value()), restart);
         functor(g, nodeId_);
         // -------------------------------------------------------------------------------------
         // non-blocking does not restart
         if constexpr (method == CONTENTION_METHOD::NON_BLOCKING) {
            if (!ht_latch.checkOrRestart(b_version.value())) { functor.undo(g); }
            return g;
         }
         // -------------------------------------------------------------------------------------
         // blocking restarts
         if (g.state == STATE::RETRY) goto restart;
         if (!ht_latch.checkOrRestart(b_version.value())) {  // Lock-coupling
            functor.undo(g);
            goto restart;
         }
         return g;
      }
      current_slot = &(tmp->next);                                       // take address of next field
      RESTART(!ht_latch.optimisticUnlatch(b_version.value()), restart);  // validate against a nullptr change
   }
   // -------------------------------------------------------------------------------------
   // PID not found
   RESTART(!ht_latch.optimisticUnlatch(b_version.value()), restart);  // validate against an concurrent insert
   g.state = STATE::NOT_FOUND;
   return g;
}
// -------------------------------------------------------------------------------------
template <typename ACCESS>
Guard Buffermanager::fix(PID pid, ACCESS functor) {
   using namespace rdma;

   // -------------------------------------------------------------------------------------
restart:
   //获取缓冲区帧，使用阻塞方式
   Guard guard = findFrameOrInsert<CONTENTION_METHOD::BLOCKING>(pid, functor, nodeId);
   ensure(guard.state != STATE::UNINITIALIZED);    //确保获取的 Guard 对象的状态不是 UNINITIALIZED
   ensure(guard.state != STATE::RETRY);
   // -------------------------------------------------------------------------------------
   // hot path
   // -------------------------------------------------------------------------------------
   if (guard.state == STATE::INITIALIZED) {
      // 数据预取，优化缓冲区帧的访问
      _mm_prefetch(&guard.frame->page->data[0], _MM_HINT_T0);
      // 如果缓冲区帧的纪元小于全局纪元，则更新纪元
      if (guard.frame->epoch < globalEpoch) guard.frame->epoch = globalEpoch.load();
      return guard;
   }
   // -------------------------------------------------------------------------------------
   // helper lambdas
   // -------------------------------------------------------------------------------------
   auto invalidateSharedConflicts = [&](StaticBitmap<64>& shared, uint64_t pVersion) {
      // Invalidate all conflicts
      shared.applyForAll([&](uint64_t id) {
         auto& context_ = threads::Worker::my().cctxs[id];
         auto pmRequest = *MessageFabric::createMessage<PossessionMoveRequest>(context_.outgoing, pid, false, 0,
                                                                               pVersion);  // move possesion no page page
         // 异步发送移动所有权的请求消息
         threads::Worker::my().writeMsgASync<PossessionMoveResponse>(id, pmRequest);
      });

      shared.applyForAll([&](uint64_t id) {
         // 收集异步发送的 PossessionMoveResponse 消息
         auto& pmResponse = threads::Worker::my().collectResponseMsgASync<PossessionMoveResponse>(id);
         // -------------------------------------------------------------------------------------
         ensure(pmResponse.resultType == RESULT::NoPage);   // 确保响应类型为 NoPage？？为什么
         shared.reset(id);    // 重置冲突标记
      });
   };
   // -------------------------------------------------------------------------------------
   auto movePageRnd = [&](StaticBitmap<64>& shared, uintptr_t pageOffset, uint64_t pVersion) {
      // 对冲突标记中的一个应用 Lambda 函数
      shared.applyToOneRnd([&](uint64_t id) {
         
         auto& context_ = threads::Worker::my().cctxs[id];
         auto pmRequest = *MessageFabric::createMessage<PossessionMoveRequest>(context_.outgoing, pid, true, pageOffset,
                                                                               pVersion);  // move possesion in page
         //同步发送移动所有权的回应
         auto& pmResponse = threads::Worker::my().writeMsgSync<PossessionMoveResponse>(id, pmRequest);
         // -------------------------------------------------------------------------------------
         // 确保响应类型为 WithPage
         ensure(pmResponse.resultType == RESULT::WithPage);
         shared.reset(id);
      });
   };
   // -------------------------------------------------------------------------------------
   auto copyPageRnd = [&](StaticBitmap<64>& shared, uintptr_t pageOffset, uint64_t pVersion, RESULT& result, uint64_t& randomId) {
      shared.applyToOneRnd([&](uint64_t id) {
         randomId = id;    // 保存随机选择的 ID
         auto& context_ = threads::Worker::my().cctxs[id];     
         //用于在页面中移动所有权，包括页面内容
         auto pcRequest = *MessageFabric::createMessage<PossessionCopyRequest>(context_.outgoing, pid, pageOffset,
                                                                               pVersion);  // move possesion incl page
         auto& pcResponse = threads::Worker::my().writeMsgSync<PossessionCopyResponse>(id, pcRequest);
         // -------------------------------------------------------------------------------------
         result = pcResponse.resultType;  //获取响应类型
      });
   };
   // cold path
   // -------------------------------------------------------------------------------------
   // SSD
   // -------------------------------------------------------------------------------------
   volatile int mask = 1;  // for backoff
   switch (guard.state) {
      case STATE::SSD: {
         ensure(guard.frame != nullptr);
         ensure(guard.frame->latch.isLatched());
         // 同步读取页面
         readPageSync(guard.frame->pid, reinterpret_cast<uint8_t*>(guard.frame->page));
         // -------------------------------------------------------------------------------------
         // update state
         guard.frame->possession = (functor.type == LATCH_STATE::EXCLUSIVE) ? POSSESSION::EXCLUSIVE : POSSESSION::SHARED;
         guard.frame->setPossessor(nodeId);  // 设置所有者为当前节点
         guard.frame->state = BF_STATE::HOT; //设置状态为 HOT，这很重要，因为它允许在没有锁的情况下进行远程复制 // important as it allows to remote copy without latch
         ensure(guard.frame->pid != EMPTY_PID); //确保缓冲区帧的页面ID不为空
         // -------------------------------------------------------------------------------------
         // downgrade latch
         if (guard.needDowngrade(functor.type)) {
            guard.downgrade(functor.type);   // 执行锁的降级
            if (guard.state == STATE::RETRY) { goto restart; }
         }
         // -------------------------------------------------------------------------------------
         guard.state = STATE::INITIALIZED;
         // -------------------------------------------------------------------------------------
         break;
      }
      // -------------------------------------------------------------------------------------
      // Remote Fix - no page and need to request it from remote
      // -------------------------------------------------------------------------------------
      case STATE::REMOTE: {
         // ------------------------------------------------------------------------------------->
         ensure(guard.frame); // 确保 Guard 对象的缓冲区帧指针不为空
         ensure(guard.frame->state == BF_STATE::IO_RDMA);   // 确保缓冲区帧的状态为 IO_RDMA
         ensure(FLAGS_nodes > 1);   // 确保节点数大于1
         ensure(guard.frame->latch.isLatched());   // 确保缓冲区帧的锁已经被获取
         ensure(guard.latchState == LATCH_STATE::EXCLUSIVE);   //确保 Guard 对象的锁状态为 EXCLUSIVE
         // -------------------------------------------------------------------------------------
         // -------------------------------------------------------------------------------------
         guard.frame->state = BF_STATE::IO_RDMA;   // 更新缓冲区帧的状态为 IO_RDMA
         // -------------------------------------------------------------------------------------
         uintptr_t pageOffset = (uintptr_t)guard.frame->page;  // 计算页面的偏移地址
         // -------------------------------------------------------------------------------------
         auto& contextT = threads::Worker::my().cctxs[pid.getOwner()];  // 获取当前节点的上下文
         auto& request = *MessageFabric::createMessage<PossessionRequest>(
            // 创建 PossessionRequest 消息，根据锁类型选择 PRX 或 PRS 消息类型
             contextT.outgoing, ((functor.type == LATCH_STATE::EXCLUSIVE) ? MESSAGE_TYPE::PRX : MESSAGE_TYPE::PRS), pid, pageOffset);
         // 异步写消息
         threads::Worker::my().writeMsgASync<PossessionResponse>(pid.getOwner(), request);
         // -------------------------------------------------------------------------------------
         // 预取页面的第一个缓存行
         _mm_prefetch(&guard.frame->page->data[0], _MM_HINT_T0);  // prefetch first cache line of page
         // -------------------------------------------------------------------------------------
         // 收集异步发送的 PossessionResponse 消息
         auto& response = threads::Worker::my().collectResponseMsgASync<PossessionResponse>(pid.getOwner());
         // -------------------------------------------------------------------------------------
         // set version from owner
         guard.frame->pVersion = response.pVersion;
         // -------------------------------------------------------------------------------------
         // 处理不同的响应类型
         if (response.resultType == RESULT::NoPageExclusiveConflict) {
            // -------------------------------------------------------------------------------------
            // Resolve Exclusive Conflict
            // -------------------------------------------------------------------------------------
            auto& context_ = threads::Worker::my().cctxs[response.conflictingNodeId];
            auto& pmRequest = *MessageFabric::createMessage<PossessionMoveRequest>(context_.outgoing, pid, true, pageOffset,
                                                                                   guard.frame->pVersion);  // move possesion incl page
            auto& pmResponse = threads::Worker::my().writeMsgSync<PossessionMoveResponse>(response.conflictingNodeId, pmRequest);
            // -------------------------------------------------------------------------------------
            ensure(pmResponse.resultType == RESULT::WithPage);
            // -------------------------------------------------------------------------------------
         } else if ((response.resultType == RESULT::NoPageSharedConflict) | (response.resultType == RESULT::WithPageSharedConflict)) {
            // -------------------------------------------------------------------------------------
            // Resolve Shared Conflicts
            // -------------------------------------------------------------------------------------
            StaticBitmap<64> shared(response.conflictingNodeId);  
            ensure(shared.any());   // 确保至少有一个共享冲突标记
            // -------------------------------------------------------------------------------------
            // Get page from random node
            if (response.resultType == RESULT::NoPageSharedConflict) { movePageRnd(shared, pageOffset, guard.frame->pVersion); }
            invalidateSharedConflicts(shared, guard.frame->pVersion);   // 使所有共享冲突无效
            // -------------------------------------------------------------------------------------
         } else if (response.resultType == RESULT::NoPageEvicted) {
            // -------------------------------------------------------------------------------------
            // Copy up-to-date page from remote
            // -------------------------------------------------------------------------------------
         restartNoPageEvicted:
            StaticBitmap<64> shared(response.conflictingNodeId);
            ensure(shared.any());
            RESULT result;  // remote copy can fail
            uint64_t randomId = 0;
            // -------------------------------------------------------------------------------------
            // 从随机节点复制页面
            copyPageRnd(shared, pageOffset, guard.frame->pVersion, result, randomId);
            // -------------------------------------------------------------------------------------
            if (result != RESULT::WithPage) {
               // new page hence we should reclaim it?
               // 如果缓冲区帧等待 MH，则解锁并退避
               if (guard.frame->mhWaiting == true) {
                  ensure(guard.frame->latch.isLatched());
                  guard.frame->latch.unlatchExclusive();
                  BACKOFF();
                  goto restart;
               }
               goto restartNoPageEvicted;
            }
         }
         // -------------------------------------------------------------------------------------
         // ensure(guard.frame->page->magicDebuggingNumber == pid);
         // -------------------------------------------------------------------------------------
         // update state
         guard.frame->possession = (functor.type == LATCH_STATE::EXCLUSIVE) ? POSSESSION::EXCLUSIVE : POSSESSION::SHARED;
         guard.frame->setPossessor(nodeId);
         guard.frame->state = BF_STATE::HOT;  // important as it allows to remote copy without latch
         // -------------------------------------------------------------------------------------
         // downgrade latch
         if (guard.needDowngrade(functor.type)) {
            guard.downgrade(functor.type);
            if (guard.state == STATE::RETRY) {
               goto restart;
            }
         }
         // -------------------------------------------------------------------------------------
         guard.state = STATE::INITIALIZED;   // 设置 Guard 对象的状态为 INITIALIZED
         // 增加计数器，表示接收到 RDMA 页面
         threads::Worker::my().counters.incr(profiling::WorkerCounters::rdma_pages_rx);
         // -------------------------------------------------------------------------------------
         break;
      }
      // -------------------------------------------------------------------------------------
      // Upgrade we are owner and need to change possession or page evicted
      // 升级，我们是所有者并且需要更改所有权或者页面被驱逐
      // ------------------------------------------------------------------------------------
      case STATE::LOCAL_POSSESSION_CHANGE: {
         ensure(pid.getOwner() == nodeId);
         ensure(guard.frame->latch.isLatched());   // 确保缓冲区帧的锁已经被获取
         ensure(guard.frame->possession != POSSESSION::NOBODY);    // 确保缓冲区帧的所有权不为 NOBODY
         // -------------------------------------------------------------------------------------
         // 如果帧状态为被驱逐，从空闲页面列表里弹出一个页面
         if (guard.frame->state == BF_STATE::EVICTED) { guard.frame->page = pageFreeList.pop(threads::ThreadContext::my().page_handle); }
         uintptr_t pageOffset = (uintptr_t)guard.frame->page;    // 计算页面的偏移地址
         // -------------------------------------------------------------------------------------
         if (guard.frame->possession == POSSESSION::EXCLUSIVE) {
            // -------------------------------------------------------------------------------------
            // Resolve Exclusive Conflict
            // -------------------------------------------------------------------------------------
            guard.frame->pVersion++;
            NodeID conflict = guard.frame->possessors.exclusive;
            auto& context_ = threads::Worker::my().cctxs[conflict];
            auto& pmRequest = *MessageFabric::createMessage<PossessionMoveRequest>(context_.outgoing, pid, true, pageOffset,
                                                                                   guard.frame->pVersion);  // move possesion incl page
            // -------------------------------------------------------------------------------------
            _mm_prefetch(&guard.frame->page->data[0], _MM_HINT_T0);  // prefetch first cache line of page
            // -------------------------------------------------------------------------------------
            // 发送消息并等待响应
            auto& pmResponse = threads::Worker::my().writeMsgSync<PossessionMoveResponse>(conflict, pmRequest);
            // -------------------------------------------------------------------------------------
            guard.frame->possessors.exclusive = 0;  // reset // 重置独占所有者
            // -------------------------------------------------------------------------------------
            ensure(pmResponse.resultType == RESULT::WithPage);    // 确保响应类型为 WithPage
         } else if (guard.frame->possession == POSSESSION::SHARED) {
            //  Upgrade
            // -------------------------------------------------------------------------------------
            _mm_prefetch(&guard.frame->page->data[0], _MM_HINT_T0);  // prefetch first cache line of page
            // -------------------------------------------------------------------------------------
            if (functor.type == LATCH_STATE::EXCLUSIVE) {
               // -------------------------------------------------------------------------------------
               // -------------------------------------------------------------------------------------
               guard.frame->pVersion++;
               guard.frame->possessors.shared.reset(nodeId);
               auto& shared = guard.frame->possessors.shared;
                // 如果页面被驱逐，则从随机节点移动页面
               if (guard.frame->state == BF_STATE::EVICTED) { movePageRnd(shared, pageOffset, guard.frame->pVersion); }
               // -------------------------------------------------------------------------------------
               invalidateSharedConflicts(shared, guard.frame->pVersion);   // 使所有共享冲突无效
               // -------------------------------------------------------------------------------------
            } else {
               ensure(guard.frame->state == BF_STATE::EVICTED);   // 确保页面状态为 EVICTED
               auto& shared = guard.frame->possessors.shared;
               ensure(shared.any());   // 确保至少有一个共享冲突标记
               RESULT result;
               uint64_t randomId = 0;
               // move possessionfirst with page copy
               // -------------------------------------------------------------------------------------
               // 从随机节点复制页面
               copyPageRnd(shared, pageOffset, guard.frame->pVersion, result, randomId);
               // -------------------------------------------------------------------------------------
               if (result != RESULT::WithPage) {
                  // 将页面推回空闲列表
                  pageFreeList.push(guard.frame->page, threads::ThreadContext::my().page_handle);
                  guard.frame->page = nullptr;
                  // 确保页面状态为 EVICTED
                  ensure(guard.frame->state == BF_STATE::EVICTED);
                  // 确保缓冲区帧的锁已经被获取
                  ensure(guard.frame->latch.isLatched());
                  // 解锁并进行退避
                  guard.frame->latch.unlatchExclusive();
                  BACKOFF();
                  goto restart;
               }
            }
         } else
            throw std::runtime_error("Invalid state in fix");
         // -------------------------------------------------------------------------------------
         // 根据锁类型更新所有权
         if (functor.type == LATCH_STATE::EXCLUSIVE)
            guard.frame->possession = POSSESSION::EXCLUSIVE;
         else
            guard.frame->possession = POSSESSION::SHARED;
         // -------------------------------------------------------------------------------------
         guard.frame->setPossessor(nodeId);  // 设置当前节点为缓冲区帧的所有者
         ensure(guard.frame->isPossessor(nodeId));   // 确保当前节点是缓冲区帧的所有者
         // -------------------------------------------------------------------------------------
         guard.frame->state = BF_STATE::HOT;    // 更新状态为 HOT
         // -------------------------------------------------------------------------------------
         // downgrade latch
         ensure(guard.frame->pid != EMPTY_PID);
         if (guard.needDowngrade(functor.type)) {
            guard.downgrade(functor.type);
            if (guard.state == STATE::RETRY) {
               goto restart;
            }
         }
         // -------------------------------------------------------------------------------------
         guard.state = STATE::INITIALIZED;
         break;
      }
      // -------------------------------------------------------------------------------------
      // Upgrade case we have the page, but need to upgrade possession on the owner / remote
      // 升级情况：我们拥有页面，但需要在所有者或远程节点上升级所有权
      // -------------------------------------------------------------------------------------
      case STATE::REMOTE_POSSESSION_CHANGE: {
         // 增加 RPC 尝试计数（计什么数？目的什么？）
         threads::Worker::my().counters.incr(profiling::WorkerCounters::w_rpc_tried);
         ensure(FLAGS_nodes > 1);
         ensure(guard.frame != nullptr);
         ensure(guard.frame->latch.isLatched());
         ensure(guard.frame->possession == POSSESSION::SHARED);
         ensure(guard.frame->state == BF_STATE::HOT);
         auto pVersionOld = guard.frame->pVersion.load();   // 保存旧版本号
         guard.frame->pVersion++; // 更新版本号以防止分布式死锁 // update here to prevent distributed deadlock
         // -------------------------------------------------------------------------------------
         auto& contextT = threads::Worker::my().cctxs[pid.getOwner()];  // 获取远程所有者的上下文
         
         auto& request = *MessageFabric::createMessage<PossessionUpdateRequest>(contextT.outgoing, pid, pVersionOld);
         // -------------------------------------------------------------------------------------
         auto& response = threads::Worker::my().writeMsgSync<PossessionUpdateResponse>(pid.getOwner(), request);

         if (response.resultType == RESULT::UpdateFailed) {
            // 确保缓冲区帧的锁已经被获取
            ensure(guard.frame->latch.isLatched());
            // 恢复版本号
            guard.frame->pVersion = pVersionOld;
            // 解锁并进行退避
            guard.frame->latch.unlatchExclusive();
            // 增加 RPC 重新启动计数
            threads::Worker::my().counters.incr(profiling::WorkerCounters::w_rpc_restarted);
            goto restart;
         }
         // -------------------------------------------------------------------------------------
         _mm_prefetch(&guard.frame->page->data[0], _MM_HINT_T0);  // prefetch first cache line of page
         // -------------------------------------------------------------------------------------
         ensure(guard.frame->pVersion == response.pVersion);   // 确保更新后的版本号与响应中的版本号相同
         guard.frame->pVersion = response.pVersion;   // 更新缓冲区帧的版本号
         // -------------------------------------------------------------------------------------
         if (response.resultType == RESULT::UpdateSucceedWithSharedConflict) {
            StaticBitmap<64> shared(response.conflictingNodeId);  // 创建包含冲突节点的共享位图
            shared.reset(nodeId);   // 重置当前节点的冲突标记
            invalidateSharedConflicts(shared, guard.frame->pVersion);   // 使所有共享冲突无效
         }
         // -------------------------------------------------------------------------------------
         // update state
         guard.frame->possession = POSSESSION::EXCLUSIVE;
         guard.frame->setPossessor(nodeId);
         // -------------------------------------------------------------------------------------
         guard.frame->state = BF_STATE::HOT;    // 更新缓冲区帧的状态为 HOT
         // -------------------------------------------------------------------------------------
         guard.state = STATE::INITIALIZED;
         break;
      }
      default:
         break;
   }
   // -------------------------------------------------------------------------------------
   // 确保缓冲区帧的全局纪元不小于全局纪元
   if (guard.frame->epoch < globalEpoch) guard.frame->epoch = globalEpoch.load();
   // -------------------------------------------------------------------------------------
   // -------------------------------------------------------------------------------------
   ensure(guard.state == STATE::INITIALIZED);
   // 如果锁状态不是 OPTIMISTIC，则确保缓冲区帧不为空
   if (guard.latchState != LATCH_STATE::OPTIMISTIC) { ensure(guard.frame != nullptr); }
   // -------------------------------------------------------------------------------------
   return guard;
}
