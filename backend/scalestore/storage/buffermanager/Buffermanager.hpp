#pragma once
// -------------------------------------------------------------------------------------
#include "AccessFunctors.hpp"
#include "BufferFrame.hpp"
#include "Defs.hpp"
#include "Guard.hpp"
#include "Partition.hpp"
#include "PartitionedQueue.hpp"
#include "scalestore/profiling/counters/WorkerCounters.hpp"
#include "scalestore/rdma/CommunicationManager.hpp"
#include "scalestore/rdma/messages/Messages.hpp"
#include "scalestore/syncprimitives/HybridLatch.hpp"
#include "scalestore/threads/Worker.hpp"
#include "scalestore/utils/MemoryManagement.hpp"
#include "scalestore/utils/Parallelize.hpp"
#include "scalestore/utils/FNVHash.hpp"
// -------------------------------------------------------------------------------------
#include <cstdint>
#include <mutex>
#include <stack>
#include <vector>
// -------------------------------------------------------------------------------------

namespace scalestore
{
namespace profiling
{
struct BMCounters;
}
namespace rdma
{
struct MessageHandler;
}
namespace storage
{
struct PageProvider;
struct BuffermanagerSampler;
// -------------------------------------------------------------------------------------
struct PageTable {
   // -------------------------------------------------------------------------------------
   static inline uint64_t Rotr64(uint64_t x, std::size_t n) {
      return (((x) >> n) | ((x) << (64 - n)));
   }
   // -------------------------------------------------------------------------------------
   // 对输入的 64 位整数进行快速哈希计算
   static inline uint64_t FasterHash(uint64_t input) {
      uint64_t local_rand = input;
      uint64_t local_rand_hash = 8;
      local_rand_hash = 40343 * local_rand_hash + ((local_rand) & 0xFFFF);
      local_rand_hash = 40343 * local_rand_hash + ((local_rand >> 16) & 0xFFFF);
      local_rand_hash = 40343 * local_rand_hash + ((local_rand >> 32) & 0xFFFF);
      local_rand_hash = 40343 * local_rand_hash + (local_rand >> 48);
      local_rand_hash = 40343 * local_rand_hash;
      return local_rand_hash; // if 64 do not rotate  // 如果是 64 位整数，则直接返回哈希值
      // else:
      // return Rotr64(local_rand_hash, 56);
   }
   // -------------------------------------------------------------------------------------
   const uint64_t size;
   utils::HugePages<BufferFrame>& bfs;// 引用，指向 BufferFrame 类型的 HugePages 对象 // complete buffer frame array
   // -------------------------------------------------------------------------------------
   PageTable(uint64_t numberBuckets, utils::HugePages<BufferFrame>& bfs) : size(Helper::nextPowerTwo(numberBuckets)), bfs(bfs) {
      ensure(numberBuckets < bfs.get_size());   // 确保 numberBuckets 小于 HugePages 中 BufferFrame 对象的大小
   }
   // -------------------------------------------------------------------------------------
   // 重载运算符[]，允许通过 PID 对象访问 PageTable 中的 BufferFrame 对象
   inline BufferFrame& operator[](PID pid) { 
   // 使用 FasterHash 计算哈希值，并通过与 (size - 1) 按位与操作得到索引
   return bfs[FasterHash(pid) & (size - 1)]; }
};
// -------------------------------------------------------------------------------------

// -------------------------------------------------------------------------------------
class Buffermanager
{
   // -------------------------------------------------------------------------------------
   //友元关系通常用于在一些情况下提供更高的访问权限，
   //但应该谨慎使用，因为它打破了封装性，可能导致更复杂的代码维护和理解。
   friend rdma::MessageHandler;
   friend storage::PageProvider;
   friend storage::BuffermanagerSampler;
   // -------------------------------------------------------------------------------------
  public:
   Buffermanager(rdma::CM<rdma::InitMessage>& cm, NodeID nodeId, s32 ssd_fd);
   ~Buffermanager() noexcept;
   // -------------------------------------------------------------------------------------
   // Deleted constructors
   //! Move constructor
   // 移动构造函数（被禁用）
   Buffermanager(Buffermanager&& other) noexcept = delete;
   //! Copy assignment operator
   Buffermanager& operator=(const Buffermanager& other) = delete;
   //! Move assignment operator
   Buffermanager& operator=(Buffermanager&& other) noexcept = delete;
   //! Copy constructor
   Buffermanager(const Buffermanager& other) = delete;
   // -------------------------------------------------------------------------------------
   // Counter specific functions
   // todo: sample one partition and multiplex?
   // 获取已使用的页面数量
   inline uint64_t getConsumedPages()
   {
      auto consumedPages = pidFreeList.approx_size();
      if (consumedPages == 0) throw std::runtime_error("No PIDs left in PID free list!");
      return (ssdSlotsSize - consumedPages);
   }
   // 获取可用的页面数量
   inline uint64_t getFreePages()
   {
      return pageFreeList.approx_size();
   }
   // 获取可用的帧数量
   inline uint64_t getFreeFrames()
   {
      auto freeFrames = frameFreeList.approx_size();
      if (freeFrames == 0) throw std::runtime_error("No frames left in free list!");
      return freeFrames;
   }
   inline uint64_t getDramPoolSize() { return dramPoolNumberPages; };   // 获取DRAM池的大小（以页面为单位）
   inline uint64_t getSSDSlots() { return ssdSlotsSize; };   // 获取SSD槽的数量
   inline uint64_t getNumberBufferframes() { return bufferFrames; };   //获取缓冲框架的数量
   inline utils::HugePages<BufferFrame>&  getBufferframes() { return bfs; };// 获取缓冲帧数组的引用
   inline uint64_t getGlobalEpoch() { return globalEpoch.load(); }   // 获取全局纪元
   // -------------------------------------------------------------------------------------
   template <typename ACCESS>
   Guard fix(PID pid, ACCESS functor); // 根据给定的 PID 和 ACCESS 函数对象修复（获取或锁定）一个 Guard 对象
   // -------------------------------------------------------------------------------------
   BufferFrame& newPage();    // 在缓冲管理器中创建一个新的页面，并返回对该页面的引用
   // 在远程缓冲管理器中分配一个新的页面，并返回对该页面的引用
   BufferFrame& newRemotePage(NodeID nodeId);  // allocates page on a remote BM
   void readPageSync(PID pid, uint8_t* destination);  // 同步读取给定 PID 对应的页面，并将数据复制到指定的 destination 内存地址
   void reclaimPage(BufferFrame& frame);  // 回收给定的页面，使其可供重新使用
   void writeAllPages();  // do not execute worker in between
   // -------------------------------------------------------------------------------------
   void reportHashTableStats();
   // -------------------------------------------------------------------------------------
   // global epoch for LRU approximation //近似LRU策略的全局纪元
   std::atomic<uint64_t> globalEpoch{0};
  // private: // for testing
   // -------------------------------------------------------------------------------------
   // dram page pool
   const uint64_t dramPoolSize = 0;
   const uint64_t dramPoolNumberPages = 0;  // pageslots
   const uint64_t ssdSlotsSize = 0;
   std::vector<std::pair<uint64_t,Page*>> dramPagePool; // number of pages in partition and ptr;
   // Page* dramPagePool;                // INIT
   const uint64_t bufferFrames = 0;         // number bfs
   utils::HugePages<BufferFrame> bfs;
   PageTable pTable;              // page table maps PID to BufferFrame
   const NodeID nodeId;
   const s32 ssd_fd;
   // -------------------------------------------------------------------------------------
   // free lists
   // -------------------------------------------------------------------------------------
   PartitionedQueue<BufferFrame*,PARTITIONS,BATCH_SIZE, utils::Stack> frameFreeList; //空闲帧列表
   PartitionedQueue<Page*,PARTITIONS,BATCH_SIZE, utils::Stack> pageFreeList; //空闲页面列表
   PartitionedQueue<PID,PARTITIONS,BATCH_SIZE, utils::Stack> pidFreeList; //pid（页面标识符）空闲列表
   // -------------------------------------------------------------------------------------
   // private helper functions
   // -------------------------------------------------------------------------------------
   // 该函数的目的是在哈希桶中插入新的缓冲帧。
   // 它首先获取与给定 PID 相关联的哈希桶缓冲帧以及哈希桶的锁。
   // 然后，它尝试获取哈希桶的独占锁，如果成功，检查缓冲帧的状态是否为 FREE。
   // 如果状态为 FREE，表示可以插入新的缓冲帧，于是调用初始化函数对象进行初始化，
   // 然后释放哈希桶的独占锁，确保缓冲帧已经被成功初始化并且被锁定，最后返回插入的缓冲帧。
   template <typename InitFunc>
   BufferFrame& insertFrame(PID pid,InitFunc&& init_bf_func){
      auto& bucket_bf =pTable[pid];    // 获取与给定 PID 相关联的哈希桶缓冲帧
      auto& ht_latch = bucket_bf.ht_bucket_latch;  // 获取哈希桶的锁
   restart:
      // -------------------------------------------------------------------------------------
      RESTART(!ht_latch.tryLatchExclusive() , restart);
      if(bucket_bf.state == BF_STATE::FREE){ // important to set it only to free when we have the HT latch or are done with the frame
         init_bf_func(bucket_bf);      // 使用初始化函数对象对缓冲帧进行初始化
         ht_latch.unlatchExclusive();
         ensure(bucket_bf.latch.isLatched());  // 确保缓冲帧已经被成功初始化并且被锁定
         return bucket_bf;
      }
      // -------------------------------------------------------------------------------------
      
      BufferFrame** f = &bucket_bf.next;
      while(*f){     // 遍历哈希桶中的缓冲帧链表，检查是否存在相同 PID 的缓冲帧
         if ((*f)->pid == pid) { throw std::runtime_error("Duplicate pid"); }
         f = &(*f)->next;
      }
      // -------------------------------------------------------------------------------------
      // 如果哈希桶的缓冲帧状态为 FREE，则尝试从空闲列表中弹出一个缓冲帧
      if (!frameFreeList.try_pop(*f, threads::ThreadContext::my().bf_handle)){
         ht_latch.unlatchExclusive();  // 无法从空闲列表中获取缓冲帧，释放哈希桶的独占锁并重新开始
         goto restart;
      }
      // -------------------------------------------------------------------------------------
      init_bf_func(**f);   // 初始化新的缓冲帧
      ht_latch.unlatchExclusive();  
      ensure((**f).latch.isLatched()); // 确保新插入的缓冲帧已经被成功初始化并且被锁
      return **f;    // 返回新插入的缓冲帧
   }

   // insert latched frame
   template <typename PageFunc>
   //这个函数的作用是从哈希桶中移除给定的缓冲帧，并执行处理页面函数。
   //如果要移除的帧是哈希桶，则通过快速路径处理。
   //否则，通过遍历哈希桶中的缓冲帧链表，找到要移除的帧，然后执行相应的操作。
   void removeFrame(BufferFrame& frame, PageFunc handle_page_func) {
      ensure(frame.latch.isLatched()); 
      // fast path check if by chance the frame we want to delete is the bucket then we save the hash function
      if (frame.isHtBucket) {    // 快速路径检查，如果要删除的帧是哈希桶，则保存哈希函数
         // -------------------------------------------------------------------------------------
         // 尝试获取哈希桶的独占锁
         while(!frame.ht_bucket_latch.tryLatchExclusive())
            ;
         // -------------------------------------------------------------------------------------
         ensure(frame.state != BF_STATE::FREE); 
         handle_page_func(frame);   // 处理页面函数
         frame.reset();    // 重置帧
         frame.ht_bucket_latch.unlatchExclusive();    // 释放哈希桶的独占锁和帧的独占锁
         frame.latch.unlatchExclusive();
         return;
      }
      // -------------------------------------------------------------------------------------
      // otherwise hash // 否则，处理哈希
      auto& bucket_bf = pTable[frame.pid];
      auto& ht_latch = bucket_bf.ht_bucket_latch; 
   restart:
      // -------------------------------------------------------------------------------------
      // 尝试获取哈希桶的独占锁
      RESTART(!ht_latch.tryLatchExclusive(), restart);
      // -------------------------------------------------------------------------------------
      // 遍历哈希桶中的缓冲帧链表
      BufferFrame** f = &bucket_bf.next;
      while (*f) {
         if ((*f)->pid == frame.pid) {
            *f = (*f)->next;  // remove frame
            ensure(frame.state != BF_STATE::FREE); 
            handle_page_func(frame);
            frame.reset();
            ht_latch.unlatchExclusive();
            frame.latch.unlatchExclusive();
            // 将帧重新加入空闲列表
            frameFreeList.try_push(&frame,threads::ThreadContext::my().bf_handle);
            return;
         }
         f = &(*f)->next; // iterate
      }
      throw std::runtime_error("removeFrame failed - page not in buffer"); // 如果未找到要移除的帧，抛出异常
   }
   // -------------------------------------------------------------------------------------
   // 这些函数使用了模板参数，其中 CONTENTION_METHOD 是一个枚举类型，
   // 表示争用解决方法（如阻塞或乐观锁等），
   // 而 ACCESS 是一个模板参数，表示执行对帧进行访问的操作的函数对象类型。
   // 这些函数的目的是在给定的进程ID和节点ID上查找或插入帧，
   // 并返回一个 Guard 对象，用于保护对该帧的访问
   template <CONTENTION_METHOD method, typename ACCESS>
   Guard findFrame(PID pid, ACCESS functor, NodeID nodeId_);
   // find frame or insert function 
   template <CONTENTION_METHOD method, typename ACCESS>
   Guard findFrameOrInsert(PID pid, ACCESS functor, NodeID nodeId_);
   // -------------------------------------------------------------------------------------

};
// -------------------------------------------------------------------------------------
// 这里的声明表明，BM 类有一个名为 global 的静态成员，
// 其类型是指向 Buffermanager 类对象的指针。
class BM
{
  public:
   static Buffermanager* global;
};
// -------------------------------------------------------------------------------------
// TEMPLATE FUNCTIONS
// -------------------------------------------------------------------------------------
#include "Buffermanager.tpp"
// -------------------------------------------------------------------------------------

}  // namespace storage
}  // namespace scalestore
