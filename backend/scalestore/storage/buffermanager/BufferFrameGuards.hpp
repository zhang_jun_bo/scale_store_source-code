#pragma once
// -------------------------------------------------------------------------------------
#include "Buffermanager.hpp"
// -------------------------------------------------------------------------------------

namespace scalestore
{
namespace storage
{
struct SharedBFGuard;
struct OptimisticBFGuard;

struct ExclusiveBFGuard {
   Guard g;    // Guard对象，用于管理缓冲区页面的访问
   ExclusiveBFGuard();  // creates new page
   ExclusiveBFGuard(PID pid);    //给定的PID创建ExclusiveBFGuard对象
   explicit ExclusiveBFGuard(NodeID remoteNodeId); // allocates page on remote node
   // -------------------------------------------------------------------------------------
   ~ExclusiveBFGuard();    // 析构函数：释放资源，确保页面被正确释放
   // -------------------------------------------------------------------------------------
   ExclusiveBFGuard(ExclusiveBFGuard&& xGuard);
   ExclusiveBFGuard& operator=(ExclusiveBFGuard&& xGuard);     //移动赋值运算符
   // -------------------------------------------------------------------------------------
   // upgrade shared guards to exclusive guard
   // S - X does not work because of sha
   ExclusiveBFGuard(SharedBFGuard&& sGuard);
   // O - X
   ExclusiveBFGuard(OptimisticBFGuard&& oGuard);
   // -------------------------------------------------------------------------------------
   ExclusiveBFGuard& operator=(ExclusiveBFGuard& other) = delete;    // 禁用复制赋值运算符，确保对象不能被复制
   ExclusiveBFGuard(ExclusiveBFGuard& other) = delete;  // copy constructor
   // -------------------------------------------------------------------------------------
   BufferFrame& getFrame() { return *g.frame; } //获取关联的BufferFrame对象的引用
   Version getVersion() { return g.vAcquired; } //获取版本信息
   STATE getState() { return g.state; }      //获取状态信息
   
   // 返回缓冲区页上索引为idx的元素的引用
   template <typename T>
   T& as(uint64_t idx)
   {
      return ((g.frame->page->getTuple<T>(idx)));
   }

   // 在缓冲区页上分配一个新的T类型对象，并返回其引用
   template <typename T>
   T& allocate()
   {
      return *(new (g.frame->page->begin()) T);
   }
   
   // 返回缓冲区页上索引为idx的元素的指针
   template <typename T>
   T* asPtr(uint64_t idx)
   {      
      auto* page = g.frame->page;
      return &((page->getTuple<T>(idx)));
   }
   
   void reclaim();      // 释放当前ExclusiveBFGuard对象所占用的资源，通常用于释放缓冲区页
   bool retry()   // 检查当前状态是否为RETRY，如果是则返回true，否则返回false
   {
      if (g.state == STATE::RETRY)  // 如果当前状态为RETRY，则返回true，否则返回false
         return true;
      return false;
   }
   // 与另一个ExclusiveBFGuard对象交换资源，即交换两个Guard对象的状态和关联的缓冲区页
   void swapWith(ExclusiveBFGuard& other){
      g.swapWith(other.g);    // 调用Guard对象的swapWith方法，交换两个Guard对象的状态和关联的缓冲区页
   }
};

struct SharedBFGuard {
   Guard g;
   SharedBFGuard(PID pid);
   ~SharedBFGuard();
   // -------------------------------------------------------------------------------------
   SharedBFGuard(SharedBFGuard&& sGuard);
   SharedBFGuard& operator=(SharedBFGuard&& sGuard);
   
   // downgrade X - S
   // no need to update remote possession information
   SharedBFGuard(ExclusiveBFGuard&& xGuard);
   // -------------------------------------------------------------------------------------
   // upgrade O - S
   // check possession information because optimistic coul have changed
   SharedBFGuard(OptimisticBFGuard&& oGuard);
   // -------------------------------------------------------------------------------------
   SharedBFGuard& operator=(SharedBFGuard& other) = delete;
   SharedBFGuard(SharedBFGuard& other) = delete;  // copy constructor
   // -------------------------------------------------------------------------------------
   BufferFrame& getFrame() { return *g.frame; }
   Version getVersion() { return g.vAcquired; }
   STATE getState() { return g.state; }
   template <typename T>
   // 返回缓冲区页上索引为idx的元素的引用
   T& as(uint64_t idx)
   {
      return ((g.frame->page->getTuple<T>(idx)));
   }
   // 检查当前状态是否为RETRY，如果是则返回true，否则返回false
   bool retry()
   {
      if (g.state == STATE::RETRY)
         return true;
      return false;
   }
};

struct OptimisticBFGuard {
   Guard g;
   OptimisticBFGuard(PID pid);
   // -------------------------------------------------------------------------------------
   OptimisticBFGuard(OptimisticBFGuard&& oGuard);
   OptimisticBFGuard& operator=(OptimisticBFGuard&& oGuard);
   // -------------------------------------------------------------------------------------
   // downgrades
   // no need to account for remote possession information
   // 构造函数：将 ExclusiveBFGuard 对象降级为 OptimisticBFGuard 对象，
   //不需要更新远程拥有信息
   OptimisticBFGuard(ExclusiveBFGuard&& xGuard);
   OptimisticBFGuard(SharedBFGuard&& sGuard);
   // -------------------------------------------------------------------------------------
   OptimisticBFGuard& operator=(OptimisticBFGuard& other) = delete;
   OptimisticBFGuard(OptimisticBFGuard& other) = delete;  // copy constructor
   // -------------------------------------------------------------------------------------
   BufferFrame& getFrame() { return *g.frame; }
   Version getVersion() { return g.vAcquired; }
   STATE getState() { return g.state; }

   // template <typename T>
   // T& as(uint64_t idx)
   // {
   //    // special care must me taken because of optimistic nature
   //    return ((g.frame->page->getTuple<T>(idx)));
   // }

   template <typename T>
   T* asPtr(uint64_t idx)
   {      
      auto* page = g.frame->page;
      if(retry()) return nullptr;   // 如果需要重试，返回nullptr
      return &((page->getTuple<T>(idx)));    // 返回缓冲区页上索引为 idx 的元素的指针
   }
   bool retry()
   {
      if (g.state == STATE::RETRY)
         return true;
      // 如果乐观锁检查失败，将状态设为 RETRY 并返回 true，否则返回 false
      if (!getFrame().latch.optimisticCheckOrRestart(g.vAcquired)) {
         g.state = STATE::RETRY;
         return true;
      }
      return false;
   }
};
}  // namespace storage
}  // namespace scalestore
