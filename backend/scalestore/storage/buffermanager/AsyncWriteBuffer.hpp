
#pragma once
#include "BufferFrame.hpp"
#include "Defs.hpp"
#include "scalestore/storage/datastructures/RingBuffer.hpp" //循环缓存池
// -------------------------------------------------------------------------------------
// -------------------------------------------------------------------------------------
#include <libaio.h>
#include <memory>

#include <functional>
#include <list>
#include <unordered_map>
// -------------------------------------------------------------------------------------
namespace scalestore
{
namespace storage
{
// -------------------------------------------------------------------------------------
// 异步写入缓冲区类定义
    class AsyncWriteBuffer {
    private:
        // 写入命令结构体，用于存储写入命令的信息
        struct WriteCommand {
            BufferFrame *bf; // 缓冲帧指针
            uint64_t epoch_added;// 添加时的时期
        };
        io_context_t aio_context; // 异步 I/O 上下文
        int fd;// 文件描述符
        u64 page_size, batch_max_size;// 页面大小和批量最大大小
        u64 pending_requests = 0;// 待处理的请求数量
        int64_t ready_to_submit = 0;// 准备提交的请求数量
        int64_t outstanding_ios = 0;// 进行中的 I/O 操作数量


    public:
        std::unique_ptr<Page[]> write_buffer;// 写入数据的缓冲区
        std::unique_ptr<WriteCommand[]> write_buffer_commands;// 写入命令数组
        std::unique_ptr<struct iocb[]> iocbs;// 异步 I/O 控制块数组
        std::unique_ptr<struct iocb *[]> iocbs_ptr; // 异步 I/O 控制块指针数组
        std::unique_ptr<struct io_event[]> events;// 异步 I/O 事件数组
        RingBuffer <uint64_t> free_slots;// 空闲槽位数组
        // -------------------------------------------------------------------------------------
        // Debug
        // -------------------------------------------------------------------------------------

        // 构造函数，初始化异步写入缓冲区对象
        AsyncWriteBuffer(int fd, u64 page_size, u64 batch_max_size);

        // Caller takes care of sync
        // 检查异步写入缓冲区是否已满
        bool full();

        // 向异步写入缓冲区中添加写入命令
        void add(BufferFrame &bf, PID pid, uint64_t epoch_added);

        u64 submit();// 提交异步写入请求
        u64 pollEventsSync();// 同步等待并处理异步 I/O 事件
        void
        getWrittenBfs(std::function<void(BufferFrame & , uint64_t)> callback, u64 n_events);// 获取已写入的缓冲帧，并通过回调函数进行处理
    };
// -------------------------------------------------------------------------------------
}  // namespace storage
}  // namespace scalestore
// -------------------------------------------------------------------------------------
