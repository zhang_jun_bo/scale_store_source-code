#pragma once
#include "scalestore/utils/MPMCQueue.hpp"
#include "BufferFrame.hpp"
// -------------------------------------------------------------------------------------
namespace scalestore {
namespace storage {
struct Partition{
   // optimized rigtorp queue with power of two arithmetic
   // however, this queue is memory hungry to prevent false sharing thus add batching?
   // -------------------------------------------------------------------------------------
   template <typename T>
   //MPMPCQueueWrapper 是一个用于包装 rigtorp::MPMCQueue 的结构体模板，通过添加元素数量的记录，提供了对存储在队列中元素的访问和操作的接口。
   struct MPMPCQueueWrapper{//为了防止伪共享而导致的内存消耗，这个队列可能会添加批处理功能。这是一个优化过的队列，使用了二次幂算术
      rigtorp::MPMCQueue<T,true> q;//q：是一个具体的队列实现。用于存储元素类型为 T 的队列。
      std::atomic<u64> elements {0};//elements：类型为 std::atomic<u64>，是一个原子类型的无符号 64 位整数。用于记录队列中元素的数量。
      // -------------------------------------------------------------------------------------
      MPMPCQueueWrapper(u64 size) : q(size){};//MPMPCQueueWrapper 构造函数：接受一个 size 参数，并将其传递给 q 的构造函数来初始化队列，用于指定队列的大小。

      u64 approxSize(){//approxSize 函数：返回队列中元素的近似数量
         return elements.load();//通过调用 elements.load() 来获取当前 elements 的值。
      }
      // push
      void push(T e){
         q.push(e);
         elements++;
      };//push 函数：接受一个类型为 T 的参数 e，将其推入队列，并通过递增 elements 的值来记录队列中的元素数量。push 函数：接受一个类型为 T 的参数 e，将其推入队列，并通过递增 elements 的值来记录队列中的元素数量。
      // pop

      T pop(){
         T e;
         q.pop(e);
         elements--;
         return e;
      }      //pop 函数：从队列中弹出一个元素，并通过递减 elements 的值来更新队列中的元素数量。之后，返回被移出的元素。
   };
   // -------------------------------------------------------------------------------------
   MPMPCQueueWrapper<BufferFrame*> frameFreeList;
   MPMPCQueueWrapper<Page*> pageFreeList;
   MPMPCQueueWrapper<PID> pidFreeList;
   // -------------------------------------------------------------------------------------
   Partition(u64 freeFramesSize, u64 freePagesSize, u64 freePIDsSize);
};
// -------------------------------------------------------------------------------------
}  // storage
}  // scalestore
