#include "Partition.hpp"
namespace scalestore {
namespace storage {
Partition::Partition( u64 freeFramesSize, u64 freePagesSize, u64 freePIDsSize)
    : frameFreeList(freeFramesSize), pageFreeList(freePagesSize), pidFreeList(freePIDsSize)//用于传递给构造函数并初始化相应的成员变量，传递给构造函数的空闲帧、空闲页和空闲进程标识符的大小。
{
}
}  // storage
}  // scalestore

