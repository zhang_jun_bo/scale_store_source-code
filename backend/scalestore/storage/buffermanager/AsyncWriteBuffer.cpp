#include "AsyncWriteBuffer.hpp"
// -------------------------------------------------------------------------------------
#include <signal.h>
#include <cstring>
#include <memory>
// -------------------------------------------------------------------------------------
namespace scalestore
{
namespace storage
{
// -------------------------------------------------------------------------------------
//这个构造函数用于初始化 AsyncWriteBuffer 对象，分配必要的内存空间，并设置异步 I/O 上下文。
AsyncWriteBuffer::AsyncWriteBuffer(int fd, u64 page_size, u64 batch_max_size) : fd(fd), page_size(page_size), batch_max_size(batch_max_size), free_slots(batch_max_size)
{
    // 分配批量最大大小的内存空间，用于存储写入数据的缓冲区
   write_buffer = std::make_unique<Page[]>(batch_max_size);
    // 分配批量最大大小的内存空间，用于存储写入命令的数组
   write_buffer_commands = std::make_unique<WriteCommand[]>(batch_max_size);
    // 分配批量最大大小的内存空间，用于存储异步 I/O 控制块的数组
   iocbs = std::make_unique<struct iocb[]>(batch_max_size);
    // 分配批量最大大小的内存空间，用于存储异步 I/O 控制块指针的数组
   iocbs_ptr = std::make_unique<struct iocb*[]>(batch_max_size);
    // 分配批量最大大小的内存空间，用于存储异步 I/O 事件的数组
   events = std::make_unique<struct io_event[]>(batch_max_size);
    // -------------------------------------------------------------------------------------
    // 初始化异步 I/O 上下文
   memset(&aio_context, 0, sizeof(aio_context));
   const int ret = io_setup(batch_max_size, &aio_context);
    // 如果初始化失败，抛出运行时异常
   if (ret != 0) {
      throw std::runtime_error(("io_setup failed, ret code = " + std::to_string(ret)));
   }
    // init free slots// 初始化空闲槽位数组
   for(uint64_t s_i = 0; s_i < batch_max_size; ++s_i)
      free_slots.add(s_i);
}

// -------------------------------------------------------------------------------------
// 检查异步写入缓冲区是否已满
bool AsyncWriteBuffer::full() {
        // 如果进行中的 I/O 操作数量与准备提交的 I/O 操作数量之和超过了批量最大大小减 2，
        // 则认为缓冲区已满，因为要给提交的 I/O 操作腾出一些槽位
        if ((outstanding_ios + ready_to_submit) >= (int64_t) batch_max_size - 2) {
            return true;
        } else {
            return false;
        }
    }

// -------------------------------------------------------------------------------------
// 向异步写入缓冲区中添加写入命令
void AsyncWriteBuffer::add(BufferFrame& bf, PID pid, uint64_t epoch_added)
{
    // 确保异步写入缓冲区不满
   ensure(!full());
    // 确保缓冲帧的页地址是 512 字节对齐的
   ensure(u64(bf.page) % 512 == 0);
    // 确保进行中的 I/O 操作数量与准备提交的 I/O 操作数量之和不超过批量最大大小
    ensure((outstanding_ios + ready_to_submit) <= (int64_t) batch_max_size);
    // -------------------------------------------------------------------------------------
    // 从空闲槽位中取出一个槽位，并更新已准备提交的槽位数量
    auto slot = free_slots.remove();
    auto slot_ptr = ready_to_submit++;
    // 填充写入命令结构体
    write_buffer_commands[slot].bf = &bf;
    write_buffer_commands[slot].epoch_added = epoch_added;
    // 设置缓冲帧的调试信息
    bf.page->magicDebuggingNumber = epoch_added;

    // 设置 I/O 操作的写入参数
    void *write_buffer_slot_ptr = bf.page;
    io_prep_pwrite(&iocbs[slot], fd, write_buffer_slot_ptr, page_size, page_size * pid.plainPID());
    iocbs[slot].data = write_buffer_slot_ptr;
    iocbs_ptr[slot_ptr] = &iocbs[slot];
}

// -------------------------------------------------------------------------------------
// 提交异步写入请求
u64 AsyncWriteBuffer::submit()
{
    // 如果有已准备提交的请求数量大于0
   if (ready_to_submit > 0) {
       // 使用 io_submit 提交异步写入请求
      int ret_code = io_submit(aio_context, ready_to_submit, iocbs_ptr.get());
       // 确保成功提交的请求数量与已准备提交的请求数量相等
      ensure(ret_code == s32(ready_to_submit));
       // 更新进行中的 I/O 操作数量和已准备提交的请求数量
      outstanding_ios += ready_to_submit;
      ready_to_submit = 0;
      return ret_code;
   }
   return 0;
}

// -------------------------------------------------------------------------------------
// 同步等待并处理异步 I/O 事件
u64 AsyncWriteBuffer::pollEventsSync()
{
    // 如果有进行中的 I/O 操作数量大于0
   if (outstanding_ios > 0) {
       // 使用 io_getevents 函数获取已完成的 I/O 事件
      const int done_requests = io_getevents(aio_context, 1, outstanding_ios, events.get(), NULL);
       // 如果获取事件失败，抛出异常
      if(done_requests<0)
         throw std::runtime_error("io_getevents failed" + std::to_string(done_requests));
       // 更新进行中的 I/O 操作数量，确保不小于0
      outstanding_ios -= done_requests;
      ensure(outstanding_ios >=0);
      return done_requests;
   }
   return 0;
}

// -------------------------------------------------------------------------------------
// 获取已写入的缓冲帧，并通过回调函数进行处理
void AsyncWriteBuffer::getWrittenBfs(std::function<void(BufferFrame&, uint64_t)> callback, u64 n_events)
{
   for (u64 i = 0; i < n_events; i++) {
       // const auto slot = (u64(events[i].data) - u64(write_buffer.get())) / page_size;
       // 计算槽位索引
       const auto slot = ((iocb *) (events[i].obj)) - (((iocbs.get())));
       // 将槽位放回空闲槽位数组
       free_slots.add(slot);
       // -------------------------------------------------------------------------------------

       // 确保事件的写入大小与页面大小相等
       ensure(events[i].res == page_size);
       // 如果 res2 不为0，产生断言失败的异常
       if (!(events[i].res2 == 0)) {
           raise(SIGTRAP);
       }
       // 调用回调函数处理已写入的缓冲帧
       callback(*write_buffer_commands[slot].bf, write_buffer_commands[slot].epoch_added);
   }
}
// -------------------------------------------------------------------------------------
}  // namespace storage
}  // namespace scalestore
   // -------------------------------------------------------------------------------------
