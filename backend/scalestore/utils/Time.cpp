#include "Time.hpp"
// -------------------------------------------------------------------------------------

namespace scalestore
{
      namespace utils
      {
            // 用于测量时间间隔或时间戳
            uint64_t getTimePoint()
            {

                  using namespace std::chrono;
                  auto now = system_clock::now(); // 获取当前时间点
                  auto now_micros = time_point_cast<microseconds>(now); // 将时间点转换为微秒级别的时间点
                  auto value = now_micros.time_since_epoch();     // 获取时间点相对于时钟纪元的时长
                  return value.count();   // 获取时长的计数值，即当前时间的微秒数
            }
      } // utils
} // namespace utils
