#pragma once
#include "Defs.hpp"
// -------------------------------------------------------------------------------------
namespace scalestore
{
  namespace utils
  {
    // -------------------------------------------------------------------------------------
    class FNV
    {
    private:
      static constexpr u64 FNV_OFFSET_BASIS_64 = 0xCBF29CE484222325L; // FNV 哈希算法的初始值
      static constexpr u64 FNV_PRIME_64 = 1099511628211L;   // FNV 哈希算法的质数

    public:
      static u64 hash(u64 val);
    };
    // -------------------------------------------------------------------------------------
  } // namespace utils
} // namespace leanstore
