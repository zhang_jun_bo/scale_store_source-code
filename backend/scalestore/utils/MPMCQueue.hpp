/*
Copyright (c) 2020 Erik Rigtorp <erik@rigtorp.se>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */

/*
TZ: Added power of two version to optimize idx and turn function for scalestore project
 */

#pragma once

#include <atomic>
#include <cassert>
#include <cstddef> // offsetof
#include <limits>
#include <memory>
#include <new> // std::hardware_destructive_interference_size
#include <stdexcept>

#ifndef __cpp_aligned_new // 是否支持 C++17 的对齐内存分配
  // 是否正在使用 Windows 操作系统。如果是，就包含 <malloc.h> 头文件，以便使用 Windows 平台提供的 _aligned_malloc 函数。
  #ifdef _WIN32
    #include <malloc.h> // _aligned_malloc
  #else
    #include <stdlib.h> // posix_memalign
  #endif
#endif

// 多生产者多消费者队列（MPMC Queue）的实现
namespace rigtorp
{
  namespace mpmc
  {

    // added for assert
    // 用于检查一个数是否是2的幂
    static constexpr bool powerOfTwo(uint64_t n)
    {
      return n && (!(n & (n - 1)));
    }
    // 定义了硬件干扰的大小 64 字节
    static constexpr size_t hardwareInterferenceSize = 64;

    #if defined(__cpp_aligned_new)
        // 若支持c++17的aligned_new，则使用std::allocator<T>（已经支持对齐分配）
        template <typename T>
        using AlignedAllocator = std::allocator<T>;
    #else
        // 若不支持
        template <typename T>
        struct AlignedAllocator
        {
          using value_type = T;
          
          //  实现了 allocate 函数，用于分配大小为 n 个元素的内存。
          T *allocate(std::size_t n)
          {
            // 检查是否超出限制
            if (n > std::numeric_limits<std::size_t>::max() / sizeof(T))
            {
              throw std::bad_array_new_length();
            }
            #ifdef _WIN32
              // Windows下的对齐内存分配
              auto *p = static_cast<T *>(_aligned_malloc(sizeof(T) * n, alignof(T)));
              if (p == nullptr)
              {
                throw std::bad_alloc();
              }
            #else
              // 非Windows下的对齐内存分配
              T *p;
              if (posix_memalign(reinterpret_cast<void **>(&p), alignof(T),
                                sizeof(T) * n) != 0)
              {
                throw std::bad_alloc();
              }
            #endif
            return p;
          }

          // 内存释放
          void deallocate(T *p, std::size_t)
          {
            #ifdef _WIN32
              _aligned_free(p);
            #else
              free(p);
            #endif
          }
        };
    #endif
    // 用于存储队列中的元素
    template <typename T>
    struct Slot
    {
      /* 在析构函数中，通过检查 turn 的最低位（turn & 1）来确定是否需要销毁元素。turn 的奇偶性表示元素是否已经被生产者构造。
         如果 turn 是奇数，说明元素已经被构造，需要调用 destroy() 方法销毁元素。*/
      ~Slot() noexcept
      {
        if (turn & 1)
        {
          destroy();
        }
      }

      // 正确的创建元素，并且存储在storge中
      template <typename... Args>
      void construct(Args &&...args) noexcept
      {
        static_assert(std::is_nothrow_constructible<T, Args &&...>::value,
                      "T must be nothrow constructible with Args&&...");
        new (&storage) T(std::forward<Args>(args)...);
      }

      // 销毁元素
      void destroy() noexcept
      {
        static_assert(std::is_nothrow_destructible<T>::value,
                      "T must be nothrow destructible");
        reinterpret_cast<T *>(&storage)->~T();
      }
      // 用于将元素从 Slot 中移出。
      T &&move() noexcept { return reinterpret_cast<T &&>(storage); }

      // Align to avoid false sharing between adjacent slots
      // 按照硬件干扰大小进行对齐，以避免伪共享（false sharing）的性能问题
      alignas(hardwareInterferenceSize) std::atomic<size_t> turn = {0};
      typename std::aligned_storage<sizeof(T), alignof(T)>::type storage;
    };

    template <typename T, bool POWER_TWO, typename Allocator = AlignedAllocator<Slot<T>>>
    class Queue
    {
    private:
      // 要求T是无异常拷贝赋值、无异常移动赋值、无异常销毁的类型
      static_assert(std::is_nothrow_copy_assignable<T>::value ||
                        std::is_nothrow_move_assignable<T>::value,
                    "T must be nothrow copy or move assignable");

      static_assert(std::is_nothrow_destructible<T>::value,
                    "T must be nothrow destructible");

    public:
      explicit Queue(const size_t capacity,
                     const Allocator &allocator = Allocator())
          : capacity_(capacity), bits_((64) - __builtin_clzll(capacity_) - 1), allocator_(allocator), head_(0), tail_(0)
      {
        // 容量小于1，抛出异常
        if (capacity_ < 1)
        {
          throw std::invalid_argument("capacity < 1");
        }

        // 确保容量是2的幂
        if constexpr (POWER_TWO)
        {
          if (!powerOfTwo(capacity_))
            throw std::invalid_argument("capacity not power of two");
        }

        // 防止伪共享，额外分配了一个槽
        slots_ = allocator_.allocate(capacity_ + 1);
        // Allocators are not required to honor alignment for over-aligned types
        // (see http://eel.is/c++draft/allocator.requirements#10) so we verify
        // alignment here
        // 内存是否对齐
        if (reinterpret_cast<size_t>(slots_) % alignof(Slot<T>) != 0)
        {
          allocator_.deallocate(slots_, capacity_ + 1);
          throw std::bad_alloc();
        }
        // 初始化槽
        for (size_t i = 0; i < capacity_; ++i)
        {
          new (&slots_[i]) Slot<T>();
        }
        // 保槽和队列的内存对齐以及大小是 cache line 的整数倍，以防止伪共享。
        static_assert(
            alignof(Slot<T>) == hardwareInterferenceSize,
            "Slot must be aligned to cache line boundary to prevent false sharing");
        static_assert(sizeof(Slot<T>) % hardwareInterferenceSize == 0,
                      "Slot size must be a multiple of cache line size to prevent "
                      "false sharing between adjacent slots");
        static_assert(sizeof(Queue) % hardwareInterferenceSize == 0,
                      "Queue size must be a multiple of cache line size to "
                      "prevent false sharing between adjacent queues");
        // 检查队列的头 head_ 和尾 tail_ 是否满足 cache line 之间的对齐。
        static_assert(
            offsetof(Queue, tail_) - offsetof(Queue, head_) ==
                static_cast<std::ptrdiff_t>(hardwareInterferenceSize),
            "head and tail must be a cache line apart to prevent false sharing");
      }

      // 销毁队列中的槽，并释放相应的内存
      ~Queue() noexcept
      {
        for (size_t i = 0; i < capacity_; ++i)
        {
          slots_[i].~Slot();
        }
        allocator_.deallocate(slots_, capacity_ + 1);
      }

      // 禁用了拷贝构造函数和赋值运算符，确保对象是非拷贝、非移动的
      Queue(const Queue &) = delete;
      Queue &operator=(const Queue &) = delete;

      // 插入新元素，会阻塞直到成功插入元素
      template <typename... Args>
      void emplace(Args &&...args) noexcept
      {
        static_assert(std::is_nothrow_constructible<T, Args &&...>::value,
                      "T must be nothrow constructible with Args&&...");
        auto const head = head_.fetch_add(1);
        auto &slot = slots_[idx(head)];
        // 等待
        while (turn(head) * 2 != slot.turn.load(std::memory_order_acquire))
          ;
        slot.construct(std::forward<Args>(args)...);
        slot.turn.store(turn(head) * 2 + 1, std::memory_order_release);
      }

      // 尝试插入元素，如果队列已满，则立即返回失败。
      template <typename... Args>
      bool try_emplace(Args &&...args) noexcept
      {
        static_assert(std::is_nothrow_constructible<T, Args &&...>::value,
                      "T must be nothrow constructible with Args&&...");
        auto head = head_.load(std::memory_order_acquire);
        for (;;)
        {
          auto &slot = slots_[idx(head)];
          if (turn(head) * 2 == slot.turn.load(std::memory_order_acquire))
          {
            if (head_.compare_exchange_strong(head, head + 1))
            {
              slot.construct(std::forward<Args>(args)...);
              slot.turn.store(turn(head) * 2 + 1, std::memory_order_release);
              return true;
            }
          }
          else
          {
            auto const prevHead = head;
            head = head_.load(std::memory_order_acquire);
            if (head == prevHead)
            {
              return false;
            }
          }
        }
      }

      // 阻塞的插入元素
      void push(const T &v) noexcept
      {
        static_assert(std::is_nothrow_copy_constructible<T>::value,
                      "T must be nothrow copy constructible");
        emplace(v);
      }

      // 阻塞的插入元素
      template <typename P,
                typename = typename std::enable_if<
                    std::is_nothrow_constructible<T, P &&>::value>::type>
      void push(P &&v) noexcept
      {
        emplace(std::forward<P>(v));
      }

      // 尝试插入元素，队列满立即返回失败
      bool try_push(const T &v) noexcept
      {
        static_assert(std::is_nothrow_copy_constructible<T>::value,
                      "T must be nothrow copy constructible");
        return try_emplace(v);
      }

      // 尝试插入元素，队列满立即返回失败
      template <typename P,
                typename = typename std::enable_if<
                    std::is_nothrow_constructible<T, P &&>::value>::type>
      bool try_push(P &&v) noexcept
      {
        return try_emplace(std::forward<P>(v));
      }

      // 弹出一个元素，阻塞的方式
      void pop(T &v) noexcept
      {
        auto const tail = tail_.fetch_add(1);
        auto &slot = slots_[idx(tail)];
        // 等待
        while (turn(tail) * 2 + 1 != slot.turn.load(std::memory_order_acquire))
          ;
        v = slot.move();
        slot.destroy();
        slot.turn.store(turn(tail) * 2 + 2, std::memory_order_release);
      }

      // 尝试弹出元素，非阻塞的方式
      bool try_pop(T &v) noexcept
      {
        auto tail = tail_.load(std::memory_order_acquire);
        for (;;)
        {
          auto &slot = slots_[idx(tail)];
          if (turn(tail) * 2 + 1 == slot.turn.load(std::memory_order_acquire))
          {
            if (tail_.compare_exchange_strong(tail, tail + 1))
            {
              v = slot.move();
              slot.destroy();
              slot.turn.store(turn(tail) * 2 + 2, std::memory_order_release);
              return true;
            }
          }
          else
          {
            auto const prevTail = tail;
            tail = tail_.load(std::memory_order_acquire);
            if (tail == prevTail)
            {
              return false;
            }
          }
        }
      }

    private:
      // 函数用于计算槽的索引
      constexpr size_t idx(size_t i) const noexcept
      {
        if constexpr (POWER_TWO)
        {
          return i & (capacity_ - 1);
        }
        return i % capacity_;
      }
      
      // 用于计算槽的序号
      constexpr size_t turn(size_t i) const noexcept
      {
        if constexpr (POWER_TWO)
        {
          return i >> bits_;
        }
        return i / capacity_;
      }

    private:
      const size_t capacity_;
      const size_t bits_;
      Slot<T> *slots_;  // 槽位是一个数组
      #if defined(__has_cpp_attribute) && __has_cpp_attribute(no_unique_address)
        Allocator allocator_ [[no_unique_address]];
      #else
        Allocator allocator_;
      #endif

      // 确保对齐
      // Align to avoid false sharing between head_ and tail_
      alignas(hardwareInterferenceSize) std::atomic<size_t> head_;
      alignas(hardwareInterferenceSize) std::atomic<size_t> tail_;
    };
  } // namespace mpmc

  template <typename T,
            bool POWER_TWO,
            typename Allocator = mpmc::AlignedAllocator<mpmc::Slot<T>>>
  using MPMCQueue = mpmc::Queue<T, POWER_TWO, Allocator>;

} // namespace rigtorp
