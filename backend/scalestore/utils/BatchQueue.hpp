#pragma once
#include <random>
#include <algorithm>
namespace scalestore
{
   namespace utils
   {
      // -------------------------------------------------------------------------------------
      // 自旋锁的实现
      class SpinLock
      {
         // 对象在内存中按照64字节对齐，atomic提供原子操作，即不会被中断
         alignas(64) std::atomic<bool> lock_ = {0};

      public:
         // 使用原子交换操作获取锁。如果锁不可用，它会在循环中自旋，直到可以获取锁。
         void lock() noexcept
         {
            for (;;)
            {
               // 获得锁后退出，exchange返回之前锁的状态，若是false则退出循环
               if (!lock_.exchange(true, std::memory_order_acquire))
               {
                  return;
               }
               // 等待锁释放
               while (lock_.load(std::memory_order_relaxed))
               {
                  // 一个特殊的指令，它告诉 CPU 在自旋等待时暂停执行，以降低功耗和资源竞争
                  _mm_pause();
               }
            }
         }

         bool try_lock() noexcept
         {
            // 首先是检查锁是否空闲的快速操作（防止不必要的缓存丢失），若空闲则获取锁
            return !lock_.load(std::memory_order_relaxed) && !lock_.exchange(true, std::memory_order_acquire);
         }

         // 释放锁
         void unlock() noexcept { lock_.store(false, std::memory_order_release); }
      };
      // -------------------------------------------------------------------------------------
      // containers
      // -------------------------------------------------------------------------------------
      // -------------------------------------------------------------------------------------
      // 循环缓冲区（RingBuffer）的模板类
      template <typename T, size_t N>
      struct RingBuffer
      {
         // 向缓冲区添加元素
         [[nodiscard]] bool try_push(const T &e)
         {
            if (full())
               return false;
            buffer[mask(write++)] = e;
            return true;
         }

         // 从缓冲区中弹出元素
         [[nodiscard]] bool try_pop(T &e)
         {
            if (empty())
               return false;
            e = buffer[mask(read++)];
            return true;
         }

         // 检查是否为空/满，获取size以及重置缓冲区
         bool empty() { return read == write; }
         bool full() { return get_size() == N; }
         uint64_t get_size() { return write - read; }
         void reset()
         {
            read = 0;
            write = 0;
         }

      private:

         // 确保环形缓冲区的索引始终在范围 [0, N-1] 内。
         constexpr uint64_t mask(uint64_t val) { return val & (N - 1); }
         // -------------------------------------------------------------------------------------
         uint64_t read = 0;
         uint64_t write = 0;
         std::array<T, N> buffer{};    // 缓冲区大小

         // 在编译时检查 N 是否是 2 的幂，因为环形缓冲区的实现通常要求大小为 2 的幂，以便使用按位与操作来实现环形索引
         static_assert(N && ((N & (N - 1)) == 0), "N not power of 2");
      };
      // -------------------------------------------------------------------------------------
      // Stack简单的模版类，T 存储类型，N 栈的大小
      template <typename T, size_t N>
      struct Stack
      {
         // 将e压入栈
         [[nodiscard]] bool try_push(const T &e)
         {
            assert(size <= N);
            if (full())
            {
               return false;
            }
            buffer[size++] = e;
            return true;
         }

         // 弹出栈顶元素
         [[nodiscard]] bool try_pop(T &e)
         {
            if (empty())
            {
               return false;
            }
            e = buffer[--size];
            return true;
         }

         // // 检查栈是否为空/满，获取size以及重置栈
         bool empty() { return size == 0; }
         bool full() { return size == (N); }
         uint64_t get_size() { return size; }
         void reset() { size = 0; }

      // size 跟踪栈的当前大小
      // buffer 是存储元素的固定大小数组
      private:
         uint64_t size = 0;
         std::array<T, N> buffer{};
      };
      // -------------------------------------------------------------------------------------
      /* T：表示要存储在 Batch 中的元素的类型
         BatchSize：表示每个 Batch 的大小（容量）
         Container：表示用于存储元素的容器模板。这是一个模板模板参数，它期望一个接受两个参数的模板，一个是元素类型 T，另一个是大小（容量）size_t。*/
      template <typename T, size_t BatchSize, template <class, size_t> class Container>
      struct alignas(64) Batch
      {
         Container<T, BatchSize> container;
         Batch *next = nullptr;
      };
      // -------------------------------------------------------------------------------------
      /* T：表示要存储在 Batch 中的元素的类型
         BatchSize：表示每个 Batch 的大小（容量）
         Container：表示用于存储元素的容器模板。这是一个模板模板参数，它期望一个接受两个参数的模板，一个是元素类型 T，另一个是大小（容量）size_t。*/
      // 批处理队列
      template <typename T, size_t BatchSize, template <class, size_t> class Container>
      struct BatchQueue
      {
         using B = Batch<T, BatchSize, Container>;
         // 队列过载常量，用于调整队列容量
         static constexpr double oversubscription = 1.1;
         // -------------------------------------------------------------------------------------

         // 队列元数据结构，用于跟踪队列状态
         struct alignas(64) Metadata
         {
            B *head{nullptr};  // 是一批不是一个元素
            uint64_t size{0}; // number of batches inside the queue
         };
         // -------------------------------------------------------------------------------------

         // 队列的属性和状态信息
         alignas(64) std::atomic<uint64_t> elements{0}; // serves as guard variable for pop
         const size_t size;
         std::unique_ptr<B[]> batches;  // 指向B[]的智能指针
         SpinLock latch;
         Metadata full;
         Metadata empty;
         // -------------------------------------------------------------------------------------
         // 构造函数，初始化队列（(size_ / BatchSize) * oversubscription：调整队列的大小）
         BatchQueue(size_t size_) : size((size_ / BatchSize) * oversubscription), batches(std::make_unique<B[]>(size))
         {
            // 初始化每个数组元素
            std::for_each(batches.get(), batches.get() + size, [&](auto &batch)
                          { push_empty(&batch); });
         }
         // -------------------------------------------------------------------------------------
         // 重置队列状态
         void reset()
         {
            if (full.size == 0)
            {
               return; // no elements to clean
            }
            assert(full.head);
            // 重置元素
            B *full_batch{nullptr};
            while (try_pop_full(full_batch))
            {
               full_batch->container.reset();
               auto rc = try_push_empty(full_batch);
               if (!rc)
                  throw std::runtime_error("Cannot push to empty queue");
            }
            assert(full.size == 0);
            assert_no_duplicates();
         }
         // 打印队列大小信息
         void print_size()
         {
            std::cout << "e " << empty.size << "\n";
            std::cout << "f " << full.size << "\n";
            std::cout << "s" << size << std::endl;
         }
         // -------------------------------------------------------------------------------------
         // 断言没有内存泄漏
         void assert_no_leaks()
         {
            if ((empty.size + full.size) != size)
               throw std::runtime_error("Leaks detected");
         }
         // 增加队列中元素的数量
         inline void increment_elements(size_t inc_size)
         {
            auto tmp_e = elements.load(); // 确保读取是原子的
            elements.store(tmp_e + inc_size, std::memory_order_release);
         };
         // 减少队列中元素的数量
         inline void decrement_elements(size_t dec_size)
         {
            auto tmp_e = elements.load();
            elements.store(tmp_e - dec_size, std::memory_order_release);
         };
         // 断言队列中没有重复的元素
         void assert_no_duplicates()
         {
            std::vector<uint64_t> histogram(size); //记录每个元素在队列中出现的次数。
            // 用于在 histogram 中更新和检查元素的出现次数
            auto idx = [&](B *batch) 
            { return (batch - &batches[0]); };
            B *current = full.head;
            while (current)
            {
               histogram[idx(current)]++;
               current = current->next;
            }
            current = empty.head;
            while (current)
            {
               histogram[idx(current)]++;
               // 检查是否重复
               if (histogram[idx(current)] > 1)
               {
                  throw std::runtime_error("found duplicate at idx " + std::to_string(idx(current)));
               }
               current = current->next;
            }
         }
         // -------------------------------------------------------------------------------------
         // 尝试将批次推入 full 状态
         [[nodiscard]] bool try_push_full(B *&batch)
         {
            latch.lock(); // spinning lock as we want to allow inserts to complete
            batch->next = full.head;
            increment_elements(batch->container.get_size());
            full.head = batch;
            full.size++;
            latch.unlock();
            return true;
         }
         // -------------------------------------------------------------------------------------
         // 尝试将批次推入 empty 状态
         [[nodiscard]] bool try_push_empty(B *&batch)
         {
            latch.lock(); // spinning lock as we want to allow inserts to complete
            batch->next = empty.head;
            empty.head = batch;
            assert(batch->container.get_size() == 0);
            empty.size++;
            latch.unlock();
            return true;
         }
         // -------------------------------------------------------------------------------------
         // 尝试从 empty 状态弹出批次
         [[nodiscard]] bool try_pop_empty(B *&batch)
         {
            latch.lock();
            // -------------------------------------------------------------------------------------
            if (!empty.head)
            {
               latch.unlock();
               return false;
            }
            // -------------------------------------------------------------------------------------
            batch = empty.head;
            empty.head = empty.head->next;
            assert(batch->container.get_size() == 0);
            empty.size--;
            latch.unlock();
            // -------------------------------------------------------------------------------------
            return true;
         }
         // 将批次推入 full 状态并尝试从 empty 状态弹出批次
         [[nodiscard]] bool push_full_and_try_pop_empty(B *&batch)
         {
            while (!latch.try_lock())
               ; // spinning without backoff to reduce latency

            if (!empty.head)
            {
               latch.unlock();
               return false;
            }
            batch->next = full.head;
            increment_elements(batch->container.get_size());
            full.head = batch;
            full.size++;
            // pop empty
            batch = empty.head;
            empty.head = empty.head->next;
            assert(batch->container.get_size() == 0);
            empty.size--;
            latch.unlock();
            return true;
         }
         // 将批次推入 empty 状态并尝试从 full 状态弹出批次
         [[nodiscard]] bool push_empty_and_try_pop_full(B *&batch)
         {
            if (elements.load() == 0)
            {
               _mm_pause(); // todo remove to outside
               return false;
            }
            // -------------------------------------------------------------------------------------
            if (!latch.try_lock())
            {
               _mm_pause(); // make outside
               return false;
            }
            // -------------------------------------------------------------------------------------
            if (!full.head)
            {
               latch.unlock();
               return false;
            }
            // push empty
            assert(batch->container.get_size() == 0);
            batch->next = empty.head;
            empty.head = batch;
            empty.size++;
            // pop full
            batch = full.head;
            full.head = full.head->next;
            decrement_elements(batch->container.get_size());
            full.size--;
            latch.unlock();
            return true;
         }

         // -------------------------------------------------------------------------------------
         // 尝试从 full 状态弹出批次
         [[nodiscard]] bool try_pop_full(B *&batch)
         {
            // -------------------------------------------------------------------------------------
            // check guard variable first to prevent contention of spinlock
            if (elements.load() == 0)
            {
               _mm_pause(); // todo remove to outside
               return false;
            }
            // -------------------------------------------------------------------------------------
            if (!latch.try_lock())
            {
               _mm_pause(); // make outside
               return false;
            }
            // -------------------------------------------------------------------------------------
            if (!full.head)
            {
               latch.unlock();
               return false;
            }
            // -------------------------------------------------------------------------------------
            batch = full.head;
            full.head = full.head->next;
            decrement_elements(batch->container.get_size());
            full.size--;
            latch.unlock();
            return true;
         }
         // -------------------------------------------------------------------------------------
         // 获取empty状态的一个Batch
         B *pop_empty()
         {
            B *ret{nullptr};
            while (!try_pop_empty(ret))
               ;
            assert(ret->container.get_size() == 0);
            return ret;
         }
         // -------------------------------------------------------------------------------------
         // 获取full状态的一个Batch
         B *pop_full()
         {
            B *ret{nullptr};
            while (!try_pop_full(ret))
               ;
            assert(ret->container.get_size() > 0);
            return ret;
         }
         // -------------------------------------------------------------------------------------
         // 将Batch推入empty
         void push_empty(B *batch)
         {
            assert(batch->container.get_size() == 0);
            while (!try_push_empty(batch))
               ;
         }
         // -------------------------------------------------------------------------------------
         // 将Batch推入full
         void push_full(B *batch)
         {
            assert(batch->container.get_size() > 0);
            while (!try_push_full(batch))
               ;
         }
         // -------------------------------------------------------------------------------------
         // 读取elements的值
         uint64_t approx_size() { return elements.load(); }
      };

   } // namespace utils
} // namespace scalestore
