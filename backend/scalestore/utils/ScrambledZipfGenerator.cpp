#include "ScrambledZipfGenerator.hpp"
// -------------------------------------------------------------------------------------
// -------------------------------------------------------------------------------------
// -------------------------------------------------------------------------------------
namespace scalestore
{
   namespace utils
   {
      // 用于生成经过处理的 Zipf 分布的伪随机数
      // -------------------------------------------------------------------------------------
      u64 ScrambledZipfGenerator::rand()
      {
         u64 zipf_value = zipf_generator(gen);
         // 对 Zipf 分布的值进行哈希处理。
         return min + (scalestore::utils::FNV::hash(zipf_value) % n);
      }
      // -------------------------------------------------------------------------------------
      u64 ScrambledZipfGenerator::rand(u64 offset)
      {
         u64 zipf_value = zipf_generator(gen);
         // 对 Zipf 分布的值进行偏移处理
         // % n 将偏移处理后的值映射到指定的范围 [min, max)。
         return (min + ((scalestore::utils::FNV::hash(zipf_value + offset)) % n));
      }

   } // namespace utils
} // namespace scalestore
