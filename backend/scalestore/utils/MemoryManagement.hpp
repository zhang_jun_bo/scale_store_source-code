#pragma once
// -------------------------------------------------------------------------------------
#include <sys/mman.h>
#include <cassert>
#include <iostream>
#include <mutex>
#include <memory_resource>
// -------------------------------------------------------------------------------------
namespace scalestore
{
   namespace utils
   {
      // -------------------------------------------------------------------------------------
      // helper classes
      // -------------------------------------------------------------------------------------

      // RAII for huge pages
      // 封装 HugePages 的分配和释放
      template <typename T>
      class HugePages
      {
         T *memory;
         size_t size;          // in bytes
         size_t highWaterMark; // max index
      public:
         HugePages(size_t size) : size(size)
         {
            // 内核选择合适的地址来映射、大小、分配的内存可读可写、使用私有映射、匿名映射
            void *p = mmap(NULL, size, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | MAP_HUGETLB, -1, 0);
            if (p == MAP_FAILED)
               throw std::runtime_error("mallocHugePages failed");
            // 将分配的内存强制类型转换为 T* 类型，并存储到类的成员变量 memory 中
            memory = static_cast<T *>(p);
            // 计算分配的内存区域的高水位标记（max index）类似阈值
            highWaterMark = (size / sizeof(T));
         }

         // 返回当前分配的内存区域的最大索引
         size_t get_size()
         {
            return highWaterMark;
         }

         // 将对象转换为 T* 类型的指针，使得可以直接使用对象作为指针来访问分配的内存区域。
         inline operator T *() { return memory; }

         // 通过索引访问分配的内存区域中的元素
         inline T &operator[](size_t index) const
         {
            return memory[index];
         }
         // 释放通过 mmap 分配的内存。
         ~HugePages() { munmap(memory, size); }
      };

      // -------------------------------------------------------------------------------------
      // similar to pmr monotonic buffer however does not grow
      // 现了一个单调递增的内存资源，用于按需分配固定大小的缓冲区。
      class MonotonicBufferResource : public std::pmr::memory_resource
      {
      public:
         void *bufferPosition = nullptr; // 指向缓冲区中当前可用位置的指针，随着新的分配而移动。
         size_t bufferSize;
         size_t sizeLeft;
         HugePages<uint8_t> buffer;

         // 初始化变量 ：后面是赋值
         MonotonicBufferResource(size_t bufferSize) : bufferSize(bufferSize), sizeLeft(bufferSize), buffer(bufferSize)
         {
            bufferPosition = buffer;
         };

         // 销毁、返回底层缓冲区的起始位置、返回剩余缓冲区大小、返回整个缓冲区大小
         ~MonotonicBufferResource() {}
         void *getUnderlyingBuffer() { return buffer; }
         size_t getSizeLeft() { return sizeLeft; }
         size_t getBufferSize() { return bufferSize; }

      protected:

         // 重载了 do_allocate 函数，该函数是 std::pmr::memory_resource 接口的一部分，用于分配内存
         void *do_allocate(size_t bytes, size_t alignment) override
         {
            // 内存不足，抛出异常
            if (sizeLeft < bytes)
               throw std::bad_alloc();
            // 按照指定的对齐方式分配内存，并更新 bufferPosition 和 sizeLeft。
            if (std::align(alignment, bytes, bufferPosition, sizeLeft))
            {
               void *result = bufferPosition;
               bufferPosition = (char *)bufferPosition + bytes;
               sizeLeft -= bytes;
               auto iptr = reinterpret_cast<std::uintptr_t>(result);
               if ((iptr % alignment) != 0)
                  throw std::runtime_error("Alignment failed");
               return result;
            }
            throw std::bad_alloc();
         }
         // 重载了 do_deallocate 函数（释放内存），MonotonicBufferResource是单调递增的，不支持释放内存
         void do_deallocate(void * /*p*/, size_t /*bytes*/, size_t /*alignment*/) override
         {
            throw std::runtime_error(" Function not supported in monotonic increasing buffer");
         }
         // 重载了 do_is_equal 函数，用于比较两个内存资源对象是否相等。
         bool do_is_equal(const memory_resource &other) const noexcept override { return this == &other; }
      };

      // 实现了一个线程安全的单调递增内存资源。继承上面的MonotonicBufferRessource类
      class SynchronizedMonotonicBufferRessource : public std::pmr::memory_resource
      {
         MonotonicBufferResource parent; // 用于实际的内存分配
         std::mutex allocteMut;  // 保护对 parent 的分配操作，确保线程安全性。

      public:
         // 构造函数，使用成员初始化列表初始化 parent，并传递 bufferSize 给 MonotonicBufferResource 的构造函数
         SynchronizedMonotonicBufferRessource(size_t bufferSize) : parent(bufferSize){};
         // 分别返回 parent 对象的底层缓冲区起始位置、剩余缓冲区大小和整个缓冲区大小。
         void *getUnderlyingBuffer() { return parent.getUnderlyingBuffer(); }
         size_t getSizeLeft() { return parent.getSizeLeft(); }
         size_t getBufferSize() { return parent.getBufferSize(); }

      protected:
         // 重载了 do_allocate 函数，使用 std::unique_lock 对 allocteMut 进行加锁，确保线程安全的分配内存。然后通过调用 parent.allocate 完成实际的内存分配
         void *do_allocate(size_t bytes, size_t alignment) override
         {
            std::unique_lock<std::mutex> allocLock(allocteMut);
            return parent.allocate(bytes, alignment);
         }
         void do_deallocate(void * /*p*/, size_t /*bytes*/, size_t /*alignment*/) override
         {
            throw std::runtime_error(" Function not supported in monotonic increasing buffer");
         }
         bool do_is_equal(const memory_resource &other) const noexcept override { return this == &other; }
      };

      // -------------------------------------------------------------------------------------
      // wraps a thread local pointer to allow for proper destruction
      // 封装线程本地指针
      template <typename T>
      struct ThreadLocalWrapper
      {
         // 确保 T 是指针类型
         static_assert(std::is_pointer<T>::value, "Expected a pointer");
         // 成员变量，存储线程本地指针，默认初始化为 nullptr
         T cached = nullptr;

          // 重载箭头操作符，方便通过该结构体访问线程本地指针
         inline T operator->() { return cached; }

         // 重载类型转换操作符，方便将结构体转换为线程本地指针
         inline operator T() { return cached; };

          // 重载赋值操作符，将传入的指针赋值给线程本地指针，并返回其引用
         inline T &operator=(const T &other)
         {
            cached = other;
            return cached;
         }
         //析构函数，在结构体被销毁时释放线程本地指针所指向的内存
         ~ThreadLocalWrapper() { delete cached; }
      };

   } // namespace utils
} // namespace scalestore
