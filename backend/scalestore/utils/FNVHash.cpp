#include "FNVHash.hpp"
// -------------------------------------------------------------------------------------
/*
   1、通过一个循环迭代输入值的每个字节，并使用 FNV 算法更新哈希值。具体而言，每个字节都按位异或（XOR）到哈希值上，然后哈希值乘以一个质数。
   2、循环迭代 8 次，因为 u64 类型有 8 个字节。
   3、最终的哈希值被返回作为函数的结果。
*/

namespace scalestore
{
   namespace utils
   // -------------------------------------------------------------------------------------
   {
      // 一个hash算法
      u64 FNV::hash(u64 val)
      {
         // from http://en.wikipedia.org/wiki/Fowler_Noll_Vo_hash
         u64 hash_val = FNV_OFFSET_BASIS_64;
         for (int i = 0; i < 8; i++)
         {
            u64 octet = val & 0x00ff;
            val = val >> 8;

            hash_val = hash_val ^ octet;
            hash_val = hash_val * FNV_PRIME_64;
         }
         return hash_val;
      }
      // -------------------------------------------------------------------------------------
   } // namespace utils
} // namespace scalestore
