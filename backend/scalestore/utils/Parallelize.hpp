#pragma once
// -------------------------------------------------------------------------------------
#include "Defs.hpp"
#include "../threads/Concurrency.hpp"
// -------------------------------------------------------------------------------------
namespace scalestore
{
   namespace utils
   {
      // 用于并行执行某个函数
      class Parallelize
      {

      public:
         // 使用hardware_concurrency硬件线程数
         template <typename F>
         static void parallelRange(uint64_t n, F function)
         {
            parallelRange(std::thread::hardware_concurrency(), n, function);
         }

         // 用户可以指定线程数
         template <typename F>
         static void parallelRange(int threads, uint64_t n, F function)
         {
            ensure(threads > 0);
            concurrency::WorkerGroup g(threads);   // 创建线程
            const uint64_t blockSize = n / threads;   // 计算每个线程处理的数据块大小
            ensure(blockSize > 0);
            g.run([&](int workerId)
                  {
               auto begin = workerId * blockSize;
               auto end = begin + blockSize;
               if(workerId == threads - 1)
                  end = n;
               function(begin,end); });
            g.wait();
         }
      };

   } // utils
} // scalestore
