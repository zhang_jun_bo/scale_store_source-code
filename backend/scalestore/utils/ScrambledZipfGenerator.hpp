#include "FNVHash.hpp"
#include "Defs.hpp"
// #include "ZipfGenerator.hpp"
#include "ZipfRejectionInversion.hpp"
// -------------------------------------------------------------------------------------
namespace scalestore
{
  namespace utils
  {

    class ScrambledZipfGenerator
    {
    public:
      u64 min, max, n;  // 下限、上限、随机数
      double theta; // Zipf的参数
      std::random_device rd;  // 用于生成真正随机数的 random_device
      std::mt19937 gen; // Mersenne Twister 伪随机数生成器
      zipf_distribution<> zipf_generator; // Zipf 分布生成器
      // 10000000000ul
      // [min, max)
      ScrambledZipfGenerator(u64 min, u64 max, double theta) : min(min), max(max), n(max - min), gen(rd()), zipf_generator((max - min) * 2, theta)
      {
      }
      u64 rand();
      u64 rand(u64 offset);
    };

    // -------------------------------------------------------------------------------------
  } // namespace utils
} // namespace scalestore
