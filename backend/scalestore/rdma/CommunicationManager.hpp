#pragma once
// -------------------------------------------------------------------------------------
#include "Defs.hpp"
#include "scalestore/Config.hpp"
#include "scalestore/utils/MemoryManagement.hpp"
// -------------------------------------------------------------------------------------
#include <arpa/inet.h>
#include <atomic>
#include <cassert>
#include <fstream> // std::ifstream
#include <gflags/gflags.h>
#include <infiniband/verbs.h>
#include <iostream>
#include <memory_resource>
#include <mutex>
#include <netdb.h>
#include <netinet/in.h>
#include <rdma/rdma_cma.h>
#include <string>
#include <sys/mman.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <thread>
#include <unistd.h>
#include <unordered_map>

static int debug = 0;
#define DEBUG_LOG(msg)                                                         \
  if (debug)                                                                   \
  std::cout << msg << std::endl
#define DEBUG_VAR(var)                                                         \
  if (debug)                                                                   \
  std::cout << #var << " " << var << std::endl

#define USE_INLINE 1

namespace scalestore {
namespace rdma {

// smaller inline size reduces WQE, max with our cards would be 220
static constexpr uint64_t INLINE_SIZE = 64; // LARGEST MESSAGE，表示内联大小为64。

enum completion : bool {//枚举类型
  signaled = true,
  unsignaled = false,
};
// application specific
enum Type {//枚举类型，用于表示应用程序特定的类型
  RDMA_CM = 0,
  WORKER = 1,
  PAGE_PROVIDER = 2,
  MESSAGE_HANDLER = 3,
};

struct RdmaContext {
  void *applicationData;//指向应用程序数据的指针。
  uint32_t rkey;//表示RDMA内存区域的rkey。
  ibv_mr *mr;//指向ibv_mr结构体的指针，用于描述RDMA内存区域。
  rdma_cm_id *id;//指向rdma_cm_id结构体的指针，用于表示RDMA通信标识符。
  Type type;//表示RDMA上下文的类型，使用Type枚举类型。
  uint64_t typeId;//表示类型ID。
  NodeID nodeId;//表示节点ID。
};

// -------------------------------------------------------------------------------------
// Compile time batching
// -------------------------------------------------------------------------------------

struct RDMABatchElement {//用于描述RDMA批处理元素的相关信息。
  void *memAddr;//指向内存地址的指针，表示要进行RDMA操作的内存地址。
  size_t size;//表示要进行RDMA操作的数据大小。
  size_t remoteOffset;//表示远程偏移量，用于指定远程内存中的偏移位置。
};

template <typename... ARGS>
void postWriteBatch(RdmaContext &context, completion wc, ARGS &&...args) {//用于批量发送RDMA写操作。
  constexpr uint64_t numberElements =
      std::tuple_size<std::tuple<ARGS...>>::value;

  auto *qp = context.id->qp;
  auto *mr = context.mr;
  auto rkey = context.rkey;
//计算ARGS参数包的数量
  struct ibv_send_wr sq_wr[numberElements];//用于描述RDMA写操作的参数
  struct ibv_sge send_sgl[numberElements];//用于描述RDMA写操作的参数
  struct ibv_send_wr *bad_wr;//用于描述RDMA写操作的参数
  RDMABatchElement element[numberElements] = {args...};

  for (uint64_t b_i = 0; b_i < numberElements; b_i++) {
    send_sgl[b_i].addr = (uint64_t)(unsigned long)element[b_i].memAddr;
    send_sgl[b_i].length = element[b_i].size;
    send_sgl[b_i].lkey = mr->lkey;
    sq_wr[b_i].opcode = IBV_WR_RDMA_WRITE;
    sq_wr[b_i].send_flags =
        ((b_i == numberElements - 1) && wc) ? IBV_SEND_SIGNALED : 0;
#ifdef USE_INLINE
    sq_wr[b_i].send_flags |=
        (element[b_i].size <= INLINE_SIZE) ? IBV_SEND_INLINE : 0;
#endif
    sq_wr[b_i].sg_list = &send_sgl[b_i];
    sq_wr[b_i].num_sge = 1;
    sq_wr[b_i].wr.rdma.rkey = rkey;
    sq_wr[b_i].wr.rdma.remote_addr = element[b_i].remoteOffset;
    sq_wr[b_i].wr_id = 0;
    sq_wr[b_i].next =
        (b_i == numberElements - 1)
            ? nullptr
            : &sq_wr[b_i + 1]; // do not forget to set this otherwise it crashes
  }
//根据ARGS参数包的数量，循环设置每个ibv_send_wr和ibv_sge结构体的属性，包括内存地址、大小、远程偏移量等
  auto ret = ibv_post_send(qp, &sq_wr[0], &bad_wr);//调用ibv_post_send函数将写操作请求提交给RDMA设备进行发送。
  if (ret)
    throw std::runtime_error("Failed to post send request" +
                             std::to_string(ret) + " " + std::to_string(errno));
}//如果发送操作失败，则抛出std::runtime_error异常。

// -------------------------------------------------------------------------------------

inline void postReceive(void *memAddr, size_t size, ibv_qp *qp, ibv_mr *mr) {//用于发送RDMA接收操作的函数postReceive。内存地址memAddr、数据大小size、指向接收端队列对列(QP)的指针qp和指向内存区域注册信息的指针mr作为参数。
  struct ibv_recv_wr rq_wr; /* recv work request record 用于描述接收操作的参数*/
  struct ibv_sge recv_sgl;  /* recv single SGE 用于描述数据传输的信息 */
  struct ibv_recv_wr *bad_wr;
  recv_sgl.addr =
      (uintptr_t)memAddr; // important to use address of buffer not of ptr
  recv_sgl.length = size;
  recv_sgl.lkey = mr->lkey;//根据传入的参数设置ibv_sge结构体的相关属性
  rq_wr.sg_list = &recv_sgl;//将ibv_sge结构体的指针赋值给ibv_recv_wr结构体的sg_list成员
  rq_wr.num_sge = 1;
  rq_wr.next = nullptr;
  rq_wr.wr_id = 1;
  auto ret = ibv_post_recv(qp, &rq_wr, &bad_wr); // returns 0 on success 调用ibv_post_recv函数将接收操作请求提交给RDMA设备进行处理。
  if (ret)
    throw std::runtime_error("Failed to post receive to QP with errno " +
                             std::to_string(ret));
}//异常处理

template <typename T>
inline void postReceive(T *memAddr, ibv_qp *qp, ibv_mr *mr) {//该函数接受指向数据的内存地址memAddr、指向接收端队列对列(QP)的指针qp和指向内存区域注册信息的指针mr作为参数。
  static_assert(!std::is_void<T>::value,
                "post receive cannot be called with void use other function");
  postReceive(memAddr, sizeof(T), qp, mr);
}//该函数使用static_assert来确保模板类型T不是void类型，然后调用了另一个重载的postReceive函数，将数据大小sizeof(T)也作为参数传递给它。

template <typename T>
inline void postReceive(T *memAddr, RdmaContext &context) {//该函数接受指向数据的内存地址memAddr和一个RdmaContext对象context作为参数。
  static_assert(!std::is_void<T>::value,
                "post receive cannot be called with void use other function");
  postReceive(memAddr, sizeof(T), context.id->qp, context.mr);
}//使用static_assert来确保模板类型T不是void类型，然后调用了前面提到的重载的postReceive函数，将数据大小sizeof(T)、context.id->qp和context.mr也作为参数传递给它。

inline void postSend(void *memAddr, size_t size, ibv_qp *qp, ibv_mr *mr,
                     completion wc) {//用于执行RDMA发送操作，该函数接受指向数据的内存地址memAddr、数据大小size、指向发送端队列对列(QP)的指针qp、指向内存区域注册信息的指针mr和一个completion对象wc作为参数。
  struct ibv_send_wr sq_wr;
  struct ibv_sge send_sgl;
  struct ibv_send_wr *bad_wr;//三个结构体 用于描述发送操作的参数和数据传输的信息。
  send_sgl.addr = (uintptr_t)memAddr;
  send_sgl.length = size;
  send_sgl.lkey = mr->lkey;
  sq_wr.opcode = IBV_WR_SEND;
  sq_wr.send_flags = wc ? IBV_SEND_SIGNALED : 0;//根据传入的参数设置了相关属性
#ifdef USE_INLINE
  sq_wr.send_flags |= (size <= INLINE_SIZE) ? IBV_SEND_INLINE : 0;
#endif
  sq_wr.sg_list = &send_sgl;
  sq_wr.num_sge = 1;
  sq_wr.next = nullptr; // do not forget to set this otherwise it  crashes
  auto ret = ibv_post_send(qp, &sq_wr, &bad_wr);//调用ibv_post_send函数将发送操作请求提交给RDMA设备进行处理。
  if (ret)
    throw std::runtime_error("Failed to post send request");
}//异常检测

template <typename T>
inline void postSend(T *memAddr, ibv_qp *qp, ibv_mr *mr, completion wc) {
  static_assert(!std::is_void<T>::value,
                " post send cannot be called with void");
  postSend(memAddr, sizeof(T), qp, mr, wc);
}
/*
该函数接受指向数据的内存地址memAddr、指向发送端队列对列(QP)的指针qp、指向内存区域注册信息的指针mr和一个completion对象wc作为参数。
该函数使用static_assert来确保模板类型T不是void类型，然后调用了另一个重载的postSend函数，将数据大小sizeof(T)也作为参数传递给它。
*/
template <typename T>
inline void postSend(T *memAddr, RdmaContext &context, completion wc) {
  static_assert(!std::is_void<T>::value,
                " post send cannot be called with void");
  postSend(memAddr, sizeof(T), context.id->qp, context.mr, wc);
}
/*
该函数接受指向数据的内存地址memAddr和一个RdmaContext对象context作为参数。
使用static_assert来确保模板类型T不是void类型，然后调用了前面提到的重载的postSend函数，将数据大小sizeof(T)、context.id->qp和context.mr也作为参数传递给它。
*/
inline void postWrite(void *memAddr, size_t size, ibv_qp *qp, ibv_mr *mr,
                      completion wc, size_t rkey, size_t remoteOffset) {//用于在RDMA环境中进行写操作的函数postWrite。
                               //接受指向数据的内存地址memAddr、数据大小size、指向发送端队列对列(QP)的指针qp、指向内存区域注册信息的指针mr、一个completion对象wc、远程内存区域的rkey和remoteOffset作为参数。
  struct ibv_send_wr sq_wr;
  struct ibv_sge send_sgl;
  struct ibv_send_wr *bad_wr;//用于描述写操作的参数和数据传输的信息
  send_sgl.addr = (uint64_t)(unsigned long)memAddr;
  send_sgl.length = size;
  send_sgl.lkey = mr->lkey;
  sq_wr.opcode = IBV_WR_RDMA_WRITE;
  sq_wr.send_flags = wc ? IBV_SEND_SIGNALED : 0;//根据传入的参数设置了相关属性
#ifdef USE_INLINE
  sq_wr.send_flags |= (size <= INLINE_SIZE) ? IBV_SEND_INLINE : 0;
#endif
  sq_wr.sg_list = &send_sgl;
  sq_wr.num_sge = 1;
  sq_wr.wr.rdma.rkey = rkey;
  sq_wr.wr.rdma.remote_addr = remoteOffset;
  sq_wr.next = nullptr; // do not forget to set this otherwise it  crashes根据传入的参数设置了相关属性
  auto ret = ibv_post_send(qp, &sq_wr, &bad_wr);//调用ibv_post_send函数将写操作请求提交给RDMA设备进行处理。
  if (ret)
    throw std::runtime_error("Failed to post send request" +
                             std::to_string(ret) + " " + std::to_string(errno));
}

template <typename T>
inline void postWrite(T *memAddr, ibv_qp *qp, ibv_mr *mr, completion wc,
                      size_t rkey, size_t remoteOffset) {//size_t rkey 表示远程节点的 MR 访问键
                      //参数包括一个指向要写入数据的内存地址 memAddr，一个表示数据类型 T 的指针，一个指向 QP（Queue Pair）的指针 qp，一个指向 MR（Memory Region）的指针 mr，以及一个 IBV（Infiniband Verbs）完成事件 wc。
  static_assert(!std::is_void<T>::value,
                "post write cannot be called with void");
  postWrite(memAddr, sizeof(T), qp, mr, wc, rkey, remoteOffset);//函数通过指定数据类型的大小 sizeof(T) 来调用另一个函数模板 postWrite。
}

template <typename T>
inline void postWrite(T *memAddr, RdmaContext &context, completion wc,
                      size_t remoteOffset) {//RdmaContext 类型包括 QP、MR、rkey 等 RDMA 相关的信息
                      //参数包括一个指向要写入数据的内存地址 memAddr，RdmaContext 类型的引用 context，以及一个表示远程内存偏移的大小 remoteOffset
  static_assert(!std::is_void<T>::value,
                "post write cannot be called with void");
  postWrite(memAddr, sizeof(T), context.id->qp, context.mr, wc, context.rkey,
            remoteOffset);
}//函数模板通过指定数据类型的大小 sizeof(T) 和 RdmaContext 的信息来调用另一个函数模板 postWrite。

inline void postRead(void *memAddr, size_t size, ibv_qp *qp, ibv_mr *mr,
                     completion wc, size_t rkey, size_t remoteOffset,
                     size_t wcId = 0) {//size_t rkey 表示远程节点的 MR 访问键，size_t remoteOffset 表示远程节点的内存偏移。size_t wcId 表示完成事件的 ID，默认值为 0。
                     //函数的参数包括一个指向要读取数据的内存地址 memAddr，一个表示数据大小的 size，一个指向 QP（Queue Pair）的指针 qp，一个指向 MR（Memory Region）的指针 mr，以及一个 IBV（Infiniband Verbs）完成事件 wc
  struct ibv_send_wr sq_wr;
  struct ibv_sge send_sgl;
  struct ibv_send_wr *bad_wr;
  //ibv_send_wr 结构体表示发送端点的工作请求，其中 opcode 设置为 IBV_WR_RDMA_READ 表示读操作，send_flags 根据 wc 的值来设置是否发送信号。sg_list 设置为指向 send_sgl 的地址，num_sge 设置为 1 表示只有一个 SGE。
  send_sgl.addr = (uint64_t)(unsigned long)memAddr;
  send_sgl.length = size;
  send_sgl.lkey = mr->lkey;
  //ibv_sge 结构体表示单个 SGE（Scatter/Gather Element），用于描述一个连续的内存区域。在这里，addr 设置为 memAddr 的地址，length 设置为数据大小 size，lkey 设置为 MR 的本地键。
  sq_wr.opcode = IBV_WR_RDMA_READ;
  sq_wr.send_flags = wc ? IBV_SEND_SIGNALED : 0;
  sq_wr.sg_list = &send_sgl;
  sq_wr.num_sge = 1;
  sq_wr.wr.rdma.rkey = rkey;
  sq_wr.wr.rdma.remote_addr = remoteOffset;
  sq_wr.wr_id = wcId;
  sq_wr.next = nullptr; // do not forget to set this otherwise it  crashes

  //通过填充 sq_wr 结构体的成员来配置发送请求。wr.rdma.rkey 设置为远程节点的 MR 访问键，wr.rdma.remote_addr 设置为远程节点的内存偏移。wr_id 设置为 wcId 表示请求的 ID。next 设置为 nullptr 来表示当前请求是发送队列的最后一个请求。
  auto ret = ibv_post_send(qp, &sq_wr, &bad_wr);
  if (ret)
    throw std::runtime_error("Failed to post send request" +
                             std::to_string(ret) + " " + std::to_string(errno));
}

template <typename T>
inline void postRead(T *memAddr, ibv_qp *qp, ibv_mr *mr, completion wc,
                     size_t rkey, size_t remoteOffset, size_t wcId = 0) {
  static_assert(!std::is_void<T>::value,
                "post write cannot be called with void");
  postRead(memAddr, sizeof(T), qp, mr, wc, rkey, remoteOffset, wcId);
}

template <typename T>
inline void postRead(T *memAddr, RdmaContext &context, completion wc,
                     size_t remoteOffset, size_t wcId = 0) {
  static_assert(!std::is_void<T>::value,
                "post write cannot be called with void");
  postRead(memAddr, sizeof(T), context.id->qp, context.mr, wc, context.rkey,
           remoteOffset, wcId);
}

// low level wrapper; once returned every wc need to be checked for success
inline int pollCompletion(ibv_cq *cq, size_t expected, ibv_wc *wcReturn) {//用于从完成队列（Completion Queue）中轮询获取完成事件。
    //函数的参数包括指向完成队列的指针 cq，一个期望的完成事件数量 expected，以及一个用于存储完成事件的 IBV（Infiniband Verbs）工作完成结构体数组 wcReturn。
  int numCompletions{0};//首先初始化一个整数变量 numCompletions 为 0，用于计数获取的完成事件数量。
  numCompletions = ibv_poll_cq(cq, expected, wcReturn);//调用 ibv_poll_cq 函数从完成队列中获取完成事件。
  //cq 参数表示要轮询的完成队列，expected 参数表示期望获取的完成事件数量，wcReturn 参数表示一个数组，用于存储获取的完成事件。获取完成事件的数量将会存储在 numCompletions 变量中。
  if (numCompletions < 0)
    throw std::runtime_error("Poll cq failed");
  return numCompletions;
}

// lambda function can be passed to evaluate status etc.
template <typename F>
inline int pollCompletion(ibv_cq *cq, size_t expected, ibv_wc *wcReturn,
                          F &func) {
  int numCompletions = pollCompletion(cq, expected, wcReturn);
  func(numCompletions);
  return numCompletions;
}

template <typename INITIAL_MSG> class CM {//模板类 模板参数是 INITIAL_MSG
public:
  //! Default constructor
  CM()//用于管理 RDMA 连接
      : port(htons(FLAGS_port)),
        mbr(FLAGS_dramGB * FLAGS_rdmaMemoryFactor * 1024 * 1024 * 1024),
        running(true), handler(&CM::handle, this) {
    // create thread
    incomingChannel = rdma_create_event_channel();//初始化成员变量 包括端口号、内存大小、运行状态标志、事件处理器。
    if (!incomingChannel)
      throw std::runtime_error("Could not create rdma_event_channels");
//创建rdma_event_channel 创建失败则抛出异常
    int ret =
        rdma_create_id(incomingChannel, &incomingCmId, nullptr, RDMA_PS_TCP);
    if (ret == -1)
      throw std::runtime_error("Could not create id");
//创建rdma id 创建失败则抛出异常
    // create rdma ressources
    struct sockaddr_storage sin;
    getAddr(FLAGS_ownIp, (struct sockaddr *)&sin);//使用 getAddr 函数获取 IP 地址信息，并将其绑定到一个 socket 地址结构体 sockaddr_storage 上
    bindHandler(sin);//调用 bindHandler 函数将 sockaddr_storage 传递给 incomingCmId 进行绑定操作。
    createPD(incomingCmId); // single pd shared between clients and server
    createMR();             // single mr shared between clients and server
    //调用 createPD 和 createMR 函数创建一个 protection domain 和一个 memory region，它们将被客户端和服务器共享使用
    // sendCq = createCQ(incomingCmId);  // completion queue for all incoming
    // requests

    if (FLAGS_nodes == 1)
      running = false;//如果节点数为 1，则设置 running 标志为 false。
    initialized = true; // sync handler thread，将 initialized 标志设置为 true，以同步处理器线程。
  }

  ~CM() {//析构函数，负责在对象销毁时进行请理操作
    // disconnect and delete application context
    for (auto *context : outgoingIds) {
      [[maybe_unused]] auto ret = rdma_disconnect(context->id);
      assert(ret == 0);
    }
    for (auto *context : incomingIds) {
      [[maybe_unused]] auto ret = rdma_disconnect(context->id);
      assert(ret == 0);
    }
    // drain disconnect events
    for (auto *c : outgoingChannels) {
      struct rdma_cm_event *event;
      [[maybe_unused]] auto ret = rdma_get_cm_event(c, &event);
      rdma_ack_cm_event(event);
      assert(ret == 0);
    }
    for (auto *context : outgoingIds) {
      rdma_destroy_qp(context->id);
    }
    for (auto *cq : outgoingCqs) {
      ibv_destroy_cq(cq);
    }
    for (auto *context : outgoingIds) {
      rdma_destroy_id(context->id);
    }

    std::vector<ibv_cq *> cqs; // order is important
    for (auto *context : incomingIds) {
      cqs.push_back(context->id->qp->send_cq);
    }
    for (auto *context : incomingIds) {
      rdma_destroy_qp(context->id);
    }

    for (auto *cq : cqs) {
      ibv_destroy_cq(cq);
    }

    for (auto *context : incomingIds) {
      rdma_destroy_id(context->id);
    }

    rdma_destroy_id(incomingCmId);
    rdma_destroy_event_channel(incomingChannel);

    for (auto *c : outgoingChannels) {
      rdma_destroy_event_channel(c);
    }
    ibv_dereg_mr(mr);
    ibv_dealloc_pd(pd);
    handler.join(); // handler joins last to drain incoming disconnection events

    for (auto *context : outgoingIds)
      delete context;
    for (auto *context : incomingIds)
      delete context;
  }

  void handle() {//用于处理 RDMA（Remote Direct Memory Access，远程直接内存访问）连接的事件
    // wait for initialization
    while (!initialized)//等待初始化完成。在循环中判断是否完成初始化，如果未完成，则阻塞等待。
      ; // block
    std::unordered_map<uintptr_t, ComSetupContext *> connections;//创建一个unordered_map，用于存储RDMA连接的上下文信息。
    while (running) {//进入主循环，处理事件。
      struct rdma_cm_event *event;
      auto ret = rdma_get_cm_event(incomingChannel, &event);//通过调用rdma_get_cm_event()函数获得一个RDMA CM事件。
      if (ret)//
        throw;
      struct rdma_cm_id *currentId = event->id;
      ComSetupContext *context;
      if (event->event ==
          RDMA_CM_EVENT_CONNECT_REQUEST) // if new connection create context
          //判断事件类型。如果是RDMA_CM_EVENT_CONNECT_REQUEST类型的事件，表示有新的连接请求，创建一个新的连接上下文，并将其添加到unordered_map中。
        connections[(uintptr_t)currentId] = new ComSetupContext;
      context = connections[(uintptr_t)currentId];//获取当前连接的上下文
      ensure(context != nullptr);

      switch (event->event) {
      case RDMA_CM_EVENT_ADDR_RESOLVED:
        DEBUG_LOG("RDMA_CM_EVENT_ADDR_RESOLVED");
        break;
      case RDMA_CM_EVENT_ROUTE_RESOLVED:
        DEBUG_LOG("RDMA_CM_EVENT_ROUTE_RESOLVED");
        break;
      case RDMA_CM_EVENT_CONNECT_REQUEST: {
        DEBUG_LOG("RDMA_CM_EVENT_CONNECT_REQUEST received");
        numberConnections++;//增加连接计数
        rdma_ack_cm_event(event);//发送连接确认事件
        ibv_cq *incomingCQ = createCQ(currentId);
        createQP(currentId, incomingCQ);//创建Completion Queue（CQ）和Queue Pair（QP），为接收数据准备缓冲区
        context->response =
            static_cast<RdmaInfo *>(mbr.allocate(sizeof(RdmaInfo)));
        postReceive(context->response, currentId->qp, mr);//分配接收数据的缓冲区，调用postReceive()函数。
        auto *applicationData =
            static_cast<INITIAL_MSG *>(mbr.allocate(sizeof(INITIAL_MSG)));
        auto *rdmaContext = createRdmaContext(currentId, applicationData);
        currentId->context = rdmaContext;
          //创建RDMA上下文，将上下文与连接关联起来，配置连接参数，并接受连接。
        struct rdma_conn_param conn_param;
        memset(&conn_param, 0, sizeof conn_param);

        // std::cout << RDMA_MAX_RESP_RES << std::endl;
        // std::cout << RDMA_MAX_INIT_DEPTH << std::endl;
        // conn_param.responder_resources = RDMA_MAX_RESP_RES;
        // conn_param.initiator_depth = RDMA_MAX_INIT_DEPTH;

        conn_param.responder_resources = RDMA_MAX_RESP_RES;
        conn_param.initiator_depth = RDMA_MAX_INIT_DEPTH;

        ret = rdma_accept(currentId, &conn_param);
        if (ret)
          throw std::runtime_error("Rdma accept failed");

        std::unique_lock<std::mutex> l(incomingMut);
        incomingIds.push_back(rdmaContext);
        break;
      }
      case RDMA_CM_EVENT_ESTABLISHED: {//表示连接已建立。打印调试信息，并进行接收操作。
        DEBUG_LOG("RDMA_CM_EVENT_ESTABLISHED");
        exchangeRdmaInfo(currentId, mr, RDMA_CM, 0, 0); // does not know node Id
        ((RdmaContext *)currentId->context)->rkey = context->response->rkey;
        ((RdmaContext *)currentId->context)->type = context->response->type;
        ((RdmaContext *)currentId->context)->typeId = context->response->typeId;
        ((RdmaContext *)currentId->context)->nodeId = context->response->nodeId;
        // std::cout <<"************* received  RKEY << "<<
        // context->response->rkey << std::endl;
        rdma_ack_cm_event(event);
        auto *sock = rdma_get_peer_addr(currentId);
        std::string ip(inet_ntoa(((sockaddr_in *)sock)->sin_addr));
        DEBUG_VAR(ip);
        numberConnectionsEstablished++;
        break;
      }
      case RDMA_CM_EVENT_ADDR_ERROR:
      case RDMA_CM_EVENT_ROUTE_ERROR:
      case RDMA_CM_EVENT_CONNECT_ERROR:
      case RDMA_CM_EVENT_UNREACHABLE:
      case RDMA_CM_EVENT_REJECTED:
        DEBUG_LOG("Unexpected Event");
        break;
      case RDMA_CM_EVENT_DISCONNECTED: {//表示连接已断开。
        DEBUG_LOG("RDMA_CM_EVENT_DISCONNECTED");
        rdma_ack_cm_event(event);//将RDMA上下文从unordered_map中移除。
        numberConnections--;
        numberConnectionsEstablished--;//关闭RDMA连接。
        break;
      }
      case RDMA_CM_EVENT_DEVICE_REMOVAL:
        DEBUG_LOG("RDMA_CM_EVENT_DEVICE_REMOVAL");
        break;
      case RDMA_CM_EVENT_TIMEWAIT_EXIT: {
        DEBUG_LOG("RDMA_CM_EVENT_TIMEWAIT_EXIT");
        rdma_ack_cm_event(event);
        break;
      }
      default:
        throw std::runtime_error("unhandled cm event received");
      }

      if (numberConnections == 0) {
        for (auto &[key, value] : connections) {
          delete value;
        }
        running = false; // no more open connections
        return;
      }
    }
    DEBUG_LOG("Stopped incoming connections handler");
  }

  // should be thread safe
  RdmaContext &initiateConnection(std::string ip, Type type, uint64_t typeId,
                                  NodeID nodeId) {//其功能是与其他节点建立一个RDMA连接，并返回与该连接相关联的上下文。
                                  //四个参数：ip表示目标节点的IP地址，type表示连接的类型，typeId表示连接的类型ID，nodeId表示目标节点的ID。
    auto *response = static_cast<RdmaInfo *>(mbr.allocate(
        sizeof(RdmaInfo))); // to not reallocate every restart and drain memory
        //调用了mbr（Memory Block Resource）中的allocate()函数，分配了一个大小为sizeof(RdmaInfo)的内存块，用于保存RDMA信息。
    auto *applicationData =
        static_cast<INITIAL_MSG *>(mbr.allocate(sizeof(INITIAL_MSG)));
        //函数又调用mbr中的allocate()函数，分配了一个大小为sizeof(INITIAL_MSG)的内存块，用于保存初始化消息。
  RETRY://RETRY标签，用于在连接建立失败后，进行重试操作。
    std::unique_lock<std::mutex> l(outgoingMut);//定义了一个outgoingMut互斥锁，用于保护发送事件的操作。
    rdma_event_channel *outgoingChannel = rdma_create_event_channel();
    if (!outgoingChannel)
      throw std::runtime_error("Could not create outgoing event channel");
        //创建了一个名为outgoingChannel的rdma_event_channel结构体，并用于事件通信。如果不能创建outgoingChannel，则抛出异常。
    struct rdma_cm_id *outgoingCmId;
    auto ret =
        rdma_create_id(outgoingChannel, &outgoingCmId, nullptr, RDMA_PS_TCP);
        //创建一个与outgoingChannel相关联的rdma_cm_id结构体，并将其保存到outgoingCmId中。
    if (ret == -1)
      throw std::runtime_error("Could not create id");
        //创建失败 抛出异常
    struct sockaddr_storage sin;
    getAddr(ip, (struct sockaddr *)&sin);//调用getAddr()函数，将目标节点的IP地址保存到sin中。
    resolveAddr(outgoingChannel, outgoingCmId, sin);//调用resolveAddr()函数，解析目标地址，并将其保存到outgoingCmId结构体中。
    ibv_cq *clientCq = createCQ(outgoingCmId);//调用createCQ()函数，创建一个名为clientCq的completion queue。
    createQP(outgoingCmId, clientCq);//调用createQP()函数，创建一个新的QP，并将其与outgoingCmId相关联。

    postReceive(response, outgoingCmId->qp, mr);//调用postReceive()函数，向QP中发送接收请求，并将其与RDMA信息关联起来，
    auto *rdmaContext = createRdmaContext(outgoingCmId, applicationData);
    outgoingCmId->context = rdmaContext;//将创建的RDMA上下文保存到outgoingCmId结构体中

    struct rdma_cm_event *event;
    struct rdma_conn_param conn_param;
    memset(&conn_param, 0, sizeof conn_param);//函数通过memset()函数将连接参数conn_param的内容全部初始化为0。

    // not yet sure if those have effect if we use plain qp's
    conn_param.responder_resources = RDMA_MAX_RESP_RES;
    conn_param.initiator_depth = RDMA_MAX_INIT_DEPTH;
    if (rdma_connect(outgoingCmId, &conn_param))
      throw std::runtime_error("Could not connect to RDMA endpoint");
    if (rdma_get_cm_event(outgoingChannel, &event))
      throw std::runtime_error("Rdma CM event failed");
    if (event->event != RDMA_CM_EVENT_ESTABLISHED) {
      DEBUG_LOG("Retry with sleep");
      rdma_ack_cm_event(event);
      rdma_destroy_id(outgoingCmId);
      rdma_destroy_event_channel(outgoingChannel);
      delete rdmaContext;
      l.unlock();
      sleep(1);
      goto RETRY;
    };
    //函数调用rdma_connect()函数，建立与目标节点的连接，并传递连接参数conn_param。如果建立连接失败，则函数调用rdma_destroy_qp()函数销毁QP，释放相关资源，并跳转到RETRY标签处，重新尝试建立连接。
    //如果连接成功建立，则函数调用rdma_ack_cm_event()函数，释放上一步的事件，并返回保存RDMA连接上下文的outgoingCmId结构体。
    DEBUG_LOG("Connection established");
    rdma_ack_cm_event(event);//函数调用rdma_ack_cm_event()函数释放连接成功的事件资源。
    exchangeRdmaInfo(outgoingCmId, mr, type, typeId, nodeId);//函数调用exchangeRdmaInfo()函数，通过outgoingCmId建立的连接与目标节点进行RDMA信息交换
    rdmaContext->rkey = response->rkey;
    rdmaContext->type = response->type;
    rdmaContext->typeId = response->typeId;
    rdmaContext->nodeId = response->nodeId;
    //函数将response结构体中的rkey、type、typeId和nodeId分别赋值给rdmaContext结构体的相应成员变量。
    // std::cout << " ****** client got RKEY "  << response->rkey << std::endl;
    outgoingIds.push_back(rdmaContext);
    outgoingCqs.push_back(clientCq);
    outgoingChannels.push_back(outgoingChannel);
    //函数将rdmaContext、clientCq和outgoingChannel分别添加到outgoingIds、outgoingCqs和outgoingChannels的末尾，作为后续维护的可用资源。
    return *rdmaContext;//函数返回保存RDMA连接上下文的rdmaContext引用。
  }

  size_t getNumberIncomingConnections() { return numberConnectionsEstablished; }//函数返回已经建立的“入站”RDMA连接的数量

  // attention no latch/ wait until all connections are finished
  std::vector<RdmaContext *> getIncomingConnections() {//getIncomingConnections()函数获取已经建立的“入站”RDMA连接的信息。
    std::unique_lock<std::mutex> l(incomingMut);//函数内部，首先使用std::unique_lockstd::mutex封装锁对象incomingMut，以获取锁保证线程安全。
    return incomingIds;//函数返回incomingIds，即保存了所有已经建立的“入站”RDMA连接上下文的std::vector
    //由于使用了unique_lock，保证在函数执行过程中incomingMut锁资源不会被其它线程占用，因此可以安全地返回incomingIds。
  };

  utils::SynchronizedMonotonicBufferRessource &getGlobalBuffer() { return mbr; }//函数直接返回了mbr，即保存了全局缓冲区资源的对象。
    //通过返回对mbr的引用，可以在函数外部使用该对象进行操作，如访问缓冲区、分配和释放缓冲区等操作。
  void exchangeInitialMesssage(RdmaContext &context,//函数用于在RDMA连接上下文中交换初始信息
                               INITIAL_MSG *initialMessage) {
    DEBUG_LOG("Exchanging Experimetn Infos");
    rdma::postSend(initialMessage, context, rdma::completion::signaled);
    //将initialMessage指向的缓冲区内容发送到指定的RDMA连接上下文context中，使用rdma::postSend函数进行发送，并设置将要返回的完成通知。该发送函数是一个异步操作，不会阻塞进程
    int completions{0};//已完成的发送操作的数量
    ibv_wc wcs[2];//ibv_wc结构体类型的wcs数组缓冲接收发送完成事件的数据，

    while (completions != 2) {//当completions不等于2，即还有未完成的发送操作时，发起pollCompletion函数进行查询。
      auto expected = 2 - completions; // prevents over polling the q and
                                       // draining it from next phase
      auto comp = pollCompletion(
          context.id->qp->send_cq, expected,
          wcs); // assumes that the completion qs are the same for send and recv
          //该函数将从RDMA发送完成队列中获取当前上下文发送完成的信息，expected为期望的返回数量，wcs指向存放返回信息的缓冲区。该函数具有可阻塞和非阻塞两种模式，在这里使用非阻塞模式。
      for (int i = 0; i < comp; i++) {
        /* verify the completion status */
        if (wcs[i].status != IBV_WC_SUCCESS) {
          throw;
        }//遍历wcs数组中的所有完成通知，并检查其状态是否正常，以及QP号是否与RDMA连接上下文中的QP号相同。
        ensure(wcs[i].qp_num == context.id->qp->qp_num);
      }
      completions += comp;
    }//将wcs数组中的完成通知数量累加到completions后，继续轮询RDMA发送完成队列，直到completions等于2，表示初始信息的交换已经完成。
  }

private:       //RDMA服务器的实现，包含了一些成员变量和内部结构体 用于管理RDMA连接和缓冲区
  struct RdmaInfo {//RdmaInfo结构体用于存储RDMA连接的一些信息
    uint32_t rkey; // remote key
    Type type;
    uint64_t typeId;
    NodeID nodeId;
    RdmaInfo(uint32_t rkey_, Type type_, uint64_t typeId_, NodeID nodeId_)
        : rkey(rkey_), type(type_), typeId(typeId_), nodeId(nodeId_) {}
  };

  struct ComSetupContext {//用于存储通信设置的上下文
    RdmaInfo *response = nullptr;
    rdma_cm_id *id = nullptr;
  };

  uint16_t port;//RDMA服务器的监听端口
  utils::SynchronizedMonotonicBufferRessource
      mbr; // can we chunk that buffer into sub buffers for clients?，用于管理缓冲区资源的同步单调缓冲区资源对象
  struct ibv_pd *pd;//rdma的保护域的指针
  struct ibv_mr *mr;//rdma内存区域的指针
  // handler section
  std::atomic<bool> running{false};//原子布尔变量，用于表示服务器是否正在运行
  std::thread handler;//线程对象，用于处理rdma事件
  struct rdma_event_channel *incomingChannel;//指向rdma事件通道的指针，用于接收传入连接的事件
  struct rdma_cm_id *incomingCmId;//指向传入rdma连接标识符的指针
  std::mutex incomingMut;//一个互斥锁，用于保护传入连接相关的操作
  std::atomic<bool> initialized{false};//用于表示服务器是否已初始化
  std::atomic<size_t> numberConnections{0};//表示当前连接数
  std::atomic<size_t> numberConnectionsEstablished{0};//表示已经建立的连接数
  std::mutex outgoingMut;//一个互斥锁，用于保护传出连接相关的操作。
  std::vector<RdmaContext *> outgoingIds;//存储传出连接的RDMA上下文（Context）指针的向量。
  std::vector<ibv_cq *> outgoingCqs;//存储传出连接的完成队列（Completion Queue）指针的向量。
  std::vector<rdma_event_channel *> outgoingChannels;//存储传出连接的RDMA事件通道指针的向量。
  std::vector<RdmaContext *> incomingIds;//存储传入连接的RDMA上下文指针的向量。

  void resolveAddr(rdma_event_channel *outgoingChannel,
                   rdma_cm_id *outgoingCmId, struct sockaddr_storage &sin) {//用于解析地址，sin存储需要解析的地址
    if (sin.ss_family == AF_INET)
      ((struct sockaddr_in *)&sin)->sin_port = port;//判断地址的协议簇，如果是IPv4（AF_INET），则设置IPv4地址的端口为预先定义的port；
    else
      ((struct sockaddr_in6 *)&sin)->sin6_port = port;//如果是IPv6（AF_INET6），则设置IPv6地址的端口为预先定义的port。

    auto ret =
        rdma_resolve_addr(outgoingCmId, nullptr, (struct sockaddr *)&sin, 2000);//调用rdma_resolve_addr函数来解析地址，outgoingCmId、nullptr表示不指定源地址，以及需要解析的目标地址（struct sockaddr类型），超时时间为2000毫秒。
    if (ret)
      throw;//如果解析失败，会抛出异常。
    struct rdma_cm_event *event;
    ret = rdma_get_cm_event(outgoingChannel, &event);
    if (ret)
      throw;//调用rdma_get_cm_event函数来获取事件。该函数的参数包括outgoingChannel和输出参数event。如果获取事件失败，会抛出异常。
    if (event->event != RDMA_CM_EVENT_ADDR_RESOLVED)
      throw;//判断事件的类型是否是地址已解析（RDMA_CM_EVENT_ADDR_RESOLVED）。如果不是，则抛出异常。
    rdma_ack_cm_event(event);//函数来确认事件的接收
    DEBUG_LOG("Addr resolved");
    ret = rdma_resolve_route(outgoingCmId, 2000);//函数来解析路由
    if (ret)
      throw;
    ret = rdma_get_cm_event(outgoingChannel, &event);//函数来获取事件
    if (ret)
      throw;
    if (event->event != RDMA_CM_EVENT_ROUTE_RESOLVED)
      throw;
    rdma_ack_cm_event(event);//函数来确认事件的接收
    DEBUG_LOG("Route Resolved");
  }

  // Helper Functions
  void bindHandler(struct sockaddr_storage &sin) {//用于绑定地址，sin：存储需要绑定的地址
    int ret;
    if (sin.ss_family == AF_INET)
      ((struct sockaddr_in *)&sin)->sin_port = port;
    else
      ((struct sockaddr_in6 *)&sin)->sin6_port = port;
//判断地址的协议簇
    ret = rdma_bind_addr(incomingCmId, (struct sockaddr *)&sin);//调用rdma_bind_addr函数来绑定地址。该函数的参数包括incomingCmId和需要绑定的地址（struct sockaddr类型）
    if (ret) {
      throw std::runtime_error("Could not bind to rdma device");
    }
    DEBUG_LOG("rdma_bind_addr successful");
    DEBUG_LOG("rdma_listen");
    ret = rdma_listen(incomingCmId, 3);//调用rdma_listen函数来监听连接请求
    if (ret) {
      throw std::runtime_error("Could not listen");//监听失败抛出异常
    }
  }

  // function to exchange rdma info including rkey
  // must ensure that before calling that an receive has been posted
  void exchangeRdmaInfo(rdma_cm_id *cmId, ibv_mr *mr, Type type,
                        uint64_t typeId, NodeID nodeId) {//使用RDMA进行通信的函数exchangeRdmaInfo，用于进行RDMA信息交换。
                        //参数解释：cmId：RDMA连接的标识符。mr：内存区域的注册描述符。type：存储在RdmaInfo结构体中的类型。typeId：存储在RdmaInfo结构体中的类型ID。nodeId：存储在RdmaInfo结构体中的节点ID。
    RdmaInfo *ownInfo = new (mbr.allocate(sizeof(RdmaInfo)))
        RdmaInfo(mr->rkey, type, typeId, nodeId);//首先创建一个新的RdmaInfo结构体并分配内存，该结构体包含了RDMA传输所需的各项信息，如远程数据区域的键、类型和节点ID。
    postSend(ownInfo, cmId->qp, mr, completion::signaled);//通过postSend函数将该结构体发送到远程节点。
    // poll completion for IBV_WC_RECV and
    int completions{0};
    ibv_wc wcs[2];
    while (completions != 2) {//循环不断调用pollCompletion函数来查询完成队列（Completion Queue）中是否有相应的完成请求
      auto expected = 2 - completions; // prevents over polling the q and，expected参数用于表示需要查询多少个完成请求
                                       // draining it from next phase
      auto comp = pollCompletion(
          cmId->qp->send_cq, expected,
          wcs); // assumes that the completion qs are the same for send and recv
      for (int i = 0; i < comp; i++) {//循环来遍历完成请求中的每个ibv_wc对象。
        /* verify the completion status */
        if (wcs[i].status != IBV_WC_SUCCESS) {
          throw;
        }//完成请求的状态不为成功，抛出异常
      }
      completions += comp;
    }
    DEBUG_LOG("Rdma Info Exchange");
  }

  // we only create one CQ for the handler and only send CQ
  struct ibv_cq *createCQ(rdma_cm_id *cmId) {//创建了一个Completion Queue（CQ），并返回该队列的指针。函数的输入参数为cmId，表示RDMA连接的标识符。
    struct ibv_cq *cq = ibv_create_cq(cmId->verbs, 16, nullptr, nullptr, 0);//使用ibv_create_cq函数来创建Completion Queue。
    //参数：cmId->verbs： verbs上下文。16： Completion Queue的大小，本例中设置为16。nullptr： Completion Queue上下文信息。nullptr： 用于共享Completion Queue的其他CQ的指针，本例中不涉及共享。0： Completion Queue的压入模式，设置为0表示该CQ没有压入指针队列（Polling Mode）。
    if (!cq)
      throw std::runtime_error("Could not create cq");
    DEBUG_LOG("CQ created");
    return cq;
  }//创建失败，抛出异常

  // can we create a single one?
  void createPD(rdma_cm_id *cmId) {//创建一个Protection Domain（PD），用于管理内存保护。函数的输入参数为cmId，表示RDMA连接的标识符。
    pd = ibv_alloc_pd(cmId->verbs);//使用ibv_alloc_pd函数来为指定的verbs上下文分配一个PD。该函数的参数为cmId->verbs，表示要为该verbs上下文创建一个PD。
    if (!pd)
      throw std::runtime_error("Could not create PD");
    DEBUG_LOG("PD created");
  }

  RdmaContext *createRdmaContext(rdma_cm_id *cmId,
                                 INITIAL_MSG *applicationData) {//创建一个RdmaContext，用于存储RDMA连接所需的各项信息。函数的输入参数为cmId，表示RDMA连接的标识符，以及applicationData，表示要发送到远程主机的初始数据。
    auto *rdmaContext = new RdmaContext();//创建一个新的RdmaContext，并分配所需的内存
    postReceive(applicationData, cmId->qp, mr);//使用postReceive函数对接收端进行初始化。
    rdmaContext->applicationData = applicationData;
    rdmaContext->mr = mr;
    rdmaContext->id = cmId;
    return rdmaContext;//将applicationData、mr和cmId存储在RdmaContext中，并返回该RdmaContext的指针。
  }

  void createMR() {//创建一个Memory Region（MR），用于描述远程节点可以访问的内存区域。
    // std::cout << "memory created " << std::endl;
    DEBUG_VAR(mbr.getUnderlyingBuffer());//输出mbr.getUnderlyingBuffer()的调试变量
    mr = ibv_reg_mr(pd, mbr.getUnderlyingBuffer(), mbr.getBufferSize(),
                    IBV_ACCESS_LOCAL_WRITE | IBV_ACCESS_REMOTE_READ |
                        IBV_ACCESS_REMOTE_WRITE);//使用ibv_reg_mr函数来注册内存区域
                        //参数：pd：表示要注册内存的Protection Domain（PD）。mbr.getUnderlyingBuffer()：表示要注册的内存区域的起始地址。mbr.getBufferSize()：表示要注册的内存区域的大小。IBV_ACCESS_LOCAL_WRITE | IBV_ACCESS_REMOTE_READ | IBV_ACCESS_REMOTE_WRITE：表示将内存区域注册为具有本地写、远程读和远程写权限。
    DEBUG_VAR(mr);
    if (!mr)
      throw std::runtime_error("Memory region could not be registered");

    DEBUG_LOG("Created Memory region");
    DEBUG_VAR(mr->lkey);
    DEBUG_VAR(mr->rkey);
  }
  // called with the correct cm_id
  void createQP(rdma_cm_id *cm_id, ibv_cq *completionQueue) {//创建一个Queue Pair（QP），用于定义两个端点之间的通信关系。
    // std::cout << "Create QP " << std::endl;
    // std::cout << "cm_id " << cm_id << std::endl;
    // std::cout << "cq " << completionQueue << std::endl;
    // std::cout << "pd " << pd << std::endl;
    struct ibv_qp_init_attr init_attr;
    int ret;
    memset(&init_attr, 0, sizeof(init_attr));//初始化一个ibv_qp_init_attr结构体，并对其进行清零操作。
    init_attr.cap.max_send_wr = 1024; // 4096;
    init_attr.cap.max_recv_wr = 1024; // 4096;//init_attr.cap.max_send_wr和init_attr.cap.max_recv_wr：分别表示发送和接收的工作请求（Work Request）的最大数量，这里设置为1024。
    init_attr.cap.max_recv_sge = 1;
    init_attr.cap.max_send_sge = 1;//init_attr.cap.max_recv_sge和init_attr.cap.max_send_sge：分别表示每个工作请求中可以包含的接收和发送Scatter-Gather元素（SGE）的最大数量，这里设置为1。
#ifdef USE_INLINE
    init_attr.cap.max_inline_data = INLINE_SIZE;//init_attr.cap.max_inline_data：表示可以直接内联发送的最大数据量，这里根据预编译宏USE_INLINE的定义设置为INLINE_SIZE。
#endif
    init_attr.qp_type = IBV_QPT_RC;//init_attr.qp_type：表示QP的类型，这里设置为IBV_QPT_RC，即Reliable Connection（RC）类型。
    init_attr.send_cq = completionQueue;
    init_attr.recv_cq = completionQueue;//init_attr.send_cq和init_attr.recv_cq：分别表示发送和接收完成队列（Completion Queue），这里设置为completionQueue。
    ret = rdma_create_qp(cm_id, pd, &init_attr);//使用rdma_create_qp函数创建QP
    if (ret)
      throw std::runtime_error("Could not create QP " + std::to_string(ret) +
                               " errno " + std::to_string(errno));
    DEBUG_VAR(init_attr.cap.max_send_wr);
    DEBUG_VAR(init_attr.cap.max_recv_wr);
    DEBUG_VAR(init_attr.cap.max_recv_sge);
    DEBUG_VAR(init_attr.cap.max_send_sge);
    DEBUG_VAR(init_attr.cap.max_inline_data);
    DEBUG_LOG("QP created");
  }
  void getAddr(std::string ip, struct sockaddr *addr) {//用于将IP地址（ip）转换成sockaddr类型的地址（addr）。
    DEBUG_LOG("get_addr");
    struct addrinfo *res;
    auto ret = getaddrinfo(ip.c_str(), NULL, NULL, &res);//使用getaddrinfo函数获取IP地址对应的addrinfo结构体，并将返回值保存在ret中。
    if (ret) {
      throw std::runtime_error("getaddrinfo failed");
    }//如果ret不为0，则说明getaddrinfo函数出错，抛出异常。

    if (res->ai_family == PF_INET)//分别检查res结构体中的ai_family属性，判断是IPv4地址还是IPv6地址，并将相应的地址复制到addr中。
      memcpy(addr, res->ai_addr, sizeof(struct sockaddr_in));
    else if (res->ai_family == PF_INET6)
      memcpy(addr, res->ai_addr, sizeof(struct sockaddr_in6));
    else
      throw std::runtime_error("Unexpected  ai_family");//如果在判断ai_family属性时发现不是IPv4或IPv6地址，则抛出异常。
    freeaddrinfo(res);//调用freeaddrinfo函数释放getaddrinfo函数中申请的内存。
  }
};
} // namespace rdma
} // namespace scalestore
