#pragma once
// -------------------------------------------------------------------------------------
#include "Defs.hpp"
//**************************用于通信协议的消息结构体*************************
// -------------------------------------------------------------------------------------

namespace scalestore
{
namespace rdma
{
// -------------------------------------------------------------------------------------
enum class MESSAGE_TYPE : uint8_t {
   Empty = 0,  // 0 initialized 空消息
   Finish = 1, //结束消息
   // -------------------------------------------------------------------------------------
   // possession requests
   PRX = 2, //获取请求类型X。
   PRS = 3, //获取请求类型S
   // -------------------------------------------------------------------------------------
   // possesion request responses
   PRRX = 4,//表示获取请求类型X的响应。
   PRRS = 5,//表示获取请求类型S的响应。
   // move requests
   PMR = 6,//表示移动请求。
   PMRR = 7,//表示移动请求的响应。
   // -------------------------------------------------------------------------------------
   // copy request
   PCR = 8,//表示复制请求。
   PCRR = 9,//表示复制请求的响应
   // -------------------------------------------------------------------------------------
   // update request
   PUR = 10,//表示更新请求
   PURR = 11,//表示更新请求的响应。
   // -------------------------------------------------------------------------------------
   // remote allocation requests
   RAR = 12,//表示远程分配请求。
   RARR  = 13,//表示远程分配请求的响应。
   // -------------------------------------------------------------------------------------
   PRFR = 14,//表示获取远程自由槽位请求。
   PRFRR =15,//表示获取远程自由槽位请求的响应。
   // -------------------------------------------------------------------------------------
   DPMR = 96, // delegate possession request//表示委托所有权请求。
   // Remote information for delegation//
   DR = 97, //表示远程信息请求。
   DRR = 98,//表示远程信息请求的响应。
   // -------------------------------------------------------------------------------------   
   Init = 99,//表示初始化消息。
   // -------------------------------------------------------------------------------------
   Test = 100,  //表示测试消息。
   
};
// -------------------------------------------------------------------------------------
enum class RESULT : uint8_t {//RESULT的枚举类，其基础类型为uint8_t
   WithPage = 1,//表示具有页面
   NoPage = 2,//表示没有页面
   NoPageSharedConflict = 3,     // other nodes posses the page in S mode，表示没有页面，但其他节点以S模式拥有页面，存在冲突。
   NoPageExclusiveConflict = 4,  // other nodes posses the page in X mode，表示没有页面，但其他节点以X模式拥有页面，存在冲突。
   WithPageSharedConflict = 5,    // other nodes posses the page but page is on owner in S mode，表示具有页面，但其他节点以S模式拥有页面，存在冲突。
   NoPageEvicted = 6, // page is outdated at owner need to get from other shared，表示页面在所有者处已过时，需要从其他共享节点获取。
   UpdateFailed = 7,//表示更新失败。
   UpdateSucceed = 8,//表示更新成功。
   UpdateSucceedWithSharedConflict =9,//表示更新成功，但存在共享冲突。
   CopyFailedWithRestart =10,//表示复制失败，需要重新开始。
   CopyFailedWithInvalidation =11,//表示复制失败，需要使原有数据无效。
};
// -------------------------------------------------------------------------------------
// INIT Message is exchanged via RDMA S/R hence not in inheritance hierarchy
// -------------------------------------------------------------------------------------
struct InitMessage {//初始化消息
   uintptr_t mbOffset;  // rdma offsets on remote，表示远程RDMA偏移量。
   uintptr_t plOffset;//表示远程偏移量。
   uintptr_t mbResponseOffset; // for page provider only ，仅用于页面提供者，表示消息缓冲区的响应偏移量。
   uintptr_t plResponseOffset;//表示响应偏移量。
   NodeID bmId;  // node id of buffermanager the initiator belongs to，表示发起者所属的缓冲区管理器节点ID。
   MESSAGE_TYPE type = MESSAGE_TYPE::Init;//表示消息类型，初始化消息类型为Init。
};
// -------------------------------------------------------------------------------------
// Protocol Messages
// -------------------------------------------------------------------------------------
struct Message {//通用的消息
   MESSAGE_TYPE type;//消息类型。
   Message() : type(MESSAGE_TYPE::Empty) {}//默认构造函数将消息类型设置为Empty。
   Message(MESSAGE_TYPE type) : type(type) {}//带参构造函数接受消息类型参数，用于设置消息类型。
};
// -------------------------------------------------------------------------------------
struct FinishRequest : public Message {//表示结束请求消息
   FinishRequest() : Message(MESSAGE_TYPE::Finish){}//构造函数将消息类型设置为Finish。
};
// -------------------------------------------------------------------------------------
struct PossessionRequest : public Message {//表示占有响应消息，继承自Message
   PID pid;//表示页面ID。
   uintptr_t pageOffset; //表示页面偏移量。
   PossessionRequest(MESSAGE_TYPE type, PID pid, uintptr_t pageOffset) : Message(type), pid(pid), pageOffset(pageOffset){}//构造函数接受消息类型、页面ID和页面偏移量参数，用于初始化成员。
};
struct __attribute__((packed)) PossessionResponse : public Message {
   RESULT resultType;//表示结果类型。
   uint64_t conflictingNodeId;//表示冲突的节点ID。
   uint64_t pVersion;//表示版本号
   bool delegated = false;//表示是否已委托。
   uint8_t receiveFlag = 1; //表示接收标志。
   PossessionResponse(RESULT result):Message(MESSAGE_TYPE::PRRS), resultType(result){};//构造函数根据参数初始化成员。
   PossessionResponse(RESULT result, NodeID conflictingNodeId):Message(MESSAGE_TYPE::PRRS), resultType(result), conflictingNodeId(conflictingNodeId){};//构造函数根据参数初始化成员。
};
// -------------------------------------------------------------------------------------
// moves possession to other party
struct __attribute__((packed)) PossessionMoveRequest : public Message {//表示占有移动请求消息
   bool needPage;//表示是否需要页面。
   uint64_t pid;//表示页面ID。
   uintptr_t pageOffset;//表示页面偏移量。
   uint64_t pVersion; // own version，表示版本号
   PossessionMoveRequest(uint64_t pid, bool needPage, uintptr_t pageOffset, uint64_t pVersion)//构造函数根据参数初始化成员。
       : Message(MESSAGE_TYPE::PMR), needPage(needPage), pid(pid), pageOffset(pageOffset), pVersion(pVersion) {}//构造函数根据参数初始化成员。
};

struct PossessionMoveResponse : public Message {//表示占有移动响应消息，继承自Message
   RESULT resultType;//表示结果类型
   uint8_t receiveFlag = 1;//表示接收标志
   PossessionMoveResponse(RESULT result) : Message(MESSAGE_TYPE::PMRR), resultType(result){}//构造函数根据参数初始化成员
};  

// -------------------------------------------------------------------------------------
// copys possession to other party
struct __attribute__((packed)) PossessionCopyRequest : public Message {//表示占有复制请求消息
   uint64_t pid;//表示页面ID
   uintptr_t pageOffset;//表示页面偏移量。
   uint64_t pVersion; // own version ，表示版本号。
   PossessionCopyRequest(uint64_t pid, uintptr_t pageOffset, uint64_t pVersion) : Message(MESSAGE_TYPE::PCR), pid(pid), pageOffset(pageOffset), pVersion(pVersion){}//构造函数根据参数初始化成员。
};

// maybe not needed
struct PossessionCopyResponse : public Message {//表示占有复制响应消息，继承自Message
   RESULT resultType;//表示结果类型。
   uint8_t receiveFlag = 1;//表示接收标志。
   PossessionCopyResponse(RESULT result) : Message(MESSAGE_TYPE::PCRR), resultType(result){}//构造函数根据参数初始化成员。
};  
// -------------------------------------------------------------------------------------

// -------------------------------------------------------------------------------------
// copys possession to other party
struct PossessionUpdateRequest : public Message {//表示占有更新请求消息
   PID pid;//表示页面ID。
   uint64_t pVersion; // own version ，表示版本号。
   PossessionUpdateRequest(PID pid, uint64_t pVersion ) : Message(MESSAGE_TYPE::PUR), pid(pid), pVersion(pVersion){}//构造函数根据参数初始化成员。
};

// maybe not needed
struct __attribute__((packed)) PossessionUpdateResponse : public Message  {//表示占有更新响应消息，继承自Message
   RESULT resultType;//表示结果类型。
   uint64_t conflictingNodeId;//表示冲突的节点ID。
   uint64_t pVersion; // own version ，表示版本号。
   uint8_t receiveFlag = 1;//表示接收标志。
   PossessionUpdateResponse(RESULT result) : Message(MESSAGE_TYPE::PURR), resultType(result){}//构造函数根据参数初始化成员。
};

// -------------------------------------------------------------------------------------
// remote allocation request
struct RemoteAllocationRequest : public Message {//表示远程分配请求消息，继承自Message
   RemoteAllocationRequest() : Message(MESSAGE_TYPE::RAR){}//构造函数将消息类型设置为RAR。
};

struct RemoteAllocationResponse : public Message  {//表示远程分配响应消息，继承自Message
   PID pid;//页面ID。
   uint8_t receiveFlag = 1;//表示接收标志。
   RemoteAllocationResponse(PID pid) : Message(MESSAGE_TYPE::RARR), pid(pid){}//构造函数根据参数初始化成员。
};


// -------------------------------------------------------------------------------------
// Deledation information only send once for setup
// transfer other MH information to MH in order to allow MH to delegate 
struct DelegationRequest : public Message {
   uintptr_t mbOffset;//
   uintptr_t mbPayload;//
   uint64_t bmId;//
   DelegationRequest(uintptr_t mbOffset, uintptr_t mbPayload, uint64_t bmId) : Message(MESSAGE_TYPE::DR), mbOffset(mbOffset), mbPayload(mbPayload), bmId(bmId){}//
};

struct DelegationResponse : public Message  {//表示委托响应消息，继承自Message
   uint8_t receiveFlag = 1;//表示接收标志，默认值为1。
   DelegationResponse() : Message(MESSAGE_TYPE::DRR){}//构造函数将消息类型设置为DRR。
};

// -------------------------------------------------------------------------------------
// Get size of Largest Message
union ALLDERIVED{//union ALLDERIVED定义了一个联合体，包含了所有消息结构体的对象，用于获取最大消息大小。每个成员都是不同类型的消息结构体。
   FinishRequest fm;//
   PossessionResponse pr;//
   PossessionRequest preq;//
   PossessionMoveResponse pmr;//
   PossessionMoveRequest pmrr;//
   PossessionCopyResponse pcr;//
   PossessionCopyRequest pcrr;//
   RemoteAllocationRequest rar;//
   RemoteAllocationResponse rarr;//
   DelegationRequest dr;//
   DelegationResponse drr;//
};

static constexpr uint64_t LARGEST_MESSAGE = sizeof(ALLDERIVED);//表示最大消息大小，通过计算ALLDERIVED的大小得到。
static_assert(LARGEST_MESSAGE <= 32, "Messags span more than one CL");


struct MessageFabric{//包含一个静态函数模板createMessage，用于创建消息对象并将其放入指定的缓冲区中。
   template<typename T, class... Args>
   static T* createMessage(void* buffer, Args&&... args){
      return new (buffer) T (args...);//函数模板接受消息类型T和其他参数，并返回消息对象的指针。
   }
};

}  // namespace rdma
}  // namespace scalestore
