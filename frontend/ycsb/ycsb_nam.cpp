#include "scalestore/storage/datastructures/BTree.hpp"
#include "PerfEvent.hpp"
#include "scalestore/Config.hpp"
#include "scalestore/ScaleStore.hpp"
#include "scalestore/rdma/CommunicationManager.hpp"
#include "scalestore/utils/RandomGenerator.hpp"
#include "scalestore/utils/ScrambledZipfGenerator.hpp"
#include "scalestore/utils/Time.hpp"
// -------------------------------------------------------------------------------------
#include <gflags/gflags.h>
// -------------------------------------------------------------------------------------
DEFINE_uint32(YCSB_read_ratio, 100, "");
DEFINE_uint64(YCSB_tuple_count, 1, " Tuple count in"); 
DEFINE_double(YCSB_zipf_factor, 0.0, "Default value according to spec");
DEFINE_double(YCSB_run_for_seconds, 10.0, "");
DEFINE_uint32(NAM_storage_nodes, 1, "");
// -------------------------------------------------------------------------------------
using u64 = uint64_t;
using u8 = uint8_t;
// -------------------------------------------------------------------------------------
static constexpr uint64_t BTREE_ID =0; 
static constexpr uint64_t BARRIER_ID =1;
// -------------------------------------------------------------------------------------
template <u64 size>
// 定义一个结构体 BytesPayload 用于表示字节类型的负载
struct BytesPayload {
   u8 value[size];  // 存储字节的数组
   BytesPayload() = default; // 默认构造函数
   // 重载运算符，用于判断两个 BytesPayload 对象是否相等
   bool operator==(BytesPayload& other) { return (std::memcmp(value, other.value, sizeof(value)) == 0); }
   // 重载运算符，用于判断两个 BytesPayload 对象是否不相等
   bool operator!=(BytesPayload& other) { return !(operator==(other)); }
   // BytesPayload(const BytesPayload& other) { std::memcpy(value, other.value, sizeof(value)); }
   // BytesPayload& operator=(const BytesPayload& other)
   // {
      // std::memcpy(value, other.value, sizeof(value));
      // return *this;
   // }
};
// -------------------------------------------------------------------------------------
// 定义一个结构体 Partition 用于表示一个区间的起始和结束
struct Partition {
   uint64_t begin;
   uint64_t end;
};
// -------------------------------------------------------------------------------------
struct YCSB_workloadInfo : public scalestore::profiling::WorkloadInfo{
   std::string experiment;
   uint64_t elements;
   uint64_t readRatio;
   double zipfFactor;

   YCSB_workloadInfo(std::string experiment, uint64_t elements, uint64_t readRatio, double zipfFactor)
      : experiment(experiment), elements(elements), readRatio(readRatio), zipfFactor(zipfFactor)
   {
   }

   
   virtual std::vector<std::string> getRow(){
      return {experiment,std::to_string(elements), std::to_string(readRatio), std::to_string(zipfFactor)};
   }

   virtual std::vector<std::string> getHeader(){
      return {"workload","elements","read ratio", "zipfFactor"};
   }
   
   virtual void csv(std::ofstream& file) override {
      file << experiment << " , ";
      file << elements << " , ";
      file << readRatio << " , ";
      file << zipfFactor << " , ";
   }
   virtual void csvHeader(std::ofstream& file) override {
      file << "Workload" << " , ";
      file << "Elements" << " , ";
      file << "ReadRatio" << " , ";
      file << "ZipfFactor" << " , ";
   }
};
// -------------------------------------------------------------------------------------
using namespace scalestore;
int main(int argc, char* argv[])
{
   using K = uint64_t;
   using V = BytesPayload<128>;

   gflags::SetUsageMessage("Catalog Test");
   gflags::ParseCommandLineFlags(&argc, &argv, true);
   // -------------------------------------------------------------------------------------
   ScaleStore scalestore;
   auto& catalog = scalestore.getCatalog();
   // -------------------------------------------------------------------------------------
   // 定义 barrier_wait 函数，用于创建分布式屏障以同步工作线程
   auto barrier_wait = [&]() {
      for (uint64_t t_i = 0; t_i < FLAGS_worker; ++t_i) {
          // 异步地在工作线程池中调度任务，创建分布式屏障以等待同步
         scalestore.getWorkerPool().scheduleJobAsync(t_i, [&, t_i]() {
            storage::DistributedBarrier barrier(catalog.getCatalogEntry(BARRIER_ID).pid);
            barrier.wait();
         });
      }
      // 等待所有工作线程池中的任务执行完成
      scalestore.getWorkerPool().joinAll();
   };
   // -------------------------------------------------------------------------------------
   // 定义 partition 函数，用于将一个范围分割成多个区块
   auto partition = [&](uint64_t id, uint64_t participants, uint64_t N) -> Partition {
      const uint64_t blockSize = N / participants;
      auto begin = id * blockSize;
      auto end = begin + blockSize;
      if (id == participants - 1)
         end = N;
      return {.begin = begin, .end = end};
   };
   // -------------------------------------------------------------------------------------
   // create Btree (0), Barrier(1)
   // -------------------------------------------------------------------------------------
   if (scalestore.getNodeID() == 0) {
       // 在工作线程池中同步地调度任务
      scalestore.getWorkerPool().scheduleJobSync(0, [&]() {
          // 创建 B 树并创建包含指定数量节点的屏障
         scalestore.createBTree<K, V>();
         scalestore.createBarrier(FLAGS_worker * FLAGS_nodes);
      });
   }
   // -------------------------------------------------------------------------------------
   //获取 YCSB_tuple_count 的值
   u64 YCSB_tuple_count = FLAGS_YCSB_tuple_count;
   // -------------------------------------------------------------------------------------
   // -------------------------------------------------------------------------------------
   // 计算当前节点的分区
   auto nodePartition = partition(scalestore.getNodeID(), FLAGS_NAM_storage_nodes, YCSB_tuple_count);
   // -------------------------------------------------------------------------------------
   // Build YCSB Table / Tree
   // -------------------------------------------------------------------------------------
   // 当前节点 ID 小于存储节点数的情况下执行以下操作
   if (scalestore.getNodeID() < FLAGS_NAM_storage_nodes) {
       // 创建 YCSB_workloadInfo 对象
      YCSB_workloadInfo builtInfo{"Build", YCSB_tuple_count, FLAGS_YCSB_read_ratio, FLAGS_YCSB_zipf_factor};
      // 启动性能分析
      scalestore.startProfiler(builtInfo);
      // 循环调度异步任务给工作线程池
      for (uint64_t t_i = 0; t_i < FLAGS_worker; ++t_i) {
         scalestore.getWorkerPool().scheduleJobAsync(t_i, [&, t_i]() {
            // -------------------------------------------------------------------------------------
            // partition
            auto nodeKeys = nodePartition.end - nodePartition.begin;
            auto threadPartition = partition(t_i, FLAGS_worker, nodeKeys);
            auto begin = nodePartition.begin + threadPartition.begin;
            auto end = nodePartition.begin + threadPartition.end;
            // 在 Catalog 中创建 B 树实例
            storage::BTree<K, V> tree(catalog.getCatalogEntry(BTREE_ID).pid);
            // -------------------------------------------------------------------------------------
            // 创建分布式屏障
            storage::DistributedBarrier barrier(catalog.getCatalogEntry(BARRIER_ID).pid);
            // -------------------------------------------------------------------------------------
            V value;
            for (K k_i = begin; k_i < end; k_i++) {
               utils::RandomGenerator::getRandString(reinterpret_cast<u8*>(&value), sizeof(V));
               tree.insert(k_i, value);
               threads::Worker::my().counters.incr(profiling::WorkerCounters::tx_p);
            }
            // -------------------------------------------------------------------------------------
         });
      }
      // 等待所有工作线程池中的任务执行完成
      scalestore.getWorkerPool().joinAll();
      // 停止性能分析
      scalestore.stopProfiler();

      std::cout << "Flushing all pages"
                << "\n";
      scalestore.getBuffermanager().writeAllPages();

      std::cout << "Done"
                << "\n";
   }

   barrier_wait();

   // -------------------------------------------------------------------------------------
   // YCSB Transaction Partitioned
   // -------------------------------------------------------------------------------------      
   // 如果当前节点的 ID 大于等于存储节点数
   if (scalestore.getNodeID() >= FLAGS_NAM_storage_nodes) {
       // 创建 ScrambledZipfGenerator 对象，用于生成 Zipf 分布的随机数
      auto zipf_random = std::make_unique<utils::ScrambledZipfGenerator>(0, YCSB_tuple_count,
                                                                         FLAGS_YCSB_zipf_factor);
      // -------------------------------------------------------------------------------------
      // zipf creation can take some time due to floating point loop therefore wait with barrier
       // zipf 的生成可能需要一些时间，因为其中包含浮点运算，因此使用屏障进行等待
      std::atomic<bool> keep_running = true;
      std::atomic<u64> running_threads_counter = 0;
      // 创建 YCSB_workloadInfo 对象用于记录实验信息
      YCSB_workloadInfo experimentInfo{"YCSB-NAM-Compute", YCSB_tuple_count, FLAGS_YCSB_read_ratio, FLAGS_YCSB_zipf_factor};
      scalestore.startProfiler(experimentInfo);
      // 计算当前节点需要睡眠的时间
      auto sleepTime = FLAGS_YCSB_run_for_seconds*(scalestore.getNodeID() - FLAGS_NAM_storage_nodes);
      std::cout << "Sleeping for " << sleepTime << "\n";
      // 如果睡眠时间不为 0，则进行睡眠
      if(sleepTime !=0){
         sleep(sleepTime);
      }
      // 循环调度异步任务给工作线程池
      for (uint64_t t_i = 0; t_i < FLAGS_worker; ++t_i) {
         scalestore.getWorkerPool().scheduleJobAsync(t_i, [&, t_i]() {
            running_threads_counter++;
            // 创建 B 树实例
            storage::BTree<K, V> tree(catalog.getCatalogEntry(BTREE_ID).pid);
            // 在保持 keep_running 为 true 的情况下执行循环
            while (keep_running) {
                // 生成 Zipf 分布的随机键
               K key = zipf_random->rand();
               ensure(key < YCSB_tuple_count);
               V result;
               // 根据 FLAGS_YCSB_read_ratio 确定读写操作的比例，并记录性能指标
               if (FLAGS_YCSB_read_ratio == 100 || utils::RandomGenerator::getRandU64(0, 100) < FLAGS_YCSB_read_ratio) {
                  auto start = utils::getTimePoint();
                  auto success = tree.lookup_opt(key, result);
                  ensure(success);
                  auto end = utils::getTimePoint();
                  threads::Worker::my().counters.incr_by(profiling::WorkerCounters::latency, (end - start));
               } else {
                  V payload;
                  utils::RandomGenerator::getRandString(reinterpret_cast<u8*>(&payload), sizeof(V));
                  auto start = utils::getTimePoint();
                  tree.insert(key, payload);
                  auto end = utils::getTimePoint();
                  threads::Worker::my().counters.incr_by(profiling::WorkerCounters::latency, (end - start));
               }
               threads::Worker::my().counters.incr(profiling::WorkerCounters::tx_p);
            }
            running_threads_counter--;
         });
      }
      // -------------------------------------------------------------------------------------
      // Join Threads
      // -------------------------------------------------------------------------------------
      // 睡眠等待，以确保所有节点执行完相同的时间
      sleep(FLAGS_YCSB_run_for_seconds * (FLAGS_nodes - FLAGS_NAM_storage_nodes));
      keep_running = false;
      // 等待所有工作线程池中的任务执行完成
      while (running_threads_counter) {
         _mm_pause();
      }
      scalestore.getWorkerPool().joinAll();
      scalestore.stopProfiler();
      barrier_wait();
   }else{
      YCSB_workloadInfo experimentInfo{"YCSB-NAM-Storage", YCSB_tuple_count, FLAGS_YCSB_read_ratio, FLAGS_YCSB_zipf_factor};
      scalestore.startProfiler(experimentInfo);
      sleep(FLAGS_YCSB_run_for_seconds * (FLAGS_nodes - FLAGS_NAM_storage_nodes));
      // sleep(FLAGS_YCSB_run_for_seconds); // run as long as all clients
      barrier_wait();
      scalestore.stopProfiler();
   }
   // -------------------------------------------------------------------------------------
   return 0;
}
