#pragma once

#include <cassert>
#include <cstring>
#include <atomic>
#include <immintrin.h>
#include <sched.h>

namespace btreeolc {

enum class PageType : uint8_t { BTreeInner=1, BTreeLeaf=2 };

static const uint64_t pageSize=4*1024;

struct OptLock {
    // 类型、版本号、锁定状态和过期标记（占位符）的原子变量
   std::atomic<uint64_t> typeVersionLockObsolete{0b100};
   // 检查版本号是否表示锁定状态
   bool isLocked(uint64_t version) {
      return ((version & 0b10) == 0b10);
   }
   // 获取读锁或者重新启动操作（乐观读）
   uint64_t readLockOrRestart(bool &needRestart) {
      uint64_t version;
      version = typeVersionLockObsolete.load();
      if (isLocked(version) || isObsolete(version)) {
         _mm_pause(); // 暂停指令执行，减少CPU资源占用
         needRestart = true;
      }
      return version;
   }
   // 获取写锁或者重新启动操作（乐观写）
   void writeLockOrRestart(bool &needRestart) {
      uint64_t version;
      version = readLockOrRestart(needRestart);
      if (needRestart) return;

      upgradeToWriteLockOrRestart(version, needRestart);
      if (needRestart) return;
   }
   // 升级为写锁或者重新启动操作（乐观写）
   void upgradeToWriteLockOrRestart(uint64_t &version, bool &needRestart) {
      if (typeVersionLockObsolete.compare_exchange_strong(version, version + 0b10)) {
         version = version + 0b10;
      } else {
         _mm_pause(); // 暂停指令执行，减少CPU资源占用
         needRestart = true;
      }
   }
   // 释放写锁
   void writeUnlock() {
      typeVersionLockObsolete.fetch_add(0b10);
   }
   // 检查版本号是否表示过期
   bool isObsolete(uint64_t version) {
      return (version & 1) == 1;
   }
   // 检查或重新启动读锁释放操作（乐观读）
   void checkOrRestart(uint64_t startRead, bool &needRestart) const {
      readUnlockOrRestart(startRead, needRestart);
   }
   // 读锁释放操作（乐观读）
   void readUnlockOrRestart(uint64_t startRead, bool &needRestart) const {
      needRestart = (startRead != typeVersionLockObsolete.load());
   }
   // 释放写锁并标记为过期
   void writeUnlockObsolete() {
      typeVersionLockObsolete.fetch_add(0b11);
   }
};

struct NodeBase : public OptLock{
   PageType type;
   uint16_t count;
};

struct BTreeLeafBase : public NodeBase {
   static const PageType typeMarker=PageType::BTreeLeaf;
};

template<class Key,class Payload>
struct BTreeLeaf : public BTreeLeafBase {
    // 结构体Entry用于存储键值对
   struct Entry {
      Key k;
      Payload p;
   };
   // 定义最大条目数，通过页面大小计算得出
   static const uint64_t maxEntries=(pageSize-sizeof(NodeBase))/(sizeof(Key)+sizeof(Payload));

   Key keys[maxEntries]; // 键数组
   Payload payloads[maxEntries];  // 值数组
   // 构造函数初始化叶子节点
   BTreeLeaf() {
      count=0; // 计数器为0
      type=typeMarker; // 类型标识
   }
   // 检查叶子节点是否已满
   bool isFull() { return count==maxEntries; };
   // 二分查找找到大于或等于给定键的索引
   unsigned lowerBound(Key k) {
      unsigned lower=0;
      unsigned upper=count;
      do {
         unsigned mid=((upper-lower)/2)+lower;
         if (k<keys[mid]) {
            upper=mid;
         } else if (k>keys[mid]) {
            lower=mid+1;
         } else {
            return mid;
         }
      } while (lower<upper);
      return lower;
   }
   // 二分查找找到大于或等于给定键的索引（另一种实现方式）
   unsigned lowerBoundBF(Key k) {
      auto base=keys;
      unsigned n=count;
      while (n>1) {
         const unsigned half=n/2;
         base=(base[half]<k)?(base+half):base;
         n-=half;
      }
      return (*base<k)+base-keys;
   }
   // 插入键值对到叶子节点
   void insert(Key k,Payload p) {
      assert(count<maxEntries); // 确保还有空间插入
      if (count) {
         unsigned pos=lowerBound(k); // 获取插入位置
         if ((pos<count) && (keys[pos]==k)) {
             // 如果键已存在，则更新值
            payloads[pos] = p;
            return;
         }
         // 移动键和值，为新键值对腾出空间
         memmove(keys+pos+1,keys+pos,sizeof(Key)*(count-pos));
         memmove(payloads+pos+1,payloads+pos,sizeof(Payload)*(count-pos));
         keys[pos]=k;
         payloads[pos]=p;
      } else {
         keys[0]=k;
         payloads[0]=p;
      }
      count++;// 增加条目数
   }
   // 分裂叶子节点，返回一个新的叶子节点
   BTreeLeaf* split(Key& sep) {
      BTreeLeaf* newLeaf = new BTreeLeaf(); // 创建一个新的叶子节点
      newLeaf->count = count-(count/2); // 计算新叶子节点的条目数
      count = count-newLeaf->count; // 更新当前节点的条目数
      // 复制键和值到新节点
      memcpy(newLeaf->keys, keys+count, sizeof(Key)*newLeaf->count);
      memcpy(newLeaf->payloads, payloads+count, sizeof(Payload)*newLeaf->count);
      sep = keys[count-1];  // 记录分裂后的分隔键
      return newLeaf;
   }
};

struct BTreeInnerBase : public NodeBase {
   static const PageType typeMarker=PageType::BTreeInner;
};

template<class Key>
struct BTreeInner : public BTreeInnerBase {
    // 定义最大条目数，通过页面大小计算得出
   static const uint64_t maxEntries=(pageSize-sizeof(NodeBase))/(sizeof(Key)+sizeof(NodeBase*));
   
   NodeBase* children[maxEntries]; // 存储子节点指针的数组
   Key keys[maxEntries];  // 键数组
   // 构造函数初始化内部节点

   BTreeInner() {
      count=0;// 计数器为0
      type=typeMarker; // 类型标识
   }
   // 检查内部节点是否已满
   bool isFull() { return count==(maxEntries-1); };
   // 二分查找找到大于或等于给定键的索引（另一种实现方式）
   unsigned lowerBoundBF(Key k) {
      auto base=keys;
      unsigned n=count;
      while (n>1) {
         const unsigned half=n/2;
         base=(base[half]<k)?(base+half):base;
         n-=half;
      }
      return (*base<k)+base-keys;
   }
   // 二分查找找到大于或等于给定键的索引
   unsigned lowerBound(Key k) {
      unsigned lower=0;
      unsigned upper=count;
      do {
         unsigned mid=((upper-lower)/2)+lower;
         if (k<keys[mid]) {
            upper=mid;
         } else if (k>keys[mid]) {
            lower=mid+1;
         } else {
            return mid;
         }
      } while (lower<upper);
      return lower;
   }
   // 分裂内部节点，返回一个新的内部节点
   BTreeInner* split(Key& sep) {
      BTreeInner* newInner=new BTreeInner(); // 创建一个新的内部节点
      newInner->count=count-(count/2); // 计算新内部节点的条目数
      count=count-newInner->count-1; // 更新当前节点的条目数
      sep=keys[count];// 记录分裂后的分隔键
      memcpy(newInner->keys,keys+count+1,sizeof(Key)*(newInner->count+1));  // 复制键和子节点到新节点
      memcpy(newInner->children,children+count+1,sizeof(NodeBase*)*(newInner->count+1));
      return newInner;
   }
   // 插入键和子节点到内部节点
   void insert(Key k,NodeBase* child) {
      assert(count<maxEntries-1); // 确保还有空间插入
      unsigned pos=lowerBound(k);  // 获取插入位置
      // 移动键和子节点，为新键值对腾出空间
      memmove(keys+pos+1,keys+pos,sizeof(Key)*(count-pos+1));
      memmove(children+pos+1,children+pos,sizeof(NodeBase*)*(count-pos+1));
      keys[pos]=k;
      children[pos]=child;
      std::swap(children[pos],children[pos+1]);// 交换子节点顺序
      count++; // 增加条目数
   }

};

template<class Key,class Value>
struct BTree {
   std::atomic<NodeBase*> root;

   BTree() {
      root = new BTreeLeaf<Key,Value>();
   }
   /*
   用于创建B树的根节点。它创建一个新的内部节点作为根节点，并设置了该节点的键和子节点指针，最后将新节点设为B树的根节点。
   */
   void makeRoot(Key k,NodeBase* leftChild,NodeBase* rightChild) {
      auto inner = new BTreeInner<Key>(); // 创建一个新的BTreeInner节点作为根节点
      inner->count = 1;
      inner->keys[0] = k;
      inner->children[0] = leftChild;
      inner->children[1] = rightChild;
      root = inner;
   }

   void yield(int count) {
      // if (count>3)
      //    sched_yield();
      // else
      (void) count;
         _mm_pause();
   }
   /*这段代码实现了B 树的插入操作，其中包含了节点分*/
   void insert(Key k, Value v) {
      int restartCount = 0; // 重启计数器，用于记录重启的次数
   restart:
      if (restartCount++)
         yield(restartCount); // 在这里调用 yield 函数，可能用于暂时释放 CPU
      bool needRestart = false;  // 标记是否需要重新开始插入操作

      // 当前节点为根节点
      NodeBase* node = root;
      uint64_t versionNode = node->readLockOrRestart(needRestart);  // 读取锁定当前节点
      if (needRestart || (node!=root)) goto restart;  // 如果需要重新开始或者当前节点不是根节点，则重新开始

      // 当前节点的父节点
      BTreeInner<Key>* parent = nullptr;
      uint64_t versionParent = 0;
      // 循环直到找到叶子节点
      while (node->type==PageType::BTreeInner) {
         auto inner = static_cast<BTreeInner<Key>*>(node);

         // 如果内部节点已满，则进行分裂
         if (inner->isFull()) {
             // 加锁
            if (parent) {
               parent->upgradeToWriteLockOrRestart(versionParent, needRestart);
               if (needRestart) goto restart;
            }
            node->upgradeToWriteLockOrRestart(versionNode, needRestart);
            if (needRestart) {
               if (parent)
                  parent->writeUnlock();
               goto restart;
            }
            if (!parent && (node != root)) { // 如果没有父节点并且当前节点不是根节点，则表示有新的父节点
               node->writeUnlock();
               goto restart;
            }
            // 进行分裂操作
            Key sep; BTreeInner<Key>* newInner = inner->split(sep);
            if (parent)
               parent->insert(sep,newInner);
            else
               makeRoot(sep,inner,newInner);
            // 解锁并重新开始
            node->writeUnlock();
            if (parent)
               parent->writeUnlock();
            goto restart;
         }

         if (parent) {
            parent->readUnlockOrRestart(versionParent, needRestart);
            if (needRestart) goto restart;
         }

         parent = inner;
         versionParent = versionNode;

         node = inner->children[inner->lowerBound(k)]; // 移动到下一个子节点
         inner->checkOrRestart(versionNode, needRestart);// 检查当前节点是否需要重新开始
         if (needRestart) goto restart;
         versionNode = node->readLockOrRestart(needRestart);// 读取锁定下一个节点
         if (needRestart) goto restart;
      }

      auto leaf = static_cast<BTreeLeaf<Key,Value>*>(node);

      // 如果叶子节点已满，则进行分裂
      if (leaf->count==leaf->maxEntries) {
          // 加锁
         if (parent) {
            parent->upgradeToWriteLockOrRestart(versionParent, needRestart);
            if (needRestart) goto restart;
         }
         node->upgradeToWriteLockOrRestart(versionNode, needRestart);
         if (needRestart) {
            if (parent) parent->writeUnlock();
            goto restart;
         }
         if (!parent && (node != root)) { // 如果没有父节点并且当前节点不是根节点，则表示有新的父节点
            node->writeUnlock();
            goto restart;
         }
         // 分裂
         Key sep; BTreeLeaf<Key,Value>* newLeaf = leaf->split(sep);
         if (parent)
            parent->insert(sep, newLeaf);
         else
            makeRoot(sep, leaf, newLeaf);
         // 解锁并重新开始
         node->writeUnlock();
         if (parent)
            parent->writeUnlock();
         goto restart;
      } else {
          // 仅锁定叶子节点
         node->upgradeToWriteLockOrRestart(versionNode, needRestart);
         if (needRestart) goto restart;
         if (parent) {
            parent->readUnlockOrRestart(versionParent, needRestart);
            if (needRestart) {
               node->writeUnlock();
               goto restart;
            }
         }
         leaf->insert(k, v); // 向叶子节点中插入键值对
         node->writeUnlock();
         return; // 操作成功，返回
      }
   }
   /*通过迭代遍历树的节点来寻找目标键值对。*/
   bool lookup(Key k, Value& result) {
      int restartCount = 0; // 重启计数器，用于记录重启的次数
   restart:
      if (restartCount++)
         yield(restartCount); // 在这里调用 yield 函数，可能用于暂时释放 CPU
      bool needRestart = false;  // 标记是否需要重新开始查找操作

      NodeBase* node = root; // 当前节点为根节点
      uint64_t versionNode = node->readLockOrRestart(needRestart);  // 读取锁定当前节点
      if (needRestart || (node!=root)) goto restart; // 如果需要重新开始或者当前节点不是根节点，则重新开始

      // 当前节点的父节点
      BTreeInner<Key>* parent = nullptr;
      uint64_t versionParent = 0;
      // 循环直到找到叶子节点
      while (node->type==PageType::BTreeInner) {
         auto inner = static_cast<BTreeInner<Key>*>(node);

         if (parent) {
            parent->readUnlockOrRestart(versionParent, needRestart);
            if (needRestart) goto restart;
         }

         parent = inner;
         versionParent = versionNode;

         node = inner->children[inner->lowerBound(k)]; // 移动到下一个子节点
         inner->checkOrRestart(versionNode, needRestart); // 检查当前节点是否需要重新开始
         if (needRestart) goto restart;
         versionNode = node->readLockOrRestart(needRestart); // 读取锁定下一个节点
         if (needRestart) goto restart;
      }

      BTreeLeaf<Key,Value>* leaf = static_cast<BTreeLeaf<Key,Value>*>(node);
      unsigned pos = leaf->lowerBound(k);
      bool success = false;
      if ((pos<leaf->count) && (leaf->keys[pos]==k)) { // 在叶子节点中查找键值对
         success = true;
         result = leaf->payloads[pos];  // 找到匹配的键值对
      }
      if (parent) {
         parent->readUnlockOrRestart(versionParent, needRestart);
         if (needRestart) goto restart;
      }
      node->readUnlockOrRestart(versionNode, needRestart);
      if (needRestart) goto restart;

      return success;// 返回查找结果（是否成功找到）
   } 

   /*它从给定键值开始扫描叶节点，并将满足范围的数据值复制到输出数组中，然后返回实际复制的元素数量。*/
   uint64_t scan(Key k, int range, Value* output) {
      int restartCount = 0;
   restart:
      if (restartCount++)
         yield(restartCount);
      bool needRestart = false;

      NodeBase* node = root; // 当前节点为根节点
      uint64_t versionNode = node->readLockOrRestart(needRestart); // 读取锁定当前节点
      if (needRestart || (node!=root)) goto restart; // 如果需要重新开始或者当前节点不是根节点，则重新开始
       
      // 当前节点的父节点
      BTreeInner<Key>* parent = nullptr;
      uint64_t versionParent = 0;
      // 循环直到找到叶子节点
      while (node->type==PageType::BTreeInner) {
         auto inner = static_cast<BTreeInner<Key>*>(node);

         if (parent) {
            parent->readUnlockOrRestart(versionParent, needRestart);
            if (needRestart) goto restart;
         }

         parent = inner;
         versionParent = versionNode;

         node = inner->children[inner->lowerBound(k)]; // 移动到下一个子节点
         inner->checkOrRestart(versionNode, needRestart); // 检查当前节点是否需要重新开始
         if (needRestart) goto restart;
         versionNode = node->readLockOrRestart(needRestart); // 读取锁定下一个节点
         if (needRestart) goto restart;
      }

      BTreeLeaf<Key,Value>* leaf = static_cast<BTreeLeaf<Key,Value>*>(node);
      unsigned pos = leaf->lowerBound(k);
      int count = 0;
      for (unsigned i=pos; i<leaf->count; i++) {
         if (count==range) // 达到指定的扫描范围
            break;
         output[count++] = leaf->payloads[i]; // 将值复制到输出数组中
      }

      if (parent) {
         parent->readUnlockOrRestart(versionParent, needRestart);
         if (needRestart) goto restart;
      }
      node->readUnlockOrRestart(versionNode, needRestart);
      if (needRestart) goto restart;

      return count;
   }


};

}
