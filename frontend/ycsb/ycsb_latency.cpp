#include "PerfEvent.hpp"
#include "scalestore/Config.hpp"
#include "scalestore/ScaleStore.hpp"
#include "scalestore/rdma/CommunicationManager.hpp"
#include "scalestore/storage/datastructures/BTree.hpp"
#include "scalestore/utils/RandomGenerator.hpp"
#include "scalestore/utils/ScrambledZipfGenerator.hpp"
#include "scalestore/utils/Time.hpp"
// -------------------------------------------------------------------------------------
#include <gflags/gflags.h>
// -------------------------------------------------------------------------------------
DEFINE_uint32(YCSB_read_ratio, 100, "");
DEFINE_bool(YCSB_all_workloads, false, "Execute all workloads i.e. 50 95 100 ReadRatio on same tree");
DEFINE_uint64(YCSB_tuple_count, 1, " Tuple count in");
DEFINE_double(YCSB_zipf_factor, 0.0, "Default value according to spec");
DEFINE_double(YCSB_run_for_seconds, 10.0, "");
DEFINE_bool(YCSB_partitioned, false, "");
DEFINE_bool(YCSB_warm_up, false, "");
DEFINE_bool(YCSB_record_latency, false, "");
DEFINE_bool(YCSB_all_zipf, false, "");
DEFINE_bool(YCSB_local_zipf, false, "");
DEFINE_bool(YCSB_flush_pages, false, "");
// -------------------------------------------------------------------------------------
using u64 = uint64_t;
using u8 = uint8_t;
// -------------------------------------------------------------------------------------
static constexpr uint64_t BTREE_ID = 0;
static constexpr uint64_t BARRIER_ID = 1;
// -------------------------------------------------------------------------------------
template <u64 size>
struct BytesPayload {
   u8 value[size];
   BytesPayload() = default;
   bool operator==(BytesPayload& other) { return (std::memcmp(value, other.value, sizeof(value)) == 0); }
   bool operator!=(BytesPayload& other) { return !(operator==(other)); }
   // BytesPayload(const BytesPayload& other) { std::memcpy(value, other.value, sizeof(value)); }
   // BytesPayload& operator=(const BytesPayload& other)
   // {
   // std::memcpy(value, other.value, sizeof(value));
   // return *this;
   // }
};
// -------------------------------------------------------------------------------------
struct Partition {
   uint64_t begin;
   uint64_t end;
};
// -------------------------------------------------------------------------------------
struct YCSB_workloadInfo : public scalestore::profiling::WorkloadInfo {
   std::string experiment;
   uint64_t elements;
   uint64_t readRatio;
   uint64_t targetThroughput;
   double zipfFactor;
   std::string zipfOffset;
   uint64_t timestamp = 0;

   YCSB_workloadInfo(std::string experiment,
                     uint64_t elements,
                     uint64_t readRatio,
                     uint64_t targetThroughput,
                     double zipfFactor,
                     std::string zipfOffset)
       : experiment(experiment),
         elements(elements),
         readRatio(readRatio),
         targetThroughput(targetThroughput),
         zipfFactor(zipfFactor),
         zipfOffset(zipfOffset) {}

   virtual std::vector<std::string> getRow() {
      return {
          experiment, std::to_string(elements),    std::to_string(readRatio), std::to_string(targetThroughput), std::to_string(zipfFactor),
          zipfOffset, std::to_string(timestamp++),
      };
   }

   virtual std::vector<std::string> getHeader() {
      return {"workload", "elements", "read ratio", "targetThroughput", "zipfFactor", "zipfOffset", "timestamp"};
   }

   virtual void csv(std::ofstream& file) override {
      file << experiment << " , ";
      file << elements << " , ";
      file << readRatio << " , ";
      file << targetThroughput << " , ";
      file << zipfFactor << " , ";
      file << zipfOffset << " , ";
      file << timestamp << " , ";
   }
   virtual void csvHeader(std::ofstream& file) override {
      file << "Workload"
           << " , ";
      file << "Elements"
           << " , ";
      file << "ReadRatio"
           << " , ";
      file << "TargetThroughput"
           << " , ";
      file << "ZipfFactor"
           << " , ";
      file << "ZipfOffset"
           << " , ";
      file << "Timestamp"
           << " , ";
   }
};

// -------------------------------------------------------------------------------------

// coordinated omission
// https://www.scylladb.com/2021/04/22/on-coordinated-omission/
std::vector<std::chrono::nanoseconds> poisson_process(double estimated_events_per_second, uint64_t runtime) {
    // 计算所需的总事件数量
   uint64_t total_number_events_needed = estimated_events_per_second * runtime;
   assert(estimated_events_per_second < 1e9);          // 确保估计的事件速率小于 1e9，否则需要更高精度（比如纳秒级别）
   std::random_device rd;                             // 用于生成随机数种子的设备
   std::mt19937 rng(rd());                            // 使用 Mersenne Twister 作为伪随机数生成器
   double lamda = estimated_events_per_second / 1e9;  // 每单位时间的到达次数
   std::exponential_distribution<double> exp(lamda); // 指数分布，用于生成到达时间间隔
   double sum_arrival_time = 0; // 用于累计到达时间
   double new_arrival_time = 0; // 存储新到达的时间间隔
   // -------------------------------------------------------------------------------------
    // 存储累计到达时间的向量，以纳秒为单位
   std::vector<std::chrono::nanoseconds> sum_arrival_times;
   sum_arrival_times.reserve(total_number_events_needed);  // 预留容器空间，避免不必要的内存重新分配
   // -------------------------------------------------------------------------------------
   // 生成指定数量的到达时间
   for (uint64_t i = 0; i < total_number_events_needed; ++i) {
      new_arrival_time = exp(rng);  // 生成指数分布的下一个随机数，表示时间间隔
      sum_arrival_time = sum_arrival_time + new_arrival_time; // 累加到达时间间隔
      sum_arrival_times.emplace_back(
          (uint64_t)(sum_arrival_time));  // 将累计到达时间添加到向量中，可能损失一些精度，因为使用了纳秒而不是微秒
   }
   return sum_arrival_times; // 返回生成的到达时间向量
}

inline void wait_until_next(decltype(std::chrono::high_resolution_clock::now()) next) {
   using namespace std::chrono;
   // 获取当前时间点
   high_resolution_clock::time_point current = high_resolution_clock::now();
   // 计算指定时间点与当前时间点的时间间隔
   duration<double> time_span = duration_cast<duration<double>>(next - current);
   // 循环等待直到时间间隔过去
   while (time_span.count() > 0) {
       // 更新当前时间点
      current = high_resolution_clock::now();
      // 重新计算时间间隔
      time_span = duration_cast<duration<double>>(next - current);
   }
}

struct ReadRatioLimits{
   uint64_t read_ratio;
   uint64_t max_throughput;
   uint64_t increment;
   uint64_t start_value;
};

struct LatencyWorkload{
   double zipf {0};
   std::vector<ReadRatioLimits> wl_desc;
};


// -------------------------------------------------------------------------------------
using namespace scalestore;
int main(int argc, char* argv[]) {
    // 使用 uint64_t 类型的键和 BytesPayload<128> 类型的值
   using K = uint64_t;
   using V = BytesPayload<128>;
   // 设置程序的用法信息
   gflags::SetUsageMessage("Catalog Test");
   // 解析命令行参数
   gflags::ParseCommandLineFlags(&argc, &argv, true);
   // -------------------------------------------------------------------------------------
   // 准备工作负载
   std::vector<std::string> workload_type;  // 存储工作负载类型，如预热或基准测试
   std::vector<LatencyWorkload> workloads = {
       // 存储不同工作负载的向量
       {.zipf = 0,
        {
           // 不同读取比例的设置
            {.read_ratio = 5, .max_throughput = 1600000, .increment = 100000, .start_value = 100000},
            {.read_ratio = 50, .max_throughput = 1800000, .increment = 100000, .start_value = 100000},
            {.read_ratio = 95, .max_throughput = 2700000, .increment = 100000, .start_value = 100000},
            {.read_ratio = 100, .max_throughput = 2900000, .increment = 100000, .start_value = 100000},
        }},
        // 第二个工作负载（zipf = 1.25）
       {.zipf = 1.25,
        {
           // 不同读取比例的设置
            {.read_ratio = 5, .max_throughput = 700000, .increment = 25000, .start_value = 100},
            {.read_ratio = 50, .max_throughput = 700000, .increment = 25000, .start_value = 100},
            {.read_ratio = 95, .max_throughput = 2000000, .increment = 50000, .start_value = 100},
            {.read_ratio = 100, .max_throughput = 20000000, .increment = 500000, .start_value = 100},
        }}};

   if (FLAGS_YCSB_warm_up) {
       // 如果需要预热（warm-up）
    // 添加 "YCSB_warm_up" 和 "YCSB_txn" 到 workload_type 向量中
      workload_type.push_back("YCSB_warm_up"); // 添加预热类型的工作负载
      workload_type.push_back("YCSB_txn"); // 添加事务类型的工作负载
   } else {
       // 如果不需要预热
    // 只添加事务类型的工作负载到 workload_type 向量中
      workload_type.push_back("YCSB_txn");
   }


   ScaleStore scalestore;
   // 获取 ScaleStore 对象中的 Catalog（目录）
   auto& catalog = scalestore.getCatalog();
   // -------------------------------------------------------------------------------------
   // Lambda 函数 partition，用于划分范围为多个分区
   auto partition = [&](uint64_t id, uint64_t participants, uint64_t N) -> Partition {
      const uint64_t blockSize = N / participants;
      auto begin = id * blockSize;
      auto end = begin + blockSize;
      if (id == participants - 1) end = N;
      return {.begin = begin, .end = end};
   };
   // Lambda 函数 barrier_wait，用于设置分布式屏障
   auto barrier_wait = [&]() {
       // 遍历并为每个线程设置分布式屏障
      for (uint64_t t_i = 0; t_i < FLAGS_worker; ++t_i) {
          // 使用 WorkerPool 中的 scheduleJobAsync 方法为每个线程调度任务
         scalestore.getWorkerPool().scheduleJobAsync(t_i, [&, t_i]() {
             // 创建分布式屏障对象并等待
            storage::DistributedBarrier barrier(catalog.getCatalogEntry(BARRIER_ID).pid);
            barrier.wait();
         });
      }
      // 所有任务完成后，等待所有线程的结束
      scalestore.getWorkerPool().joinAll();
   };
   // -------------------------------------------------------------------------------------
   // create Btree (0), Barrier(1)
   // -------------------------------------------------------------------------------------
   if (scalestore.getNodeID() == 0) {
       // 在 WorkerPool 中同步调度任务到节点 0
      scalestore.getWorkerPool().scheduleJobSync(0, [&]() {
          // 在当前节点创建 B 树和屏障
         scalestore.createBTree<K, V>();
         scalestore.createBarrier(FLAGS_worker * FLAGS_nodes);
      });
   }
   // -------------------------------------------------------------------------------------
   u64 YCSB_tuple_count = FLAGS_YCSB_tuple_count;  // YCSB 表中元组的数量
   // -------------------------------------------------------------------------------------
   // 对当前节点进行分区，根据节点数量和 YCSB 元组数量
   auto nodePartition = partition(scalestore.getNodeID(), FLAGS_nodes, YCSB_tuple_count);
   // -------------------------------------------------------------------------------------
   // 构建 YCSB 表 / 树
   // -------------------------------------------------------------------------------------
   // YCSB 构建信息
   YCSB_workloadInfo builtInfo{"Build",     YCSB_tuple_count,       FLAGS_YCSB_read_ratio,
                               uint64_t(0), FLAGS_YCSB_zipf_factor, (FLAGS_YCSB_local_zipf ? "local_zipf" : "global_zipf")};
   scalestore.startProfiler(builtInfo);// 启动性能分析
   // 循环在 WorkerPool 中异步调度任务
   for (uint64_t t_i = 0; t_i < FLAGS_worker; ++t_i) {
      scalestore.getWorkerPool().scheduleJobAsync(t_i, [&, t_i]() {
         // -------------------------------------------------------------------------------------
         // 分配节点键范围给线程
         auto nodeKeys = nodePartition.end - nodePartition.begin;
         auto threadPartition = partition(t_i, FLAGS_worker, nodeKeys);
         auto begin = nodePartition.begin + threadPartition.begin;
         auto end = nodePartition.begin + threadPartition.end;
         // 在当前节点获取 B 树的实例
         storage::BTree<K, V> tree(catalog.getCatalogEntry(BTREE_ID).pid);
         // -------------------------------------------------------------------------------------
         // 创建分布式屏障对象并等待
         storage::DistributedBarrier barrier(catalog.getCatalogEntry(BARRIER_ID).pid);
         barrier.wait();
         // -------------------------------------------------------------------------------------
         V value;
         // 在给定范围内为 B 树插入键值对
         for (K k_i = begin; k_i < end; k_i++) {
            utils::RandomGenerator::getRandString(reinterpret_cast<u8*>(&value), sizeof(V));
            tree.insert(k_i, value);
            // 增加当前线程的事务计数
            threads::Worker::my().counters.incr(profiling::WorkerCounters::tx_p);
         }
         // -------------------------------------------------------------------------------------
         // 再次等待分布式屏障
         barrier.wait();
      });
   }
   scalestore.getWorkerPool().joinAll();
   scalestore.stopProfiler();

   if (FLAGS_YCSB_flush_pages) {
      std::cout << "Flushing all pages"
                << "\n";
      scalestore.getBuffermanager().writeAllPages();

      std::cout << "Done"
                << "\n";
   }
   // 遍历工作负载
   for (auto& workload : workloads){
      auto ZIPF = workload.zipf; // 获取当前工作负载的 ZIPF 值
      // -------------------------------------------------------------------------------------
      // YCSB Transaction
      // -------------------------------------------------------------------------------------
       // 根据是否启用了分区 YCSB，创建不同的 ScrambledZipfGenerator 实例
      std::unique_ptr<utils::ScrambledZipfGenerator> zipf_random;
      if (FLAGS_YCSB_partitioned)
         zipf_random = std::make_unique<utils::ScrambledZipfGenerator>(nodePartition.begin, nodePartition.end - 1, ZIPF);
      else
         zipf_random = std::make_unique<utils::ScrambledZipfGenerator>(0, YCSB_tuple_count, ZIPF);

      // -------------------------------------------------------------------------------------
      // zipf creation can take some time due to floating point loop therefore wait with barrier
       // 等待 barrier 完成 zipf 生成，因为生成 zipf 可能会花费一些时间
      barrier_wait();
      // -------------------------------------------------------------------------------------
      // 遍历工作负载的描述
      for(auto p : workload.wl_desc){
         auto READ_RATIO = p.read_ratio;  // 读取比例
         auto MAX_TROUGHPUT = p.max_throughput; // 最大吞吐量
         auto START_TARGET_TROUGHPUT = p.start_value; // 起始目标吞吐量
         auto TARGET_TROUGHPUT_INCREMENT = p.increment; // 目标吞吐量递增值
         // 根据不同的目标吞吐量循环
         for (uint64_t TARGET_TROUGHPUT = START_TARGET_TROUGHPUT; TARGET_TROUGHPUT <= MAX_TROUGHPUT;
              TARGET_TROUGHPUT += TARGET_TROUGHPUT_INCREMENT) {
            std::cout << "new target throughput = " << TARGET_TROUGHPUT << "\n";
            // 遍历工作负载类型
            for (auto TYPE : workload_type) {
               barrier_wait(); // 等待 barrier 完成
               std::atomic<bool> keep_running = true;  // 标志：保持运行
               std::atomic<u64> running_threads_counter = 0; // 运行线程计数器
               [[maybe_unused]] uint64_t zipf_offset = 0;
               if (FLAGS_YCSB_local_zipf) zipf_offset = (YCSB_tuple_count / FLAGS_nodes) * scalestore.getNodeID();
               // 根据不同的 YCSB 配置信息创建实验信息
               YCSB_workloadInfo experimentInfo{
                   TYPE, YCSB_tuple_count, READ_RATIO, TARGET_TROUGHPUT, ZIPF, (FLAGS_YCSB_local_zipf ? "local_zipf" : "global_zipf")};
               scalestore.startProfiler(experimentInfo); // 启动性能分析器
               // 存储每个线程的微秒级延迟
               std::vector<uint64_t> tl_microsecond_latencies[FLAGS_worker];
               // 在 WorkerPool 中异步调度任务
               // 循环迭代，对每个线程进行异步作业调度
               for (uint64_t t_i = 0; t_i < FLAGS_worker; ++t_i) {
                  scalestore.getWorkerPool().scheduleJobAsync(t_i, [&, t_i]() {
                     running_threads_counter++; // 增加运行线程计数器
                     // reserve tl_microsecond_latencies with lambda
                     uint64_t ops = 0; // 操作计数器
                     // -------------------------------------------------------------------------------------
                     // generate events
                     auto ttp_per_sec = TARGET_TROUGHPUT / FLAGS_worker; // 每秒目标吞吐量
                     auto latency_samples = ttp_per_sec * 3;  // 样本秒数
                     uint64_t samples = 0; // 样本计数器
                     auto sum_arrival_times = poisson_process(ttp_per_sec, 10); // 生成符合泊松分布的事件到达时间
                     // -------------------------------------------------------------------------------------
                     // 创建分布式障碍
                     storage::DistributedBarrier barrier(catalog.getCatalogEntry(BARRIER_ID).pid);
                     storage::BTree<K, V> tree(catalog.getCatalogEntry(BTREE_ID).pid);
                     barrier.wait(); // 等待障碍
                     auto start = std::chrono::high_resolution_clock::now();  // 记录开始时间
                     // 对每个事件进行迭代处理
                     for (auto& t : sum_arrival_times) {
                        // -------------------------------------------------------------------------------------
                         // 如果不需要继续运行，跳出循环
                        if (!keep_running) break;
                        // -------------------------------------------------------------------------------------
                        // 生成符合 ZIPF 分布的键值
                        K key = zipf_random->rand(zipf_offset);
                        // K key = utils::RandomGenerator::getRandU64(0, YCSB_tuple_count-1);
                        ensure(key < YCSB_tuple_count); // 确保 key 在范围内
                        V result;
                        // -------------------------------------------------------------------------------------
                        auto next = start + t; // 计算下一个请求的发送时间
                        wait_until_next(next);  // 等待直到发送下一个请求
                        // -------------------------------------------------------------------------------------
                        // 根据 READ_RATIO 来执行读或写操作
                        if (READ_RATIO == 100 || utils::RandomGenerator::getRandU64(0, 100) < READ_RATIO) {
                            // 进行查找操作
                           auto success = tree.lookup_opt(key, result);
                           ensure(success);
                           auto end = std::chrono::high_resolution_clock::now();
                           auto latency = std::chrono::duration_cast<std::chrono::microseconds>(end - next);
                           // 跳过开始两秒的样本
                           if (ops > ttp_per_sec*2 && samples < latency_samples){
                              samples++;
                              tl_microsecond_latencies[t_i].push_back(latency.count());
                           }
                        } else {
                            // 执行插入操作
                           V payload;
                           utils::RandomGenerator::getRandString(reinterpret_cast<u8*>(&payload), sizeof(V));
                           tree.insert(key, payload);
                           auto end = std::chrono::high_resolution_clock::now();
                           auto latency = std::chrono::duration_cast<std::chrono::microseconds>(end - next);
                           // 跳过开始两秒的样本
                           if (ops > ttp_per_sec*2 && samples < latency_samples) {
                              samples++;
                              tl_microsecond_latencies[t_i].push_back(latency.count());
                           }
                        }
                        threads::Worker::my().counters.incr(profiling::WorkerCounters::tx_p); // 计数器递增
                        ops++; // 操作计数器递增
                     }
                     running_threads_counter--; // 运行线程计数器减一
                  });
               }
               sleep(10);
               keep_running = false;
               while (running_threads_counter) {
                  _mm_pause();
               }
               // -------------------------------------------------------------------------------------
               // Join Threads
               // -------------------------------------------------------------------------------------
               scalestore.getWorkerPool().joinAll();
               // -------------------------------------------------------------------------------------
               scalestore.stopProfiler();
               // -------------------------------------------------------------------------------------

               // if warmup continue
               if (TYPE.compare("YCSB_txn") != 0) {
                  std::cout << "Skip writting to file "
                            << "\n";
                  continue;
               }

               // 合并所有线程的延迟数据到一个单一向量中
               std::vector<uint64_t> microsecond_latencies;
               for (uint64_t t_i = 0; t_i < FLAGS_worker; ++t_i) {
                  microsecond_latencies.insert(microsecond_latencies.end(), tl_microsecond_latencies[t_i].begin(),
                                               tl_microsecond_latencies[t_i].end());
               }
               // 生成器随机重排延迟样本
               {
                  std::cout << "Shuffle samples " << microsecond_latencies.size() << std::endl;
                  std::random_device rd;
                  std::mt19937 g(rd());
                  std::shuffle(microsecond_latencies.begin(), microsecond_latencies.end(), g);

                  // 可能写入1000个样本 
                  std::ofstream latency_file;
                  std::ofstream::openmode open_flags = std::ios::app;
                  std::string filename = "latency_samples_" + FLAGS_csvFile;
                  bool csv_initialized = std::filesystem::exists(filename);
                  latency_file.open(filename, open_flags);
                  if (!csv_initialized) {
                     latency_file << "nodeId,workload,tag,ReadRatio,targetThroughput,YCSB_tuple_count,zipf,latency" << std::endl;
                  }
                  // 写入文件的样本数量，最多1000个
                  uint64_t samples_persist = (microsecond_latencies.size() > 1000) ? 1000 : microsecond_latencies.size();
                  // 将样本写入文件
                  for (uint64_t s_i = 0; s_i < samples_persist; s_i++) {
                     latency_file << scalestore.getNodeID() << "," << TYPE << "," << FLAGS_tag << "," << READ_RATIO << ","
                                  << TARGET_TROUGHPUT << "," << YCSB_tuple_count << "," << ZIPF << "," << microsecond_latencies[s_i]
                                  << std::endl;
                  }
                  latency_file.close(); // 关闭文件流
               }
               std::cout << "Sorting Latencies"
                         << "\n";
               // 对延迟数据进行排序
               std::sort(microsecond_latencies.begin(), microsecond_latencies.end());
               // 输出最小值、中位数、最大值和 99% 的延迟值
               std::cout << "Latency (min/median/max/99%): " << (microsecond_latencies[0]) << ","
                         << (microsecond_latencies[microsecond_latencies.size() / 2]) << "," << (microsecond_latencies.back()) << ","
                         << (microsecond_latencies[(int)(microsecond_latencies.size() * 0.99)]) << std::endl;
               // -------------------------------------------------------------------------------------
               // 将延迟数据写入CSV文件
               std::ofstream latency_file;
               std::ofstream::openmode open_flags = std::ios::app;
               std::string filename = "latency_" + FLAGS_csvFile;
               bool csv_initialized = std::filesystem::exists(filename);
               latency_file.open(filename, open_flags);
               if (!csv_initialized) {
                   // 如果CSV文件尚不存在，则添加标题行
                  latency_file << "nodeId,workload,tag,ReadRatio,targetThroughput,YCSB_tuple_count,zipf,min,median,max,95th,99th,999th"
                               << std::endl;
               }
               // 将数据写入文件
               latency_file << scalestore.getNodeID() << "," << TYPE << "," << FLAGS_tag << "," << READ_RATIO << "," << TARGET_TROUGHPUT
                            << "," << YCSB_tuple_count << "," << ZIPF << "," << (microsecond_latencies[0]) << ","
                            << (microsecond_latencies[microsecond_latencies.size() / 2]) << "," << (microsecond_latencies.back()) << ","
                            << (microsecond_latencies[(int)(microsecond_latencies.size() * 0.95)]) << ","
                            << (microsecond_latencies[(int)(microsecond_latencies.size() * 0.99)]) << ","
                            << (microsecond_latencies[(int)(microsecond_latencies.size() * 0.999)]) << std::endl;
               latency_file.close(); // 关闭文件流
            } 
         }
      }
   }
   std::cout << "Starting hash table report "
             << "\n";
   scalestore.getBuffermanager().reportHashTableStats();
   return 0;
}
