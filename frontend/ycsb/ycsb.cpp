#include "PerfEvent.hpp"
#include "scalestore/Config.hpp"
#include "scalestore/ScaleStore.hpp"
#include "scalestore/rdma/CommunicationManager.hpp"
#include "scalestore/storage/datastructures/BTree.hpp"
#include "scalestore/utils/RandomGenerator.hpp"
#include "scalestore/utils/ScrambledZipfGenerator.hpp"
#include "scalestore/utils/Time.hpp"
// -------------------------------------------------------------------------------------
#include <gflags/gflags.h>
// -------------------------------------------------------------------------------------
DEFINE_uint32(YCSB_read_ratio, 100, "");
DEFINE_bool(YCSB_all_workloads, false , "Execute all workloads i.e. 50 95 100 ReadRatio on same tree");
DEFINE_uint64(YCSB_tuple_count, 1, " Tuple count in");
DEFINE_double(YCSB_zipf_factor, 0.0, "Default value according to spec");
DEFINE_double(YCSB_run_for_seconds, 10.0, "");
DEFINE_bool(YCSB_partitioned, false, "");
DEFINE_bool(YCSB_warm_up, false, "");
DEFINE_bool(YCSB_record_latency, false, "");
DEFINE_bool(YCSB_all_zipf, false, "");
DEFINE_bool(YCSB_local_zipf, false, "");
DEFINE_bool(YCSB_flush_pages, false, "");
// -------------------------------------------------------------------------------------
using u64 = uint64_t;
using u8 = uint8_t;
// -------------------------------------------------------------------------------------
static constexpr uint64_t BTREE_ID = 0;
static constexpr uint64_t BARRIER_ID = 1;
// -------------------------------------------------------------------------------------
template <u64 size>
/*定义了一个结构体 BytesPayload，包含一个大小为 size 的无符号字节数组 value。结构体重载了==和!=操作符，用于比较两个 BytesPayload 对象是否相等和不相等。
同时，还包含了被注释掉的拷贝构造函数和拷贝赋值运算符，这些函数会将另一个 BytesPayload 对象的数据值复制到当前对象中。*/
struct BytesPayload {
   u8 value[size]; // 一个包含 size 大小的字节数组，存储数据值
   BytesPayload() = default; // 默认构造函数
   // 重载操作符'=='，用于比较两个 BytesPayload 对象是否相等
   bool operator==(BytesPayload& other) { return (std::memcmp(value, other.value, sizeof(value)) == 0); }
   // 重载操作符'!='，用于比较两个 BytesPayload 对象是否不相等
   bool operator!=(BytesPayload& other) { return !(operator==(other)); }
   // BytesPayload(const BytesPayload& other) { std::memcpy(value, other.value, sizeof(value)); }
   // BytesPayload& operator=(const BytesPayload& other)
   // {
      // std::memcpy(value, other.value, sizeof(value));
      // return *this;
   // }
};
// -------------------------------------------------------------------------------------
struct Partition {
   uint64_t begin;
   uint64_t end;
};
// -------------------------------------------------------------------------------------
// 定义了 YCSB_workloadInfo 结构体，它继承自 scalestore::profiling::WorkloadInfo 类
struct YCSB_workloadInfo : public scalestore::profiling::WorkloadInfo {
   std::string experiment; // 实验名称
   uint64_t elements; // 元素数量
   uint64_t readRatio;  // 读取比例
   double zipfFactor; // Zipf 分布的因子
   std::string zipfOffset;// Zipf 分布的偏移量
   uint64_t timestamp = 0; // 时间戳，默认为 0
   // 构造函数，用于初始化 YCSB_workloadInfo 对象的各个成员变量
   YCSB_workloadInfo(std::string experiment, uint64_t elements, uint64_t readRatio, double zipfFactor, std::string zipfOffset)
      : experiment(experiment), elements(elements), readRatio(readRatio), zipfFactor(zipfFactor), zipfOffset(zipfOffset)
   {
   }

   // 获取一行数据的方法
   virtual std::vector<std::string> getRow(){
      return {
          experiment, std::to_string(elements),    std::to_string(readRatio), std::to_string(zipfFactor),
          zipfOffset, std::to_string(timestamp++),
      };
   }
   // 获取表头的方法
   virtual std::vector<std::string> getHeader(){
      return {"workload","elements","read ratio", "zipfFactor", "zipfOffset", "timestamp"};
   }
   
   // 以 CSV 格式输出数据到文件的方法
   virtual void csv(std::ofstream& file) override
   {
      file << experiment << " , ";
      file << elements << " , ";
      file << readRatio << " , ";
      file << zipfFactor << " , ";
      file << zipfOffset << " , ";
      file << timestamp << " , ";
   }
   // 以 CSV 格式输出表头到文件的方法
   virtual void csvHeader(std::ofstream& file) override
   {
      file << "Workload"
           << " , ";
      file << "Elements"
           << " , ";
      file << "ReadRatio"
           << " , ";
      file << "ZipfFactor"
           << " , ";
      file << "ZipfOffset"
           << " , ";
      file << "Timestamp"
           << " , ";
   }
};
// -------------------------------------------------------------------------------------
using namespace scalestore;
int main(int argc, char* argv[])
{
   using K = uint64_t; // 定义 K 类型为 uint64_t，用于表示键值
   using V = BytesPayload<128>; // 定义 V 类型为 BytesPayload<128>，用于表示值，值的大小为 128 字节
    
   gflags::SetUsageMessage("Catalog Test"); // 设置使用说明信息为 "Catalog Test"
   gflags::ParseCommandLineFlags(&argc, &argv, true);  // 解析命令行参数
   // -------------------------------------------------------------------------------------
   // prepare workload
   std::vector<std::string> workload_type; // 存储工作负载类型（预热或基准测试）
   std::vector<uint32_t> workloads;// 存储工作负载的读取比例
   std::vector<double> zipfs;// 存储 Zipf 分布的因子
   // 如果设置了 FLAGS_YCSB_all_workloads 标志，则添加默认的工作负载读取比例
   if(FLAGS_YCSB_all_workloads){
      workloads.push_back(5); 
      workloads.push_back(50); 
      workloads.push_back(95);
      workloads.push_back(100);
   }else{
       // 否则，将 FLAGS_YCSB_read_ratio 设置为工作负载的读取比例
      workloads.push_back(FLAGS_YCSB_read_ratio);
   }

   
   if(FLAGS_YCSB_warm_up){
       // 如果设置了 FLAGS_YCSB_warm_up 标志，则将 "YCSB_warm_up" 和 "YCSB_txn" 添加到工作负载类型中
      workload_type.push_back("YCSB_warm_up");
      workload_type.push_back("YCSB_txn");
   }else{
       // 否则，只将 "YCSB_txn" 添加到工作负载类型中
      workload_type.push_back("YCSB_txn");
   }

   if(FLAGS_YCSB_all_zipf){
      // zipfs.insert(zipfs.end(), {0.0,1.0,1.25,1.5,1.75,2.0});
      // zipfs.insert(zipfs.end(), {1.05,1.1,1.15,1.20,1.3,1.35,1.4,1.45});
      zipfs.insert(zipfs.end(), {0,0.25,0.5,0.75,1.0,1.25,1.5,1.75,2.0});
   }else{
      zipfs.push_back(FLAGS_YCSB_zipf_factor);
   }
   // -------------------------------------------------------------------------------------
   ScaleStore scalestore;
   auto& catalog = scalestore.getCatalog();
   // -------------------------------------------------------------------------------------
   //定义了一个分区策略 id（标识当前分区的索引），participants（分区总数），N（要进行分区的元素总数）。
   //根据总数 N 将一段数据分成 participants 个部分。在这个分割过程中，每个分区的大小都尽可能相等。如果数据无法整除到 participants 个部分，最后一个分区的大小将可能不同于其他分区。
   auto partition = [&](uint64_t id, uint64_t participants, uint64_t N) -> Partition {
      const uint64_t blockSize = N / participants;
      auto begin = id * blockSize;
      auto end = begin + blockSize;
      if (id == participants - 1)
         end = N;
      return {.begin = begin, .end = end};
   };
   //该函数通过使用 ScaleStore 中的 WorkerPool 在多个工作线程中创建分布式屏障（DistributedBarrier），然后让这些线程等待屏障的触发。
   auto barrier_wait = [&]() {
      for (uint64_t t_i = 0; t_i < FLAGS_worker; ++t_i) {
         scalestore.getWorkerPool().scheduleJobAsync(t_i, [&, t_i]() {
             //每次循环调用 scalestore.getWorkerPool().scheduleJobAsync() 来调度一个异步任务.
             // 在这个异步任务中，每个工作线程创建了一个 storage::DistributedBarrier 类的对象，并调用了该对象的 wait() 函数，以等待特定的分布式屏障。
            storage::DistributedBarrier barrier(catalog.getCatalogEntry(BARRIER_ID).pid);
            barrier.wait();
         });
      }
      //等待所有的异步任务（工作线程）完成它们的工作，从而等待所有线程都通过屏障
      scalestore.getWorkerPool().joinAll();
   }; 
   // -------------------------------------------------------------------------------------
   // create Btree (0), Barrier(1)
   // -------------------------------------------------------------------------------------
   if (scalestore.getNodeID() == 0) {
       // 调度一个同步任务。这意味着任务将在当前节点的线程池中以同步方式执行。
      scalestore.getWorkerPool().scheduleJobSync(0, [&]() {
          //该函数用于创建一个名为BTree的数据结构，该BTree的键类型为K，值类型为V。
         scalestore.createBTree<K, V>();
         //该函数创建一个屏障 此操作用于在分布式环境中设置一个共享的屏障，以便多个节点的工作线程能够同步等待达到指定的数量后才能继续执行后续的操作。
         scalestore.createBarrier(FLAGS_worker * FLAGS_nodes);
      });
   }
   // -------------------------------------------------------------------------------------
   u64 YCSB_tuple_count = FLAGS_YCSB_tuple_count;
   // -------------------------------------------------------------------------------------
   auto nodePartition = partition(scalestore.getNodeID(), FLAGS_nodes, YCSB_tuple_count);
   // -------------------------------------------------------------------------------------
   // Build YCSB Table / Tree
   // -------------------------------------------------------------------------------------
   YCSB_workloadInfo builtInfo{"Build", YCSB_tuple_count, FLAGS_YCSB_read_ratio, FLAGS_YCSB_zipf_factor, (FLAGS_YCSB_local_zipf?"local_zipf":"global_zipf")};
   scalestore.startProfiler(builtInfo);
   /*迭代创建多个工作线程，在每个工作线程中进行如下操作：
    计算节点键的数量和每个线程需要处理的键的范围。
    为每个线程创建一个B树对象并初始化它。
    使用分布式屏障进行同步，以确保所有线程都准备好继续执行。
    在键的范围内为每个键生成一个随机值，并将键值对插入到相应的B树中。
    增加工作线程的事务计数器，用于记录事务操作的执行情况。
    再次使用分布式屏障进行同步，等待所有线程完成它们的操作。*/
   for (uint64_t t_i = 0; t_i < FLAGS_worker; ++t_i) {
       // 为每个工作线程t_i，在ScaleStore的工作池中异步调度一个任务
      scalestore.getWorkerPool().scheduleJobAsync(t_i, [&, t_i]() {
         // -------------------------------------------------------------------------------------
         // 进行分区
         auto nodeKeys = nodePartition.end - nodePartition.begin;// 计算节点键数量
         auto threadPartition = partition(t_i, FLAGS_worker, nodeKeys); // 对节点键进行线程分区
         auto begin = nodePartition.begin + threadPartition.begin;  // 计算线程处理的键开始位置
         auto end = nodePartition.begin + threadPartition.end; // 计算线程处理的键结束位置
         storage::BTree<K, V> tree(catalog.getCatalogEntry(BTREE_ID).pid); // 创建B树对象
         // -------------------------------------------------------------------------------------
         storage::DistributedBarrier barrier(catalog.getCatalogEntry(BARRIER_ID).pid);// 创建分布式屏障对象
         barrier.wait(); // 等待所有线程到达这一点，进行同步操作
         // -------------------------------------------------------------------------------------
         V value;
         for (K k_i = begin; k_i < end; k_i++) {
             // 生成一个随机的字节序列，将其作为值 value
            utils::RandomGenerator::getRandString(reinterpret_cast<u8*>(&value), sizeof(V));
            // 将键值对插入B树中
            tree.insert(k_i, value);
            // 增加工作线程的事务计数器，用于记录事务操作的执行情况
            threads::Worker::my().counters.incr(profiling::WorkerCounters::tx_p);
         }
         // -------------------------------------------------------------------------------------
         barrier.wait(); // 再次进行同步，等待所有线程完成它们的操作
      });
   }
   //等待所有异步任务执行完成，确保所有工作线程都已结束。
   scalestore.getWorkerPool().joinAll();
   //停止记录性能数据的分析器。
   scalestore.stopProfiler();
   if(FLAGS_YCSB_flush_pages){
      std::cout << "Flushing all pages"
                << "\n";
      //所有页面刷新到持久存储。
      scalestore.getBuffermanager().writeAllPages();
      
      std::cout << "Done"
                << "\n";
   }
         
   for (auto ZIPF : zipfs) {
      // -------------------------------------------------------------------------------------
      // YCSB Transaction
      // -------------------------------------------------------------------------------------
       // 针对 zipfs 容器中的每个 ZIPF 值进行迭代
       // YCSB Transaction，执行 YCSB 事务
      std::unique_ptr<utils::ScrambledZipfGenerator> zipf_random;
      // 根据 FLAGS_YCSB_partitioned 标志选择性地创建 zipf_random 对象
      if (FLAGS_YCSB_partitioned)
         zipf_random = std::make_unique<utils::ScrambledZipfGenerator>(nodePartition.begin, nodePartition.end - 1,
                                                                       ZIPF);
      else
         zipf_random = std::make_unique<utils::ScrambledZipfGenerator>(0, YCSB_tuple_count, ZIPF);

      // -------------------------------------------------------------------------------------
      // zipf creation can take some time due to floating point loop therefore wait with barrier
       // 创建 Zipf 分布可能需要一些时间，因此等待栅栏
      barrier_wait();
      // -------------------------------------------------------------------------------------
       // 针对 workloads 和 workload_type 容器中的每个 READ_RATIO 和 TYPE 值进行迭代
      for (auto READ_RATIO : workloads) {
         for (auto TYPE : workload_type) {
            // 等待栅栏同步
            barrier_wait();
            // 初始化一些线程控制的原子变量
            std::atomic<bool> keep_running = true;
            std::atomic<u64> running_threads_counter = 0;
            // 根据 FLAGS_YCSB_local_zipf 标志设置 zipf_offset 变量的值
            uint64_t zipf_offset = 0;
            if (FLAGS_YCSB_local_zipf) zipf_offset = (YCSB_tuple_count / FLAGS_nodes) * scalestore.getNodeID();
            // 创建 YCSB_workloadInfo 对象描述实验信息，并启动性能分析器
            YCSB_workloadInfo experimentInfo{TYPE, YCSB_tuple_count, READ_RATIO, ZIPF, (FLAGS_YCSB_local_zipf?"local_zipf":"global_zipf")};
            scalestore.startProfiler(experimentInfo);
            // 针对 FLAGS_worker 数量的工作线程进行迭代调度异步任务
            for (uint64_t t_i = 0; t_i < FLAGS_worker; ++t_i) {
               scalestore.getWorkerPool().scheduleJobAsync(t_i, [&, t_i]() {
                  running_threads_counter++;
                  // 初始化 BTree 和 DistributedBarrier 对象
                  storage::DistributedBarrier barrier(catalog.getCatalogEntry(BARRIER_ID).pid);
                  storage::BTree<K, V> tree(catalog.getCatalogEntry(BTREE_ID).pid);
                  barrier.wait();
                  // 在保持 keep_running 为 true 的情况下执行 YCSB 事务测试
                  while (keep_running) {
                      // 根据 Zipf 分布生成的 key 进行操作
                     K key = zipf_random->rand(zipf_offset);
                     ensure(key < YCSB_tuple_count);
                     V result;
                     // 根据 READ_RATIO 执行读或写操作，并更新性能统计信息
                     if (READ_RATIO == 100 || utils::RandomGenerator::getRandU64(0, 100) < READ_RATIO) {
                        auto start = utils::getTimePoint();
                        auto success = tree.lookup_opt(key, result);
                        ensure(success);
                        auto end = utils::getTimePoint();
                        threads::Worker::my().counters.incr_by(profiling::WorkerCounters::latency, (end - start));
                     } else {
                        V payload;
                        utils::RandomGenerator::getRandString(reinterpret_cast<u8*>(&payload), sizeof(V));
                        auto start = utils::getTimePoint();
                        tree.insert(key, payload);
                        auto end = utils::getTimePoint();
                        threads::Worker::my().counters.incr_by(profiling::WorkerCounters::latency, (end - start));
                     }
                     threads::Worker::my().counters.incr(profiling::WorkerCounters::tx_p);
                  }
                  running_threads_counter--;
               });
            }
            // -------------------------------------------------------------------------------------
            // Join Threads
            // -------------------------------------------------------------------------------------
             // 等待一段时间后设置 keep_running 为 false，结束测试
            sleep(FLAGS_YCSB_run_for_seconds);
            keep_running = false;
            // 等待所有工作线程完成
            while (running_threads_counter) {
               _mm_pause();
            }
            // 停止性能分析器
            scalestore.getWorkerPool().joinAll();
            // -------------------------------------------------------------------------------------
            scalestore.stopProfiler();
            // 如果 FLAGS_YCSB_record_latency 为 true，则执行以下操作：
            if (FLAGS_YCSB_record_latency) {
                // 设置用于控制线程状态的原子变量
               std::atomic<bool> keep_running = true;
               // 定义用于采样的 LATENCY_SAMPLES
               constexpr uint64_t LATENCY_SAMPLES = 1e6;
               // 创建 YCSB_workloadInfo 对象，描述实验信息，并启动性能分析器
               YCSB_workloadInfo experimentInfo{"Latency", YCSB_tuple_count, READ_RATIO, ZIPF, (FLAGS_YCSB_local_zipf?"local_zipf":"global_zipf")};
               scalestore.startProfiler(experimentInfo);
               // 存储线程级别的微秒级延迟
               std::vector<uint64_t> tl_microsecond_latencies[FLAGS_worker];
               // 针对每个工作线程调度异步任务
               for (uint64_t t_i = 0; t_i < FLAGS_worker; ++t_i) {
                  scalestore.getWorkerPool().scheduleJobAsync(t_i, [&, t_i]() {
                     running_threads_counter++;

                     uint64_t ops = 0;
                     storage::DistributedBarrier barrier(catalog.getCatalogEntry(BARRIER_ID).pid);
                     storage::BTree<K, V> tree(catalog.getCatalogEntry(BTREE_ID).pid);
                     barrier.wait();

                     // use for GAM comparision
                     // V payload;
                     // utils::RandomGenerator::getRandString(reinterpret_cast<u8*>(&payload), sizeof(V));
                      // 在 keep_running 为 true 的情况下执行延迟测试
                     while (keep_running) {
                        K key = zipf_random->rand(zipf_offset);
                        ensure(key < YCSB_tuple_count);
                        V result;
                        if (READ_RATIO == 100 || utils::RandomGenerator::getRandU64(0, 100) < READ_RATIO) {
                           auto start = utils::getTimePoint();
                           auto success = tree.lookup_opt(key, result);
                           ensure(success);
                           auto end = utils::getTimePoint();
                           threads::Worker::my().counters.incr_by(profiling::WorkerCounters::latency, (end - start));
                           if(ops < LATENCY_SAMPLES)
                              tl_microsecond_latencies[t_i].push_back(end - start);
                        } else {
                           // out-comment for GAM comparision
                            // 用于 GAM 比较
                           V payload;
                           utils::RandomGenerator::getRandString(reinterpret_cast<u8*>(&payload), sizeof(V)); 
                           auto start = utils::getTimePoint();
                           tree.insert(key, payload);
                           auto end = utils::getTimePoint();
                           threads::Worker::my().counters.incr_by(profiling::WorkerCounters::latency, (end - start));
                           if(ops < LATENCY_SAMPLES)
                              tl_microsecond_latencies[t_i].push_back(end - start);
                        }
                        threads::Worker::my().counters.incr(profiling::WorkerCounters::tx_p);
                        ops++;
                     }
                     running_threads_counter--;
                  });
               }
               // 等待一段时间后停止测试
               sleep(10);
               // 等待所有工作线程完成
               keep_running = false;
               while (running_threads_counter) {
                  _mm_pause();
               }
               // -------------------------------------------------------------------------------------
               // Join Threads
               // -------------------------------------------------------------------------------------
               scalestore.getWorkerPool().joinAll();
               // -------------------------------------------------------------------------------------
               scalestore.stopProfiler();
               // -------------------------------------------------------------------------------------

               // combine vector of threads into one
                // 将线程级别的延迟向量合并成一个向量
               std::vector<uint64_t> microsecond_latencies;
               for (uint64_t t_i = 0; t_i < FLAGS_worker; ++t_i) {
                  microsecond_latencies.insert(microsecond_latencies.end(), tl_microsecond_latencies[t_i].begin(), tl_microsecond_latencies[t_i].end());
               }
               
               {
                   // 输出打乱顺序的延迟样本数量
                  std::cout << "Shuffle samples " << microsecond_latencies.size() << std::endl;
                  // 创建用于随机数生成的随机设备
                  std::random_device rd;
                  std::mt19937 g(rd());
                  // 打乱延迟样本的顺序
                  std::shuffle(microsecond_latencies.begin(), microsecond_latencies.end(), g);

                  // 将400个样本写入文件
                  std::ofstream latency_file;
                  std::ofstream::openmode open_flags = std::ios::app;
                  std::string filename = "latency_samples_" + FLAGS_csvFile;
                  bool csv_initialized = std::filesystem::exists(filename);
                  latency_file.open(filename, open_flags);
                  // 如果文件未初始化，则添加标题行
                  if (!csv_initialized) {
                     latency_file << "workload,tag,ReadRatio,YCSB_tuple_count,zipf,latency" << std::endl;
                  }
                  // 将400个样本写入文件
                  for(uint64_t s_i =0; s_i < 1000; s_i++){
                     latency_file << TYPE << ","<<FLAGS_tag << "," << READ_RATIO << "," << YCSB_tuple_count << "," << ZIPF << "," << microsecond_latencies[s_i] << std::endl;
                  }
                  // 关闭文件
                  latency_file.close();
                  
               }
               std::cout << "Sorting Latencies"
                         << "\n";

               std::sort(microsecond_latencies.begin(), microsecond_latencies.end());
               std::cout << "Latency (min/median/max/99%): " << (microsecond_latencies[0]) << ","
                         << (microsecond_latencies[microsecond_latencies.size() / 2]) << ","
                         << (microsecond_latencies.back()) << ","
                         << (microsecond_latencies[(int)(microsecond_latencies.size() * 0.99)]) << std::endl;
               // -------------------------------------------------------------------------------------
               // write to csv file
               std::ofstream latency_file; // 声明输出文件流对象
               std::ofstream::openmode open_flags = std::ios::app; // 打开文件的模式
               std::string filename = "latency_" + FLAGS_csvFile;// 文件名
               bool csv_initialized = std::filesystem::exists(filename); // 检查文件是否存在
               // 打开文件
               latency_file.open(filename, open_flags);
               // 如果文件未初始化，添加标题行
               if (!csv_initialized) {
                  latency_file << "workload,tag,ReadRatio,YCSB_tuple_count,zipf,min,median,max,95th,99th,999th" << std::endl;
               }
               // 在文件末尾追加延迟数据行
               latency_file << TYPE << ","<<FLAGS_tag << ","  << READ_RATIO << "," << YCSB_tuple_count << "," << ZIPF << "," << (microsecond_latencies[0])
                            << "," << (microsecond_latencies[microsecond_latencies.size() / 2]) << ","
                            << (microsecond_latencies.back()) << ","
                            << (microsecond_latencies[(int)(microsecond_latencies.size() * 0.95)]) << ","
                            << (microsecond_latencies[(int)(microsecond_latencies.size() * 0.99)]) << ","
                            << (microsecond_latencies[(int)(microsecond_latencies.size() * 0.999)])
                            << std::endl;
               // 关闭文件流
               latency_file.close();
            }
         }
      }
   }
   std::cout << "Starting hash table report " << "\n";
   scalestore.getBuffermanager().reportHashTableStats();
   return 0;
}
