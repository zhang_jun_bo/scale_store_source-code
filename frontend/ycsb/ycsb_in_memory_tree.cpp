#include "PerfEvent.hpp"
#include "scalestore/Config.hpp"
#include "scalestore/utils/RandomGenerator.hpp"
#include "scalestore/threads/Concurrency.hpp"
#include "scalestore/utils/ScrambledZipfGenerator.hpp"
#include "scalestore/utils/Time.hpp"
#include "btree.hpp"
// -------------------------------------------------------------------------------------
#include <thread>
#include <gflags/gflags.h>
// -------------------------------------------------------------------------------------
DEFINE_uint32(YCSB_read_ratio, 100, "");
DEFINE_bool(YCSB_all_workloads, false, "Execute all workloads i.e. 50 95 100 ReadRatio on same tree");
DEFINE_uint64(YCSB_tuple_count, 1, " Tuple count in");
DEFINE_double(YCSB_zipf_factor, 0.0, "Default value according to spec");
DEFINE_double(YCSB_run_for_seconds, 10.0, "");
DEFINE_bool(YCSB_partitioned, false, "");
DEFINE_bool(YCSB_warm_up, false, "");
DEFINE_bool(YCSB_record_latency, false, "");
DEFINE_bool(YCSB_all_zipf, false, "");
// -------------------------------------------------------------------------------------
using u64 = uint64_t;
using u8 = uint8_t;
// -------------------------------------------------------------------------------------
static constexpr uint64_t BTREE_ID = 0;
static constexpr uint64_t BARRIER_ID = 1;
// -------------------------------------------------------------------------------------
template <u64 size>
struct BytesPayload {
	u8 value[size]; // 定义了一个名为 value 的 u8 类型数组成员，用于存储字节数据
	BytesPayload() = default; // 默认构造函数的声明，使用 "= default" 表示使用默认生成的构造函数
	// 重载了相等运算符，比较两个 BytesPayload 实例的 value 数组是否相等，使用 std::memcmp 函数进行字节级别的比较
	bool operator==(BytesPayload& other) { return (std::memcmp(value, other.value, sizeof(value)) == 0); }
	// 重载了不等运算符，返回相等运算符的否定结果
	bool operator!=(BytesPayload& other) { return !(operator==(other)); }
	// BytesPayload(const BytesPayload& other) { std::memcpy(value, other.value, sizeof(value)); }
	// BytesPayload& operator=(const BytesPayload& other)
	// {
	   // std::memcpy(value, other.value, sizeof(value));
	   // return *this;
	// }
};
// -------------------------------------------------------------------------------------
struct Partition {
	uint64_t begin;
	uint64_t end;
};
// -------------------------------------------------------------------------------------
int main(int argc, char* argv[])
{
	using namespace scalestore;
	using K = uint64_t;
	using V = BytesPayload<120>;

	gflags::SetUsageMessage("Catalog Test");
	gflags::ParseCommandLineFlags(&argc, &argv, true);
	// -------------------------------------------------------------------------------------
	// 创建一个Lambda函数，用于将一个范围分成多个分区
	auto partition = [&](uint64_t id, uint64_t participants, uint64_t N) -> Partition {
		// 计算每个分区的大小
		const uint64_t blockSize = N / participants;
		// 计算当前分区的起始位置
		auto begin = id * blockSize;
		// 计算当前分区的结束位置
		auto end = begin + blockSize;
		// 如果当前分区是最后一个分区，则结束位置应该是范围的末尾（N）
		if (id == participants - 1)
			end = N;
		// 返回一个包含起始位置和结束位置的Partition结构
		return { .begin = begin, .end = end };
		};

	// -------------------------------------------------------------------------------------
	// 准备工作负载所需的数据结构和线程容器
	std::vector<std::string> workload_type; // 用于存储工作负载类型，比如预热或基准测试
	std::vector<uint32_t> workloads; // 用于存储工作负载参数
	std::vector<double> zipfs;// 用于存储 Zipf 分布参数
	std::vector<std::thread> threads;// 用于存储线程
	// 根据标志 FLAGS_YCSB_all_workloads 来确定工作负载类型和参数
	if (FLAGS_YCSB_all_workloads) {
		// 如果需要所有的工作负载
		// 添加不同百分比的工作负载到 workloads 容器中
		workloads.push_back(5); // 添加 5% 的工作负载
		workloads.push_back(50); // 添加 50% 的工作负载
		workloads.push_back(95); // 添加 95% 的工作负载
		workloads.push_back(100); // 添加 100% 的工作负载
	}
	else {
		// 否则，只添加指定的读取比例 FLAGS_YCSB_read_ratio 作为工作负载
		workloads.push_back(FLAGS_YCSB_read_ratio);
	}

	// 根据标志 FLAGS_YCSB_warm_up 来确定工作负载类型
	if (FLAGS_YCSB_warm_up) {
		// 如果需要预热
		// 将 "YCSB_warm_up" 和 "YCSB_txn" 添加到 workload_type 容器中
		workload_type.push_back("YCSB_warm_up"); // 添加预热类型的工作负载
		workload_type.push_back("YCSB_txn"); // 添加事务类型的工作负载
	}
	else {
		// 否则，只添加事务类型的工作负载
		workload_type.push_back("YCSB_txn");
	}
	// 根据标志 FLAGS_YCSB_all_zipf 来确定是否添加 Zipf 分布的参数到 zipfs 向量中
	if (FLAGS_YCSB_all_zipf) {
		// zipfs.insert(zipfs.end(), {0.0,1.0,1.25,1.5,1.75,2.0});
		// 如果需要所有的 Zipf 参数
		// 向 zipfs 向量添加一组预定义的 Zipf 分布参数
		zipfs.insert(zipfs.end(), { 1.05,1.1,1.15,1.20,1.3,1.35,1.4,1.45 });
	}
	else {
		zipfs.push_back(FLAGS_YCSB_zipf_factor);
	}
	// -------------------------------------------------------------------------------------
	// -------------------------------------------------------------------------------------
	// 创建内存中的 B 树（BTree）
	// -------------------------------------------------------------------------------------
	btreeolc::BTree<K, V> tree;  // 使用模板创建 BTree，K 和 V 是键和值的类型
	concurrency::Barrier barrier(FLAGS_worker); // 创建一个并发屏障，用于同步多个线程的执行
	// -------------------------------------------------------------------------------------
	u64 YCSB_tuple_count = FLAGS_YCSB_tuple_count; // YCSB_tuple_count 是 YCSB 表中元组的数量
	// -------------------------------------------------------------------------------------
	// Build YCSB Table / Tree
	// -------------------------------------------------------------------------------------
	// 构建 YCSB 表或树
	std::cout << "Building B-Tree ";
	// 启动多个线程以并行方式向 B 树中插入元素
	for (uint64_t t_i = 0; t_i < FLAGS_worker; ++t_i) {
		threads.emplace_back([&, t_i]() {
			// -------------------------------------------------------------------------------------
			 // 对当前线程的元组范围进行划分
			auto threadPartition = partition(t_i, FLAGS_worker, YCSB_tuple_count);
			auto begin = threadPartition.begin;  // 当前线程处理范围的起始键值
			auto end = threadPartition.end; // 当前线程处理范围的结束键值
			// -------------------------------------------------------------------------------------
			// 等待所有线程准备好再开始插入操作
			barrier.wait();
			// -------------------------------------------------------------------------------------
			V value; // 用于存储值的变量
			for (K k_i = begin; k_i < end; k_i++) {
				// 生成随机值，并将键-值对插入 B 树
				utils::RandomGenerator::getRandString(reinterpret_cast<u8*>(&value), sizeof(V));
				tree.insert(k_i, value);
			}
			// -------------------------------------------------------------------------------------
			 // 等待所有线程完成插入操作
			barrier.wait();
			});
	}

	for (auto& thread : threads) {
		thread.join();
	}
	threads.clear();
	std::cout << "[OK] " << "\n";

	struct tx_counter {
		alignas(64) std::atomic<uint64_t> txn;
	};

	for (auto ZIPF : zipfs) {
		// 遍历不同的 Zipf 参数
		// -------------------------------------------------------------------------------------
		// YCSB Transaction
		// -------------------------------------------------------------------------------------
		 // YCSB 事务
		std::unique_ptr<utils::ScrambledZipfGenerator> zipf_random =
			std::make_unique<utils::ScrambledZipfGenerator>(0, YCSB_tuple_count, ZIPF);

		// 遍历不同的读取比例和工作负载类型
		for (auto READ_RATIO : workloads) {
			for (auto TYPE : workload_type) {
				std::atomic<bool> keep_running = true; // 用于标识是否保持运行
				std::atomic<u64> running_threads_counter = 0;  // 用于计数运行中的线程数
				// std::atomic<uint64_t> thread_tx[FLAGS_worker];            
				tx_counter thread_tx[FLAGS_worker]; // 用于跟踪每个线程的事务计数
				// 启动多个线程以模拟 YCSB 工作负载
				for (uint64_t t_i = 0; t_i < FLAGS_worker; ++t_i) {
					threads.emplace_back([&, t_i]() {

						running_threads_counter++;
						thread_tx[t_i].txn = 0;
						barrier.wait();
						// 在保持运行标志有效的情况下模拟 YCSB 工作负载
						while (keep_running) {
							K key = zipf_random->rand();
							ensure(key < YCSB_tuple_count);
							V result;

							if (READ_RATIO == 100 || utils::RandomGenerator::getRandU64(0, 100) < READ_RATIO) {
								// auto start = utils::getTimePoint();
								// 读取操作
								auto success = tree.lookup(key, result);
								ensure(success);
								// auto end = utils::getTimePoint();
								// threads::Worker::my().counters.incr_by(profiling::WorkerCounters::latency, (end - start));
							}
							else {
								// 写入操作
								V payload;
								utils::RandomGenerator::getRandString(reinterpret_cast<u8*>(&payload), sizeof(V));
								// auto start = utils::getTimePoint();
								tree.insert(key, payload);
								// auto end = utils::getTimePoint();
								// threads::Worker::my().counters.incr_by(profiling::WorkerCounters::latency, (end - start));
							}
							// threads::Worker::my().counters.incr(profiling::WorkerCounters::opsPerSec);
							// 记录事务计数
							thread_tx[t_i].txn++;
						}
						running_threads_counter--;
						});
				}
				// -------------------------------------------------------------------------------------
				// profiler
				 // 启动一个用于记录性能数据的线程
				threads.emplace_back([&]() {
					running_threads_counter++;

					u64 time = 0;
					std::cout << "t,workload,read-ratio,tag,tx_committed" << std::endl;
					while (keep_running) {
						std::cout << time++ << "," << TYPE << "," << READ_RATIO << "," << FLAGS_tag << ",";
						u64 total_committed = 0;
						// 获取并重置每个线程的事务计数
						for (u64 t_i = 0; t_i < FLAGS_worker; t_i++) {
							total_committed += thread_tx[t_i].txn.exchange(0);
						}
						std::cout << total_committed << std::endl;
						sleep(1);
					}
					running_threads_counter--;
					});

				// -------------------------------------------------------------------------------------
				// Join Threads
				// -------------------------------------------------------------------------------------
				// 运行 YCSB 指定时长
				sleep(FLAGS_YCSB_run_for_seconds);
				// 终止 YCSB 模拟运行
				keep_running = false;
				// 等待所有线程完成
				while (running_threads_counter) {
					_mm_pause();
				}
				// 等待所有线程结束并清理线程容器
				for (auto& thread : threads) {
					thread.join();
				}
				threads.clear();
			}
		}
	}

	return 0;
}
