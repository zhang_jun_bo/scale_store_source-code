#pragma once

/*
* 定义了 warehouse_t 结构体，描述了仓库的各个属性，包括仓库ID、名称、地址、城市、州、邮政编码、税率、年至今总收入等字段。
同时，它还包含了一个嵌套的 Key 结构体，用于定义仓库的键，并重载了比较运算符来支持键对象之间的比较操作。
在结构体中还有一个名为 padding 的填充字段，用于解决争用问题。
*/
struct warehouse_t {
	static constexpr int id = 0;
	struct Key {
		static constexpr int id = 0;
		Integer w_id;

		bool operator==(const Key& other) { return w_id == other.w_id; }
		bool operator>(const Key& other) const { return w_id > other.w_id; }
		bool operator>=(const Key& other) const { return w_id >= other.w_id; }
		bool operator<(const Key& other) const { return w_id < other.w_id; }
	};
	Varchar<10> w_name;
	Varchar<20> w_street_1;
	Varchar<20> w_street_2;
	Varchar<20> w_city;
	Varchar<2> w_state;
	Varchar<9> w_zip;
	Numeric w_tax;
	Numeric w_ytd;
	uint8_t padding[1024];  // fix for contention; could be solved with contention split from
	// http://cidrdb.org/cidr2021/papers/cidr2021_paper21.pdf
};

/*
* 定义了 district_t 结构体，表示地区的各个属性，包括地区ID、仓库ID、名称、地址、城市、州、邮政编码、税率、年至今总收入、下一个订单ID等字段。
  同时，它还包含了一个嵌套的 Key 结构体，用于定义地区的键，并重载了比较运算符来支持键对象之间的比较操作。
*/
struct district_t {
	static constexpr int id = 1;
	struct Key {
		static constexpr int id = 1;
		Integer d_w_id;
		Integer d_id;

		bool operator==(const Key& other) { return (d_w_id == other.d_w_id) && (d_id == other.d_id); }
		bool operator>(const Key& other) const {
			if (d_w_id > other.d_w_id) return true;
			if (d_w_id < other.d_w_id) return false;
			return (d_id > other.d_id);  // equal
		}

		bool operator<(const Key& other) const {
			if (d_w_id < other.d_w_id) return true;
			if (d_w_id > other.d_w_id) return false;
			return (d_id < other.d_id);  // equal
		}
		bool operator>=(const Key& other) const { return !(*this < other); }
	};
	Varchar<10> d_name;
	Varchar<20> d_street_1;
	Varchar<20> d_street_2;
	Varchar<20> d_city;
	Varchar<2> d_state;
	Varchar<9> d_zip;
	Numeric d_tax;
	Numeric d_ytd;
	Integer d_next_o_id;
};

/*
* 定义了 customer_t 结构体，表示客户的各个属性信息，包括客户ID、仓库ID、地区ID、名字、中间名、姓氏、地址、城市、州、邮政编码、电话号码、注册时间、信用类型、信用额度、折扣率、余额、年至今付款总额、付款次数、交付次数、客户数据等字段。
  同时，它还包含了一个嵌套的 Key 结构体，用于定义客户的键，并重载了比较运算符来支持键对象之间的比较操作。
*/
struct customer_t {
	static constexpr int id = 2;
	struct Key {
		static constexpr int id = 2;
		Integer c_w_id;
		Integer c_d_id;
		Integer c_id;

		bool operator==(const Key& other) { return (c_w_id == other.c_w_id) && (c_d_id == other.c_d_id) && (c_id == other.c_id); }

		bool operator<(const Key& other) const {
			if (c_w_id < other.c_w_id) return true;
			if (c_w_id > other.c_w_id) return false;
			// equal c_w_id
			if (c_d_id < other.c_d_id) return true;
			if (c_d_id > other.c_d_id) return false;
			// equal
			return (c_id < other.c_id);  // equal
		}
		bool operator>(const Key& other) const { return (other < *this); }
		bool operator>=(const Key& other) const { return !(*this < other); }
	};
	Varchar<16> c_first;
	Varchar<2> c_middle;
	Varchar<16> c_last;
	Varchar<20> c_street_1;
	Varchar<20> c_street_2;
	Varchar<20> c_city;
	Varchar<2> c_state;
	Varchar<9> c_zip;
	Varchar<16> c_phone;
	Timestamp c_since;
	Varchar<2> c_credit;
	Numeric c_credit_lim;
	Numeric c_discount;
	Numeric c_balance;
	Numeric c_ytd_payment;
	Numeric c_payment_cnt;
	Numeric c_delivery_cnt;
	Varchar<500> c_data;
};

/*
* 定义了 customer_wdl_t 结构体，表示带有仓库、地区和名字的客户信息。
其中包括了一个嵌套的 Key 结构体，用于定义客户的键，并重载了比较运算符来支持键对象之间的比较操作。该结构体包含了客户的仓库 ID、地区 ID、姓氏、名字和客户 ID 等字段。
*/
struct customer_wdl_t {
	static constexpr int id = 3;
	struct Key {
		static constexpr int id = 3;
		Integer c_w_id;
		Integer c_d_id;
		Varchar<16> c_last;
		Varchar<16> c_first;

		bool operator==(const Key& other) const {
			return (c_w_id == other.c_w_id) && (c_d_id == other.c_d_id) && (c_last == other.c_last) && (c_first == other.c_first);
		}

		bool operator<(const Key& other) const {
			if (c_w_id < other.c_w_id) return true;
			if (c_w_id > other.c_w_id) return false;

			if (c_d_id < other.c_d_id) return true;
			if (c_d_id > other.c_d_id) return false;

			if (c_last < other.c_last) return true;
			if (c_last > other.c_last) return false;

			return (c_first < other.c_first);
		}
		bool operator>(const Key& other) const { return (other < *this); }
		bool operator>=(const Key& other) const { return !(*this < other); }
	};
	Integer c_id;
};

/*
定义了 history_t 结构体，表示历史信息。
它包含了一个嵌套的 Key 结构体，用于定义历史信息的键，并重载了比较运算符来支持键对象之间的比较操作。
history_t 结构体包括了历史信息的客户 ID、地区 ID、仓库 ID、日期时间戳、金额和数据等字段。
*/
struct history_t {
	static constexpr int id = 4;
	struct Key {
		static constexpr int id = 4;
		Integer thread_id;
		Integer h_pk;

		bool operator==(const Key& other) const {
			return (thread_id == other.thread_id) && (h_pk == other.h_pk);
		}

		bool operator<(const Key& other) const {
			if (thread_id < other.thread_id) return true;
			if (thread_id > other.thread_id) return false;
			return (h_pk < other.h_pk);
		}

		bool operator>(const Key& other) const { return (other < *this); }
		bool operator>=(const Key& other) const { return !(*this < other); }
	};
	Integer h_c_id;
	Integer h_c_d_id;
	Integer h_c_w_id;
	Integer h_d_id;
	Integer h_w_id;
	Timestamp h_date;
	Numeric h_amount;
	Varchar<24> h_data;
};
/*
定义了 neworder_t 结构体，表示新订单信息。
它包含了一个嵌套的 Key 结构体，用于定义新订单信息的键，并重载了比较运算符来支持键对象之间的比较操作。
neworder_t 结构体的 Key 包括了新订单信息的仓库 ID、地区 ID 和订单 ID。
*/
struct neworder_t {
	static constexpr int id = 5;
	struct Key {
		static constexpr int id = 5;
		Integer no_w_id;
		Integer no_d_id;
		Integer no_o_id;

		bool operator==(const Key& other) const {
			return (no_w_id == other.no_w_id) && (no_d_id == other.no_d_id) && (no_o_id == other.no_o_id);
		}

		bool operator<(const Key& other) const {
			if (no_w_id < other.no_w_id) return true;
			if (no_w_id > other.no_w_id) return false;
			// equal c_w_id
			if (no_d_id < other.no_d_id) return true;
			if (no_d_id > other.no_d_id) return false;
			// equal
			return (no_o_id < other.no_o_id);  // equal
		}

		bool operator>(const Key& other) const { return (other < *this); }
		bool operator>=(const Key& other) const { return !(*this < other); }
	};
};

/*
定义了 order_t 结构体，表示订单信息。它包含了一个嵌套的 Key 结构体，用于定义订单信息的键，并重载了比较运算符来支持键对象之间的比较操作。
order_t 结构体的 Key 包括了订单信息的仓库 ID、地区 ID 和订单 ID，还包括了订单信息的其他属性如客户 ID、入库日期、运货人 ID、订单行数和所有本地信息。
*/
struct order_t {
	static constexpr int id = 6;
	struct Key {
		static constexpr int id = 6;
		Integer o_w_id;
		Integer o_d_id;
		Integer o_id;

		bool operator==(const Key& other) const { return (o_w_id == other.o_w_id) && (o_d_id == other.o_d_id) && (o_id == other.o_id); }

		bool operator<(const Key& other) const {
			if (o_w_id < other.o_w_id) return true;
			if (o_w_id > other.o_w_id) return false;
			// equal c_w_id
			if (o_d_id < other.o_d_id) return true;
			if (o_d_id > other.o_d_id) return false;
			// equal
			return (o_id < other.o_id);  // equal
		}

		bool operator>(const Key& other) const { return (other < *this); }
		bool operator>=(const Key& other) const { return !(*this < other); }
	};
	Integer o_c_id;
	Timestamp o_entry_d;
	Integer o_carrier_id;
	Numeric o_ol_cnt;
	Numeric o_all_local;
};

/*
定义了 order_wdc_t 结构体，表示订单信息（包含仓库、地区和客户 ID）。
它包含了一个嵌套的 Key 结构体，用于定义订单信息的键，并重载了比较运算符来支持键对象之间的比较操作。
order_wdc_t 结构体的 Key 包括了订单信息的仓库 ID、地区 ID、客户 ID 和订单 ID。
*/
struct order_wdc_t {
	static constexpr int id = 7;
	struct Key {
		static constexpr int id = 7;
		Integer o_w_id;
		Integer o_d_id;
		Integer o_c_id;
		Integer o_id;

		bool operator==(const Key& other) const {
			return (o_w_id == other.o_w_id) && (o_d_id == other.o_d_id) && (o_c_id == other.o_c_id) && (o_id == other.o_id);
		}

		bool operator<(const Key& other) const {
			if (o_w_id < other.o_w_id) return true;
			if (o_w_id > other.o_w_id) return false;

			if (o_d_id < other.o_d_id) return true;
			if (o_d_id > other.o_d_id) return false;

			if (o_c_id < other.o_c_id) return true;
			if (o_c_id > other.o_c_id) return false;
			return (o_id < other.o_id);
		}
		bool operator>(const Key& other) const { return (other < *this); }
		bool operator>=(const Key& other) const { return !(*this < other); }
	};
};

/*
定义了 orderline_t 结构体，表示订单行信息。它包含了一个嵌套的 Key 结构体，用于定义订单行信息的键，并重载了比较运算符来支持键对象之间的比较操作。
orderline_t 结构体的 Key 包括了订单行信息的仓库 ID、地区 ID、订单 ID 和订单行号，而其他成员变量包括了订单行信息的订单商品 ID、供应仓库 ID、交付时间戳、数量、金额和分销信息。
*/
struct orderline_t {
	static constexpr int id = 8;
	struct Key {
		static constexpr int id = 8;
		Integer ol_w_id;
		Integer ol_d_id;
		Integer ol_o_id;
		Integer ol_number;

		bool operator==(const Key& other) const {
			return (ol_w_id == other.ol_w_id) && (ol_d_id == other.ol_d_id) && (ol_o_id == other.ol_o_id) && (ol_number == other.ol_number);
		}

		bool operator<(const Key& other) const {
			if (ol_w_id < other.ol_w_id) return true;
			if (ol_w_id > other.ol_w_id) return false;

			if (ol_d_id < other.ol_d_id) return true;
			if (ol_d_id > other.ol_d_id) return false;

			if (ol_o_id < other.ol_o_id) return true;
			if (ol_o_id > other.ol_o_id) return false;
			return (ol_number < other.ol_number);
		}
		bool operator>(const Key& other) const { return (other < *this); }
		bool operator>=(const Key& other) const { return !(*this < other); }
	};
	Integer ol_i_id;
	Integer ol_supply_w_id;
	Timestamp ol_delivery_d;
	Numeric ol_quantity;
	Numeric ol_amount;
	Varchar<24> ol_dist_info;
};

/*
定义了 item_t 结构体，表示商品信息。它包含了一个嵌套的 Key 结构体，用于定义商品信息的键，并重载了比较运算符来支持键对象之间的比较操作。
item_t 结构体的 Key 包括了商品信息的商品 ID，而其他成员变量包括了商品信息的商品影像 ID、商品名称、商品价格和商品数据。
*/
struct item_t {
	static constexpr int id = 9;
	struct Key {
		static constexpr int id = 9;
		Integer i_id;

		bool operator==(const Key& other) { return i_id == other.i_id; }
		bool operator>(const Key& other) const { return i_id > other.i_id; }
		bool operator>=(const Key& other) const { return i_id >= other.i_id; }
		bool operator<(const Key& other) const { return i_id < other.i_id; }
	};
	Integer i_im_id;
	Varchar<24> i_name;
	Numeric i_price;
	Varchar<50> i_data;
};

/*
定义了 stock_t 结构体，表示库存信息。它包含了一个嵌套的 Key 结构体，用于定义库存信息的键，并重载了比较运算符来支持键对象之间的比较操作。
stock_t 结构体的 Key 包括了库存信息的仓库 ID 和商品 ID，而其他成员变量包括了库存数量、分销区域信息、年至今总销售量、订单数量、远程订单数量以及库存数据等信息。
*/
struct stock_t {
	static constexpr int id = 10;
	struct Key {
		static constexpr int id = 10;
		Integer s_w_id;
		Integer s_i_id;


		bool operator==(const Key& other) const {
			return (s_w_id == other.s_w_id) && (s_i_id == other.s_i_id);
		}

		bool operator<(const Key& other) const {
			if (s_w_id < other.s_w_id) return true;
			if (s_w_id > other.s_w_id) return false;
			return (s_i_id < other.s_i_id);
		}
		bool operator>(const Key& other) const { return (other < *this); }
		bool operator>=(const Key& other) const { return !(*this < other); }
	};
	Numeric s_quantity;
	Varchar<24> s_dist_01;
	Varchar<24> s_dist_02;
	Varchar<24> s_dist_03;
	Varchar<24> s_dist_04;
	Varchar<24> s_dist_05;
	Varchar<24> s_dist_06;
	Varchar<24> s_dist_07;
	Varchar<24> s_dist_08;
	Varchar<24> s_dist_09;
	Varchar<24> s_dist_10;
	Numeric s_ytd;
	Numeric s_order_cnt;
	Numeric s_remote_cnt;
	Varchar<50> s_data;
};

// -------------------------------------------------------------------------------------
// output operator
std::ostream& operator<<(std::ostream& os, item_t c) {
	os << "item: " << c.i_im_id << " " << c.i_name.toString();
	return os;
}
