#pragma once
#include "types.hpp"
// -------------------------------------------------------------------------------------
#include "scalestore/Config.hpp"
#include "scalestore/ScaleStore.hpp"
// -------------------------------------------------------------------------------------
#include <cassert>
#include <cstdint>
#include <cstring>
#include <map>
#include <string> 

using namespace scalestore;

template <class Record>
struct ScaleStoreAdapter{
    // 使用 Record::Key 作为 BTree 的键，Record 作为值
   using BTree = storage::BTree<typename Record::Key, Record>;
   // 存储 ScaleStore 相关信息的适配器
   std::string name; // 适配器名称
   PID tree_pid; // B 树的 PID
   // 默认构造函数
   ScaleStoreAdapter(){};
   // 构造函数，接受 ScaleStore 和名称
   ScaleStoreAdapter(ScaleStore& db, std::string name): name(name){
      auto& catalog = db.getCatalog();
      // 如果是节点 0，创建 B 树
      if(db.getNodeID() == 0){
         db.createBTree<typename Record::Key, Record>();
      }
      tree_pid = catalog.getCatalogEntry(Record::id).pid;
   };
   // 插入记录
   void insert(const typename Record::Key& rec_key, const Record& record){
      BTree tree(tree_pid);
      tree.insert(rec_key, record);
   }
   // 扫描 B 树
   template <class Fn>
   void scan(const typename Record::Key& key, const Fn& fn) {
      BTree tree(tree_pid);
      tree.template scan<typename BTree::ASC_SCAN>(key, [&](typename Record::Key& key, Record& record) { return fn(key, record); });
   }
   // 逆序扫描 B 树
   template <class Fn>
   void scanDesc(const typename Record::Key& key, const Fn& fn) {
      BTree tree(tree_pid);
      tree.template scan<typename BTree::DESC_SCAN>(key, [&](typename Record::Key& key, Record& record) { return fn(key, record); });
   }
   // 查找特定字段的值
   template <class Field>
   auto lookupField(const typename Record::Key& key, [[maybe_unused]] Field Record::*f) {
      BTree tree(tree_pid);
      Field local_f;
      auto res = tree.lookup_opt(key, [&](Record& value) { local_f = value.*f; });
      ensure(res);
      return local_f;
   }
   // 更新记录
   template <class Fn>
   // void update1(const typename Record::Key& key, const Fn& fn, storage::btree::WALUpdateGenerator wal_update_generator)
   void update1(const typename Record::Key& key, const Fn& fn)
   {
      BTree tree(tree_pid);
      auto res = tree.lookupAndUpdate(key, [&](Record& value) { fn(value); });
      ensure(res);
   }
   // 查找记录
   template <class Fn>
   void lookup1(const typename Record::Key& key, const Fn& fn) {
      BTree tree(tree_pid);
      const auto res = tree.lookup_opt(key, fn);
      ensure(res);
   }
   // 移除记录
   bool erase(const typename Record::Key& key) {
      BTree tree(tree_pid);
      const auto res = tree.remove(key);
      return res;
   }
};
