#include "adapter.hpp"
#include "scalestore/Config.hpp"
#include "scalestore/ScaleStore.hpp"
#include "scalestore/rdma/CommunicationManager.hpp"
#include "scalestore/storage/datastructures/BTree.hpp"
#include "scalestore/utils/RandomGenerator.hpp"
#include "scalestore/utils/ScrambledZipfGenerator.hpp"
#include "scalestore/utils/Time.hpp"
#include "schema.hpp"
#include "types.hpp"
// -------------------------------------------------------------------------------------
#include <gflags/gflags.h>
// -------------------------------------------------------------------------------------
#include <unistd.h>
#include <iostream>
#include <set>
#include <string>
#include <vector>
// -------------------------------------------------------------------------------------
DEFINE_uint32(tpcc_warehouse_count, 1, "");
DEFINE_int32(tpcc_abort_pct, 0, "");
DEFINE_uint64(run_until_tx, 0, "");
DEFINE_bool(tpcc_warehouse_affinity, false, "");  // maps every thread to a dedicated warehouse
DEFINE_bool(tpcc_warehouse_locality, false, "");  // maps threads to local warehouses
DEFINE_bool(tpcc_fast_load, false, "");
DEFINE_bool(tpcc_remove, true, "");
DEFINE_double(TPCC_run_for_seconds, 10.0, "");
// -------------------------------------------------------------------------------------
using namespace scalestore;
// adapter and ids
ScaleStoreAdapter<warehouse_t> warehouse;       // 0
ScaleStoreAdapter<district_t> district;         // 1
ScaleStoreAdapter<customer_t> customer;         // 2
ScaleStoreAdapter<customer_wdl_t> customerwdl;  // 3
ScaleStoreAdapter<history_t> history;           // 4
ScaleStoreAdapter<neworder_t> neworder;         // 5
ScaleStoreAdapter<order_t> order;               // 6
ScaleStoreAdapter<order_wdc_t> order_wdc;       // 7
ScaleStoreAdapter<orderline_t> orderline;       // 8
ScaleStoreAdapter<item_t> item;                 // 9
ScaleStoreAdapter<stock_t> stock;               // 10
static constexpr uint64_t barrier_id = 11;

// -------------------------------------------------------------------------------------
// yeah, dirty include...
#include "tpcc_workload.hpp"
// -------------------------------------------------------------------------------------
/**
 * @brief 根据两个 chrono 时间点之间的时间差计算每秒事务数（TPS），以百万为单位。
 *
 * @param begin 开始时间点。
 * @param end 结束时间点。
 * @param factor 计算中使用的因子。
 * @return 返回以百万为单位的每秒事务数（TPS）。
 */
double calculateMTPS(std::chrono::high_resolution_clock::time_point begin, std::chrono::high_resolution_clock::time_point end, u64 factor) {
	// 计算给定时间点之间的微秒级持续时间，并将其转换为秒  通过将因子除以持续时间（秒）来计算每秒事务数（TPS）
	double tps = ((factor * 1.0 / (std::chrono::duration_cast<std::chrono::microseconds>(end - begin).count() / 1000000.0)));
	// 将 TPS 转换为百万单位
	return (tps / 1000000.0);
}
// -------------------------------------------------------------------------------------
/**
 * @brief TPCC_workloadInfo 类，继承自 scalestore::profiling::WorkloadInfo。
 * 用于 TPCC 工作负载信息的记录和处理。
 */
struct TPCC_workloadInfo : public scalestore::profiling::WorkloadInfo {
	std::string experiment;
	uint64_t warehouses;
	std::string configuration;

	/**
	* @brief 构造函数，初始化 TPCC_workloadInfo 对象。
	* @param experiment 实验名称
	* @param warehouses 仓库数量
	* @param configuration 配置信息
	*/
	TPCC_workloadInfo(std::string experiment, uint64_t warehouses, std::string configuration)
		: experiment(experiment), warehouses(warehouses), configuration(configuration)
	{
	}

	/**
	* @brief 获取该工作负载信息的一行数据。
	* @return 包含实验名称、仓库数量和配置信息的字符串向量。
	*/
	virtual std::vector<std::string> getRow() {
		return { experiment,std::to_string(warehouses), configuration };
	}
	/**
	* @brief 获取该工作负载信息的表头。
	* @return 包含列标题的字符串向量。
	*/
	virtual std::vector<std::string> getHeader() {
		return { "experiment","warehouses","configuration" };
	}

	/**
	* @brief 将工作负载信息以 CSV 格式写入文件。
	* @param file 输出 CSV 文件流。
	*/
	virtual void csv(std::ofstream& file) override
	{
		file << experiment << " , ";
		file << warehouses << " , ";
		file << configuration << " , ";
		file << configuration << " , ";
	}
	/**
	* @brief 将工作负载信息的 CSV 表头写入文件。
	* @param file 输出 CSV 文件流。
	*/
	virtual void csvHeader(std::ofstream& file) override
	{
		file << "Experiment"
			<< " , ";
		file << "Warehouses"
			<< " , ";
		file << "Configuration"
			<< " , ";
	}
};
// -------------------------------------------------------------------------------------
// MAIN
int main(int argc, char* argv[]) {
	//设置程序的用法消息为 "Leanstore TPC-C"。
	gflags::SetUsageMessage("Leanstore TPC-C");

	gflags::ParseCommandLineFlags(&argc, &argv, true);
	// -------------------------------------------------------------------------------------
	// 创建 ScaleStore 实例
	ScaleStore db;

	// 获取数据库的目录信息
	auto& catalog = db.getCatalog();
	// -------------------------------------------------------------------------------------
	// 定义 barrier_wait Lambda 函数，用于创建和等待分布式屏障以同步工作线程
	auto barrier_wait = [&]() {
		// 遍历工作线程数量
		for (uint64_t t_i = 0; t_i < FLAGS_worker; ++t_i) {
			// 异步地将工作任务调度到工作线程中
			db.getWorkerPool().scheduleJobAsync(t_i, [&, t_i]() {
				// 创建分布式屏障并等待同步
				storage::DistributedBarrier barrier(catalog.getCatalogEntry(barrier_id).pid);
				barrier.wait();
				});
		}
		// 等待所有工作线程执行完毕
		db.getWorkerPool().joinAll();
		};
	// -------------------------------------------------------------------------------------
	// 定义名为 `partition` 的 Lambda 函数，用于对给定范围进行分区
	auto partition = [&](uint64_t id, uint64_t participants, uint64_t N) -> Partition {
		// 计算每个参与者（participant）的块大小
		const uint64_t blockSize = N / participants;

		// 计算当前参与者的起始位置和结束位置
		auto begin = id * blockSize;
		auto end = begin + blockSize;
		// 如果当前参与者是最后一个参与者，则设置结束位置为 N
		if (id == participants - 1) end = N;
		// 返回表示分区的结构体 Partition
		return { .begin = begin, .end = end };
		};
	// -------------------------------------------------------------------------------------
	warehouseCount = FLAGS_tpcc_warehouse_count;  // xxx remove as it is defined in workload
	// -------------------------------------------------------------------------------------
	// 确保仓库数量大于等于节点数
	ensure((uint64_t)warehouseCount >= FLAGS_nodes);
	// 如果启用了 TPC-C 仓库亲和性（warehouse affinity）, 确保仓库数量大于等于（工作线程数 * 节点数）
	if (FLAGS_tpcc_warehouse_affinity) ensure((uint64_t)warehouseCount >= (FLAGS_worker * FLAGS_nodes));
	// -------------------------------------------------------------------------------------
	// 生成数据表
	db.getWorkerPool().scheduleJobSync(0, [&]() {
		// 使用 ScaleStoreAdapter 适配器创建不同的数据表实例
		warehouse = ScaleStoreAdapter<warehouse_t>(db, "warehouse");
		district = ScaleStoreAdapter<district_t>(db, "district");
		customer = ScaleStoreAdapter<customer_t>(db, "customer");
		customerwdl = ScaleStoreAdapter<customer_wdl_t>(db, "customerwdl");
		history = ScaleStoreAdapter<history_t>(db, "history");
		neworder = ScaleStoreAdapter<neworder_t>(db, "neworder");
		order = ScaleStoreAdapter<order_t>(db, "order");
		order_wdc = ScaleStoreAdapter<order_wdc_t>(db, "order_wdc");
		orderline = ScaleStoreAdapter<orderline_t>(db, "orderline");
		item = ScaleStoreAdapter<item_t>(db, "item");
		stock = ScaleStoreAdapter<stock_t>(db, "stock");
		// -------------------------------------------------------------------------------------
		// 如果当前节点为第一个节点（节点ID为0），则创建一个分布式屏障（distributed barrier）
		if (db.getNodeID() == 0) db.createBarrier(FLAGS_worker * FLAGS_nodes);  // distributed barrier
		});
	// -------------------------------------------------------------------------------------
	// load data
	// 通过 partition 函数计算每个节点需要处理的仓库范围
	warehouse_range_node = partition(db.getNodeID(), FLAGS_nodes, warehouseCount);

	// 在节点0上加载商品（item）数据，因为它是只读的，并且无论如何都会被复制
	if (db.getNodeID() == 0) {
		db.getWorkerPool().scheduleJobSync(0, [&]() { loadItem(); });
	}

	// 生成仓库数据
	db.getWorkerPool().scheduleJobSync(0, [&]() { loadWarehouse(warehouse_range_node.begin, warehouse_range_node.end); });

	{  // 为每个线程分配并行任务来填充其他的表
		std::atomic<uint32_t> g_w_id = warehouse_range_node.begin + 1;
		for (uint32_t t_i = 0; t_i < FLAGS_worker; t_i++) {
			db.getWorkerPool().scheduleJobAsync(t_i, [&, t_i]() {
				thread_id = t_i + (db.getNodeID() * FLAGS_worker);
				ensure((uint64_t)thread_id < MAX_THREADS);
				while (true) {
					uint32_t w_id = g_w_id++;
					if (w_id > warehouse_range_node.end) { return; }
					// 开始事务
					loadStock(w_id);
					loadDistrinct(w_id);
					for (Integer d_id = 1; d_id <= 10; d_id++) {
						loadCustomer(w_id, d_id);
						loadOrders(w_id, d_id);
					}
					// 提交事务
				}
				});
		}
		db.getWorkerPool().joinAll();
	}
	// 定义了一个 consistencyCheck 函数，用于执行表的一致性检查
	auto consistencyCheck = [&]() {
		// 在线程池中的节点0执行同步任务
		db.getWorkerPool().scheduleJobSync(0, [&]() {
			// -------------------------------------------------------------------------------------
			   // 用于统计表中元组数量的变量
			uint64_t warehouse_tbl = 0;
			uint64_t district_tbl = 0;
			uint64_t customer_tbl = 0;
			uint64_t customer_wdl_tbl = 0;
			uint64_t history_tbl = 0;
			uint64_t neworder_tbl = 0;
			uint64_t order_tbl = 0;
			uint64_t order_wdc_tbl = 0;
			uint64_t orderline_tbl = 0;
			uint64_t item_tbl = 0;
			uint64_t stock_tbl = 0;
			// 对各个表执行扫描，并计算各表中的元组数量
			warehouse.scan({ .w_id = 0 }, [&]([[maybe_unused]] warehouse_t::Key key, [[maybe_unused]] warehouse_t record) {
				warehouse_tbl++;
				return true;
				});
			district.scan({ .d_w_id = 0, .d_id = 0 }, [&]([[maybe_unused]] district_t::Key key, [[maybe_unused]] district_t record) {
				district_tbl++;
				return true;
				});
			customer.scan({ .c_w_id = 0, .c_d_id = 0, .c_id = 0 },
				[&]([[maybe_unused]] customer_t::Key key, [[maybe_unused]] customer_t record) {
					customer_tbl++;
					return true;
				});
			customerwdl.scan({ .c_w_id = 0, .c_d_id = 0, .c_last = "", .c_first = "" },
				[&]([[maybe_unused]] customer_wdl_t::Key key, [[maybe_unused]] customer_wdl_t record) {
					customer_wdl_tbl++;
					return true;
				});
			history.scan({ .thread_id = 0, .h_pk = 0 }, [&]([[maybe_unused]] history_t::Key key, [[maybe_unused]] history_t record) {
				history_tbl++;
				return true;
				});
			neworder.scan({ .no_w_id = 0, .no_d_id = 0, .no_o_id = 0 },
				[&]([[maybe_unused]] neworder_t::Key key, [[maybe_unused]] neworder_t record) {
					neworder_tbl++;
					return true;
				});

			order.scan({ .o_w_id = 0, .o_d_id = 0, .o_id = 0 }, [&]([[maybe_unused]] order_t::Key key, [[maybe_unused]] order_t record) {
				order_tbl++;
				return true;
				});

			order_wdc.scan({ .o_w_id = 0, .o_d_id = 0, .o_c_id = 0, .o_id = 0 },
				[&]([[maybe_unused]] order_wdc_t::Key key, [[maybe_unused]] order_wdc_t record) {
					order_wdc_tbl++;
					return true;
				});

			orderline.scan({ .ol_w_id = 0, .ol_d_id = 0, .ol_o_id = 0, .ol_number = 0 },
				[&]([[maybe_unused]] orderline_t::Key key, [[maybe_unused]] orderline_t record) {
					orderline_tbl++;
					return true;
				});

			item.scan({ .i_id = 0 }, [&]([[maybe_unused]] item_t::Key key, [[maybe_unused]] item_t record) {
				item_tbl++;
				return true;
				});

			stock.scan({ .s_w_id = 0, .s_i_id = 0 }, [&]([[maybe_unused]] stock_t::Key key, [[maybe_unused]] stock_t record) {
				stock_tbl++;
				return true;
				});
			// 输出每个表中元组的数量
			std::cout << "#Tuples in tables:" << std::endl;
			std::cout << "#Warehouse " << warehouse_tbl << "\n";
			std::cout << "#district " << district_tbl << "\n";
			std::cout << "#customer " << customer_tbl << "\n";
			std::cout << "#customer wdl " << customer_wdl_tbl << "\n";
			std::cout << "#history " << history_tbl << "\n";
			std::cout << "#neworder " << neworder_tbl << "\n";
			std::cout << "#order " << order_tbl << "\n";
			std::cout << "#order wdl " << order_wdc_tbl << "\n";
			std::cout << "#orderline " << orderline_tbl << "\n";
			std::cout << "#item " << item_tbl << "\n";
			std::cout << "#stock " << stock_tbl << "\n";
			});
		};
	// 执行一致性检查
	consistencyCheck();
	// 等待2秒
	sleep(2);
	// -------------------------------------------------------------------------------------
	// 计算已使用的存储空间（以 GiB 为单位）
	double gib = (db.getBuffermanager().getConsumedPages() * storage::EFFECTIVE_PAGE_SIZE / 1024.0 / 1024.0 / 1024.0);
	std::cout << "data loaded - consumed space in GiB = " << gib << std::endl;

	// -------------------------------------------------------------------------------------
	// 等待所有节点都完成之前的任务
	barrier_wait();
	// -------------------------------------------------------------------------------------
	{
		// 定义多个原子变量和数组，用于线程控制和统计数据
		std::atomic<bool> keep_running = true;
		std::atomic<u64> running_threads_counter = 0;
		u64 tx_per_thread[FLAGS_worker];
		u64 remote_new_order_per_thread[FLAGS_worker];  // per specification
		u64 remote_tx_per_thread[FLAGS_worker];
		u64 delivery_aborts_per_thread[FLAGS_worker];
		u64 txn_profile[FLAGS_worker][transaction_types::MAX];
		u64 txn_lat[FLAGS_worker][transaction_types::MAX];
		u64 txn_pay_lat[FLAGS_worker][10];
		// 根据不同的参数设置实验配置信息
		std::string configuration;
		if (FLAGS_tpcc_warehouse_affinity) {
			configuration = "warehouse_affinity";
		}
		else if (FLAGS_tpcc_warehouse_locality) {
			configuration = "warehouse_locality";
		}
		else {
			configuration = "warehouse_no_locality";
		}
		TPCC_workloadInfo experimentInfo{ "TPCC", (uint64_t)warehouseCount, configuration };
		db.startProfiler(experimentInfo);
		// 启动多个异步线程执行任务
		for (uint64_t t_i = 0; t_i < FLAGS_worker; ++t_i) {
			db.getWorkerPool().scheduleJobAsync(t_i, [&, t_i]() {
				running_threads_counter++;
				thread_id = t_i + (db.getNodeID() * FLAGS_worker);
				volatile u64 tx_acc = 0;
				storage::DistributedBarrier barrier(catalog.getCatalogEntry(barrier_id).pid);
				barrier.wait();
				// 在保持运行状态的情况下执行循环任务
				while (keep_running) {
					uint32_t w_id;
					if (FLAGS_tpcc_warehouse_affinity) {
						w_id = t_i + 1 + warehouse_range_node.begin;
					}
					else if (FLAGS_tpcc_warehouse_locality) {
						w_id = urand(warehouse_range_node.begin + 1, warehouse_range_node.end);
					}
					else {
						w_id = urand(1, FLAGS_tpcc_warehouse_count);
						if (w_id <= (uint32_t)warehouse_range_node.begin || (w_id > (uint32_t)warehouse_range_node.end)) remote_node_new_order++;
					}
					tx(w_id);
					/*
					if (FLAGS_tpcc_abort_pct && urand(0, 100) <= FLAGS_tpcc_abort_pct) {
					   // abort
					} else {
					   // commit
					}
					*/

					tx_acc++;
					threads::Worker::my().counters.incr(profiling::WorkerCounters::tx_p);
				}
				// 存储每个线程的统计信息
				tx_per_thread[t_i] = tx_acc;
				remote_new_order_per_thread[t_i] = remote_new_order;
				remote_tx_per_thread[t_i] = remote_node_new_order;
				delivery_aborts_per_thread[t_i] = delivery_aborts;
				int idx = 0;
				for (auto& tx_count : txns)
					txn_profile[t_i][idx++] = tx_count;
				idx = 0;
				for (auto& tx_l : txn_latencies) {
					txn_lat[t_i][idx] = (tx_l / (double)txn_profile[t_i][idx]);
					idx++;
				}

				idx = 0;
				for (auto& tx_l : txn_paymentbyname_latencies) {
					txn_pay_lat[t_i][idx] = ((tx_l) / (double)txn_profile[t_i][transaction_types::STOCK_LEVEL]);
					idx++;
				}
				// 线程结束
				running_threads_counter--;
				});
		}
		// -------------------------------------------------------------------------------------
		// Join Threads
		// -------------------------------------------------------------------------------------
		// 执行一段时间后停止线程
		sleep(FLAGS_TPCC_run_for_seconds);
		keep_running = false;
		while (running_threads_counter) {
			_mm_pause();
		}
		db.getWorkerPool().joinAll();
		// -------------------------------------------------------------------------------------
		db.stopProfiler();
		// 输出统计信息
		std::cout << "tx per thread " << std::endl;
		for (u64 t_i = 0; t_i < FLAGS_worker; t_i++) {
			std::cout << tx_per_thread[t_i] << ",";
		}
		std::cout << "\n";
		// 输出远程节点的交易数量
		std::cout << "remote node txn " << std::endl;

		for (u64 t_i = 0; t_i < FLAGS_worker; t_i++) {
			std::cout << remote_tx_per_thread[t_i] << ",";
		}
		std::cout << "\n";
		// 输出按照规范的远程新订单数量
		std::cout << "remote new order per specification " << std::endl;
		for (u64 t_i = 0; t_i < FLAGS_worker; t_i++) {
			std::cout << remote_new_order_per_thread[t_i] << ",";
		}
		std::cout << std::endl;

		std::cout << "aborts " << std::endl;
		for (u64 t_i = 0; t_i < FLAGS_worker; t_i++) {
			std::cout << delivery_aborts_per_thread[t_i] << ",";
		}
		std::cout << std::endl;
		std::cout << "txn profile "
			<< "\n";

		for (u64 t_i = 0; t_i < FLAGS_worker; t_i++) {
			for (u64 i = 0; i < transaction_types::MAX; i++) {
				std::cout << txn_profile[t_i][i] << ",";
			}
			std::cout << "\n";
		}
		std::cout << "\n";

		for (u64 t_i = 0; t_i < FLAGS_worker; t_i++) {
			for (u64 i = 0; i < transaction_types::MAX; i++) {
				std::cout << txn_lat[t_i][i] << ",";
			}
			std::cout << "\n";
		}
		std::cout << "\n";

		for (u64 t_i = 0; t_i < FLAGS_worker; t_i++) {
			for (u64 i = 0; i < 10; i++) {
				std::cout << txn_pay_lat[t_i][i] << ",";
			}
			std::cout << "\n";
		}
		std::cout << "\n";
		//计算并输出数据加载所占用的内存空间以及哈希表的统计报告
		double gib = (db.getBuffermanager().getConsumedPages() * storage::EFFECTIVE_PAGE_SIZE / 1024.0 / 1024.0 / 1024.0);
		std::cout << "data loaded - consumed space in GiB = " << gib << std::endl;
		std::cout << "Starting hash table report "
			<< "\n";
		db.getBuffermanager().reportHashTableStats();
	}
	consistencyCheck();

	return 0;
}
