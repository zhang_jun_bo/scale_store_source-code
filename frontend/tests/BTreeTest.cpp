#include "PerfEvent.hpp"
#include "scalestore/Config.hpp"
#include "scalestore/ScaleStore.hpp"
#include "scalestore/rdma/CommunicationManager.hpp"
#include "scalestore/storage/datastructures/BTree.hpp"
#include "scalestore/utils/RandomGenerator.hpp"
#include "scalestore/utils/ScrambledZipfGenerator.hpp"
#include "scalestore/utils/Time.hpp"
// -------------------------------------------------------------------------------------
#include <gflags/gflags.h>
// -------------------------------------------------------------------------------------
static constexpr uint64_t BTREE_ID = 0;
static constexpr uint64_t BARRIER_ID = 1;
static constexpr uint64_t CUSTOM_BTREE_ID = 2;
// -------------------------------------------------------------------------------------
struct BTree_workloadInfo : public scalestore::profiling::WorkloadInfo {
   std::string experiment;
   uint64_t elements;
   uint64_t readRatio;
   double zipfFactor;

   BTree_workloadInfo(std::string experiment, uint64_t elements) : experiment(experiment), elements(elements) {}

   virtual std::vector<std::string> getRow() { return {experiment, std::to_string(elements)}; }

   virtual std::vector<std::string> getHeader() { return {"workload", "SSD data GB"}; }

   virtual void csv(std::ofstream& file) override {
      file << experiment << " , ";
      file << elements << " , ";
   }
   virtual void csvHeader(std::ofstream& file) override {
      file << "Workload"
           << " , ";
      file << "SSDdataGB"
           << " , ";
   }
};
// -------------------------------------------------------------------------------------
uint64_t estimateTuples(uint64_t sizeInGB, uint64_t sizeOfTuple) {
   return (((sizeInGB * 1024 * 1024 * 1024) / sizeOfTuple)) / 2;
};

// -------------------------------------------------------------------------------------
void test_integer_btree(scalestore::ScaleStore& scalestore) {
    
   using namespace scalestore;
   using K = uint64_t;
   using V = uint64_t;
   // -------------------------------------------------------------------------------------
   struct Record {
      K key;
      V value;
   };
   // -------------------------------------------------------------------------------------
   auto& catalog = scalestore.getCatalog();
   //根据给定的内存和节点数量，使用 estimateTuples 方法计算出要存储在 B 树中的记录数量。
   // -------------------------------------------------------------------------------------
   auto numberTuples = estimateTuples((FLAGS_dramGB / FLAGS_nodes) * 0.8, sizeof(Record));
   // -------------------------------------------------------------------------------------
   // LAMBDAS to reduce boilerplate code
   // //以并行的方式在多个线程中执行基于 B 树的任务，通过 execute_on_tree 函数调度和同步线程，并传递 B 树实例给定的任务回调函数。
   //这是一个 Lambda 函数，它接受一个回调函数作为参数，并使用多线程方式在 B 树上执行任务。
   auto execute_on_tree = [&](std::function<void(uint64_t t_i, storage::BTree<K, V> & tree)> callback) {
      for (uint64_t t_i = 0; t_i < FLAGS_worker; ++t_i) {
          //调度异步作业，每个作业获取 B 树和分布式屏障（DistributedBarrier）的实例。
         scalestore.getWorkerPool().scheduleJobAsync(t_i, [&, t_i]() {
             //然后使用分布式屏障同步所有线程，确保它们同时开始执行任务。
            // -------------------------------------------------------------------------------------
            storage::BTree<K, V> tree(catalog.getCatalogEntry(BTREE_ID).pid);
            // -------------------------------------------------------------------------------------
            storage::DistributedBarrier barrier(catalog.getCatalogEntry(BARRIER_ID).pid);
            barrier.wait();
            // -------------------------------------------------------------------------------------
            //每个作业执行回调函数 callback，该函数以当前线程索引和 B 树实例为参数，用于执行特定于 B 树的任务。
            callback(t_i, tree);
         });
      }
      scalestore.getWorkerPool().joinAll();
   };

   // -------------------------------------------------------------------------------------
   // create Btree (0), Barrier(1)
   // -------------------------------------------------------------------------------------
   //如果当前节点的 ID 为 0，它将创建一个新的 B 树，并创建一个具有指定线程或节点数量的分布式屏障。
   if (scalestore.getNodeID() == 0) {
       //调度同步作业，该作业在节点 0 上同步执行。
      scalestore.getWorkerPool().scheduleJobSync(0, [&]() {
          //在节点 0 上创建了一个新的 B 树。
         scalestore.createBTree<K, V>();
         //创建了一个分布式屏障（barrier），该屏障能够同步 FLAGS_worker * 
         scalestore.createBarrier(FLAGS_worker * FLAGS_nodes);
      });
   }
   // -------------------------------------------------------------------------------------
   // 1. build tree mutlithreaded 在一个多线程环境下构建了一个 B 树，并在每个线程中进行了插入和查找操作，最后对构建 B 树的操作进行了性能分析。
   // -------------------------------------------------------------------------------------
   {
       //创建了一个名为 builtInfo 的 BTree_workloadInfo 对象，用于描述构建 B 树的工作负载信息。
      BTree_workloadInfo builtInfo{"Build B-Tree", numberTuples};
      //使用 scalestore.startProfiler(builtInfo) 开始对构建 B 树的操作进行性能分析。
      scalestore.startProfiler(builtInfo);
      // -------------------------------------------------------------------------------------
      execute_on_tree([&](uint64_t t_i, storage::BTree<K, V>& tree) {
         uint64_t start = (scalestore.getNodeID() * FLAGS_worker) + t_i;
         const uint64_t inc = FLAGS_worker * FLAGS_nodes;
         for (; start < numberTuples; start = start + inc) {
             //每个线程都执行了一系列的插入操作，向 B 树中插入不同的键值对，然后进行查找。
            tree.insert(start, 1);
            V value = 99;
            auto found = tree.lookup(start, value);
            ensure(found);
            ensure(value == 1);
            threads::Worker::my().counters.incr(profiling::WorkerCounters::tx_p);
         }
      });
      scalestore.stopProfiler();
   }
   {
      // -------------------------------------------------------------------------------------
      // Scan ASC
      // -------------------------------------------------------------------------------------
      // 创建一个描述升序扫描操作的 BTree_workloadInfo 对象
      BTree_workloadInfo builtInfo{"Scan Asc", numberTuples};
      // 开始对升序扫描操作进行性能分析
      scalestore.startProfiler(builtInfo);
      // 创建一个用于存储全局结果的原子变量
      std::atomic<uint64_t> global_result{0};
      // 在 B 树上执行升序扫描操作
      execute_on_tree([&](uint64_t /*t_i*/, storage::BTree<K, V>& tree) {
          // 设置起始键值对索引值
         uint64_t start = 0;
         uint64_t current =0;// 当前键值对的键值
         uint64_t scan_result = 0;// 存储扫描结果
         // 在 B 树上执行升序扫描操作，并对每个键值对执行回调函数
         tree.scan<storage::BTree<K, V>::ASC_SCAN>(start, [&scan_result,&current](K key, V value) {
             // 对扫描结果进行累加
            scan_result += value;
            // 确保当前键值对的键值等于当前值
            ensure(current == key);
            current++;
            // 对升序扫描过程中的计数器进行增加
            threads::Worker::my().counters.incr(profiling::WorkerCounters::tx_p);
            return true; // 继续扫描
         });
         // 确保升序扫描的结果与期望的元组数相等
         ensure (scan_result == numberTuples);
      });
      // 停止升序扫描操作的性能分析
      scalestore.stopProfiler();
   }

   {
      // -------------------------------------------------------------------------------------
      // Scan DESC
      // -------------------------------------------------------------------------------------
       // 创建一个描述降序扫描操作的 BTree_workloadInfo 对象
      BTree_workloadInfo builtInfo{"Scan Desc", numberTuples};
      // 开始对降序扫描操作进行性能分析
      scalestore.startProfiler(builtInfo);
      // 创建一个用于存储全局结果的原子变量
      std::atomic<uint64_t> global_result{0};
      // 在 B 树上执行降序扫描操作
      execute_on_tree([&](uint64_t /*t_i*/, storage::BTree<K, V>& tree) {
          // 设置起始键值对索引值为 numberTuples
         uint64_t start = numberTuples;
         uint64_t current = numberTuples-1;// 当前键值对的键值
         uint64_t scan_result = 0;// 存储扫描结果
         // 在 B 树上执行降序扫描操作，并对每个键值对执行回调函数
         tree.scan<storage::BTree<K, V>::DESC_SCAN>(start, [&scan_result, &current](K key, V value) {
             // 对扫描结果进行累加
            scan_result += value;
            // 确保当前键值对的键值等于当前值
            ensure(current == key);
            current--;
            // 对降序扫描过程中的计数器进行增加
            threads::Worker::my().counters.incr(profiling::WorkerCounters::tx_p);
            return true; // 继续扫描
         }); 
         // 确保降序扫描的结果与期望的元组数相等
         ensure(scan_result == numberTuples);
      });
      // 停止降序扫描操作的性能分析
      scalestore.stopProfiler();
   }
   {
      // -------------------------------------------------------------------------------------
      // Remove half of the tuples  
      // -------------------------------------------------------------------------------------
      {
           // 创建一个描述移除操作的 BTree_workloadInfo 对象
         BTree_workloadInfo builtInfo{"Remove range", numberTuples};
         // 开始对移除操作进行性能分析
         scalestore.startProfiler(builtInfo);
         // -------------------------------------------------------------------------------------
         // 执行在 B 树上移除一半元组的操作
         execute_on_tree([&](uint64_t t_i, storage::BTree<K, V>& tree) {
             // 设置起始索引值，以节点 ID 作为偏移量
            uint64_t start = (scalestore.getNodeID() * FLAGS_worker) + t_i;
            const uint64_t inc = FLAGS_worker * FLAGS_nodes;
            // 遍历需要移除的键值对并执行移除操作
            for (; start < numberTuples/2; start = start + inc) {
                // 在 B 树中插入键值对
               tree.insert(start, 1);
               // 移除键值对，并确保移除成功
               auto removed = tree.remove(start);
               ensure(removed);
               // 对移除操作中的计数器进行增加
               threads::Worker::my().counters.incr(profiling::WorkerCounters::tx_p);
            }
         });
         // 停止移除操作的性能分析
         scalestore.stopProfiler();
      }
   }
   {
      // -------------------------------------------------------------------------------------
      // RepeatScan ASC
      // -------------------------------------------------------------------------------------
       // 创建一个描述扫描操作的 BTree_workloadInfo 对象
      BTree_workloadInfo builtInfo{"Scan Asc", numberTuples};
      // 开始对扫描操作进行性能分析
      scalestore.startProfiler(builtInfo);
      // 初始化一个原子计数器来记录全局结果
      std::atomic<uint64_t> global_result{0};
      // 在 B 树上执行重复的升序扫描操作
      execute_on_tree([&](uint64_t /*t_i*/, storage::BTree<K, V>& tree) {
          // 设置起始索引值为0
         uint64_t start = 0;
         // 初始化扫描结果的变量
         uint64_t scan_result = 0;
         // 执行升序扫描操作
         tree.scan<storage::BTree<K, V>::ASC_SCAN>(start, [&](K key, V /*value*/) {
             // 检查键值对的键是否小于一半的 numberTuples
         if (key < numberTuples / 2) {
            scan_result++; // 如果键小于一半的 numberTuples，则增加扫描结果的计数器
            return true; // 继续扫描
         }
         return false; // 否则停止扫描
         });
         // 确保升序扫描结果为0，即键值对不应该小于一半的 numberTuples
         ensure (scan_result == 0);
      });
      // 停止扫描操作的性能分析
      scalestore.stopProfiler();
   }

   {
      // -------------------------------------------------------------------------------------
      // Scan DESC
      // -------------------------------------------------------------------------------------
      // 使用 'numberTuples' 元素创建一个操作“扫描 Desc”（Scan Desc）的工作负载信息对象
      BTree_workloadInfo builtInfo{"Scan Desc", numberTuples};
      // 启动性能分析器以测量此操作的性能指标
      scalestore.startProfiler(builtInfo);
      // 将原子计数器 'global_result' 初始化为零
      std::atomic<uint64_t> global_result{0};
      // 使用多线程执行BTree上的操作
      execute_on_tree([&](uint64_t /*t_i*/, storage::BTree<K, V>& tree) {
          // 将降序扫描的起始点设置为 'numberTuples / 2 - 1'
         uint64_t start = (numberTuples / 2) - 1;
         // 初始化一个 'scan_result' 计数器用于计算扫描的元素数量
         uint64_t scan_result = 0;
         // 在BTree上进行降序扫描，起始位置为 'start'
         tree.scan<storage::BTree<K, V>::DESC_SCAN>(start, [&](K key, V /*value*/) {
             // 检查当前键是否大于零
            if (key > 0) {
                // 如果键大于零，则增加 'scan_result' 计数器
               scan_result++;
               return true;
            }
            return false;
         });
         // 确保 'scan_result' 计数器等于零
         ensure(scan_result == 0);
      });
      // 停止性能分析器，结束此操作的性能测量
      scalestore.stopProfiler();
   }
   }
// -------------------------------------------------------------------------------------
template <int maxLength>
struct Varchar {
   int16_t length; // 存储字符串长度
   char data[maxLength]; // 存储字符串数据
   // 默认构造函数，将长度初始化为零
   Varchar() : length(0) {}
   // 接收C风格字符串构造函数
   Varchar(const char* str) {
      int l = strlen(str);
      assert(l <= maxLength); // 确保字符串长度不超过最大长度
      length = l;
      memcpy(data, str, l); // 将字符串数据复制到对象中
   }
   // 复制构造函数，接收另一个Varchar对象作为参数
   template <int otherMaxLength>
   Varchar(const Varchar<otherMaxLength>& other) {
      assert(other.length <= maxLength); // 确保其他Varchar对象长度不超过最大长度
      length = other.length;
      memcpy(data, other.data, length); // 复制其他对象的数据到当前对象
   }
   // 在当前字符串末尾追加字符
   void append(char x) { data[length++] = x; };
   // 将Varchar对象转换为std::string
   std::string toString() { return std::string(data, length); };
   // 重载操作符，用于将两个Varchar对象拼接成一个新的Varchar对象
   template <int otherMaxLength>
   Varchar<maxLength> operator||(const Varchar<otherMaxLength>& other) const {
      Varchar<maxLength> tmp;
      assert((static_cast<int32_t>(length) + other.length) <= maxLength); // 确保拼接后的长度不超过最大长度
      tmp.length = length + other.length;
      memcpy(tmp.data, data, length);  // 将当前对象的数据复制到新对象中
      memcpy(tmp.data + length, other.data, other.length);  // 将另一个对象的数据复制到新对象中
      return tmp; // 返回拼接后的新对象
   }
   // 重载操作符，用于比较两个Varchar对象是否相等
   bool operator==(const Varchar<maxLength>& other) const { return (length == other.length) && (memcmp(data, other.data, length) == 0); }
   // 重载操作符，用于比较两个Varchar对象的大小关系（大于）
   bool operator>(const Varchar<maxLength>& other) const {
      int cmp = memcmp(data, other.data, (length < other.length) ? length : other.length);
      if (cmp)
         return cmp > 0;
      else
         return length > other.length;
   }
   // 重载操作符，用于比较两个Varchar对象的大小关系（小于）
   bool operator<(const Varchar<maxLength>& other) const {
      int cmp = memcmp(data, other.data, (length < other.length) ? length : other.length);
      if (cmp)
         return cmp < 0;
      else
         return length < other.length;
   }
};

struct SecondaryKey {
   Varchar<16> name; // 名称字段，使用长度为16的Varchar类型存储
   int id; // ID字段，整数类型
   // 重载操作符，用于判断两个SecondaryKey对象是否相等
   bool operator==(const SecondaryKey& other) const { return (id == other.id) && (name == other.name);  } // 判断名称和ID是否相同
   // 重载操作符，用于判断当前SecondaryKey对象是否小于另一个SecondaryKey对象
   bool operator<(const SecondaryKey& other) const {
      if (name < other.name) return true; // 如果名称小于另一个对象的名称，返回true
      if (name > other.name) return false; // 如果名称大于另一个对象的名称，返回false
      // 如果名称相等，则比较ID
      return(id < other.id); // 如果ID小于另一个对象的ID，返回true，否则返回false
   }
   // 重载操作符，用于判断当前SecondaryKey对象是否大于另一个SecondaryKey对象
   bool operator>(const SecondaryKey& other) const {
      if( name > other.name) return true; // 如果名称大于另一个对象的名称，返回true
      if( name < other.name) return false;// 如果名称小于另一个对象的名称，返回false
      // 如果名称相等，则比较ID
      return (id > other.id); // 如果ID大于另一个对象的ID，返回true，否则返回false
   }
   // 重载操作符，用于判断当前SecondaryKey对象是否大于等于另一个SecondaryKey对象
   bool operator>=(const SecondaryKey& other) const { return !(*this < other); // 利用小于操作符进行判断，返回相反的结果
   }
};
//将 SecondaryKey 对象输出到输出流中。
std::ostream& operator<<(std::ostream& os, SecondaryKey& c) {
   os << c.name.toString() << " " << c.id;
   return os;
}

// -------------------------------------------------------------------------------------
void test_custom_key_btree(scalestore::ScaleStore& scalestore) {
    // 定义字符串向量
   std::vector<std::string> names = {"mueller", "el-hindi", "feil"};
   // 定义数据结构类型别名
   using namespace scalestore;
   using K = SecondaryKey;// 键类型
   using V = uint64_t; // 值类型
   // -------------------------------------------------------------------------------------
     // 定义记录结构
   struct Record {
      K key;  // 键
      V value; // 值
   };
   // -------------------------------------------------------------------------------------
    // 获取 Catalog（目录）的引用
   auto& catalog = scalestore.getCatalog();
   // -------------------------------------------------------------------------------------
   // 计算元组数量
   auto numberTuples = estimateTuples((FLAGS_dramGB / FLAGS_nodes) * 0.8, sizeof(Record));
   // -------------------------------------------------------------------------------------
   // LAMBDAS to reduce boilerplate code
   // 定义执行 B 树操作的 Lambda 函数，减少重复代码
   auto execute_on_tree = [&](std::function<void(uint64_t t_i, storage::BTree<K, V> & tree)> callback) {
      for (uint64_t t_i = 0; t_i < FLAGS_worker; ++t_i) {
         scalestore.getWorkerPool().scheduleJobAsync(t_i, [&, t_i]() {
            // -------------------------------------------------------------------------------------
             // 创建 B 树
            storage::BTree<K, V> tree(catalog.getCatalogEntry(CUSTOM_BTREE_ID).pid);
            // -------------------------------------------------------------------------------------
            // 创建分布式屏障
            storage::DistributedBarrier barrier(catalog.getCatalogEntry(BARRIER_ID).pid);
            barrier.wait(); // 等待屏障信号
            // -------------------------------------------------------------------------------------
             // 执行指定的回调函数
            callback(t_i, tree);
         });
      }
      scalestore.getWorkerPool().joinAll();
   };

   // -------------------------------------------------------------------------------------
   // 创建 B 树和屏障
   // -------------------------------------------------------------------------------------
   if (scalestore.getNodeID() == 0) {
      scalestore.getWorkerPool().scheduleJobSync(0, [&]() {
         scalestore.createBTree<K, V>(); // 创建 B 树
      });
   }
   // -------------------------------------------------------------------------------------
   // 1. 多线程构建 B 树
   // -------------------------------------------------------------------------------------
   {
      BTree_workloadInfo builtInfo{"Build B-Tree", numberTuples};
      scalestore.startProfiler(builtInfo);
      // -------------------------------------------------------------------------------------
      execute_on_tree([&](uint64_t t_i, storage::BTree<K, V>& tree) {
         uint64_t start = (scalestore.getNodeID() * FLAGS_worker) + t_i;
         const uint64_t inc = FLAGS_worker * FLAGS_nodes;
         // 循环构建 B 树
         for (; start < numberTuples; start = start + inc) {
            tree.insert({.name = names[start%names.size()].c_str(), .id=(int)start}, 1);
            threads::Worker::my().counters.incr(profiling::WorkerCounters::tx_p);
         }
      });
      scalestore.stopProfiler();
   }

   {
      // -------------------------------------------------------------------------------------
      // 扫描 B 树，按升序
      // -------------------------------------------------------------------------------------
      BTree_workloadInfo builtInfo{"Scan Asc", numberTuples};
      scalestore.startProfiler(builtInfo);
      std::atomic<uint64_t> global_result{0};
      execute_on_tree([&](uint64_t /*t_i*/, storage::BTree<K, V>& tree) {
         SecondaryKey start = {.name = "", .id = 0};
         uint64_t scan_result = 0;
         // 执行升序扫描
         tree.scan<storage::BTree<K, V>::ASC_SCAN>(start, [&scan_result]([[maybe_unused]] K key, V value) {
                                                             
            scan_result += value;
            threads::Worker::my().counters.incr(profiling::WorkerCounters::tx_p);
            return true;
         });
         ensure (scan_result == numberTuples);
      });
      scalestore.stopProfiler();
   }
}

// -------------------------------------------------------------------------------------
using namespace scalestore;
int main(int argc, char* argv[]) {
   // -------------------------------------------------------------------------------------
     // 设置使用消息和解析命令行标志
   gflags::SetUsageMessage("BTree tests");
   gflags::ParseCommandLineFlags(&argc, &argv, true);
   // -------------------------------------------------------------------------------------
   // 确保节点数量为 1
   ensure(FLAGS_nodes == 1);
   // 创建 ScaleStore 实例
   ScaleStore scalestore;
   // -------------------------------------------------------------------------------------
   // 运行整数类型键的 B 树测试
   test_integer_btree(scalestore);
   // -------------------------------------------------------------------------------------
   // 运行自定义键类型的 B 树测试
   test_custom_key_btree(scalestore);
   return 0;
};
