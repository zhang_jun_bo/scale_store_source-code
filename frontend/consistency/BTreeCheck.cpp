// -------------------------------------------------------------------------------------
// Consistency Check for ScaleStore
// 1. partition keys and build the tree multi threaded
// 2. partition keys and update keys by 1 for every access
// 3. shuffle keys and iterate once randomly over all keys and add thread id
// 4. run consistency checks
// -------------------------------------------------------------------------------------
#include "PerfEvent.hpp"
#include "scalestore/Config.hpp"
#include "scalestore/ScaleStore.hpp"
#include "scalestore/rdma/CommunicationManager.hpp"
#include "scalestore/storage/datastructures/BTree.hpp"
#include "scalestore/utils/RandomGenerator.hpp"
#include "scalestore/utils/ScrambledZipfGenerator.hpp"
#include "scalestore/utils/Time.hpp"
// -------------------------------------------------------------------------------------
#include <gflags/gflags.h>
// -------------------------------------------------------------------------------------
// 定义命令行标志
DEFINE_string(mode, "in-memory", "options are {in-memory,out-of-memory}");
DEFINE_uint64(rounds, 10, "Update rounds per thread");
// -------------------------------------------------------------------------------------
// 定义一些常量
static constexpr uint64_t BTREE_ID = 0;
static constexpr uint64_t BARRIER_ID = 1;
// -------------------------------------------------------------------------------------
// 定义了一个结构，用于记录一致性测试的信息
struct Consistency_workloadInfo : public scalestore::profiling::WorkloadInfo {
   std::string experiment; // 实验名称
   uint64_t elements;     // 元素数量
   // 构造函数，初始化实验名称和元素数量
   Consistency_workloadInfo(std::string experiment, uint64_t elements) : experiment(experiment), elements(elements) {}
   // 获取一行数据
   virtual std::vector<std::string> getRow() { return {experiment, std::to_string(elements)}; }
   // 获取表头信息
   virtual std::vector<std::string> getHeader() { return {"workload", "elements"}; }
   // 将数据写入 CSV 文件
   virtual void csv(std::ofstream& file) override {
      file << experiment << " , ";
      file << elements << " , ";
   }
   // 将表头信息写入 CSV 文件
   virtual void csvHeader(std::ofstream& file) override {
      file << "Workload"
           << " , ";
      file << "Elements"
           << " , ";
   }
};
class Barrier {
  private: 
   const std::size_t threadCount; // 线程数量
   alignas(64) std::atomic<std::size_t> cntr;  // 等待计数器
   alignas(64) std::atomic<uint8_t> round; // 轮次计数器

  public:
   // 构造函数，初始化 Barrier 对象，设置线程数量、等待计数器和轮次计数器
   explicit Barrier(std::size_t threadCount) : threadCount(threadCount), cntr(threadCount), round(0) {}
   // 等待函数，带有自定义的结束处理器 finalizer
   template <typename F>
   bool wait(F finalizer) {
      auto prevRound = round.load();  // 必须在 fetch_sub 之前进行
      auto prev = cntr.fetch_sub(1); // 递减等待计数器
      if (prev == 1) {
          // 最后一个线程到达
         cntr = threadCount;// 重置等待计数器
         auto r = finalizer();// 调用自定义的结束处理器
         round++;// 增加轮次计数器
         return r;// 返回结束处理器的结果
      } else {
          // 等待直到屏障准备好再次使用
         while (round == prevRound) {
             // 告诉处理器暂停执行，等待低能耗状态结束
            asm("pause");
            asm("pause");
            asm("pause");
         }
         return false;// 返回 false 表示未到达屏障
      }
   }
   // 简化的等待函数，调用 wait() 函数，默认结束处理器返回 true
   inline bool wait() {
      return wait([]() { return true; });
   }
};

// -------------------------------------------------------------------------------------
using namespace scalestore;
int main(int argc, char* argv[]) {
   using K = uint64_t;
   using V = uint64_t;
   // -------------------------------------------------------------------------------------
   struct Record {
      K key;
      V value;
   };
   // -------------------------------------------------------------------------------------
   // 设置命令行参数的使用消息为 "Catalog Test"
   gflags::SetUsageMessage("Catalog Test");
   // 解析命令行参数并将其存储在相应的 FLAGS 中
   gflags::ParseCommandLineFlags(&argc, &argv, true);
   // -------------------------------------------------------------------------------------
   std::cout << "FLAGS_messageHandlerThreads " << FLAGS_messageHandlerThreads << std::endl;
   // 创建 ScaleStore 实例
   ScaleStore scalestore;
   // 获取 ScaleStore 实例的目录（Catalog）引用
   auto& catalog = scalestore.getCatalog();
   // 根据传入的参数 sizeInGB 和 sizeOfTuple 估算所需的元组数量
   auto estimate_tuples = [&](uint64_t sizeInGB, uint64_t sizeOfTuple) {
      std::cout << sizeInGB << "\n";
      std::cout << sizeOfTuple << "\n";
      std::cout << (sizeInGB * 1024 * 1024 * 1024) << "\n";
      std::cout << (sizeInGB * 1024 * 1024 * 1024) / sizeOfTuple << "\n";
      // 计算所需的元组数量，并返回结果的一半
      return (((sizeInGB * 1024 * 1024 * 1024) / sizeOfTuple)) / 2;
   };
   // -------------------------------------------------------------------------------------
   // 定义变量 numberTuples，用于存储估算得到的元组数量
   uint64_t numberTuples = 0;
   // 检查 FLAGS_mode 的值是否为 "in-memory"
   if (FLAGS_mode.compare("in-memory") == 0) {
       // 确保即使是复制后的数据也能够在内存中容纳
       // 使用 estimate_tuples 函数估算所需元组的数量，并存储在 numberTuples 变量中
      numberTuples = estimate_tuples((FLAGS_dramGB / FLAGS_nodes) * 0.8, sizeof(Record));
   } else if (FLAGS_mode.compare("out-of-memory") == 0) {
       // 检查 FLAGS_mode 的值是否为 "out-of-memory"
        // 避免数据能够完全存储在内存中
       // 使用 estimate_tuples 函数估算所需元组的数量，并存储在 numberTuples 变量中
      numberTuples = estimate_tuples((FLAGS_ssd_gib * 0.5 * FLAGS_nodes), sizeof(Record));
   } else {
       // 如果 FLAGS_mode 的值既不是 "in-memory" 也不是 "out-of-memory"，抛出运行时错误
      throw std::runtime_error("Invalid gflag mode");
   }
   // -------------------------------------------------------------------------------------
   // LAMBDAS to reduce boilerplate code
   // 创建名为 local_barrier 的 Barrier 类实例，用于协调多个线程的同步操作，设置线程数量为 FLAGS_worker
   Barrier local_barrier(FLAGS_worker);
   // 定义 execute_on_tree 函数，接受回调函数作为参数，用于在多个线程上执行树的操作
   auto execute_on_tree = [&](std::function<void(uint64_t t_i, storage::BTree<K, V> & tree)> callback) {
       // 遍历多个工作线程，每个工作线程执行一个异步任务
      for (uint64_t t_i = 0; t_i < FLAGS_worker; ++t_i) {
         // 调用 scalestore 实例的 scheduleJobAsync 函数，创建一个异步任务
         scalestore.getWorkerPool().scheduleJobAsync(t_i, [&, t_i]() {
          // 创建 BTree 对象，传入相应的 pid 获取 BTree 对象的实例
            storage::BTree<K, V> tree(catalog.getCatalogEntry(BTREE_ID).pid);
            // 创建 DistributedBarrier 对象，传入相应的 pid 获取分布式屏障的实例
            storage::DistributedBarrier barrier(catalog.getCatalogEntry(BARRIER_ID).pid);

            barrier.wait();  // 等待所有线程到达此分布式屏障
            // -------------------------------------------------------------------------------------

            callback(t_i, tree);
            // 等待本地屏障以保证线程同步
            local_barrier.wait();
         });
      }
      // 等待所有工作线程完成任务
      scalestore.getWorkerPool().joinAll();
   };

   // -------------------------------------------------------------------------------------
   // create Btree (0), Barrier(1) 创建 BTree（0），Barrier（1）
   // -------------------------------------------------------------------------------------
   /*
   * 如果当前节点是 ID 为 0 的节点，创建了 BTree 和 Barrier。
    进行了一致性检查，使用多线程构建了 BTree，并在每个线程上进行了插入和查找操作，确保构建的树的一致性和正确性。
    记录了构建树的性能信息，包括构建过程中的插入和查找操作的一致性检查信息。
   */
   if (scalestore.getNodeID() == 0) {
       // 如果当前节点是 0 号节点，创建 BTree 和 Barrier
      scalestore.getWorkerPool().scheduleJobSync(0, [&]() {
         scalestore.createBTree<K, V>();  // 创建 BTree
         scalestore.createBarrier(FLAGS_worker * FLAGS_nodes); // 创建 Barrier
      });
   }
   // -------------------------------------------------------------------------------------
   // CONSISTENCY CHECKS
   // -------------------------------------------------------------------------------------
   // 1. 使用多线程构建树
   {
       // 创建 Consistency_workloadInfo 对象用于记录构建树的一致性检查
      Consistency_workloadInfo builtInfo{"Build B-Tree", numberTuples};
      scalestore.startProfiler(builtInfo);  // 启动性能分析器记录构建树的信息
      // -------------------------------------------------------------------------------------
      // 在树上执行多线程操作
      execute_on_tree([&](uint64_t t_i, storage::BTree<K, V>& tree) {
          // 根据当前节点 ID 和工作线程 ID 开始构建树
         uint64_t start = (scalestore.getNodeID() * FLAGS_worker) + t_i;
         const uint64_t inc = FLAGS_worker * FLAGS_nodes;
         // 对每个线程负责的部分进行插入和查找操作
         for (; start < numberTuples; start = start + inc) {
            tree.insert(start, 1);  // 插入键值对到树中
            V value = 99;
            auto found = tree.lookup(start, value); // 查找键值对
            ensure(found); // 确保查找成功
            ensure(value == 1); // 确保值正确
            threads::Worker::my().counters.incr(profiling::WorkerCounters::tx_p); // 记录性能指标
         } 
      });
      scalestore.stopProfiler(); // 停止性能分析器
   }

   {
      // -------------------------------------------------------------------------------------
       // 对构建的树进行一致性检查
      Consistency_workloadInfo builtInfo{"Build Verify", numberTuples};
      scalestore.startProfiler(builtInfo); // 启动性能分析器记录一致性检查的信息
      std::atomic<uint64_t> global_result{0}; // 全局结果的原子变量初始化为0
      execute_on_tree([&](uint64_t t_i, storage::BTree<K, V>& tree) {
         uint64_t start = t_i;   // 扫描所有页面
         const uint64_t inc = FLAGS_worker;
         uint64_t local_result = 0; // 本地结果初始化为0
         // 遍历每个线程负责的部分，进行查找操作并累加值到本地结果中
         for (; start < numberTuples; start = start + inc) {
            V value = 99;
            auto found = tree.lookup(start, value);
            local_result += value;
            ensure(found); // 确保查找成功
            ensure(value == 1);// 确保值正确
            threads::Worker::my().counters.incr(profiling::WorkerCounters::tx_p); // 记录性能指标
         }
         global_result += local_result; // 将本地结果累加到全局结果中
      });
      scalestore.stopProfiler(); // 停止性能分析器
      ensure(global_result == numberTuples); // 确保全局结果等于预期的元组数量
   }
   // -------------------------------------------------------------------------------------
    // 2. 多线程对多个回合中的自己的键进行加 1 操作
   {
      Consistency_workloadInfo builtInfo{"Update by 1 B-Tree", numberTuples};
      scalestore.startProfiler(builtInfo); // 启动性能分析器记录更新操作的信息
      // -------------------------------------------------------------------------------------
      execute_on_tree([&](uint64_t t_i, storage::BTree<K, V>& tree) {
         for (uint64_t rnd = 0; rnd < FLAGS_rounds; rnd++) {  // 多个回合循环更新操作
            uint64_t start = (scalestore.getNodeID() * FLAGS_worker) + t_i;
            const uint64_t inc = FLAGS_worker * FLAGS_nodes;
            // 遍历每个线程负责的部分，对键进行查找和增加操作
            for (; start < numberTuples; start = start + inc) {
               auto updated = tree.lookupAndUpdate(start, [](V& value) { value++; });  // 查找并更新键值
               ensure(updated);// 确保更新操作成功
               threads::Worker::my().counters.incr(profiling::WorkerCounters::tx_p);// 记录性能指标
            }
         }
      });
      scalestore.stopProfiler(); // 停止性能分析器
   }
   {
       // 验证更新的一致性
      Consistency_workloadInfo builtInfo{"Verify Update", numberTuples};
      scalestore.startProfiler(builtInfo); // 启动性能分析器记录验证操作的信息
      // -------------------------------------------------------------------------------------
      // consistency check for updates
      execute_on_tree([&](uint64_t t_i, storage::BTree<K, V>& tree) {
         uint64_t start = t_i;
         const uint64_t inc = FLAGS_worker;
         // 遍历每个线程负责的部分，对键进行查找并验证其值是否正确
         for (; start < numberTuples; start = start + inc) {
            V value = 99;
            auto found = tree.lookup(start, value); // 查找键值
            ensure(found);// 确保键值存在
            ensure(value == (1 + FLAGS_rounds)); // 确保值正确（经过了更新操作，值应为初始值加上更新回合数）
            threads::Worker::my().counters.incr(profiling::WorkerCounters::tx_p);  // 记录性能指标
         }
      });
      scalestore.stopProfiler();// 停止性能分析器
   }
   // -------------------------------------------------------------------------------------
    // 增加所有键的值，通过添加线程/客户端 ID
   {
      Consistency_workloadInfo builtInfo{"Update by thread id B-Tree", numberTuples};
      scalestore.startProfiler(builtInfo);  // 启动性能分析器，记录操作信息
      // -------------------------------------------------------------------------------------
      execute_on_tree([&](uint64_t t_i, storage::BTree<K, V>& tree) {
         const uint64_t thread_id = (scalestore.getNodeID() * FLAGS_worker) + t_i;
         // 遍历所有键，将键值增加一个特定的线程/客户端 ID
         for (uint64_t start = 0; start < numberTuples; start++) {
            auto updated = tree.lookupAndUpdate(start, [&](V& value) { value = value + thread_id; });
            ensure(updated);// 确保更新操作成功
            threads::Worker::my().counters.incr(profiling::WorkerCounters::tx_p); // 记录性能指标
         }
      });
      // -------------------------------------------------------------------------------------
      auto before = (1 + FLAGS_rounds); // 前一个值
      auto expected = ((FLAGS_worker * FLAGS_nodes) * ((FLAGS_worker * FLAGS_nodes) - 1) / 2);  // 预期的总和
      expected = expected + before;// 增加前一个值到预期的总和中
      // -------------------------------------------------------------------------------------
       // 对更新操作进行一致性检查
      execute_on_tree([&]([[maybe_unused]] uint64_t t_i, storage::BTree<K, V>& tree) {
          // 遍历所有键，验证其值是否正确
         for (uint64_t start = 0; start < numberTuples; start++) {
            V value = 99;
            auto found = tree.lookup(start, value); // 查找键值
            ensure(found);// 确保键值存在
            ensure(value == expected);// 确保值正确（等于预期的总和）
            threads::Worker::my().counters.incr(profiling::WorkerCounters::tx_p); // 记录性能指标
         }
      });
      scalestore.stopProfiler();  // 停止性能分析器
   }
   std::cout << "Starting hash table report "
             << "\n";
   scalestore.getBuffermanager().reportHashTableStats();

   // -------------------------------------------------------------------------------------
   return 0;
}
