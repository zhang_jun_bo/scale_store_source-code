// -------------------------------------------------------------------------------------
// Consistency Check for ScaleStore
// 1. partition keys and build the tree multi threaded
// 2. partition keys and update keys by 1 for every access
// 3. shuffle keys and iterate once randomly over all keys and add thread id
// 4. run consistency checks
// -------------------------------------------------------------------------------------
#include "PerfEvent.hpp"
#include "scalestore/Config.hpp"
#include "scalestore/ScaleStore.hpp"
#include "scalestore/rdma/CommunicationManager.hpp"
#include "scalestore/storage/datastructures/BTree.hpp"
#include "scalestore/utils/RandomGenerator.hpp"
#include "scalestore/utils/ScrambledZipfGenerator.hpp"
#include "scalestore/utils/Time.hpp"
// -------------------------------------------------------------------------------------
#include <gflags/gflags.h>
// -------------------------------------------------------------------------------------
DEFINE_double(run_for_seconds, 10.0, ""); // 定义一个 double 类型的全局变量 run_for_seconds，初始值为 10.0
// -------------------------------------------------------------------------------------
static constexpr uint64_t BTREE_ID = 0; // 定义一个名为 BTREE_ID 的常量，并赋值为 0
static constexpr uint64_t BARRIER_ID = 1; // 定义一个名为 BARRIER_ID 的常量，并赋值为 1
// -------------------------------------------------------------------------------------
std::atomic<uint64_t> threads_active = 0; // 定义一个原子类型的 uint64_t 变量 threads_active，并初始化为 0，用于记录活跃线程数
// -------------------------------------------------------------------------------------
struct Consistency_workloadInfo : public scalestore::profiling::WorkloadInfo {
   std::string experiment; // 用于存储实验名称的字符串变量
   uint64_t elements;  // 用于存储元素数量的 uint64_t 变量
   // 构造函数，初始化 experiment 和 elements 变量
   Consistency_workloadInfo(std::string experiment, uint64_t elements) : experiment(experiment), elements(elements) {}
   // 返回一行数据的字符串向量，包含实验名称、元素数量和活跃线程数
   virtual std::vector<std::string> getRow() { return {experiment, std::to_string(elements), std::to_string(threads_active.load())}; }
   // 返回标题行的字符串向量，包含列的名称：工作负载、元素和活跃线程数
   virtual std::vector<std::string> getHeader() { return {"workload", "elements","thread_active"}; }
   // 将数据写入 CSV 文件的方法
   virtual void csv(std::ofstream& file) override {
      file << experiment << " , ";
      file << elements << " , ";
   }
   // 写入 CSV 文件的标题行的方法
   virtual void csvHeader(std::ofstream& file) override {
      file << "Workload"
           << " , ";
      file << "Elements"
           << " , ";
   }
};
class Barrier {
  private:
   const std::size_t threadCount; // 线程计数器，表示参与同步的线程数
   alignas(64) std::atomic<std::size_t> cntr; // 对齐的原子计数器，用于同步等待线程
   alignas(64) std::atomic<uint8_t> round; // 对齐的原子计数器，用于标记同步轮次

  public:
   explicit Barrier(std::size_t threadCount) : threadCount(threadCount), cntr(threadCount), round(0) {
       // 构造函数，初始化线程计数器、原子计数器 cntr 和 round
   }
   // 等待函数，接受一个 finalizer 函数作为参数
   template <typename F>
   bool wait(F finalizer) { 
      auto prevRound = round.load();  // 获取当前同步轮次的值
      auto prev = cntr.fetch_sub(1); // 对计数器减 1，获取原值
      if (prev == 1) {
          // 最后一个线程到达
         cntr = threadCount; // 重置计数器为初始值
         auto r = finalizer(); // 执行 finalizer 函数
         round++; // 增加同步轮次
         return r; // 返回 finalizer 函数的结果
      } else {
         while (round == prevRound) {
             // 等待，直到 barrier 准备好重新使用
            asm("pause");// CPU 指令，暂停执行一段时间
            asm("pause");
            asm("pause");
         }
         return false; // 返回 false 表示等待
      }
   }
   // 内联函数，调用 wait 函数，默认 finalizer 返回 true
   inline bool wait() {
      return wait([]() { return true; });
   }
};

// -------------------------------------------------------------------------------------
using namespace scalestore;
int main(int argc, char* argv[]) {
   using K = uint64_t;
   using V = uint64_t;
   // -------------------------------------------------------------------------------------
   struct Record {
      K key;
      V value;
   };
   // -------------------------------------------------------------------------------------
   // 设置程序的命令行参数说明
   gflags::SetUsageMessage("Catalog Test");
   // 解析命令行参数
   gflags::ParseCommandLineFlags(&argc, &argv, true);
   // -------------------------------------------------------------------------------------
   // 输出 FLAGS_messageHandlerThreads 的值
   std::cout << "FLAGS_messageHandlerThreads " << FLAGS_messageHandlerThreads << std::endl;
   // 创建 ScaleStore 实例
   ScaleStore scalestore;
   // 获取 ScaleStore 的目录实例
   auto& catalog = scalestore.getCatalog();
   // -------------------------------------------------------------------------------------
   // 定义一个常量 numberTuples，表示元组的数量（这里设置为 100，足以存储在单个页面中）
   constexpr uint64_t numberTuples = 100;  // fits on a single page
   // -------------------------------------------------------------------------------------
   // LAMBDAS to reduce boilerplate code

   auto barrier_wait = [&]() {
       // 遍历所有的工作线程
      for (uint64_t t_i = 0; t_i < FLAGS_worker; ++t_i) {
          // 在工作线程池中异步调度一个任务
         scalestore.getWorkerPool().scheduleJobAsync(t_i, [&, t_i]() {
             // 创建一个分布式障碍对象，并等待所有工作线程到达
            storage::DistributedBarrier barrier(catalog.getCatalogEntry(BARRIER_ID).pid);
            barrier.wait();
         });
      }
      // 等待所有工作线程完成任务
      scalestore.getWorkerPool().joinAll();
   };

   
   Barrier local_barrier(FLAGS_worker);  // 创建一个 Barrier 对象用于同步指定数量的线程
   std::atomic<bool> keep_running = true;  // 用于控制线程执行状态的原子布尔变量
   std::atomic<u64> running_threads_counter = 0;  // 记录当前正在运行的线程数量的原子计数器
   // 执行任务的函数 execute_on_tree
   auto execute_on_tree = [&](std::function<void(uint64_t t_i, storage::BTree<K, V> & tree)> callback) {
       // 遍历指定数量的工作线程
      for (uint64_t t_i = 0; t_i < FLAGS_worker; ++t_i) {
          // 在工作线程池中异步调度一个任务
         scalestore.getWorkerPool().scheduleJobAsync(t_i, [&, t_i]() {
            running_threads_counter++; // 当前运行线程数加一
            threads_active++; // 线程活跃计数器加一
            // -------------------------------------------------------------------------------------
            // 创建 BTree 对象和 DistributedBarrier 对象
            storage::BTree<K, V> tree(catalog.getCatalogEntry(BTREE_ID).pid);
            // -------------------------------------------------------------------------------------
            storage::DistributedBarrier barrier(catalog.getCatalogEntry(BARRIER_ID).pid);
            barrier.wait(); // 等待所有线程到达栅栏处
            // -------------------------------------------------------------------------------------
            // 执行回调函数并减少线程活跃计数器和当前运行线程数
            callback(t_i, tree);
            threads_active--;
            local_barrier.wait();
            running_threads_counter--;
         });
      }
      // 线程休眠一段时间
      sleep(FLAGS_run_for_seconds);
      // 设置标志，通知线程停止
      keep_running = false;
      // 等待当前所有线程都完成任务
      while (running_threads_counter) {
         _mm_pause();
      }
      // 等待所有工作线程执行完毕
      scalestore.getWorkerPool().joinAll();
   };

   // -------------------------------------------------------------------------------------
   // create Btree (0), Barrier(1)
   // -------------------------------------------------------------------------------------
   if (scalestore.getNodeID() == 0) {
       // 如果当前节点为 ID 为 0 的节点
      scalestore.getWorkerPool().scheduleJobSync(0, [&]() {
        // 在当前节点的工作线程池中同步执行一个作业
        // 在这个作业中，创建一个 BTree 和一个 Barrier
         scalestore.createBTree<K, V>();
         scalestore.createBarrier(FLAGS_worker * FLAGS_nodes);
      });
   }
   // -------------------------------------------------------------------------------------
   // Contention build and update
   // -------------------------------------------------------------------------------------
   // 1. build tree mutlithreaded
   {
      // 创建一个描述构建 B-Tree 操作的信息
      Consistency_workloadInfo builtInfo{"Build B-Tree", numberTuples};
      // 启动性能分析器以记录 B-Tree 的构建过程
      scalestore.startProfiler(builtInfo);
      // -------------------------------------------------------------------------------------
       // 执行多线程操作以构建 B-Tree
      execute_on_tree([&](uint64_t t_i, storage::BTree<K, V>& tree) {
         V values_seen = 0;
         K key = 0;
         // 在 keep_running 为 true 时，持续构建 B-Tree
         while (keep_running) {
            key = (key+1) % numberTuples; // 循环使用 key
            V value = utils::RandomGenerator::getRandU64(0, 100000000); // 生成随机值
            tree.insert(key, value);// 插入键值对
            V lkp_value = 99;
            auto found = tree.lookup(key, lkp_value);// 查找键对应的值
            // auto found = tree.lookup_opt(key, lkp_value);            
            values_seen += lkp_value;
            ensure(found); // 确保查找成功
         }
         std::cout << "t_i " << t_i << " values " << values_seen << std::endl;
      });
      // 停止性能分析器记录
      scalestore.stopProfiler();
   }
   {
      Consistency_workloadInfo builtInfo{"Barrier", numberTuples};
      scalestore.startProfiler(builtInfo);
      barrier_wait();
      scalestore.stopProfiler();
      
   }
   // -------------------------------------------------------------------------------------

   return 0;
}
