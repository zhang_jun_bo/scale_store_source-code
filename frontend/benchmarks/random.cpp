#include "PerfEvent.hpp"
#include "scalestore/Config.hpp"
#include "scalestore/ScaleStore.hpp"
#include "scalestore/rdma/CommunicationManager.hpp"
#include "scalestore/storage/datastructures/BTree.hpp"
#include "scalestore/utils/RandomGenerator.hpp"
#include "scalestore/utils/ScrambledZipfGenerator.hpp"
#include "scalestore/utils/Time.hpp"
// -------------------------------------------------------------------------------------
#include <gflags/gflags.h>
#include <numeric>
// -------------------------------------------------------------------------------------
static constexpr uint64_t BARRIER_ID = 0;
// -------------------------------------------------------------------------------------
// 定义一个无符号 64 位整数类型的标志，表示每个线程要执行的操作总数
DEFINE_uint64(run_for_operations, 1e6, "Total number of operations executed per thread");
// 定义一个布尔类型的标志，用于控制是否开启性能监测或调试功能，默认为 false
DEFINE_bool(attach_perf, false, "break for perf");
// ------------------------------------------------------------------------------------- 
// 定义一个名为 OLAP_workloadInfo 的结构体，它继承自 scalestore::profiling::WorkloadInfo 类
struct OLAP_workloadInfo : public scalestore::profiling::WorkloadInfo {
   std::string experiment;// 实验名称
   uint64_t elements;// 元素数量
   uint64_t readRatio;// 读取比率
   double zipfFactor;// Zipf 因子
   // 构造函数，初始化 experiment 和 elements 成员变量
   OLAP_workloadInfo(std::string experiment, uint64_t elements)
       : experiment(experiment), elements(elements)
   {
   }

   // 返回存储当前对象数据的一行字符串向量
   virtual std::vector<std::string> getRow(){
      return {experiment,std::to_string(elements), std::to_string(readRatio), std::to_string(zipfFactor)};
   }
   // 返回描述当前对象数据的标题行字符串向量
   virtual std::vector<std::string> getHeader(){
      return {"workload","elements","read ratio", "zipfFactor"};
   }
   
  
   // 将当前对象的数据写入 CSV 文件
   virtual void csv(std::ofstream& file) override
   {
      file << experiment << " , ";
      file << elements << " , ";

   }
   // 将当前对象数据的标题写入 CSV 文件
   virtual void csvHeader(std::ofstream& file) override
   {
      file << "Workload"
           << " , ";
      file << "SSDdataGB"
           << " , ";
   }
};

// -------------------------------------------------------------------------------------
using namespace scalestore;
int main(int argc, char* argv[])
{
   // -------------------------------------------------------------------------------------
    //设置用于帮助信息的消息字符串。
   gflags::SetUsageMessage("Internal hash table benchmark");
   //解析命令行标志，允许在命令行中设置程序的参数。
   gflags::ParseCommandLineFlags(&argc, &argv, true);
   // -------------------------------------------------------------------------------------
   //一个 lambda 函数，检查 FLAGS_attach_perf 参数的值。如果其值为真，程序会显示消息提示用户按下回车键。
   auto wait_for_key_press = [&]() {
      if (FLAGS_attach_perf) {
         std::cout << "Press enter"
                   << "\n";

         char temp;
         std::cin.get(temp);
      }
   };
   // -------------------------------------------------------------------------------------
   //创建了一个名为 counters 的 std::vector<uint64_t>，其大小由 FLAGS_worker 决定。
   std::vector<uint64_t> counters(FLAGS_worker);
   //调用了 wait_for_key_press() 函数。
   wait_for_key_press();
   uint64_t counter = 0;
   //在每次循环中，将随机生成一个介于 0 和 100 之间的整数，然后将其加到 counter 变量上。
   for (u64 op_i = 0; op_i < FLAGS_run_for_operations; op_i++)
      counter += utils::RandomGenerator::getRandU64(0, 100);

   std::cout << counter << "\n";

   return 0;
};
