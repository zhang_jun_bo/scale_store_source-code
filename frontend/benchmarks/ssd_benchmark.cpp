#include "PerfEvent.hpp"
#include "scalestore/Config.hpp"
#include "scalestore/ScaleStore.hpp"
#include "scalestore/rdma/CommunicationManager.hpp"
#include "scalestore/storage/datastructures/BTree.hpp"
#include "scalestore/storage/datastructures/SmallMap.hpp"
#include "scalestore/utils/RandomGenerator.hpp"
#include "scalestore/utils/ScrambledZipfGenerator.hpp"
#include "scalestore/utils/Time.hpp"
// -------------------------------------------------------------------------------------
#include <gflags/gflags.h>
// -------------------------------------------------------------------------------------
static constexpr uint64_t BARRIER_ID = 0;
// -------------------------------------------------------------------------------------
DEFINE_uint32(writer_threads, 1,"");
DEFINE_uint32(run_for_seconds, 30, "");

struct SSD_workloadInfo : public scalestore::profiling::WorkloadInfo {
   std::string experiment;
   uint64_t writer_threads = FLAGS_writer_threads;
   uint64_t reader_threads = FLAGS_worker;
   std::atomic<int64_t>& w_pages;
   uint64_t timestamp = 0;

   SSD_workloadInfo(std::string experiment, std::atomic<int64_t>& w_pages)
      : experiment(experiment), w_pages(w_pages)
   {
   }

   
   virtual std::vector<std::string> getRow(){
      return {
          experiment, std::to_string(writer_threads), std::to_string(reader_threads), std::to_string(w_pages), std::to_string(timestamp++),
      };
   }

   virtual std::vector<std::string> getHeader(){
      return {"experiment","writers","readers", "w_pages", "timestamp"};
   }
   

   virtual void csv(std::ofstream& file) override
   {
      file << experiment << " , ";
      file << writer_threads << " , ";
      file << reader_threads << " , ";
      file << w_pages << " , ";
      file << timestamp << " , ";
   }
   virtual void csvHeader(std::ofstream& file) override
   {
      file << "experiment"
           << " , ";
      file << "writers"
           << " , ";
      file << "readers"
           << " , ";
      file << "w_pages"
           << " , ";
      file << "Timestamp"
           << " , ";
   }
};

// -------------------------------------------------------------------------------------
using namespace scalestore;
int main(int argc, char* argv[]) {
   // -------------------------------------------------------------------------------------
   gflags::SetUsageMessage("Internal hash table benchmark");
   gflags::ParseCommandLineFlags(&argc, &argv, true);
   // -------------------------------------------------------------------------------------
   // 创建 ScaleStore 实例
   ScaleStore scalestore;
   // 获取缓冲管理器的缓冲区大小
   auto bufferSize = scalestore.getBuffermanager().getDramPoolSize();
   // fill until full
   std::vector<PID> pids;
   scalestore.getWorkerPool().scheduleJobSync(0, [&]() {
       // 填充直到达到缓冲区大小的 95%
      for (uint64_t op_i = 0; op_i < bufferSize * 0.95; op_i++) {
          // 获取独占锁并添加新页面到 pids 向量
         scalestore::storage::ExclusiveBFGuard new_page_guard;
         pids.push_back(new_page_guard.getFrame().pid);
      }
   });
   scalestore.getBuffermanager().reportHashTableStats();
   // -------------------------------------------------------------------------------------
   // 创建 writer_threads 向量，用于写入操作
   std::vector<std::thread> writer_threads;
   // 创建线程同步屏障
   concurrency::Barrier barrier(FLAGS_worker + FLAGS_writer_threads);
   // 定义用于停止写入的原子布尔值和记录写入页面数量的原子变量
   std::atomic<bool> stopped {false};
   std::atomic<int64_t> w_pages = 0;
   // 循环创建 writer_threads 数量的线程
   for(uint64_t t_i =0; t_i < FLAGS_writer_threads; t_i++)
      writer_threads.emplace_back([&, t_i]() {
       // 创建异步写入缓冲区
         storage::AsyncWriteBuffer async_write_buffer(scalestore.getSSDFD(), storage::PAGE_SIZE, 1024);
         // 等待所有线程创建完成
         barrier.wait();
         // 在未停止的情况下持续执行写入操作
         while(!stopped){
             // 随机选择一个页面进行共享锁保护
            auto idx = utils::RandomGenerator::getRandU64(0, pids.size());
            scalestore::storage::SharedBFGuard sguard(pids[idx]);
            // 如果写入缓冲区已满，进行轮询并将写入的页面解锁
            if (async_write_buffer.full()) {
                
               const uint64_t polled_events = async_write_buffer.pollEventsSync();
               if (polled_events > 0) {
                  async_write_buffer.getWrittenBfs(
                     [&](storage::BufferFrame& frame, uint64_t /*epoch*/) {
                        // todo get pid because it could have changed with io_map
                        frame.latch.unlatchShared();
                        w_pages++;
                     },
                     polled_events);
               }
            }
            // 将页面添加到异步写入缓冲区
            async_write_buffer.add(sguard.getFrame(), sguard.getFrame().pid, 0);
            
         }
      });

   // pin
   // 将所有写入线程绑定到 CPU 核心
   for (auto& t : writer_threads)
      threads::CoreManager::getInstance().pinThreadRoundRobin(t.native_handle());

   // worker threads reading from ssd
   // 创建工作线程读取 SSD
   SSD_workloadInfo builtInfo{"SSD Benchmark", w_pages};
   scalestore.startProfiler(builtInfo);
   // 创建工作线程数，并让它们异步读取 SSD
   for (uint64_t t_i = 0; t_i < FLAGS_worker; ++t_i) {
      scalestore.getWorkerPool().scheduleJobAsync(t_i, [&, t_i]() {
         barrier.wait();
         auto idx = utils::RandomGenerator::getRandU64(0, pids.size());
         scalestore::storage::SharedBFGuard sguard(pids[idx]);
         scalestore.getBuffermanager().readPageSync(sguard.getFrame().pid, reinterpret_cast<uint8_t*>(sguard.getFrame().page));
      });
   }
   // 等待一定时间后停止写入操作，并等待所有线程执行完成
   sleep(FLAGS_run_for_seconds);
   stopped = true;
   scalestore.getWorkerPool().joinAll();
   scalestore.stopProfiler();
   // 等待所有写入线程执行完成
   for (auto& t : writer_threads){
      t.join();
   }

   return 0;
}
