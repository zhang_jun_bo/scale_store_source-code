#include "PerfEvent.hpp"
#include "scalestore/Config.hpp"
#include "scalestore/ScaleStore.hpp"
#include "scalestore/rdma/CommunicationManager.hpp"
#include "scalestore/storage/datastructures/BTree.hpp"
#include "scalestore/utils/RandomGenerator.hpp"
#include "scalestore/utils/ScrambledZipfGenerator.hpp"
#include "scalestore/utils/Time.hpp"
// -------------------------------------------------------------------------------------
#include <gflags/gflags.h>

/*
* 这段代码是一个基准测试，包括插入和查找操作的性能测试。程序初始化了一个 ScaleStore 实例和对应的 Catalog，然后模拟了插入和查找操作，最后输出性能测试的结果。
*/
// -------------------------------------------------------------------------------------
// 定义一个静态的障碍ID
static constexpr uint64_t BARRIER_ID = 0;
// -------------------------------------------------------------------------------------
// 定义命令行参数：运行操作总数和是否附加 perf
DEFINE_uint64(run_for_operations, 1e6, "Total number of operations executed per thread");
DEFINE_bool(attach_perf, false, "break for perf");
// ------------------------------------------------------------------------------------- 
// 结构体 OLAP_workloadInfo 继承自 scalestore::profiling::WorkloadInfo，存储有关工作负载的信息
struct OLAP_workloadInfo : public scalestore::profiling::WorkloadInfo {
   // 成员变量：实验名称、元素数量、读取比率和 Zipf 因子
   std::string experiment;
   uint64_t elements;
   uint64_t readRatio;
   double zipfFactor;
   // 构造函数，初始化实验名称和元素数量
   OLAP_workloadInfo(std::string experiment, uint64_t elements)
       : experiment(experiment), elements(elements)
   {
   }
   // 虚函数重写，返回包含实验和 SSD 数据量的行和表头
   virtual std::vector<std::string> getRow(){
      return {experiment,std::to_string(elements)};
   }

   virtual std::vector<std::string> getHeader(){
      return {"workload","SSD data GB"};
   }
   
   virtual void csv(std::ofstream& file) override
   {
      file << experiment << " , ";
      file << elements << " , ";

   }
   virtual void csvHeader(std::ofstream& file) override
   {
      file << "Workload"
           << " , ";
      file << "SSDdataGB"
           << " , ";

   }
};

// -------------------------------------------------------------------------------------
using namespace scalestore;
int main(int argc, char* argv[])
{
   // -------------------------------------------------------------------------------------
   // 解析命令行参数
   gflags::SetUsageMessage("Internal hash table benchmark");
   gflags::ParseCommandLineFlags(&argc, &argv, true);
   // -------------------------------------------------------------------------------------
   // 创建 ScaleStore 实例和对应的 Catalog 实例
   ScaleStore scalestore;
   auto& catalog = scalestore.getCatalog();
   // -------------------------------------------------------------------------------------
   // WAIT FUNCTION
   // 定义用于等待的函数和等待按键输入的函数
   auto barrier_wait = [&]() {
       // 创建分布式障碍，等待所有线程到达
      for (uint64_t t_i = 0; t_i < FLAGS_worker; ++t_i) {
         scalestore.getWorkerPool().scheduleJobAsync(t_i, [&, t_i]() {
            storage::DistributedBarrier barrier(catalog.getCatalogEntry(BARRIER_ID).pid);
            barrier.wait();
         });
      }
      scalestore.getWorkerPool().joinAll();
   };

   auto wait_for_key_press = [&](){
                                if(FLAGS_attach_perf){
                                   std::cout << "Press enter" << "\n";

                                   char temp;
                                   std::cin.get(temp);
                                }
                             };
   // -------------------------------------------------------------------------------------
   // BARRIER
   // 创建障碍，等待所有节点
   if (scalestore.getNodeID() == 0) {
      scalestore.getWorkerPool().scheduleJobSync(0, [&]() {
         scalestore.createBarrier(FLAGS_worker * FLAGS_nodes);
      });
   }
   // OLAP_workloadInfo builtInfo{"Build", FLAGS_run_for_operations};
   // scalestore.startProfiler(builtInfo);
   // 等待所有线程准备完成
   barrier_wait();
   // -------------------------------------------------------------------------------------
   // Benchmark
   // -------------------------------------------------------------------------------------
   // 在向量中存储 PID，模拟插入操作
   std::vector<double> ops(FLAGS_worker);
   std::vector<PID> pids(FLAGS_worker*FLAGS_run_for_operations);
   wait_for_key_press();
   // -------------------------------------------------------------------------------------
   // 插入操作的基准测试
   for (uint64_t t_i = 0; t_i < FLAGS_worker; ++t_i) {
       // 异步执行插入操作
      scalestore.getWorkerPool().scheduleJobAsync(t_i, [&, t_i]() {
         uint64_t pages{0};
         auto start_time_microseconds = utils::getTimePoint();
         for (uint64_t op_i = 0; op_i < FLAGS_run_for_operations; op_i++) {
            scalestore::storage::ExclusiveBFGuard new_page_guard;
            auto idx = (FLAGS_run_for_operations * t_i) + pages;
            pages++;
            pids[idx] = new_page_guard.getFrame().pid;
         }
         auto end_time_microseconds = utils::getTimePoint();
         ops[t_i] = 
            ((double)(FLAGS_run_for_operations)) / (((double)end_time_microseconds - start_time_microseconds) / (1e6));
      });
   }
   scalestore.getWorkerPool().joinAll();
//   scalestore.stopProfiler();
   // 输出插入操作性能结果
   std::cout << std::fixed;
   std::cout << std::reduce(ops.begin(), ops.end()) / FLAGS_worker << " insert operations per thread \n";
   // -------------------------------------------------------------------------------------
   wait_for_key_press();
   // lookup
   // -------------------------------------------------------------------------------------
   // 查找操作的基准测试
   for (uint64_t t_i = 0; t_i < FLAGS_worker; ++t_i) {
       // 异步执行查找操作
      scalestore.getWorkerPool().scheduleJobAsync(t_i, [&, t_i]() {
         uint64_t pages{0};
         auto start_time_microseconds = utils::getTimePoint();
         for (uint64_t op_i = 0; op_i < FLAGS_run_for_operations; op_i++) {
            scalestore::storage::SharedBFGuard s_guard(pids[utils::RandomGenerator::getRandU64(0, (FLAGS_worker*FLAGS_run_for_operations)-1)]);
            pages++;
         }
         auto end_time_microseconds = utils::getTimePoint();
         ops[t_i] = 
            ((double)(FLAGS_run_for_operations)) / (((double)end_time_microseconds - start_time_microseconds) / (1e6));
      });
   }
   scalestore.getWorkerPool().joinAll();
   // 输出查找操作性能结果
   std::cout << std::fixed;
   std::cout << std::reduce(ops.begin(), ops.end()) / FLAGS_worker << " lookup operations per thread \n";   

   return 0;
   };
