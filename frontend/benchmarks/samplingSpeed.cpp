#include "PerfEvent.hpp"
#include "scalestore/Config.hpp"
#include "scalestore/ScaleStore.hpp"
#include "scalestore/rdma/CommunicationManager.hpp"
#include "scalestore/storage/datastructures/BTree.hpp"
#include "scalestore/storage/datastructures/SmallMap.hpp"
#include "scalestore/utils/RandomGenerator.hpp"
#include "scalestore/utils/ScrambledZipfGenerator.hpp"
#include "scalestore/utils/Time.hpp"
// -------------------------------------------------------------------------------------
#include <gflags/gflags.h>
// -------------------------------------------------------------------------------------
static constexpr uint64_t BARRIER_ID = 0;
// -------------------------------------------------------------------------------------
//定义了一个 run_for_operations 的无符号 64 位整数类型的参数，其默认值为 1e6（即 1000000），表示每个线程执行的操作总数。
DEFINE_uint64(run_for_operations, 1e6, "Total number of operations executed per thread");
//定义了一个 simulated_write_ratio 的无符号 32 位整数类型的参数，默认值为 0，可能表示模拟写入的比率。
DEFINE_uint32(simulated_write_ratio, 0, "");
//定义了一个 simulated_eviction_window 的无符号 32 位整数类型的参数，默认值为 10，可能表示模拟驱逐的窗口大小。
DEFINE_uint32(simulated_eviction_window, 10, "");
//定义了一个 buffer_to_ssd_ratio 的无符号 32 位整数类型的参数，默认值为 1，可能表示缓冲区与 SSD 的比率。
DEFINE_uint32(buffer_to_ssd_ratio, 1, "");
//定义了一个 frames_to_evicted_frames_ratio 的无符号 32 位整数类型的参数，默认值为 0，可能表示帧与被驱逐帧的比率。
DEFINE_uint32(frames_to_evicted_frames_ratio, 0, "");
//定义了一个 run_for_seconds 的无符号 32 位整数类型的参数，默认值为 30，可能表示程序运行的秒数。
DEFINE_uint32(run_for_seconds, 30, "");
//定义了一个 test_max_read_perf 的布尔类型参数，默认值为 false，可能表示是否测试最大读取性能。
DEFINE_bool(test_max_read_perf, false, "");
//定义了一个 io_map 的布尔类型参数，默认值为 false，可能表示是否启用 I/O 映射。
DEFINE_bool(io_map, false, "");
//定义了一个 new_code 的布尔类型参数，默认值为 true，可能表示是否使用新的代码.
DEFINE_bool(new_code, true, "");
//定义了一个 linear_model 的布尔类型参数，默认值为 false，可能表示是否使用线性模型。
DEFINE_bool(linear_model, false, "");
//定义了一个 write_to_SSD 的布尔类型参数，默认值为 true，可能表示是否写入到 SSD。
DEFINE_bool(write_to_SSD, true, "");

// -------------------------------------------------------------------------------------
struct OLAP_workloadInfo : public scalestore::profiling::WorkloadInfo {
    //定义了一组引用和引用初始化，引用的类型为 std::atomic<int64_t> 和 uint64_t
   std::atomic<int64_t>& pages;
   std::atomic<int64_t>& freed_pages;
   std::atomic<int64_t>& w_pages;
   std::atomic<int64_t>& hit_empty_bf;
   std::atomic<int64_t>& failed_to_upgrade;
   uint64_t& eviction_window;
   std::atomic<int64_t>& sampled;
   std::atomic<int64_t>& found_candidates;
   std::atomic<int64_t>& pp_rounds_active;
   std::atomic<int64_t>& hit_spinlock;
   std::atomic<int64_t>& adjusted_model;
   //// 定义了一系列临时变量并初始化为 0
   int64_t tmp_freed_pages;
   int64_t tmp_w_pages;
   int64_t tmp_hit_empty_bf;
   int64_t tmp_failed_to_upgrade;
   int64_t tmp_sampled;
   int64_t tmp_found_candidates;
   int64_t tmp_pp_rounds_active;
   int64_t tmp_hit_spinlock;
   int64_t tmp_adjusted_model;
   /// 初始化两个 uint64_t 类型的变量为 0，其余的 uint64_t 变量初始化为对应 gflags 中的值
   uint64_t timestamp = 0;
   uint64_t simulated_write_ratio = FLAGS_simulated_write_ratio;
   uint64_t simulated_eviction_window = FLAGS_simulated_eviction_window;
   uint64_t buffer_to_ssd_ratio = FLAGS_buffer_to_ssd_ratio;
   uint64_t frames_to_evicted_frames_ratio = FLAGS_frames_to_evicted_frames_ratio;
   std::string code = FLAGS_new_code ? "new" : "old";
   std::string io_map = FLAGS_io_map ? "true" : "false";
   // OLAP_workloadInfo 构造函数，接受多个 std::atomic<int64_t>& 类型的引用和一个 uint64_t& 类型的引用作为参数
   OLAP_workloadInfo(std::atomic<int64_t>& pages,
                     std::atomic<int64_t>& freed_pages,
                     std::atomic<int64_t>& w_pages,
                     std::atomic<int64_t>& hit_empty_bf,
                     std::atomic<int64_t>& failed_to_upgrade,
                     uint64_t& eviction_window,
                     std::atomic<int64_t>& sampled,
                     std::atomic<int64_t>& found_candidates,
                     std::atomic<int64_t>& pp_rounds_active,
                     std::atomic<int64_t>& hit_spinlock,
                     std::atomic<int64_t>& adjusted_model)
       // 使用成员初始化列表初始化 OLAP_workloadInfo 对象的成员变量
       : pages(pages), // 初始化 pages 成员变量为传入的 pages 引用
         freed_pages(freed_pages), // 初始化 freed_pages 成员变量为传入的 freed_pages 引用
         w_pages(w_pages), // 初始化 w_pages 成员变量为传入的 w_pages 引用
         hit_empty_bf(hit_empty_bf), // 初始化 hit_empty_bf 成员变量为传入的 hit_empty_bf 引用
         failed_to_upgrade(failed_to_upgrade), // 初始化 failed_to_upgrade 成员变量为传入的 failed_to_upgrade 引用
         eviction_window(eviction_window),   // 初始化 eviction_window 成员变量为传入的 eviction_window 引用
         sampled(sampled), // 初始化 sampled 成员变量为传入的 sampled 引用
         found_candidates(found_candidates), // 初始化 found_candidates 成员变量为传入的 found_candidates 引用
         pp_rounds_active(pp_rounds_active),  // 初始化 pp_rounds_active 成员变量为传入的 pp_rounds_active 引用
         hit_spinlock(hit_spinlock),  // 初始化 hit_spinlock 成员变量为传入的 hit_spinlock 引用
         adjusted_model(adjusted_model) {}  // 初始化 adjusted_model 成员变量为传入的 adjusted_model 引用
   // 定义了一个返回字符串向量的虚拟函数 getRow()
   virtual std::vector<std::string> getRow() {
       // 使用 std::atomic::exchange(0) 交换并重置各个原子变量的值，并将旧值保存到对应的临时变量中
      tmp_freed_pages = freed_pages.exchange(0); // 释放页面的原子变量交换并保存旧值到临时变量
      tmp_w_pages = w_pages.exchange(0); // 待写入页面的原子变量交换并保存旧值到临时变量
      tmp_hit_empty_bf = hit_empty_bf.exchange(0); // 空布隆过滤器命中的原子变量交换并保存旧值到临时变量
      tmp_failed_to_upgrade = failed_to_upgrade.exchange(0); // 升级失败的原子变量交换并保存旧值到临时变量
      tmp_sampled = sampled.exchange(0); // 采样的原子变量交换并保存旧值到临时变量
      tmp_found_candidates = found_candidates.exchange(0); // 发现候选项的原子变量交换并保存旧值到临时变量
      tmp_pp_rounds_active = pp_rounds_active.exchange(0);  // 热点轮次的原子变量交换并保存旧值到临时变量
      tmp_hit_spinlock = hit_spinlock.exchange(0);  // 命中自旋锁的原子变量交换并保存旧值到临时变量
      tmp_adjusted_model = adjusted_model.exchange(0);  // 调整模型的原子变量交换并保存旧值到临时变量

      return {std::to_string(pages.load()),
              std::to_string(tmp_freed_pages),
              std::to_string(tmp_w_pages),
              std::to_string(tmp_hit_empty_bf),
              std::to_string(timestamp++),
              std::to_string(simulated_write_ratio),
              std::to_string(simulated_eviction_window),
              std::to_string(buffer_to_ssd_ratio),
              std::to_string(frames_to_evicted_frames_ratio),
              code,
              io_map,
              std::to_string(tmp_failed_to_upgrade),
              std::to_string(eviction_window),
              std::to_string(tmp_sampled),
              std::to_string(tmp_found_candidates),
              std::to_string(tmp_pp_rounds_active),
              std::to_string(tmp_hit_spinlock),
              std::to_string(tmp_adjusted_model)};
   }
   // 虚拟函数 getHeader() 定义了一个字符串向量，包含了各个数据的列标题信息
   virtual std::vector<std::string> getHeader() {
      return {"exp_pages",
              "exp_freed_pages",
              "w_pages",
              "h_empty_bf",
              "timestamp",
              "simulated_write_ratio",
              "simulated_eviction_window",
              "buffer_to_ssd_ratio",
              "frames_to_evicted_frames_ratio",
              "code",
              "io_map",
              "failed_to_upgrade",
              "eviction_window",
              "sampled",
              "found_candidates",
              "pp_rounds_active",
              "hit_spinlock",
              "adjusted_model"};
   }
   // 虚拟函数 csv 用于将数据以逗号分隔的形式写入到输出文件流中
   virtual void csv(std::ofstream& file) override {
      file << pages << " , ";
      file << tmp_freed_pages << " , ";
      file << tmp_w_pages << " , ";
      file << tmp_hit_empty_bf << " , ";
      file << timestamp << " , ";
      file << simulated_write_ratio << " , ";
      file << simulated_eviction_window << " , ";
      file << buffer_to_ssd_ratio << " , ";
      file << frames_to_evicted_frames_ratio << " , ";
      file << code << " , ";
      file << io_map << " , ";
      file << tmp_failed_to_upgrade << " , ";
      file << eviction_window << " , ";
      file << tmp_sampled << " , ";
      file << tmp_found_candidates << " , ";
      file << tmp_pp_rounds_active << " , ";
      file << tmp_hit_spinlock << " , ";
      file << tmp_adjusted_model << " , ";
   }
   // 虚拟函数 csvHeader 用于向输出文件流写入 CSV 格式的列标题
   virtual void csvHeader(std::ofstream& file) override {
      file << "exp_pages, exp_freed_pages, w_pages, h_empty_bf, timestamp, simulated_write_ratio, "
              "simulated_eviction_window,buffer_to_ssd_ratio, frames_to_evicted_frames_ratio, code, io_map, failed_to_upgrade, "
              "eviction_window,sampled,found_candidates,pp_rounds_active,hit_spinlock,adjusted_model,";
   }
};
// -------------------------------------------------------------------------------------
using namespace scalestore;
int main(int argc, char* argv[]) {
   // -------------------------------------------------------------------------------------
   // 设置使用说明信息，用于程序的帮助提示
   gflags::SetUsageMessage("Internal hash table benchmark");
   // 解析命令行参数
   gflags::ParseCommandLineFlags(&argc, &argv, true);
   // -------------------------------------------------------------------------------------
   ScaleStore scalestore;  // 创建一个 ScaleStore 实例
   auto bufferSize = scalestore.getBuffermanager().getDramPoolSize(); // 获取缓冲管理器的 DRAM 池大小
   auto number_bfs = scalestore.getBuffermanager().getNumberBufferframes(); // 获取缓冲管理器的缓冲帧数量
   // fill until full
   std::vector<PID> pids;
   scalestore.getWorkerPool().scheduleJobSync(0, [&]() {
       // 循环填充直到达到 95% 的缓冲区大小
      for (uint64_t op_i = 0; op_i < bufferSize * 0.95; op_i++) {
          // 创建 ExclusiveBFGuard 对象，用于管理独占缓冲帧
         scalestore::storage::ExclusiveBFGuard new_page_guard;
         pids.push_back(new_page_guard.getFrame().pid);
         // to simulate steady state set dirty flags
         // ensures that percentag of dirty is according to workload
          // 模拟稳态设置脏标志
        // 确保脏比例符合工作负载
         if (utils::RandomGenerator::getRandU64(0, 100) <= FLAGS_simulated_write_ratio) {
            new_page_guard.getFrame().dirty = true; // 设置帧为脏
         } else {
            new_page_guard.getFrame().dirty = false; // 设置帧为非脏
         }
      }
   });
   scalestore.getBuffermanager().reportHashTableStats(); // 报告哈希表统计信息

   // simulates evicted frames i.e. increases the collision and work in the hash table
   // 模拟被驱逐的帧，增加哈希表中的冲突并增加工作量
   scalestore.getWorkerPool().scheduleJobSync(0, [&]() {
      const uint64_t ratio = FLAGS_frames_to_evicted_frames_ratio; // 获取帧到被驱逐帧的比率
      const uint64_t target_frame = (bufferSize * 0.95);  // 目标帧数量为缓冲区大小的 95%
      uint64_t page_slot = 0;
      // 遍历比率次数
      for (uint64_t r_i = 0; r_i < ratio; r_i++) {
         page_slot = 0;
         NodeID nid = r_i + 1;  // not owner // 不是所有者的 NodeID
         // 在目标帧数量内生成 PID，并模拟插入到缓冲区
         for (uint64_t p_i = 0; p_i < target_frame; p_i++) {
             
            PID randomPID(nid, page_slot);
            scalestore.getBuffermanager().insertFrame(randomPID, [&](storage::BufferFrame& frame) {
               frame.latch.latchExclusive(); // 获取独占锁
               frame.page = nullptr;
               frame.pid = randomPID; // 设置帧的PID
               frame.setPossession(storage::POSSESSION::EXCLUSIVE); // 设置帧的拥有权为独占
               frame.setPossessor(scalestore.getNodeID() + 1);  // 设置拥有者为当前节点的下一个节点
               frame.state = storage::BF_STATE::EVICTED; // 设置帧状态为被驱逐
               frame.pVersion = 0; // 版本号置为0
            });
            page_slot++; // 递增页槽
         }
      }
   });
   scalestore.getBuffermanager().reportHashTableStats(); // 报告哈希表的统计信息
   //计算并输出了哈希表中的缓冲帧数量，该数量是缓冲池大小的95%乘以一个比例值
   std::cout << "BufferFrames in HT " << (bufferSize * 0.95) * (1 + FLAGS_frames_to_evicted_frames_ratio) << std::endl;
   // 定义一系列原子变量用于跟踪各种统计信息
   std::atomic<bool> stopped = false;
   std::atomic<bool> pp_stopped = false;
   std::atomic<uint64_t> count = 0;
   std::atomic<uint64_t> filled = 0;
   std::atomic<int64_t> pages = 0;
   std::atomic<int64_t> freed_pages = 0;
   std::atomic<int64_t> hit_empty_bf = 0;
   std::atomic<int64_t> w_pages = 0;
   std::atomic<int64_t> failed_to_upgrade = 0;
   std::atomic<int64_t> spinlock = 0;
   std::atomic<int64_t> total_read_pages = 0;
   std::atomic<int64_t> total_written_pages = 0;
   std::atomic<int64_t> sampled = 0;
   std::atomic<int64_t> found_candidates = 0;
   std::atomic<int64_t> pp_rounds_active = 0;
   std::atomic<int64_t> adjusted_model = 0;
   uint64_t eviction_window = 0;

   constexpr uint64_t SAMPLES = 50;

   int64_t freeBFLimit = (std::ceil(((FLAGS_freePercentage * 1.0 * (bufferSize * 0.95)) / 100.0))); //可用于释放的缓冲帧数量的限制
   std::thread pp([&]() {
      using namespace storage;
      // 引入必要的命名空间并初始化变量
      uint64_t old_page_count = pages;
      uint64_t evict_counter = 0;
      // -------------------------------------------------------------------------------------
      // 定义增加 Priority Promotion（PP）线程中 epoch 的条件
      auto pp_increment_epoch_condition = [&](uint64_t tmpPages, uint64_t evicted) {
         return ((pages + (freeBFLimit * 0.1)) < (tmpPages + evicted));
      };
      // -------------------------------------------------------------------------------------
       // 创建必要的数据结构和变量
      std::vector<std::pair<uint64_t, BufferFrame*>> orderedSample(SAMPLES);
      std::cout << "Buffer frames " << number_bfs << std::endl;
      auto& bfs = scalestore.getBuffermanager().getBufferframes();
      AsyncWriteBuffer async_write_buffer(scalestore.getSSDFD(), PAGE_SIZE, 1024);
      SmallMap<PID, uint64_t> io_map(8196, 2000);
      // -------------------------------------------------------------------------------------
      constexpr uint64_t BATCH_SIZE = 1024;
      const uint64_t htSize = Helper::nextPowerTwo(bufferSize);
      std::vector<storage::BufferFrame*> batch(BATCH_SIZE);
      std::vector<std::pair<uint64_t, BufferFrame*>> candidate_batch(BATCH_SIZE);
      std::vector<uint64_t> sample_epochs(BATCH_SIZE);
      uint64_t current_batch = 0;
      bool epoch_changed = true;
      bool adjust_linear_model = true;
      uint64_t private_pages = 0;
      uint64_t active_pp = 0;
      // -------------------------------------------------------------------------------------
      // 从 BufferManager 获取全局 epoch 值
      eviction_window = scalestore.getBuffermanager().globalEpoch;
      // -------------------------------------------------------------------------------------
      // 启动 PP 线程以管理缓冲帧及其操作
      auto poll_write_buffer = [&]() {
         const uint64_t polled_events = async_write_buffer.pollEventsSync();
         if (polled_events > 0) {
            async_write_buffer.getWrittenBfs(
               [&](BufferFrame& frame, uint64_t /*epoch*/) {
                   // todo get pid because it could have changed with io_map
                   if (FLAGS_io_map) {
                      auto version = io_map.find(frame.pid)->second;
                      if (frame.latch.optimisticUpgradeToExclusive(version)) {
                         frame.dirty = false;
                         frame.latch.unlatchExclusive();
                         // free writes and track stats for epoch increment
                         private_pages++;
                         // pages++;
                         freed_pages++;
                         evict_counter++;
                      }
                      io_map.erase(frame.pid);
                   } else {
                      ensure(frame.dirty);
                      frame.dirty = false;
                      // free writes and track stats for epoch increment
                      // pages++;
                      private_pages++;
                      freed_pages++;
                      evict_counter++;
                      frame.latch.unlatchShared();
                   }
                   // coolingBuffer.add(&frame);
                   w_pages++;
                   total_written_pages++;
                },
                polled_events);
         }
      };

      auto start = utils::getTimePoint();
      while (!pp_stopped) {
          // 若 write_to_SSD 标志已设置，则轮询写入缓冲区以处理待写入的内容
         if (FLAGS_write_to_SSD) { poll_write_buffer(); }
         ensure(pages >= 0);
         // 确保页数非负，并处理 epoch 增加的条件
         if (pp_increment_epoch_condition(old_page_count,
                                          evict_counter)) {  // condition for epoch increase
         // 增加 epoch，并更新相关变量
         // 根据线性模型调整 eviction_window，若 linear_model 标志已设置
            evict_counter = 0;
            scalestore.getBuffermanager().globalEpoch++;
            old_page_count = pages;
            epoch_changed = true;
            if (FLAGS_linear_model) { eviction_window++; }
         }
         // 检查页数是否低于空闲缓冲帧限制
         if (pages.load() < freeBFLimit) {
             // 实现了核心的 PP 逻辑，包括缓冲帧管理、采样和驱逐
            auto start_active = utils::getTimePoint();
            pp_rounds_active++;
            // start eviction
            // how long are we in that phase ? time base or page based?

            [[maybe_unused]] uint64_t samples = 0;
            [[maybe_unused]] uint64_t picks = 0;
            // uint64_t deviationFactor = 0;
            // uint64_t threshold = 0;
            // fill vector with ptrs
             // 根据指定的标志和条件，调整缓冲帧页数，并执行各种操作
            if (FLAGS_new_code) {
                // 计算起始和结束位置以填充 batch 向量
               uint64_t begin = current_batch % htSize;
               uint64_t end = current_batch + BATCH_SIZE;
               if (end > htSize) end = htSize;
               current_batch = end % htSize;
               ensure(end - begin <= BATCH_SIZE);

               uint64_t entries = end - begin;
               // fill vector
               uint64_t idx = 0;
               // -------------------------------------------------------------------------------------
               // 填充 batch 向量
               for (uint64_t pos_bf = begin; pos_bf < end; pos_bf++) {
                  batch[idx++] = &bfs[pos_bf];
               }
               // sample here based on candidate batch
                  // 基于候选 batch 进行采样
               if (FLAGS_linear_model) {
                  if (adjust_linear_model) {
                      // 从 entries 中提取样本，并进行排序以计算驱逐窗口
                     uint64_t samples = 0;
                     for (uint64_t b_i = 0; b_i < entries; b_i++) {
                        auto& frame = *batch[b_i];
                        if ((frame.state == BF_STATE::FREE) | (frame.state == BF_STATE::EVICTED)) { continue; }
                        sample_epochs[samples] = frame.epoch;
                        samples++;
                     }
                     auto it_end = sample_epochs.begin() + samples;
                     std::sort(sample_epochs.begin(), it_end);
                     uint64_t eviction_idx = samples * (FLAGS_simulated_eviction_window / 100.0);
                     eviction_window = sample_epochs[eviction_idx];
                     adjust_linear_model = false;
                  }
               } else {
                  if (epoch_changed) {
                      // 如果 epoch 已更改，则重新对 entries 中的样本进行排序以计算驱逐窗口
                     uint64_t samples = 0;
                     for (uint64_t b_i = 0; b_i < entries; b_i++) {
                        auto& frame = *batch[b_i];
                        if ((frame.state == BF_STATE::FREE) | (frame.state == BF_STATE::EVICTED)) { continue; }
                        sample_epochs[samples] = frame.epoch;
                        samples++;
                     }
                     auto it_end = sample_epochs.begin() + samples;
                     std::sort(sample_epochs.begin(), it_end);
                     uint64_t eviction_idx = samples * (FLAGS_simulated_eviction_window / 100.0);
                     eviction_window = sample_epochs[eviction_idx];
                     epoch_changed = false;
                  }
               }

               // traverse the hash table
                 // 遍历哈希表
               uint64_t write_idx = 0;
               uint64_t candidate_max = 0;
               uint64_t sampled_round = 0;
               uint64_t found_candidates_round = 0;
               while (entries > 0) {
                  // traverse current level
                    // 遍历当前级别的缓冲帧
                  for (uint64_t b_i = 0; b_i < entries; b_i++) {
                      // 检查缓冲帧的状态并进行相应操作
                     auto* tmp_ptr = batch[b_i];
                     auto& frame = *tmp_ptr;
                        // 如果缓冲帧状态允许进行采样，则对其进行采样
                     if ((frame.state == BF_STATE::FREE) | (frame.state == BF_STATE::EVICTED)) {
                         // 处理空缓冲帧状态
                        hit_empty_bf++;
                        continue;
                     }
                     // 对满足条件的缓冲帧进行采样
                     if (frame.pid != EMPTY_PID && !frame.latch.isLatched()) {
                        // if (utils::RandomGenerator::getRandU64(0, 100) <= FLAGS_simulated_eviction_window) {
                        sampled++;
                        sampled_round++;
                        if (frame.epoch <= eviction_window) {
                            // 如果缓冲帧的 epoch 小于等于驱逐窗口，则将其作为候选缓冲帧
                           candidate_batch[candidate_max++] = {frame.epoch, tmp_ptr};
                           found_candidates++;
                           found_candidates_round++;
                        }
                     } else {
                        hit_empty_bf++;
                     }
                     // XXX optimistic latching? otherwise pointer might be nulltpr
                     // XXX 乐观锁定？否则指针可能为空
                     if (frame.next) batch[write_idx++] = frame.next;
                  }
                  
                  // iterate over candidates
                     // 遍历候选缓冲帧
                  
                  /* 循环遍历 candidate_max 数量的候选缓冲帧。
                    对每个候选缓冲帧进行操作：
                    检查缓冲帧是否可以进行乐观锁定操作，若无法锁定则跳过当前缓冲帧。
                    检查当前缓冲帧的 epoch 是否与当前保存的 epoch 值相同，如果不同则跳过当前缓冲帧。
                    检查当前缓冲帧的状态，如果为空闲或者已经被驱逐，则跳过当前缓冲帧。
                    尝试对当前缓冲帧进行乐观锁定升级为独占锁，如果无法升级则增加 failed_to_upgrade 计数并跳过当前缓冲帧。
                    确保当前缓冲帧状态既不是空闲也不是已被驱逐。
                    如果当前缓冲帧是属于当前节点所有：
                    如果启用了 FLAGS_test_max_read_perf，则释放独占锁并将 private_pages 计数增加。
                    如果当前缓冲帧是脏页（dirty），则根据一些条件执行异步写入或者放入异步写入缓冲区。
                    如果当前缓冲帧不是脏页，则释放独占锁并将 private_pages 计数增加，并更新相应统计数据。
                    这段代码主要负责对候选缓冲帧进行状态检查和更新操作，以及根据一定条件对缓冲帧进行异步写入或更新统计数据。
                  */
                  for (uint64_t c_i = 0; c_i < candidate_max; c_i++) {
                      // 对候选缓冲帧进行处理和更新操作
                     // for (uint64_t o_s = 0; o_s < (samples * 0.1); o_s++) {
                     auto& frame = *candidate_batch[c_i].second;
                     auto& epoch = candidate_batch[c_i].first;

                     auto version = frame.latch.optimisticLatchOrRestart();
                     if (!version.has_value()) continue;

                     if (epoch != frame.epoch.load()) { continue; }
                     if ((frame.pid == EMPTY_PID) || (frame.state == BF_STATE::FREE) || (frame.state == BF_STATE::EVICTED)) { continue; }
                     if (!frame.latch.optimisticUpgradeToExclusive(version.value())) {
                        failed_to_upgrade++;
                        continue;
                     }

                     ensure(frame.state != BF_STATE::FREE);
                     ensure(frame.state != BF_STATE::EVICTED);

                     if ((frame.pid.getOwner() == scalestore.getNodeID())) {
                        // -------------------------------------------------------------------------------------
                        // no interfeering writes
                        if (FLAGS_test_max_read_perf) {
                           frame.latch.unlatchExclusive();
                           private_pages++;
                           // pages++;
                           continue;
                        }
                        // Dirty add to async write buffer
                        // simulate read ratio because all pages are dirty
                        if (frame.dirty) {
                           if (FLAGS_io_map && io_map.find(frame.pid) != io_map.end()) {  // prevent double writes
                              frame.latch.unlatchExclusive();
                              continue;  // pick next sample
                           }

                           if (async_write_buffer.full()) {
                              ensure(frame.latch.isLatched());
                              frame.latch.unlatchExclusive();
                              // poll here
                              std::cout << "write buffer full"
                                        << "\n";
                              break;
                           }
                           if (frame.pid == 0) {
                              ensure(frame.latch.isLatched());
                              frame.latch.unlatchExclusive();
                              continue;
                           }
                           if (FLAGS_io_map) {
                              io_map.insert({frame.pid, frame.latch.version + 0b10});
                              frame.latch.unlatchExclusive();
                           } else {
                              if (!frame.latch.tryDowngradeExclusiveToShared()) {
                                 hit_empty_bf++;
                                 continue;
                              }
                              ensure(!frame.latch.isLatched());
                           }

                           if (!FLAGS_write_to_SSD) {
                              frame.dirty = false;
                              private_pages++;
                              // pages++;
                              freed_pages++;
                              evict_counter++;
                              frame.latch.unlatchShared();
                              continue;
                           }
                           async_write_buffer.add(frame, frame.pid, epoch);
                           continue;
                        }
                        ensure(!frame.dirty);
                        frame.latch.unlatchExclusive();
                        private_pages++;
                        // pages++;
                        freed_pages++;
                        evict_counter++;
                     }
                  }
                  // 提交到 SSD 写入缓冲区并执行相应操作
                  if (FLAGS_write_to_SSD) {
                     [[maybe_unused]] auto nsubmit = async_write_buffer.submit();
                     poll_write_buffer();
                  }

                  // -------------------------------------------------------------------------------------
                  // next iteration
                  entries = write_idx;
                  write_idx = 0;
                  candidate_max = 0;
               }

               pages += private_pages;
               private_pages = 0;
               // 更新页数及状态，根据 linear_model 调整模型
               if (FLAGS_linear_model) {
                  double percentage = (found_candidates_round / (double)sampled_round);
                  double target_percentage = (FLAGS_simulated_eviction_window / 100.0);
                  if (percentage > target_percentage * 2 || percentage < target_percentage / 2) {
                     adjust_linear_model = true;
                     adjusted_model++;
                  }
               }

            } else {
               // old code
               //通过随机选择缓冲帧并对选定的缓冲帧进行状态检查，从而生成符合特定条件的样本数据。
               while (samples < SAMPLES) {
                  uint64_t found = 0;
                  uint64_t tried = 0;
                  //在每次循环迭代中，通过随机选取缓冲帧的方式获取样本数据。 随机选择索引范围，遍历选定范围内的缓冲帧。
                  // uint64_t pickIdx = utils::RandomGenerator::getRandU64(0, htSize);
                  uint64_t pickIdx = utils::RandomGenerator::getRandU64(0, number_bfs - 64);
                  // SSD code
                  //对于所选范围内的每个缓冲帧，检查其状态，如果是空闲或已被驱逐状态，则计数 并继续下一轮循环 会得到一批满足条件的缓冲帧样本，并存储其相关信息用于后续的处理。
                  for (uint64_t p_i = 0; p_i < 64; ++p_i) {
                     // -------------------------------------------------------------------------------------
                     if (samples == SAMPLES) break;  // break loop if we have enough samples
                     // -------------------------------------------------------------------------------------
                     //否则，进一步判断是否满足一定条件 若满足条件则将该缓冲帧的信息存入 orderedSample 数组中，并递增 samples 计数
                     auto& frame = bfs[pickIdx + p_i];
                     if ((frame.state == BF_STATE::FREE) | (frame.state == BF_STATE::EVICTED)) {
                        hit_empty_bf++;
                        continue;
                     }
                     //检查当前缓冲帧的状态，要求其不是空闲状态  且未被其他进程锁定
                     /*
                     * 在一定条件下随机选择缓冲帧，并根据特定的条件将其存储为样本数据，同时统计符合条件和不符合条件的缓冲帧数量。
                     */
                     if (frame.pid != EMPTY_PID && !frame.latch.isLatched()) {
                         //生成一个介于 0 和 100 之间的随机数，判断是否小于等于 FLAGS_simulated_eviction_window 的值
                        if (utils::RandomGenerator::getRandU64(0, 100) <= FLAGS_simulated_eviction_window) {
                           orderedSample[samples].first = frame.epoch;
                           orderedSample[samples].second = &frame;
                           ensure(orderedSample[samples].second != nullptr);
                           samples++;
                           found++;
                        }
                        tried++;
                     } else {
                        hit_empty_bf++;
                     }
                  }
                  picks++;
               }

               if (samples == 0) goto escape;
               for (uint64_t o_s = 0; o_s < (samples); o_s++) {
                  // for (uint64_t o_s = 0; o_s < (samples * 0.1); o_s++) {
                   //从 orderedSample 中获取样本的缓冲帧和其对应的epoch。
                  auto& frame = *orderedSample[o_s].second;
                  auto& epoch = orderedSample[o_s].first;

                  auto version = frame.latch.optimisticLatchOrRestart();
                  //获取缓冲帧的版本，并检查其是否有效。如果版本无效，则继续处理下一个样本。
                  if (!version.has_value()) continue;
                  //检查缓冲帧的epoch是否与当前epoch相符，若不相符，则继续处理下一个样本。
                  if (epoch != frame.epoch.load()) { continue; }
                  //检查缓冲帧的状态是否为空闲或已被淘汰，若是，则继续处理下一个样本。
                  if ((frame.pid == EMPTY_PID) || (frame.state == BF_STATE::FREE) || (frame.state == BF_STATE::EVICTED)) { continue; }
                  //尝试将缓冲帧的锁升级到独占模式，若升级失败，则计数 hit_empty_bf 增加，并继续处理下一个样本。
                  if (!frame.latch.optimisticUpgradeToExclusive(version.value())) {
                     hit_empty_bf++;
                     continue;
                  }
                  //确保缓冲帧状态不为空闲或已被淘汰。
                  ensure(frame.state != BF_STATE::FREE);
                  ensure(frame.state != BF_STATE::EVICTED);
                  //如果缓冲帧所属的节点是当前节点：
                  if ((frame.pid.getOwner() == scalestore.getNodeID())) {
                     // -------------------------------------------------------------------------------------
                     // no interfeering writes
                      //如果设置了 FLAGS_test_max_read_perf 标志，则解除独占锁并增加 pages 计数，然后处理下一个样本。
                     if (FLAGS_test_max_read_perf) {
                        frame.latch.unlatchExclusive();
                        pages++;
                        continue;
                     }
                     // Dirty add to async write buffer
                     // simulate read ratio because all pages are dirty
                     //如果缓冲帧是脏页（被修改过）：
                     if (frame.dirty) {
                         //如果启用了 FLAGS_io_map 并且已经在 io_map 中找到了与该页关联的记录（为了避免重复写入），则解除独占锁并处理下一个样本。
                        if (FLAGS_io_map && io_map.find(frame.pid) != io_map.end()) {  // prevent double writes
                           frame.latch.unlatchExclusive();
                           continue;  // pick next sample
                        }
                        //如果异步写入缓冲区已满，则确保已获取独占锁后解除独占锁，并跳转到标签 escape 处继续执行代码。
                        if (async_write_buffer.full()) {
                           ensure(frame.latch.isLatched());
                           frame.latch.unlatchExclusive();
                           goto escape;
                        }
                        //如果缓冲帧的页标识为 0，则确保已获取独占锁后解除独占锁，并处理下一个样本。
                        if (frame.pid == 0) {
                           ensure(frame.latch.isLatched());
                           frame.latch.unlatchExclusive();
                           continue;
                        }
                        //如果启用了 FLAGS_io_map，则将该页的版本信息插入 io_map 中，确保已获取独占锁后解除独占锁。
                        if (FLAGS_io_map) {
                           io_map.insert({frame.pid, frame.latch.version + 0b10});
                           frame.latch.unlatchExclusive();
                        } else {
                           if (!frame.latch.tryDowngradeExclusiveToShared()) continue;
                           ensure(!frame.latch.isLatched());
                        }
                        //如果无需将数据写入 SSD，则增加 pages、freed_pages 和 evict_counter 计数，然后解除独占锁。
                        if (!FLAGS_write_to_SSD) {
                           pages++;
                           freed_pages++;
                           evict_counter++;
                           frame.latch.unlatchShared();
                           continue;
                        }
                        //否则，将该页加入异步写入缓冲区，然后处理下一个样本。
                        async_write_buffer.add(frame, frame.pid, epoch);
                        continue;
                     }
                     //如果缓冲帧不是脏页，则确保已获取独占锁后解除独占锁，并增加 pages、freed_pages 和 evict_counter 计数。
                     ensure(!frame.dirty);
                     frame.latch.unlatchExclusive();
                     pages++;
                     freed_pages++;
                     evict_counter++;
                  }
               }
            escape:
               filled++;
               if (!FLAGS_write_to_SSD) { [[maybe_unused]] auto nsubmit = async_write_buffer.submit(); }
            }
            auto end_active = utils::getTimePoint();
            active_pp += end_active -start_active;
         }
      }
      auto end = utils::getTimePoint();
      std::cout << "PP was running " << (end-start)  << "\n";
      std::cout << "PP was busy " <<  active_pp  << "\n";
      std::cout << "PP was idle " << (end-start)-active_pp  << "\n";

   });
   // -------------------------------------------------------------------------------------
   cpu_set_t cpuset;
   CPU_ZERO(&cpuset);
   CPU_SET(17, &cpuset);
   /*
   * 使用 pthread_setaffinity_np 函数来将线程 pp 绑定到特定的 CPU 核心集合，这个集合通过 cpuset 定义。pthread_setaffinity_np 通常用于控制线程运行在特定的 CPU 核心或者 CPU 核心集合上，
     这可以用于优化性能或者其他需要特定处理器资源的场景。如果绑定线程到 CPU 核心集合时出现错误，将抛出 std::runtime_error 异常，
     报告无法将第一个参数指定的线程（17 号线程）绑定到第二个参数指定的 CPU 核心集合上。
   */
   if (pthread_setaffinity_np(pp.native_handle(), sizeof(cpu_set_t), &cpuset) != 0) {
      throw std::runtime_error("Could not pin thread " + std::to_string(17) + " to thread " + std::to_string(17));
   }
   // -------------------------------------------------------------------------------------

   // -------------------------------------------------------------------------------------
   // sample buffer frame single threaded
   //在多线程环境下模拟 OLAP（联机分析处理）工作负载。
   OLAP_workloadInfo builtInfo{pages,   freed_pages,      w_pages,          hit_empty_bf, failed_to_upgrade, eviction_window,
                               sampled, found_candidates, pp_rounds_active, spinlock,     adjusted_model};
   scalestore.startProfiler(builtInfo);
   for (uint64_t t_i = 0; t_i < FLAGS_worker; ++t_i) {
      scalestore.getWorkerPool().scheduleJobAsync(t_i, [&, t_i]() {
         const uint64_t blockSize = pids.size() / FLAGS_worker;
         auto begin = t_i * blockSize;
         auto end = begin + blockSize;
         if (t_i == FLAGS_worker - 1) end = pids.size();
         //循环开始时等待 pages.load() 达到 (freeBFLimit * 0.9)，以达到稳定状态。
         while (pages.load() < (freeBFLimit * 0.9)) {}  // spin to reach steady state
         while (!stopped) {
             //生成一个随机索引 idx 以从 pids 中选择一个项目。
            auto idx = utils::RandomGenerator::getRandU64(begin, end);
            //根据随机数 FLAGS_simulated_write_ratio 的值，选择创建一个 ExclusiveBFGuard 或 SharedBFGuard。
            if (utils::RandomGenerator::getRandU64(0, 100) <= FLAGS_simulated_write_ratio) {
               scalestore::storage::ExclusiveBFGuard xguard(pids[idx]);
               xguard.getFrame().page->data[0]++;
               if (utils::RandomGenerator::getRandU64(0, FLAGS_buffer_to_ssd_ratio) != 0) {
                  auto start = utils::getTimePoint();
                  if (!FLAGS_test_max_read_perf) {
                     while (pages <= 100) {
                        spinlock++;
                        _mm_pause();
                     }
                     pages--;
                  }
                  if (FLAGS_write_to_SSD) {
                      //如果 FLAGS_write_to_SSD 为真，则调用 scalestore.getBuffermanager().readPageSync 以将页面写入 SSD。
                     scalestore.getBuffermanager().readPageSync(xguard.getFrame().pid, reinterpret_cast<uint8_t*>(xguard.getFrame().page));
                  }else{
                      //否则，进行一个 600 次的忙等待（busy-waiting）循环。
                     for(uint64_t i=0; i< 600; i++){
                         _mm_pause();
                     }
                  }
                  total_read_pages++;
                  ensure(xguard.getFrame().dirty);
                  auto end = utils::getTimePoint();
                  threads::Worker::my().counters.incr_by(profiling::WorkerCounters::latency, (end - start));
               }
            } else {
               scalestore::storage::SharedBFGuard xguard(pids[idx]);
               xguard.getFrame().page->data[0]++;
               if (utils::RandomGenerator::getRandU64(0, FLAGS_buffer_to_ssd_ratio) != 0) {
                  auto start = utils::getTimePoint();
                  if (!FLAGS_test_max_read_perf) {
                     while (pages <= 100) {
                        spinlock++;
                        _mm_pause();
                        
                     }
                     pages--;
                  }
                  if (FLAGS_write_to_SSD) {
                     scalestore.getBuffermanager().readPageSync(xguard.getFrame().pid, reinterpret_cast<uint8_t*>(xguard.getFrame().page));
                  }else{
                     for(uint64_t i=0; i< 600; i++){
                        _mm_pause();
                     }
                  }
                  total_read_pages++;
                  auto end = utils::getTimePoint();
                  threads::Worker::my().counters.incr_by(profiling::WorkerCounters::latency, (end - start));
               }
            }
            threads::Worker::my().counters.incr(profiling::WorkerCounters::tx_p);
         }
      });
   }
   sleep(FLAGS_run_for_seconds);
   stopped = true;
   scalestore.getWorkerPool().joinAll();
   scalestore.stopProfiler();
   pp_stopped = true;
   pp.join();
   std::cout << "found samples " << count << std::endl;
   std::cout << "free pages " << pages << std::endl;
   std::cout << "total written pages " << total_written_pages << std::endl;
   std::cout << "total read pages " << total_read_pages << std::endl;
   std::cout << "hit spinlock " << spinlock << std::endl;
   std::cout << filled << std::endl;
   return 0;
};
